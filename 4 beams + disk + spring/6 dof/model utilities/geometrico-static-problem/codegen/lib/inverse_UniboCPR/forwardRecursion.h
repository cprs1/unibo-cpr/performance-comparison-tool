/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * forwardRecursion.h
 *
 * Code generation for function 'forwardRecursion'
 *
 */

#ifndef FORWARDRECURSION_H
#define FORWARDRECURSION_H

/* Include files */
#include "inverse_UniboCPR_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void forwardRecursion(const double p1L[3], const double h1L[4],
                      const double qei_data[], double L, double Nf, double p[3],
                      double h[4], double dpdqa[3], emxArray_real_T *dhdqa,
                      emxArray_real_T *dpdqe, emxArray_real_T *dhdqe);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (forwardRecursion.h) */
