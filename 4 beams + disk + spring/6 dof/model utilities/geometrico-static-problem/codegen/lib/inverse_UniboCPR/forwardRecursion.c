/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * forwardRecursion.c
 *
 * Code generation for function 'forwardRecursion'
 *
 */

/* Include files */
#include "forwardRecursion.h"
#include "OdefunAssumedForward.h"
#include "PhiMatr.h"
#include "explicitRungeKutta.h"
#include "inverse_UniboCPR_data.h"
#include "inverse_UniboCPR_emxutil.h"
#include "inverse_UniboCPR_rtwutil.h"
#include "inverse_UniboCPR_types.h"
#include "ntrp45.h"
#include "rt_nonfinite.h"
#include "rt_nonfinite.h"
#include <math.h>
#include <string.h>

/* Function Declarations */
static void b_forwardRecursion_anonFcn1(const double qei_data[], double Nf,
                                        double L, double s,
                                        const emxArray_real_T *y,
                                        emxArray_real_T *varargout_1);

static void forwardRecursion_anonFcn1(const double qei_data[], double Nf,
                                      double L, const emxArray_real_T *y,
                                      emxArray_real_T *varargout_1);

/* Function Definitions */
static void b_forwardRecursion_anonFcn1(const double qei_data[], double Nf,
                                        double L, double s,
                                        const emxArray_real_T *y,
                                        emxArray_real_T *varargout_1)
{
  emxArray_real_T *C;
  emxArray_real_T *Phi;
  emxArray_real_T *a;
  emxArray_real_T *b_C;
  emxArray_real_T *c_C;
  double A[16];
  double Dh[12];
  double b_y[12];
  double R[9];
  double b_A[4];
  double b_L[4];
  double b_a[4];
  double k[3];
  const double *y_data;
  double Nftot;
  double R_tmp;
  double b_R_tmp;
  double c_R_tmp;
  double d_R_tmp;
  double e_R_tmp;
  double f_R_tmp;
  double g_R_tmp;
  double h_R_tmp;
  double *C_data;
  double *Phi_data;
  double *a_data;
  double *b_C_data;
  int aoffset;
  int b_i;
  int b_k;
  int coffset;
  int i;
  int i1;
  int j;
  int n_tmp_tmp;
  y_data = y->data;
  Nftot = 3.0 * Nf;
  R_tmp = 3.0 * Nftot + 11.0;
  if (R_tmp > (3.0 * Nftot + 10.0) + 4.0) {
    i = 0;
  } else {
    i = (int)R_tmp - 1;
  }
  if (R_tmp + 4.0 > ((3.0 * Nftot + 10.0) + 4.0) + 4.0 * Nftot) {
    i1 = 0;
  } else {
    i1 = (int)(R_tmp + 4.0) - 1;
  }
  /*  CONVERT QUATERNION HP INTO ROTATION MATRIX */
  /*  hP = h1 + h2 i + h3 j + h4 k (h1 = scalar component) */
  R_tmp = y_data[3] * y_data[3];
  b_R_tmp = y_data[4] * y_data[4];
  c_R_tmp = y_data[5] * y_data[5];
  d_R_tmp = y_data[6] * y_data[6];
  R[0] = ((R_tmp + b_R_tmp) - c_R_tmp) - d_R_tmp;
  e_R_tmp = y_data[4] * y_data[5];
  f_R_tmp = y_data[3] * y_data[6];
  R[1] = 2.0 * (f_R_tmp + e_R_tmp);
  g_R_tmp = y_data[3] * y_data[5];
  h_R_tmp = y_data[4] * y_data[6];
  R[2] = 2.0 * (h_R_tmp - g_R_tmp);
  R[3] = 2.0 * (e_R_tmp - f_R_tmp);
  R_tmp -= b_R_tmp;
  R[4] = (R_tmp + c_R_tmp) - d_R_tmp;
  b_R_tmp = y_data[5] * y_data[6];
  e_R_tmp = y_data[3] * y_data[4];
  R[5] = 2.0 * (e_R_tmp + b_R_tmp);
  R[6] = 2.0 * (g_R_tmp + h_R_tmp);
  R[7] = 2.0 * (b_R_tmp - e_R_tmp);
  R[8] = (R_tmp - c_R_tmp) + d_R_tmp;
  emxInit_real_T(&Phi, 2);
  PhiMatr(s, Nf, Phi);
  Phi_data = Phi->data;
  coffset = Phi->size[1];
  k[0] = 0.0;
  k[1] = 0.0;
  k[2] = 0.0;
  for (b_k = 0; b_k < coffset; b_k++) {
    aoffset = b_k * 3;
    R_tmp = qei_data[b_k];
    k[0] += Phi_data[aoffset] * R_tmp;
    k[1] += Phi_data[aoffset + 1] * R_tmp;
    k[2] += Phi_data[aoffset + 2] * R_tmp;
  }
  A[0] = 0.0;
  A[4] = -k[0];
  A[8] = -k[1];
  A[12] = -k[2];
  A[1] = k[0];
  A[5] = 0.0;
  A[9] = k[2];
  A[13] = -k[1];
  A[2] = k[1];
  A[6] = -k[2];
  A[10] = 0.0;
  A[14] = k[0];
  A[3] = k[2];
  A[7] = k[1];
  A[11] = -k[0];
  A[15] = 0.0;
  Dh[0] = 2.0 * y_data[5];
  Dh[3] = 2.0 * y_data[6];
  Dh[6] = 2.0 * y_data[3];
  Dh[9] = 2.0 * y_data[4];
  R_tmp = 2.0 * -y_data[4];
  Dh[1] = R_tmp;
  Dh[4] = 2.0 * -y_data[3];
  Dh[7] = 2.0 * y_data[6];
  Dh[10] = 2.0 * y_data[5];
  Dh[2] = 2.0 * y_data[3];
  Dh[5] = R_tmp;
  Dh[8] = 2.0 * -y_data[5];
  Dh[11] = 2.0 * y_data[6];
  b_R_tmp = L * 0.5;
  for (aoffset = 0; aoffset < 12; aoffset++) {
    b_y[aoffset] = L * Dh[aoffset];
  }
  n_tmp_tmp = (int)Nftot;
  emxInit_real_T(&C, 2);
  aoffset = C->size[0] * C->size[1];
  C->size[0] = 3;
  C->size[1] = (int)Nftot;
  emxEnsureCapacity_real_T(C, aoffset);
  C_data = C->data;
  for (j = 0; j < n_tmp_tmp; j++) {
    coffset = j * 3;
    aoffset = j << 2;
    for (b_i = 0; b_i < 3; b_i++) {
      C_data[coffset + b_i] = ((b_y[b_i] * y_data[i1 + aoffset] +
                                b_y[b_i + 3] * y_data[(i1 + aoffset) + 1]) +
                               b_y[b_i + 6] * y_data[(i1 + aoffset) + 2]) +
                              b_y[b_i + 9] * y_data[(i1 + aoffset) + 3];
    }
  }
  b_y[0] = -y_data[4];
  b_y[4] = -y_data[5];
  b_y[8] = -y_data[6];
  b_y[1] = y_data[3];
  b_y[5] = -y_data[6];
  b_y[9] = y_data[5];
  b_y[2] = y_data[6];
  b_y[6] = y_data[3];
  b_y[10] = -y_data[4];
  b_y[3] = -y_data[5];
  b_y[7] = y_data[4];
  b_y[11] = y_data[3];
  b_k = Phi->size[1];
  emxInit_real_T(&b_C, 2);
  aoffset = b_C->size[0] * b_C->size[1];
  b_C->size[0] = 4;
  b_C->size[1] = Phi->size[1];
  emxEnsureCapacity_real_T(b_C, aoffset);
  b_C_data = b_C->data;
  for (j = 0; j < b_k; j++) {
    coffset = j << 2;
    aoffset = j * 3;
    for (b_i = 0; b_i < 4; b_i++) {
      b_C_data[coffset + b_i] = (b_y[b_i] * Phi_data[aoffset] +
                                 b_y[b_i + 4] * Phi_data[aoffset + 1]) +
                                b_y[b_i + 8] * Phi_data[aoffset + 2];
    }
  }
  emxFree_real_T(&Phi);
  emxInit_real_T(&c_C, 2);
  aoffset = c_C->size[0] * c_C->size[1];
  c_C->size[0] = 4;
  c_C->size[1] = (int)Nftot;
  emxEnsureCapacity_real_T(c_C, aoffset);
  Phi_data = c_C->data;
  for (j = 0; j < n_tmp_tmp; j++) {
    coffset = j << 2;
    for (b_i = 0; b_i < 4; b_i++) {
      Phi_data[coffset + b_i] = ((A[b_i] * y_data[i1 + coffset] +
                                  A[b_i + 4] * y_data[(i1 + coffset) + 1]) +
                                 A[b_i + 8] * y_data[(i1 + coffset) + 2]) +
                                A[b_i + 12] * y_data[(i1 + coffset) + 3];
    }
  }
  if (b_C->size[1] == c_C->size[1]) {
    coffset = (int)(3.0 * Nftot);
    for (i1 = 0; i1 < 4; i1++) {
      R_tmp = A[i1];
      c_R_tmp = L * R_tmp * y_data[i];
      d_R_tmp = R_tmp * y_data[3];
      R_tmp = A[i1 + 4];
      c_R_tmp += L * R_tmp * y_data[i + 1];
      d_R_tmp += R_tmp * y_data[4];
      R_tmp = A[i1 + 8];
      c_R_tmp += L * R_tmp * y_data[i + 2];
      d_R_tmp += R_tmp * y_data[5];
      R_tmp = A[i1 + 12];
      c_R_tmp += L * R_tmp * y_data[i + 3];
      d_R_tmp += R_tmp * y_data[6];
      b_A[i1] = d_R_tmp;
      b_L[i1] = c_R_tmp;
    }
    emxInit_real_T(&a, 2);
    i1 = a->size[0] * a->size[1];
    a->size[0] = 4;
    a->size[1] = b_C->size[1];
    emxEnsureCapacity_real_T(a, i1);
    a_data = a->data;
    aoffset = 4 * b_C->size[1];
    for (i1 = 0; i1 < aoffset; i1++) {
      a_data[i1] = b_R_tmp * (b_C_data[i1] + Phi_data[i1]);
    }
    aoffset = (int)(4.0 * Nftot);
    for (i1 = 0; i1 < 4; i1++) {
      b_a[i1] =
          ((b_R_tmp * A[i1] * y_data[3] + b_R_tmp * A[i1 + 4] * y_data[4]) +
           b_R_tmp * A[i1 + 8] * y_data[5]) +
          b_R_tmp * A[i1 + 12] * y_data[6];
    }
    i1 = varargout_1->size[0];
    varargout_1->size[0] = (coffset + aoffset) + 14;
    emxEnsureCapacity_real_T(varargout_1, i1);
    Phi_data = varargout_1->data;
    for (i1 = 0; i1 < 3; i1++) {
      R_tmp = R[i1 + 6];
      k[i1] = (((L * Dh[i1] * y_data[i] + L * Dh[i1 + 3] * y_data[i + 1]) +
                L * Dh[i1 + 6] * y_data[i + 2]) +
               L * Dh[i1 + 9] * y_data[i + 3]) +
              R_tmp;
      Phi_data[i1] = L * R_tmp;
    }
    Phi_data[3] = b_a[0];
    Phi_data[4] = b_a[1];
    Phi_data[5] = b_a[2];
    Phi_data[6] = b_a[3];
    Phi_data[7] = k[0];
    Phi_data[8] = k[1];
    Phi_data[9] = k[2];
    for (i = 0; i < coffset; i++) {
      Phi_data[i + 10] = C_data[i];
    }
    Phi_data[coffset + 10] = 0.5 * (b_L[0] + b_A[0]);
    Phi_data[coffset + 11] = 0.5 * (b_L[1] + b_A[1]);
    Phi_data[coffset + 12] = 0.5 * (b_L[2] + b_A[2]);
    Phi_data[coffset + 13] = 0.5 * (b_L[3] + b_A[3]);
    for (i = 0; i < aoffset; i++) {
      Phi_data[(i + coffset) + 14] = a_data[i];
    }
    emxFree_real_T(&a);
  } else {
    e_binary_expand_op(varargout_1, L, R, b_R_tmp, A, y, Dh, i, C, Nftot,
                       b_R_tmp, b_C, c_C);
  }
  emxFree_real_T(&c_C);
  emxFree_real_T(&b_C);
  emxFree_real_T(&C);
}

static void forwardRecursion_anonFcn1(const double qei_data[], double Nf,
                                      double L, const emxArray_real_T *y,
                                      emxArray_real_T *varargout_1)
{
  static const signed char b_iv[6] = {1, -1, 1, -1, 1, -1};
  emxArray_int8_T *Phi;
  emxArray_real_T *C;
  emxArray_real_T *a;
  emxArray_real_T *b_C;
  emxArray_real_T *c_C;
  double A[16];
  double Dh[12];
  double b_y[12];
  double R[9];
  double b_A[4];
  double b_L[4];
  double b_a[4];
  double k[3];
  const double *y_data;
  double Nftot;
  double R_tmp;
  double b_R_tmp;
  double c_R_tmp;
  double d_R_tmp;
  double e_R_tmp;
  double f_R_tmp;
  double g_R_tmp;
  double h_R_tmp;
  double *C_data;
  double *a_data;
  double *b_C_data;
  double *c_C_data;
  int aoffset;
  int b_i;
  int b_k;
  int coffset;
  int i;
  int i1;
  int j;
  int n_tmp_tmp;
  signed char b_data[6];
  signed char *Phi_data;
  y_data = y->data;
  Nftot = 3.0 * Nf;
  R_tmp = 3.0 * Nftot + 11.0;
  if (R_tmp > (3.0 * Nftot + 10.0) + 4.0) {
    i = 0;
  } else {
    i = (int)R_tmp - 1;
  }
  if (R_tmp + 4.0 > ((3.0 * Nftot + 10.0) + 4.0) + 4.0 * Nftot) {
    i1 = 0;
  } else {
    i1 = (int)(R_tmp + 4.0) - 1;
  }
  /*  CONVERT QUATERNION HP INTO ROTATION MATRIX */
  /*  hP = h1 + h2 i + h3 j + h4 k (h1 = scalar component) */
  R_tmp = y_data[3] * y_data[3];
  b_R_tmp = y_data[4] * y_data[4];
  c_R_tmp = y_data[5] * y_data[5];
  d_R_tmp = y_data[6] * y_data[6];
  R[0] = ((R_tmp + b_R_tmp) - c_R_tmp) - d_R_tmp;
  e_R_tmp = y_data[4] * y_data[5];
  f_R_tmp = y_data[3] * y_data[6];
  R[1] = 2.0 * (f_R_tmp + e_R_tmp);
  g_R_tmp = y_data[3] * y_data[5];
  h_R_tmp = y_data[4] * y_data[6];
  R[2] = 2.0 * (h_R_tmp - g_R_tmp);
  R[3] = 2.0 * (e_R_tmp - f_R_tmp);
  R_tmp -= b_R_tmp;
  R[4] = (R_tmp + c_R_tmp) - d_R_tmp;
  b_R_tmp = y_data[5] * y_data[6];
  e_R_tmp = y_data[3] * y_data[4];
  R[5] = 2.0 * (e_R_tmp + b_R_tmp);
  R[6] = 2.0 * (g_R_tmp + h_R_tmp);
  R[7] = 2.0 * (b_R_tmp - e_R_tmp);
  R[8] = (R_tmp - c_R_tmp) + d_R_tmp;
  /*  vect =   [ones(1,numel(s)) ; */
  /*           s ; */
  /*           s.^2 ;  */
  /*           s.^3; */
  /*           s.^4; */
  /*           s.^5; */
  /*           ]; */
  if (Nf < 1.0) {
    coffset = 0;
  } else {
    coffset = (int)Nf;
  }
  if (coffset - 1 >= 0) {
    memcpy(&b_data[0], &b_iv[0], (unsigned int)coffset * sizeof(signed char));
  }
  emxInit_int8_T(&Phi);
  j = Phi->size[0] * Phi->size[1];
  Phi->size[0] = 3;
  Phi->size[1] = (coffset + (int)Nf) + (int)Nf;
  emxEnsureCapacity_int8_T(Phi, j);
  Phi_data = Phi->data;
  for (j = 0; j < coffset; j++) {
    Phi_data[3 * j] = b_data[j];
  }
  aoffset = (int)Nf;
  for (j = 0; j < aoffset; j++) {
    Phi_data[3 * (j + coffset)] = 0;
  }
  aoffset = (int)Nf;
  for (j = 0; j < aoffset; j++) {
    Phi_data[3 * ((j + coffset) + (int)Nf)] = 0;
  }
  aoffset = (int)Nf;
  for (j = 0; j < aoffset; j++) {
    Phi_data[3 * j + 1] = 0;
  }
  for (j = 0; j < coffset; j++) {
    Phi_data[3 * (j + (int)Nf) + 1] = b_data[j];
  }
  aoffset = (int)Nf;
  for (j = 0; j < aoffset; j++) {
    Phi_data[3 * ((j + coffset) + (int)Nf) + 1] = 0;
  }
  aoffset = (int)Nf;
  for (j = 0; j < aoffset; j++) {
    Phi_data[3 * j + 2] = 0;
  }
  aoffset = (int)Nf;
  for (j = 0; j < aoffset; j++) {
    Phi_data[3 * (j + (int)Nf) + 2] = 0;
  }
  for (j = 0; j < coffset; j++) {
    Phi_data[3 * ((j + (int)Nf) + (int)Nf) + 2] = b_data[j];
  }
  coffset = Phi->size[1];
  k[0] = 0.0;
  k[1] = 0.0;
  k[2] = 0.0;
  for (b_k = 0; b_k < coffset; b_k++) {
    aoffset = b_k * 3;
    R_tmp = qei_data[b_k];
    k[0] += (double)Phi_data[aoffset] * R_tmp;
    k[1] += (double)Phi_data[aoffset + 1] * R_tmp;
    k[2] += (double)Phi_data[aoffset + 2] * R_tmp;
  }
  A[0] = 0.0;
  A[4] = -k[0];
  A[8] = -k[1];
  A[12] = -k[2];
  A[1] = k[0];
  A[5] = 0.0;
  A[9] = k[2];
  A[13] = -k[1];
  A[2] = k[1];
  A[6] = -k[2];
  A[10] = 0.0;
  A[14] = k[0];
  A[3] = k[2];
  A[7] = k[1];
  A[11] = -k[0];
  A[15] = 0.0;
  Dh[0] = 2.0 * y_data[5];
  Dh[3] = 2.0 * y_data[6];
  Dh[6] = 2.0 * y_data[3];
  Dh[9] = 2.0 * y_data[4];
  R_tmp = 2.0 * -y_data[4];
  Dh[1] = R_tmp;
  Dh[4] = 2.0 * -y_data[3];
  Dh[7] = 2.0 * y_data[6];
  Dh[10] = 2.0 * y_data[5];
  Dh[2] = 2.0 * y_data[3];
  Dh[5] = R_tmp;
  Dh[8] = 2.0 * -y_data[5];
  Dh[11] = 2.0 * y_data[6];
  b_R_tmp = L * 0.5;
  for (j = 0; j < 12; j++) {
    b_y[j] = L * Dh[j];
  }
  n_tmp_tmp = (int)Nftot;
  emxInit_real_T(&C, 2);
  j = C->size[0] * C->size[1];
  C->size[0] = 3;
  C->size[1] = (int)Nftot;
  emxEnsureCapacity_real_T(C, j);
  C_data = C->data;
  for (j = 0; j < n_tmp_tmp; j++) {
    coffset = j * 3;
    aoffset = j << 2;
    for (b_i = 0; b_i < 3; b_i++) {
      C_data[coffset + b_i] = ((b_y[b_i] * y_data[i1 + aoffset] +
                                b_y[b_i + 3] * y_data[(i1 + aoffset) + 1]) +
                               b_y[b_i + 6] * y_data[(i1 + aoffset) + 2]) +
                              b_y[b_i + 9] * y_data[(i1 + aoffset) + 3];
    }
  }
  b_y[0] = -y_data[4];
  b_y[4] = -y_data[5];
  b_y[8] = -y_data[6];
  b_y[1] = y_data[3];
  b_y[5] = -y_data[6];
  b_y[9] = y_data[5];
  b_y[2] = y_data[6];
  b_y[6] = y_data[3];
  b_y[10] = -y_data[4];
  b_y[3] = -y_data[5];
  b_y[7] = y_data[4];
  b_y[11] = y_data[3];
  b_k = Phi->size[1];
  emxInit_real_T(&b_C, 2);
  j = b_C->size[0] * b_C->size[1];
  b_C->size[0] = 4;
  b_C->size[1] = Phi->size[1];
  emxEnsureCapacity_real_T(b_C, j);
  b_C_data = b_C->data;
  for (j = 0; j < b_k; j++) {
    coffset = j << 2;
    aoffset = j * 3;
    for (b_i = 0; b_i < 4; b_i++) {
      b_C_data[coffset + b_i] = (b_y[b_i] * (double)Phi_data[aoffset] +
                                 b_y[b_i + 4] * (double)Phi_data[aoffset + 1]) +
                                b_y[b_i + 8] * (double)Phi_data[aoffset + 2];
    }
  }
  emxFree_int8_T(&Phi);
  emxInit_real_T(&c_C, 2);
  j = c_C->size[0] * c_C->size[1];
  c_C->size[0] = 4;
  c_C->size[1] = (int)Nftot;
  emxEnsureCapacity_real_T(c_C, j);
  c_C_data = c_C->data;
  for (j = 0; j < n_tmp_tmp; j++) {
    coffset = j << 2;
    for (b_i = 0; b_i < 4; b_i++) {
      c_C_data[coffset + b_i] = ((A[b_i] * y_data[i1 + coffset] +
                                  A[b_i + 4] * y_data[(i1 + coffset) + 1]) +
                                 A[b_i + 8] * y_data[(i1 + coffset) + 2]) +
                                A[b_i + 12] * y_data[(i1 + coffset) + 3];
    }
  }
  if (b_C->size[1] == c_C->size[1]) {
    coffset = (int)(3.0 * Nftot);
    for (i1 = 0; i1 < 4; i1++) {
      R_tmp = A[i1];
      c_R_tmp = L * R_tmp * y_data[i];
      d_R_tmp = R_tmp * y_data[3];
      R_tmp = A[i1 + 4];
      c_R_tmp += L * R_tmp * y_data[i + 1];
      d_R_tmp += R_tmp * y_data[4];
      R_tmp = A[i1 + 8];
      c_R_tmp += L * R_tmp * y_data[i + 2];
      d_R_tmp += R_tmp * y_data[5];
      R_tmp = A[i1 + 12];
      c_R_tmp += L * R_tmp * y_data[i + 3];
      d_R_tmp += R_tmp * y_data[6];
      b_A[i1] = d_R_tmp;
      b_L[i1] = c_R_tmp;
    }
    emxInit_real_T(&a, 2);
    i1 = a->size[0] * a->size[1];
    a->size[0] = 4;
    a->size[1] = b_C->size[1];
    emxEnsureCapacity_real_T(a, i1);
    a_data = a->data;
    aoffset = 4 * b_C->size[1];
    for (i1 = 0; i1 < aoffset; i1++) {
      a_data[i1] = b_R_tmp * (b_C_data[i1] + c_C_data[i1]);
    }
    aoffset = (int)(4.0 * Nftot);
    for (i1 = 0; i1 < 4; i1++) {
      b_a[i1] =
          ((b_R_tmp * A[i1] * y_data[3] + b_R_tmp * A[i1 + 4] * y_data[4]) +
           b_R_tmp * A[i1 + 8] * y_data[5]) +
          b_R_tmp * A[i1 + 12] * y_data[6];
    }
    i1 = varargout_1->size[0];
    varargout_1->size[0] = (coffset + aoffset) + 14;
    emxEnsureCapacity_real_T(varargout_1, i1);
    b_C_data = varargout_1->data;
    for (i1 = 0; i1 < 3; i1++) {
      R_tmp = R[i1 + 6];
      k[i1] = (((L * Dh[i1] * y_data[i] + L * Dh[i1 + 3] * y_data[i + 1]) +
                L * Dh[i1 + 6] * y_data[i + 2]) +
               L * Dh[i1 + 9] * y_data[i + 3]) +
              R_tmp;
      b_C_data[i1] = L * R_tmp;
    }
    b_C_data[3] = b_a[0];
    b_C_data[4] = b_a[1];
    b_C_data[5] = b_a[2];
    b_C_data[6] = b_a[3];
    b_C_data[7] = k[0];
    b_C_data[8] = k[1];
    b_C_data[9] = k[2];
    for (i = 0; i < coffset; i++) {
      b_C_data[i + 10] = C_data[i];
    }
    b_C_data[coffset + 10] = 0.5 * (b_L[0] + b_A[0]);
    b_C_data[coffset + 11] = 0.5 * (b_L[1] + b_A[1]);
    b_C_data[coffset + 12] = 0.5 * (b_L[2] + b_A[2]);
    b_C_data[coffset + 13] = 0.5 * (b_L[3] + b_A[3]);
    for (i = 0; i < aoffset; i++) {
      b_C_data[(i + coffset) + 14] = a_data[i];
    }
    emxFree_real_T(&a);
  } else {
    e_binary_expand_op(varargout_1, L, R, b_R_tmp, A, y, Dh, i, C, Nftot,
                       b_R_tmp, b_C, c_C);
  }
  emxFree_real_T(&c_C);
  emxFree_real_T(&b_C);
  emxFree_real_T(&C);
}

void forwardRecursion(const double p1L[3], const double h1L[4],
                      const double qei_data[], double L, double Nf, double p[3],
                      double h[4], double dpdqa[3], emxArray_real_T *dhdqa,
                      emxArray_real_T *dpdqe, emxArray_real_T *dhdqe)
{
  emxArray_real_T *f;
  emxArray_real_T *f0;
  emxArray_real_T *tout;
  emxArray_real_T *varargin_1;
  emxArray_real_T *y;
  emxArray_real_T *y2F;
  emxArray_real_T *ynew;
  emxArray_real_T *yout;
  emxArray_real_T *youtnew;
  double toutnew[4];
  double tref[3];
  double Nftot;
  double absh;
  double absx;
  double d;
  double hmin;
  double rh;
  double t;
  double tnew;
  double *f0_data;
  double *f_data;
  double *tout_data;
  double *y2F_data;
  double *ynew_data;
  double *yout_data;
  int Bcolidx;
  int chunk;
  int exitg1;
  int exitg2;
  int exponent;
  int i;
  int i1;
  int ia;
  int j;
  int loop_ub;
  int neq;
  int nout;
  int nrows;
  int nx;
  int outidx;
  boolean_T Done;
  boolean_T MinStepExit;
  boolean_T NoFailedAttempts;
  Nftot = 3.0 * Nf;
  emxInit_real_T(&y2F, 1);
  loop_ub = (int)(3.0 * Nftot + 3.0);
  nx = (int)(4.0 * Nftot);
  i = y2F->size[0];
  y2F->size[0] = (loop_ub + nx) + 11;
  emxEnsureCapacity_real_T(y2F, i);
  y2F_data = y2F->data;
  y2F_data[0] = p1L[0];
  y2F_data[1] = p1L[1];
  y2F_data[2] = p1L[2];
  y2F_data[3] = h1L[0];
  y2F_data[4] = h1L[1];
  y2F_data[5] = h1L[2];
  y2F_data[6] = h1L[3];
  for (i = 0; i < loop_ub; i++) {
    y2F_data[i + 7] = 0.0;
  }
  y2F_data[loop_ub + 7] = 0.0;
  y2F_data[loop_ub + 8] = 0.0;
  y2F_data[loop_ub + 9] = 0.0;
  y2F_data[loop_ub + 10] = 0.0;
  for (i = 0; i < nx; i++) {
    y2F_data[(i + loop_ub) + 11] = 0.0;
  }
  neq = y2F->size[0];
  emxInit_real_T(&f0, 1);
  forwardRecursion_anonFcn1(qei_data, Nf, L, y2F, f0);
  f0_data = f0->data;
  if ((unsigned int)y2F->size[0] == 0U) {
    i = MAX_int32_T;
  } else {
    i = (int)(8192U / (unsigned int)y2F->size[0]);
  }
  if (i + 4 >= 200) {
    chunk = 200;
  } else {
    chunk = i + 4;
  }
  emxInit_real_T(&tout, 2);
  i = tout->size[0] * tout->size[1];
  tout->size[0] = 1;
  tout->size[1] = chunk;
  emxEnsureCapacity_real_T(tout, i);
  tout_data = tout->data;
  for (i = 0; i < chunk; i++) {
    tout_data[i] = 0.0;
  }
  emxInit_real_T(&yout, 2);
  i = yout->size[0] * yout->size[1];
  yout->size[0] = y2F->size[0];
  yout->size[1] = chunk;
  emxEnsureCapacity_real_T(yout, i);
  yout_data = yout->data;
  loop_ub = y2F->size[0] * chunk;
  for (i = 0; i < loop_ub; i++) {
    yout_data[i] = 0.0;
  }
  nout = 0;
  loop_ub = y2F->size[0];
  for (i = 0; i < loop_ub; i++) {
    yout_data[i] = y2F_data[i];
  }
  absh = 0.1;
  nx = y2F->size[0];
  emxInit_real_T(&ynew, 1);
  i = ynew->size[0];
  ynew->size[0] = y2F->size[0];
  emxEnsureCapacity_real_T(ynew, i);
  ynew_data = ynew->data;
  for (nrows = 0; nrows < nx; nrows++) {
    ynew_data[nrows] = fabs(y2F_data[nrows]);
  }
  loop_ub = ynew->size[0];
  for (i = 0; i < loop_ub; i++) {
    rh = ynew_data[i];
    if (rh >= 0.001) {
      ynew_data[i] = rh;
    } else {
      ynew_data[i] = 0.001;
    }
  }
  i = ynew->size[0];
  ynew->size[0] = f0->size[0];
  emxEnsureCapacity_real_T(ynew, i);
  ynew_data = ynew->data;
  loop_ub = f0->size[0];
  for (i = 0; i < loop_ub; i++) {
    ynew_data[i] = f0_data[i] / ynew_data[i];
  }
  rh = 0.0;
  i = ynew->size[0];
  for (nrows = 0; nrows < i; nrows++) {
    absx = fabs(ynew_data[nrows]);
    if (rtIsNaN(absx) || (absx > rh)) {
      rh = absx;
    }
  }
  rh /= 0.20095091452076641;
  if (0.1 * rh > 1.0) {
    absh = 1.0 / rh;
  }
  if (!(absh >= 7.90505033345994E-323)) {
    absh = 7.90505033345994E-323;
  }
  t = 0.0;
  emxInit_real_T(&f, 2);
  i = f->size[0] * f->size[1];
  f->size[0] = y2F->size[0];
  f->size[1] = 7;
  emxEnsureCapacity_real_T(f, i);
  f_data = f->data;
  loop_ub = y2F->size[0] * 7;
  for (i = 0; i < loop_ub; i++) {
    f_data[i] = 0.0;
  }
  loop_ub = f0->size[0];
  for (i = 0; i < loop_ub; i++) {
    f_data[i] = f0_data[i];
  }
  MinStepExit = false;
  Done = false;
  emxInit_real_T(&youtnew, 2);
  emxInit_real_T(&varargin_1, 2);
  do {
    exitg1 = 0;
    absx = fabs(t);
    if (rtIsInf(absx) || rtIsNaN(absx)) {
      rh = rtNaN;
    } else if (absx < 4.4501477170144028E-308) {
      rh = 4.94065645841247E-324;
    } else {
      frexp(absx, &exponent);
      rh = ldexp(1.0, exponent - 53);
    }
    hmin = 16.0 * rh;
    if ((hmin >= absh) || rtIsNaN(absh)) {
      rh = hmin;
    } else {
      rh = absh;
    }
    if ((rh >= 0.1) || rtIsNaN(rh)) {
      absh = 0.1;
    } else {
      absh = rh;
    }
    absx = absh;
    d = fabs(1.0 - t);
    if (1.1 * absh >= d) {
      absx = 1.0 - t;
      absh = d;
      Done = true;
    }
    NoFailedAttempts = true;
    do {
      exitg2 = 0;
      Bcolidx = 0;
      loop_ub = y2F->size[0];
      for (j = 0; j < 5; j++) {
        Bcolidx += j;
        i = ynew->size[0];
        ynew->size[0] = y2F->size[0];
        emxEnsureCapacity_real_T(ynew, i);
        ynew_data = ynew->data;
        for (i = 0; i < loop_ub; i++) {
          ynew_data[i] = y2F_data[i];
        }
        if (!(absx == 0.0)) {
          nx = Bcolidx;
          i = neq * j + 1;
          for (outidx = 1; neq < 0 ? outidx >= i : outidx <= i; outidx += neq) {
            rh = absx * dv[nx];
            i1 = (outidx + neq) - 1;
            for (ia = outidx; ia <= i1; ia++) {
              nrows = ia - outidx;
              ynew_data[nrows] += f_data[ia - 1] * rh;
            }
            nx++;
          }
        }
        b_forwardRecursion_anonFcn1(qei_data, Nf, L, t + absx * dv1[j], ynew,
                                    f0);
        f0_data = f0->data;
        nx = f0->size[0];
        for (i = 0; i < nx; i++) {
          f_data[i + f->size[0] * (j + 1)] = f0_data[i];
        }
      }
      tnew = t + absx;
      i = ynew->size[0];
      ynew->size[0] = y2F->size[0];
      emxEnsureCapacity_real_T(ynew, i);
      ynew_data = ynew->data;
      loop_ub = y2F->size[0];
      for (i = 0; i < loop_ub; i++) {
        ynew_data[i] = y2F_data[i];
      }
      if (!(absx == 0.0)) {
        i = neq * 5 + 1;
        for (outidx = 1; neq < 0 ? outidx >= i : outidx <= i; outidx += neq) {
          rh = absx * dv[Bcolidx + 5];
          i1 = (outidx + neq) - 1;
          for (ia = outidx; ia <= i1; ia++) {
            nrows = ia - outidx;
            ynew_data[nrows] += f_data[ia - 1] * rh;
          }
          Bcolidx++;
        }
      }
      b_forwardRecursion_anonFcn1(qei_data, Nf, L, tnew, ynew, f0);
      f0_data = f0->data;
      loop_ub = f0->size[0];
      for (i = 0; i < loop_ub; i++) {
        f_data[i + f->size[0] * 6] = f0_data[i];
      }
      if (Done) {
        tnew = 1.0;
      }
      nx = f->size[0];
      i = f0->size[0];
      f0->size[0] = f->size[0];
      emxEnsureCapacity_real_T(f0, i);
      f0_data = f0->data;
      for (loop_ub = 0; loop_ub < nx; loop_ub++) {
        rh = 0.0;
        for (nrows = 0; nrows < 7; nrows++) {
          rh += f_data[nrows * f->size[0] + loop_ub] * dv2[nrows];
        }
        f0_data[loop_ub] = rh;
      }
      absx = absh * maxScaledError(f0, y2F, ynew);
      if (!(absx <= 0.001)) {
        if (absh <= hmin) {
          MinStepExit = true;
          exitg2 = 1;
        } else {
          if (NoFailedAttempts) {
            NoFailedAttempts = false;
            rh = 0.8 * rt_powd_snf(0.001 / absx, 0.2);
            if ((rh <= 0.1) || rtIsNaN(rh)) {
              rh = 0.1;
            }
            rh *= absh;
            if ((hmin >= rh) || rtIsNaN(rh)) {
              absh = hmin;
            } else {
              absh = rh;
            }
          } else {
            rh = 0.5 * absh;
            if ((hmin >= rh) || rtIsNaN(rh)) {
              absh = hmin;
            } else {
              absh = rh;
            }
          }
          absx = absh;
          Done = false;
        }
      } else {
        exitg2 = 1;
      }
    } while (exitg2 == 0);
    if (MinStepExit) {
      exitg1 = 1;
    } else {
      outidx = nout + 1;
      rh = tnew - t;
      d = t + rh * 0.25;
      tref[0] = d;
      toutnew[0] = d;
      d = t + rh * 0.5;
      tref[1] = d;
      toutnew[1] = d;
      d = t + rh * 0.75;
      tref[2] = d;
      toutnew[2] = d;
      toutnew[3] = tnew;
      ntrp45(tref, t, y2F, rh, f, varargin_1);
      y2F_data = varargin_1->data;
      nx = varargin_1->size[0];
      i = youtnew->size[0] * youtnew->size[1];
      youtnew->size[0] = varargin_1->size[0];
      youtnew->size[1] = 4;
      emxEnsureCapacity_real_T(youtnew, i);
      f0_data = youtnew->data;
      loop_ub = varargin_1->size[0];
      for (i = 0; i < 3; i++) {
        for (i1 = 0; i1 < loop_ub; i1++) {
          f0_data[i1 + youtnew->size[0] * i] =
              y2F_data[i1 + varargin_1->size[0] * i];
        }
      }
      for (i = 0; i < nx; i++) {
        f0_data[i + youtnew->size[0] * 3] = ynew_data[i];
      }
      nout += 4;
      if (nout + 1 > tout->size[1]) {
        nx = tout->size[1];
        i = tout->size[0] * tout->size[1];
        tout->size[0] = 1;
        tout->size[1] += chunk;
        emxEnsureCapacity_real_T(tout, i);
        tout_data = tout->data;
        i = (unsigned char)chunk;
        nrows = yout->size[0];
        Bcolidx = yout->size[1];
        i1 = yout->size[0] * yout->size[1];
        yout->size[1] += chunk;
        emxEnsureCapacity_real_T(yout, i1);
        yout_data = yout->data;
        for (j = 0; j < i; j++) {
          tout_data[nx + j] = 0.0;
          for (loop_ub = 0; loop_ub < nrows; loop_ub++) {
            yout_data[loop_ub + yout->size[0] * (Bcolidx + j)] = 0.0;
          }
        }
      }
      for (nrows = 0; nrows < 4; nrows++) {
        nx = nrows + outidx;
        tout_data[nx] = toutnew[nrows];
        for (j = 0; j < neq; j++) {
          yout_data[j + yout->size[0] * nx] =
              f0_data[j + youtnew->size[0] * nrows];
        }
      }
      if (Done) {
        exitg1 = 1;
      } else {
        if (NoFailedAttempts) {
          rh = 1.25 * rt_powd_snf(absx / 0.001, 0.2);
          if (rh > 0.2) {
            absh /= rh;
          } else {
            absh *= 5.0;
          }
        }
        t = tnew;
        i = y2F->size[0];
        y2F->size[0] = ynew->size[0];
        emxEnsureCapacity_real_T(y2F, i);
        y2F_data = y2F->data;
        loop_ub = ynew->size[0];
        for (i = 0; i < loop_ub; i++) {
          y2F_data[i] = ynew_data[i];
        }
        i = f0->size[0];
        f0->size[0] = f->size[0];
        emxEnsureCapacity_real_T(f0, i);
        f0_data = f0->data;
        loop_ub = f->size[0];
        for (i = 0; i < loop_ub; i++) {
          f0_data[i] = f_data[i + f->size[0] * 6];
        }
        loop_ub = f0->size[0];
        for (i = 0; i < loop_ub; i++) {
          f_data[i] = f0_data[i];
        }
      }
    }
  } while (exitg1 == 0);
  emxFree_real_T(&varargin_1);
  emxFree_real_T(&youtnew);
  emxFree_real_T(&f);
  emxFree_real_T(&tout);
  emxFree_real_T(&f0);
  emxFree_real_T(&y2F);
  if (nout + 1 < 1) {
    loop_ub = -1;
  } else {
    loop_ub = nout;
  }
  emxInit_real_T(&y, 2);
  i = y->size[0] * y->size[1];
  y->size[0] = loop_ub + 1;
  y->size[1] = yout->size[0];
  emxEnsureCapacity_real_T(y, i);
  y2F_data = y->data;
  nx = yout->size[0];
  for (i = 0; i < nx; i++) {
    for (i1 = 0; i1 <= loop_ub; i1++) {
      y2F_data[i1 + y->size[0] * i] = yout_data[i + yout->size[0] * i1];
    }
  }
  emxFree_real_T(&yout);
  i = ynew->size[0];
  ynew->size[0] = y->size[1];
  emxEnsureCapacity_real_T(ynew, i);
  ynew_data = ynew->data;
  loop_ub = y->size[1];
  for (i = 0; i < loop_ub; i++) {
    ynew_data[i] = y2F_data[(y->size[0] + y->size[0] * i) - 1];
  }
  emxFree_real_T(&y);
  p[0] = ynew_data[0];
  p[1] = ynew_data[1];
  p[2] = ynew_data[2];
  h[0] = ynew_data[3];
  h[1] = ynew_data[4];
  h[2] = ynew_data[5];
  h[3] = ynew_data[6];
  dpdqa[0] = ynew_data[7];
  dpdqa[1] = ynew_data[8];
  dpdqa[2] = ynew_data[9];
  d = 3.0 * Nftot + 10.0;
  if (d < 11.0) {
    i = 0;
    i1 = 0;
  } else {
    i = 10;
    i1 = (int)d;
  }
  loop_ub = i1 - i;
  i1 = dpdqe->size[0];
  dpdqe->size[0] = loop_ub;
  emxEnsureCapacity_real_T(dpdqe, i1);
  y2F_data = dpdqe->data;
  for (i1 = 0; i1 < loop_ub; i1++) {
    y2F_data[i1] = ynew_data[i + i1];
  }
  rh = 3.0 * Nftot + 11.0;
  if (rh > d + 4.0) {
    i = 0;
    i1 = 0;
  } else {
    i = (int)rh - 1;
    i1 = (int)(d + 4.0);
  }
  loop_ub = i1 - i;
  i1 = dhdqa->size[0];
  dhdqa->size[0] = loop_ub;
  emxEnsureCapacity_real_T(dhdqa, i1);
  y2F_data = dhdqa->data;
  for (i1 = 0; i1 < loop_ub; i1++) {
    y2F_data[i1] = ynew_data[i + i1];
  }
  d = (d + 4.0) + 4.0 * Nftot;
  if (rh + 4.0 > d) {
    i = 0;
    i1 = 0;
  } else {
    i = (int)(rh + 4.0) - 1;
    i1 = (int)d;
  }
  loop_ub = i1 - i;
  i1 = dhdqe->size[0];
  dhdqe->size[0] = loop_ub;
  emxEnsureCapacity_real_T(dhdqe, i1);
  y2F_data = dhdqe->data;
  for (i1 = 0; i1 < loop_ub; i1++) {
    y2F_data[i1] = ynew_data[i + i1];
  }
  emxFree_real_T(&ynew);
}

/* End of code generation (forwardRecursion.c) */
