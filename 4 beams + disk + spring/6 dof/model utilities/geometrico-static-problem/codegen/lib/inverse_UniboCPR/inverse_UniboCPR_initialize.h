/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * inverse_UniboCPR_initialize.h
 *
 * Code generation for function 'inverse_UniboCPR_initialize'
 *
 */

#ifndef INVERSE_UNIBOCPR_INITIALIZE_H
#define INVERSE_UNIBOCPR_INITIALIZE_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
extern void inverse_UniboCPR_initialize(void);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (inverse_UniboCPR_initialize.h) */
