/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ntrp45.c
 *
 * Code generation for function 'ntrp45'
 *
 */

/* Include files */
#include "ntrp45.h"
#include "inverse_UniboCPR_emxutil.h"
#include "inverse_UniboCPR_types.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */
void ntrp45(const double t[3], double t0, const emxArray_real_T *b_y0, double h,
            const emxArray_real_T *f, emxArray_real_T *y)
{
  static const double b[7] = {-2.859375,
                              0.0,
                              4.0431266846361185,
                              -3.90625,
                              2.7939268867924527,
                              -1.5714285714285714,
                              1.5};
  static const double b_b[7] = {3.0833333333333335,
                                0.0,
                                -6.2893081761006293,
                                10.416666666666666,
                                -6.8773584905660377,
                                3.6666666666666665,
                                -4.0};
  static const double c_b[7] = {-1.1328125,
                                0.0,
                                2.6954177897574123,
                                -5.859375,
                                3.7610554245283021,
                                -1.9642857142857142,
                                2.5};
  emxArray_real_T *fhBI1;
  emxArray_real_T *fhBI2;
  emxArray_real_T *fhBI3;
  emxArray_real_T *fhBI4;
  double b_y[7];
  const double *f_data;
  const double *y0_data;
  double s;
  double *fhBI1_data;
  double *fhBI2_data;
  double *fhBI3_data;
  double *fhBI4_data;
  double *y_data;
  int i;
  int k;
  int loop_ub;
  int neq;
  f_data = f->data;
  y0_data = b_y0->data;
  neq = b_y0->size[0];
  emxInit_real_T(&fhBI1, 1);
  i = fhBI1->size[0];
  fhBI1->size[0] = f->size[0];
  emxEnsureCapacity_real_T(fhBI1, i);
  fhBI1_data = fhBI1->data;
  loop_ub = f->size[0];
  for (i = 0; i < loop_ub; i++) {
    fhBI1_data[i] = f_data[i] * h;
  }
  for (i = 0; i < 7; i++) {
    b_y[i] = h * b[i];
  }
  loop_ub = f->size[0];
  emxInit_real_T(&fhBI2, 1);
  i = fhBI2->size[0];
  fhBI2->size[0] = f->size[0];
  emxEnsureCapacity_real_T(fhBI2, i);
  fhBI2_data = fhBI2->data;
  for (i = 0; i < loop_ub; i++) {
    s = 0.0;
    for (k = 0; k < 7; k++) {
      s += f_data[k * f->size[0] + i] * b_y[k];
    }
    fhBI2_data[i] = s;
  }
  for (i = 0; i < 7; i++) {
    b_y[i] = h * b_b[i];
  }
  loop_ub = f->size[0];
  emxInit_real_T(&fhBI3, 1);
  i = fhBI3->size[0];
  fhBI3->size[0] = f->size[0];
  emxEnsureCapacity_real_T(fhBI3, i);
  fhBI3_data = fhBI3->data;
  for (i = 0; i < loop_ub; i++) {
    s = 0.0;
    for (k = 0; k < 7; k++) {
      s += f_data[k * f->size[0] + i] * b_y[k];
    }
    fhBI3_data[i] = s;
  }
  for (i = 0; i < 7; i++) {
    b_y[i] = h * c_b[i];
  }
  loop_ub = f->size[0];
  emxInit_real_T(&fhBI4, 1);
  i = fhBI4->size[0];
  fhBI4->size[0] = f->size[0];
  emxEnsureCapacity_real_T(fhBI4, i);
  fhBI4_data = fhBI4->data;
  for (i = 0; i < loop_ub; i++) {
    s = 0.0;
    for (k = 0; k < 7; k++) {
      s += f_data[k * f->size[0] + i] * b_y[k];
    }
    fhBI4_data[i] = s;
  }
  i = y->size[0] * y->size[1];
  y->size[0] = b_y0->size[0];
  y->size[1] = 3;
  emxEnsureCapacity_real_T(y, i);
  y_data = y->data;
  for (loop_ub = 0; loop_ub < 3; loop_ub++) {
    s = (t[loop_ub] - t0) / h;
    for (k = 0; k < neq; k++) {
      y_data[k + y->size[0] * loop_ub] =
          (((fhBI4_data[k] * s + fhBI3_data[k]) * s + fhBI2_data[k]) * s +
           fhBI1_data[k]) *
              s +
          y0_data[k];
    }
  }
  emxFree_real_T(&fhBI4);
  emxFree_real_T(&fhBI3);
  emxFree_real_T(&fhBI2);
  emxFree_real_T(&fhBI1);
}

/* End of code generation (ntrp45.c) */
