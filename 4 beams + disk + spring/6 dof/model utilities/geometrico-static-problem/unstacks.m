function [qa,qe1v,qe2v,qm1,qm2,Lf,qd,qp,lambdad,lambdat,lambdas] = unstacks(guess,Nftot,nj)
qa = guess(1:6,1); %OK
qe = guess(1+6:6+8*Nftot,1); %OK
qe1v = qe(1:4*Nftot,1); %OK
qe2v = qe(1+4*Nftot:8*Nftot,1); %OK
qm = guess(1+6+8*Nftot:6+8*Nftot+4*Nftot,1); %OK
qm1 = qm(1:2*Nftot); %OK
qm2 = qm(1+2*Nftot:4*Nftot); %OK
Lf = guess(1+6+8*Nftot+4*Nftot:6+8*Nftot+4*Nftot+4,1); %OK
qd = guess(1+6+8*Nftot+4*Nftot+4:6+8*Nftot+4*Nftot+4+6,1); %OK
qp = guess(1+6+8*Nftot+4*Nftot+4+6:6+8*Nftot+4*Nftot+4+6+6,1); %OK
lambdad = guess(1+6+8*Nftot+4*Nftot+4+6+6:6+8*Nftot+4*Nftot+4+6+6+4*4,1); %OK
lambdat = guess(1+6+8*Nftot+4*Nftot+4+6+6+4*4:6+8*Nftot+4*Nftot+4+6+6+4*4+4*nj,1); %OK
lambdas = guess(1+6+8*Nftot+4*Nftot+4+6+6+4*4+4*nj:6+8*Nftot+4*Nftot+4+6+6+4*4+4*nj+12,1); %OK

end