function [wbase,Qc,dwbasedqe,dQcdqe,dwbasedw0,dQcdw0] = backwardRecursionExt(twrench,qe,Nf,L,wd)

Nftot = 6*Nf;
y2B = [twrench;zeros(Nftot,1);zeros(Nftot*(6+Nftot),1);reshape(eye(6),6*6,1);zeros(6*Nftot,1)];
funbackward = @(s,y) OdefunAssumeBackwardExt(s,y,qe,wd,Nf,L,'fix');
[~,y] = ode45(funbackward,[1,0],y2B);
yforces = y(end,:)';

% primar variables
wbase = yforces(1:6,1);
Qc =   yforces(1+6:6+Nftot,1);

% derivatives
dwbasedqe = yforces(1+6+Nftot:6+Nftot+6*Nftot,1);
dQcdqe = yforces(1+6+Nftot+6*Nftot:6+Nftot+6*Nftot+Nftot*Nftot,1);
dwbasedw0 = yforces(1+6+Nftot+6*Nftot+Nftot*Nftot:6+Nftot+6*Nftot+Nftot*Nftot+6*6,1);
dQcdw0 = yforces(1+6+Nftot+6*Nftot+Nftot*Nftot+6*6:6+Nftot+6*Nftot+Nftot*Nftot+6*6+6*Nftot,1);

end