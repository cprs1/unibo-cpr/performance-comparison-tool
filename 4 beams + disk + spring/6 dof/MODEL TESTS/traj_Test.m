%% Unibo CPR Structure - inverse problem
% Assumed strain mode approach, forward-backward formulation
% Federico Zaccaria 19 Jan 2022

clear
close all
clc
%% PARAMETERS

[geometry,params,Kbt_1,Kbt_2,Km] = createRobotParams();
params.g = [0;0;0]; % gravity
params.tipwrench = [0;0;0;0;0;-30]; % tip wrench
[~,nj] = size(geometry.jointmatr);  

%% INVERSE PROBLEM
xP = 0; yP = 0; zP = 0.1; alpha = 0*pi/180; beta = 2*pi/180; gamma = 0*pi/180;
qpv = [xP;yP;zP;alpha;beta;gamma];

%% MODEL PARAMETERS
Nf = 4;
params.Nf = Nf;
M = @(s) PhiMatr(s,1,Nf);
Mm = @(s) [PhiMatr(s,1,Nf),zeros(3,3*Nf);zeros(3,3*Nf),PhiMatr(s,1,Nf)];

Kad1 = integral(@(s) M(s)'*Kbt_1*M(s),0,1,'ArrayValued',true);
Kad2 = integral(@(s) M(s)'*Kbt_2*M(s),0,1,'ArrayValued',true);

Kadm = integral(@(s) Mm(s)'*Km*Mm(s),0,1,'ArrayValued',true);

params.rotparams = 'TT';
%% Solution
qa0 = [0;0;0.4;0.4;0.4;0.4];
qe0 = repmat([0.0;zeros(3*Nf-1,1)],8,1);
qm0 = zeros(2*6*Nf,1);
Lf0 = [0.1;0.1;0.1;0.1];
qd0 = [0;0;0.3;0;0;0];
qp0 = qpv;
lambda0 = zeros(4*nj+4*4+6+6,1);
guess0 = [qa0;qe0;qm0;Lf0;qd0;qp0;lambda0];

options = optimoptions('fsolve','display','iter-detailed','Algorithm','trust-region','SpecifyObjectiveGradient',true,'CheckGradients',false,'Maxiterations',500);

j = 1;
jmax = 700;
flag = 1;

flagv1 = NaN*zeros(jmax,1);
flagv2 = NaN*zeros(jmax,1);
flagvs = NaN*zeros(jmax,1);
yv = NaN*zeros(jmax,1);

h = [];
while j<jmax && (flag==1||flag==3)

fun = @(guess) inverse_UniboCPR_disk_spring(guess,geometry,params,Kad2,Kadm,qpv);
[sol,~,flag,~,jac] = fsolve(fun,guess0,options);
    jac = full(jac);
options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',false,'Maxiterations',20);

%% TUBE POSITION COMPUTATION
if j==1
    qa0 = sol(1:2);
    qt0 = repmat([5.2;zeros(3*Nf-1,1)],4,1);
    lambdat0 = zeros(4*5,1);
    guesst0 = [qa0;qt0;lambdat0];
    funt = @(guess) tubes_UniboCPR(guess,geometry,params,Kad1,sol(1:2));
optionst = optimoptions('fsolve','display','iter-detailed','Maxiterations',50,'SpecifyObjectiveGradient',true,'CheckGradients',false,'Maxiterations',50);

[solt,~,~,~,jact] = fsolve(funt,guesst0,optionst);

end

%% PLOT & STRAIN RECOVERY
% delete(h);
% h = plotUniboCPR_disk_spring(sol,solt,geometry,Nf,100,geometry.hr);
% pause(0.001);
% hold on
% drawnow

%% SINGULARITY AND EQUILIBRIUM STABILITY ANALYSIS

[flag1,flag2] = SingularitiesUniboPrototypeDiskSpring(sol,jac,geometry,params);
flags = StabilityUniboPrototypeDiskSpring(sol,jac,nj,params);

%% update
flagv1(j,1) = flag1;
% flagv1(j,1) = norm(inv(jac),Inf);

flagv2(j,1) = flag2;
flagvs(j,1) = flags;
yv(j,1) = qpv(3);
guess0 = sol;
qpv = qpv + [0;0;0.001;0*pi/180;0*pi/180;0];
j = j+1;

end

%%
figure()
yyaxis left
semilogy(180*yv/pi,flagv1,'r-')
hold on
semilogy(180*yv/pi,flagv2,'k-')
grid minor
yyaxis right
plot(180*yv/pi,flagvs,'-')