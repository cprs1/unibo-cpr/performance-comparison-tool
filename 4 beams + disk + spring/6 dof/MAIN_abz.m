%% ZETA_ALPHA_BETA_FLOODING
% F.Zaccaria 13 February 2022
% first the zero torsion wk is computed over each z (fixed xy).
% then the properties are projected at each xyz point we use the flooding
% to reconstruct the values

%% MAIN FILE
clear
close all
clc

%% ROBOT PARAMETERS
FILE_UniboCPR_robotfile_abz

%% SIMULATION PARAMETERS
FILE_inputfile

%%
instantplot = 0;
figure()
plot3(pstart(1),pstart(2),pstart(3),'.')
grid minor
axis(boxsize)
hold on 
drawnow
[WK,guesses,outstruct] = FCN_FloodingSpatialWK(fcn,params,instantplot);

%%

FCN_plot_space(WK,params)

%% Slice

% one slice:
figure()
z = 0.2;
[h,Lmax,dispmax] = PlotTorsionSlice(WK,z,guesses);

%% INDEX
[idx,idy,idu,A] = index_computation(WK,params);

%%
idx*180/pi
idy*180/pi
idu*180/pi
A
save WK WK
save guesses guesses