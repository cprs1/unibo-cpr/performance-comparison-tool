function id2 = getNeighborsPlanar(WK,pstart,params,fact)
stepsizerad = params.stepsizerad;
stepsizetilt = params.stepsizetilt;

id = ((abs(WK(:,2)-pstart(1))<=fact*stepsizerad) & (abs(WK(:,3)-pstart(2))<=fact*stepsizetilt));
id2 = WK(id==1,1);
end