/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * eul2quat.h
 *
 * Code generation for function 'eul2quat'
 *
 */

#ifndef EUL2QUAT_H
#define EUL2QUAT_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void b_eul2quat(const double eul[3], const char seq[3], double q[4]);

void eul2quat(const double eul[3], double q[4]);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (eul2quat.h) */
