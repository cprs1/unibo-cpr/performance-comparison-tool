/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_inverse_UniboCPR_api.h
 *
 * Code generation for function 'inverse_UniboCPR'
 *
 */

#ifndef _CODER_INVERSE_UNIBOCPR_API_H
#define _CODER_INVERSE_UNIBOCPR_API_H

/* Include files */
#include "emlrt.h"
#include "tmwtypes.h"
#include <string.h>

/* Type Definitions */
#ifndef typedef_struct0_T
#define typedef_struct0_T
typedef struct {
  char_T endanglesconvention[3];
  real_T endangles[12];
  real_T platangles[12];
  real_T basematr[30];
  real_T jointmatr[30];
  real_T beamradius;
  real_T basepoints[12];
  real_T baseangles[12];
  real_T framepoints[12];
  real_T platpoints[12];
  real_T platanglesperm[9];
  real_T endpoints[12];
  real_T L_tube;
  real_T hr;
} struct0_T;
#endif /* typedef_struct0_T */

#ifndef struct_emxArray_char_T_1x3
#define struct_emxArray_char_T_1x3
struct emxArray_char_T_1x3 {
  char_T data[3];
  int32_T size[2];
};
#endif /* struct_emxArray_char_T_1x3 */
#ifndef typedef_emxArray_char_T_1x3
#define typedef_emxArray_char_T_1x3
typedef struct emxArray_char_T_1x3 emxArray_char_T_1x3;
#endif /* typedef_emxArray_char_T_1x3 */

#ifndef typedef_struct1_T
#define typedef_struct1_T
typedef struct {
  real_T fg1;
  real_T fg2;
  real_T Young;
  real_T actlims[2];
  real_T displims[2];
  real_T maxstrain;
  real_T beamradius;
  real_T basepoints[12];
  real_T Ltube;
  real_T g[3];
  real_T tipwrench[6];
  real_T Nf;
  emxArray_char_T_1x3 rotparams;
  real_T pstart[3];
  real_T pstartang[2];
} struct1_T;
#endif /* typedef_struct1_T */

#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T
struct emxArray_real_T {
  real_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};
#endif /* struct_emxArray_real_T */
#ifndef typedef_emxArray_real_T
#define typedef_emxArray_real_T
typedef struct emxArray_real_T emxArray_real_T;
#endif /* typedef_emxArray_real_T */

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void inverse_UniboCPR(real_T guess_data[], int32_T guess_size[1],
                      struct0_T *geometry, struct1_T *params, real_T Kad[144],
                      real_T qpv[6], emxArray_real_T *eq,
                      emxArray_real_T *gradeq);

void inverse_UniboCPR_api(const mxArray *const prhs[5], int32_T nlhs,
                          const mxArray *plhs[2]);

void inverse_UniboCPR_atexit(void);

void inverse_UniboCPR_initialize(void);

void inverse_UniboCPR_terminate(void);

void inverse_UniboCPR_xil_shutdown(void);

void inverse_UniboCPR_xil_terminate(void);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (_coder_inverse_UniboCPR_api.h) */
