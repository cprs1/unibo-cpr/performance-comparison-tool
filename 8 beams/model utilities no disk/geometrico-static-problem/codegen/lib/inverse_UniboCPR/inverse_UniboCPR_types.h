/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * inverse_UniboCPR_types.h
 *
 * Code generation for function 'inverse_UniboCPR'
 *
 */

#ifndef INVERSE_UNIBOCPR_TYPES_H
#define INVERSE_UNIBOCPR_TYPES_H

/* Include files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef typedef_struct0_T
#define typedef_struct0_T
typedef struct {
  char endanglesconvention[3];
  double endangles[12];
  double platangles[12];
  double basematr[30];
  double jointmatr[30];
  double beamradius;
  double basepoints[12];
  double baseangles[12];
  double framepoints[12];
  double platpoints[12];
  double platanglesperm[9];
  double endpoints[12];
  double L_tube;
  double hr;
} struct0_T;
#endif /* typedef_struct0_T */

#ifndef struct_emxArray_char_T_1x3
#define struct_emxArray_char_T_1x3
struct emxArray_char_T_1x3 {
  char data[3];
  int size[2];
};
#endif /* struct_emxArray_char_T_1x3 */
#ifndef typedef_emxArray_char_T_1x3
#define typedef_emxArray_char_T_1x3
typedef struct emxArray_char_T_1x3 emxArray_char_T_1x3;
#endif /* typedef_emxArray_char_T_1x3 */

#ifndef typedef_struct1_T
#define typedef_struct1_T
typedef struct {
  double fg1;
  double fg2;
  double Young;
  double actlims[2];
  double displims[2];
  double maxstrain;
  double beamradius;
  double basepoints[12];
  double Ltube;
  double g[3];
  double tipwrench[6];
  double Nf;
  emxArray_char_T_1x3 rotparams;
  double pstart[3];
  double pstartang[2];
} struct1_T;
#endif /* typedef_struct1_T */

#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T
struct emxArray_real_T {
  double *data;
  int *size;
  int allocatedSize;
  int numDimensions;
  boolean_T canFreeData;
};
#endif /* struct_emxArray_real_T */
#ifndef typedef_emxArray_real_T
#define typedef_emxArray_real_T
typedef struct emxArray_real_T emxArray_real_T;
#endif /* typedef_emxArray_real_T */

#ifndef struct_emxArray_int8_T
#define struct_emxArray_int8_T
struct emxArray_int8_T {
  signed char *data;
  int *size;
  int allocatedSize;
  int numDimensions;
  boolean_T canFreeData;
};
#endif /* struct_emxArray_int8_T */
#ifndef typedef_emxArray_int8_T
#define typedef_emxArray_int8_T
typedef struct emxArray_int8_T emxArray_int8_T;
#endif /* typedef_emxArray_int8_T */

#endif
/* End of code generation (inverse_UniboCPR_types.h) */
