/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * OdefunAssumedForward.h
 *
 * Code generation for function 'OdefunAssumedForward'
 *
 */

#ifndef ODEFUNASSUMEDFORWARD_H
#define ODEFUNASSUMEDFORWARD_H

/* Include files */
#include "inverse_UniboCPR_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void e_binary_expand_op(emxArray_real_T *in1, double in2, const double in3[9],
                        double in4, const double in5[16],
                        const emxArray_real_T *in6, const double in7[12],
                        int in8, const emxArray_real_T *in10, double in11,
                        double in12, const emxArray_real_T *in13,
                        const emxArray_real_T *in14);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (OdefunAssumedForward.h) */
