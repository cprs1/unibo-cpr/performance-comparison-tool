/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * mtimes.c
 *
 * Code generation for function 'mtimes'
 *
 */

/* Include files */
#include "mtimes.h"
#include "inverse_UniboCPR_emxutil.h"
#include "inverse_UniboCPR_types.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */
void b_mtimes(const double A[36], const emxArray_real_T *B, emxArray_real_T *C)
{
  const double *B_data;
  double s;
  double *C_data;
  int aoffset;
  int coffset_tmp;
  int i;
  int j;
  int k;
  int n;
  B_data = B->data;
  n = B->size[1];
  coffset_tmp = C->size[0] * C->size[1];
  C->size[0] = 6;
  C->size[1] = B->size[1];
  emxEnsureCapacity_real_T(C, coffset_tmp);
  C_data = C->data;
  for (j = 0; j < n; j++) {
    coffset_tmp = j * 6;
    for (i = 0; i < 6; i++) {
      aoffset = i * 6;
      s = 0.0;
      for (k = 0; k < 6; k++) {
        s += A[aoffset + k] * B_data[coffset_tmp + k];
      }
      C_data[coffset_tmp + i] = s;
    }
  }
}

void c_mtimes(const emxArray_real_T *A, const emxArray_real_T *B,
              emxArray_real_T *C)
{
  const double *A_data;
  const double *B_data;
  double s;
  double *C_data;
  int boffset;
  int coffset;
  int i;
  int j;
  int k;
  int m;
  int n;
  B_data = B->data;
  A_data = A->data;
  m = A->size[0];
  n = B->size[1];
  coffset = C->size[0] * C->size[1];
  C->size[0] = A->size[0];
  C->size[1] = B->size[1];
  emxEnsureCapacity_real_T(C, coffset);
  C_data = C->data;
  for (j = 0; j < n; j++) {
    coffset = j * m;
    boffset = j * 6;
    for (i = 0; i < m; i++) {
      s = 0.0;
      for (k = 0; k < 6; k++) {
        s += A_data[k * A->size[0] + i] * B_data[boffset + k];
      }
      C_data[coffset + i] = s;
    }
  }
}

void d_mtimes(const emxArray_real_T *A, const double B[30], emxArray_real_T *C)
{
  const double *A_data;
  double s;
  double *C_data;
  int boffset;
  int coffset;
  int i;
  int j;
  int k;
  int m;
  A_data = A->data;
  m = A->size[0];
  coffset = C->size[0] * C->size[1];
  C->size[0] = A->size[0];
  C->size[1] = 5;
  emxEnsureCapacity_real_T(C, coffset);
  C_data = C->data;
  for (j = 0; j < 5; j++) {
    coffset = j * m;
    boffset = j * 6;
    for (i = 0; i < m; i++) {
      s = 0.0;
      for (k = 0; k < 6; k++) {
        s += A_data[k * A->size[0] + i] * B[boffset + k];
      }
      C_data[coffset + i] = s;
    }
  }
}

void e_mtimes(const double A[30], const emxArray_real_T *B, emxArray_real_T *C)
{
  const double *B_data;
  double s;
  double *C_data;
  int aoffset;
  int boffset;
  int coffset;
  int i;
  int j;
  int k;
  int n;
  B_data = B->data;
  n = B->size[1];
  coffset = C->size[0] * C->size[1];
  C->size[0] = 5;
  C->size[1] = B->size[1];
  emxEnsureCapacity_real_T(C, coffset);
  C_data = C->data;
  for (j = 0; j < n; j++) {
    coffset = j * 5;
    boffset = j * B->size[0];
    for (i = 0; i < 5; i++) {
      aoffset = i * 6;
      s = 0.0;
      for (k = 0; k < 6; k++) {
        s += A[aoffset + k] * B_data[boffset + k];
      }
      C_data[coffset + i] = s;
    }
  }
}

void f_mtimes(const double A[36], const emxArray_real_T *B, emxArray_real_T *C)
{
  const double *B_data;
  double s;
  double *C_data;
  int boffset;
  int coffset;
  int i;
  int j;
  int k;
  int n;
  B_data = B->data;
  n = B->size[1];
  coffset = C->size[0] * C->size[1];
  C->size[0] = 6;
  C->size[1] = B->size[1];
  emxEnsureCapacity_real_T(C, coffset);
  C_data = C->data;
  for (j = 0; j < n; j++) {
    coffset = j * 6;
    boffset = j * B->size[0];
    for (i = 0; i < 6; i++) {
      s = 0.0;
      for (k = 0; k < 6; k++) {
        s += A[k * 6 + i] * B_data[boffset + k];
      }
      C_data[coffset + i] = s;
    }
  }
}

void mtimes(const double A[12], const emxArray_real_T *B, emxArray_real_T *C)
{
  const double *B_data;
  double *C_data;
  int boffset;
  int coffset;
  int i;
  int j;
  int n;
  B_data = B->data;
  n = B->size[1];
  coffset = C->size[0] * C->size[1];
  C->size[0] = 3;
  C->size[1] = B->size[1];
  emxEnsureCapacity_real_T(C, coffset);
  C_data = C->data;
  for (j = 0; j < n; j++) {
    coffset = j * 3;
    boffset = j << 2;
    for (i = 0; i < 3; i++) {
      C_data[coffset + i] =
          ((A[i] * B_data[boffset] + A[i + 3] * B_data[boffset + 1]) +
           A[i + 6] * B_data[boffset + 2]) +
          A[i + 9] * B_data[boffset + 3];
    }
  }
}

/* End of code generation (mtimes.c) */
