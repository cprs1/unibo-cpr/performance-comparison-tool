/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * mtimes.h
 *
 * Code generation for function 'mtimes'
 *
 */

#ifndef MTIMES_H
#define MTIMES_H

/* Include files */
#include "inverse_UniboCPR_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void b_mtimes(const double A[36], const emxArray_real_T *B, emxArray_real_T *C);

void c_mtimes(const emxArray_real_T *A, const emxArray_real_T *B,
              emxArray_real_T *C);

void d_mtimes(const emxArray_real_T *A, const double B[30], emxArray_real_T *C);

void e_mtimes(const double A[30], const emxArray_real_T *B, emxArray_real_T *C);

void f_mtimes(const double A[36], const emxArray_real_T *B, emxArray_real_T *C);

void mtimes(const double A[12], const emxArray_real_T *B, emxArray_real_T *C);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (mtimes.h) */
