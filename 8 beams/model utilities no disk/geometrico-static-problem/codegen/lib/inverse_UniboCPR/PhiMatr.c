/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * PhiMatr.c
 *
 * Code generation for function 'PhiMatr'
 *
 */

/* Include files */
#include "PhiMatr.h"
#include "inverse_UniboCPR_emxutil.h"
#include "inverse_UniboCPR_rtwutil.h"
#include "inverse_UniboCPR_types.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */
void PhiMatr(double x, double Nf, emxArray_real_T *M)
{
  double vect[6];
  double b_vect_tmp;
  double c_vect_tmp;
  double vect_tmp;
  double *M_data;
  int b_loop_ub;
  int i;
  int loop_ub;
  vect[0] = 1.0;
  vect[1] = 2.0 * x - 1.0;
  vect_tmp = x * x;
  vect[2] = (6.0 * vect_tmp - 6.0 * x) + 1.0;
  b_vect_tmp = rt_powd_snf(x, 3.0);
  vect[3] = ((20.0 * b_vect_tmp - 30.0 * vect_tmp) + 12.0 * x) - 1.0;
  c_vect_tmp = rt_powd_snf(x, 4.0);
  vect[4] = (((70.0 * c_vect_tmp - 140.0 * b_vect_tmp) + 90.0 * vect_tmp) -
             20.0 * x) +
            1.0;
  vect[5] = ((((252.0 * rt_powd_snf(x, 5.0) - 630.0 * c_vect_tmp) +
               560.0 * b_vect_tmp) -
              210.0 * vect_tmp) +
             30.0 * x) -
            1.0;
  /*  vect =   [ones(1,numel(s)) ; */
  /*           s ; */
  /*           s.^2 ;  */
  /*           s.^3; */
  /*           s.^4; */
  /*           s.^5; */
  /*           ]; */
  if (Nf < 1.0) {
    loop_ub = 0;
  } else {
    loop_ub = (int)Nf;
  }
  i = M->size[0] * M->size[1];
  M->size[0] = 3;
  M->size[1] = (loop_ub + (int)Nf) + (int)Nf;
  emxEnsureCapacity_real_T(M, i);
  M_data = M->data;
  for (i = 0; i < loop_ub; i++) {
    M_data[3 * i] = vect[i];
  }
  b_loop_ub = (int)Nf;
  for (i = 0; i < b_loop_ub; i++) {
    M_data[3 * (i + loop_ub)] = 0.0;
  }
  b_loop_ub = (int)Nf;
  for (i = 0; i < b_loop_ub; i++) {
    M_data[3 * ((i + loop_ub) + (int)Nf)] = 0.0;
  }
  b_loop_ub = (int)Nf;
  for (i = 0; i < b_loop_ub; i++) {
    M_data[3 * i + 1] = 0.0;
  }
  for (i = 0; i < loop_ub; i++) {
    M_data[3 * (i + (int)Nf) + 1] = vect[i];
  }
  b_loop_ub = (int)Nf;
  for (i = 0; i < b_loop_ub; i++) {
    M_data[3 * ((i + (int)Nf) + loop_ub) + 1] = 0.0;
  }
  b_loop_ub = (int)Nf;
  for (i = 0; i < b_loop_ub; i++) {
    M_data[3 * i + 2] = 0.0;
  }
  b_loop_ub = (int)Nf;
  for (i = 0; i < b_loop_ub; i++) {
    M_data[3 * (i + (int)Nf) + 2] = 0.0;
  }
  for (i = 0; i < loop_ub; i++) {
    M_data[3 * ((i + (int)Nf) + (int)Nf) + 2] = vect[i];
  }
}

/* End of code generation (PhiMatr.c) */
