function [eq,gradeq] = forward_UniboCPR(guess,geometry,params,Kad,qav)
Nf = params.Nf;
Nftot = 3*Nf;
rotparams = params.rotparams;
Ci = geometry.jointmatr;
[~,nj] = size(Ci);
hr = geometry.hr;
platangles = geometry.platangles;
platpoints = geometry.platpoints;
endpoints = geometry.endpoints;
endangles = geometry.endangles;
g = params.g;
wd = [zeros(3,1);g*params.fg2];

% extract variables
[qa,qe,qp,lambda] = unstack(guess,Nftot,nj);

pbase = [qa(1);qa(2);hr];
Lv = qa(3:6,1);
pplat = qp(1:3,1);
[Rp,dRpda,dRpdb,dRpdc] = rotationParametrization(qp(4:6,1),rotparams);

% initialize
beameq = zeros(4*Nftot,1);
dbeameqdqa = zeros(4*Nftot,6);
dbeameqdqe = zeros(4*Nftot,4*Nftot);
dbeameqdlambda = zeros(4*Nftot,4*nj);
wrencheq = params.tipwrench;
dwrenchdqa = zeros(6,6);
dwrenchdqe = zeros(6,4*Nftot);
dwrenchdqp = zeros(6,6);
dwrenchdlambda = zeros(6,4*nj);
constr = zeros(4*nj,1);
dconstrdqa = zeros(4*nj,6);
dconstrdqe = zeros(4*nj,4*Nftot);
dconstrdqp = zeros(4*nj,6);

for i = 1:4
    % extract variables of the th-i beam
    qei = qe(1+Nftot*(i-1):Nftot*i,1);
    lambdai = lambda(1+nj*(i-1):nj*i,1);
    Li = Lv(i);
    p1 = pbase + platpoints(:,i);
    h1 = eul2quat(platangles(:,i)','XYZ')';
    twrench = (Ci*lambdai);
    
    % forward recursion
    [p2,h2,dp2dLvi,dh2dLvi,dp2dqei,dh2dqei]  = forwardRecursion(p1,h1,qei,Li,Nf);
    dp2dqei = reshape(dp2dqei,3,Nftot);
    dh2dqei = reshape(dh2dqei,4,Nftot);
    R2 = quat2rotmatrix(h2);
    % backward recursion
    [~,Qc,~,dQcdLvi,~,dQcdqei,~,dQcdw0i] = backwardRecursion(twrench,qei,Nf,Li,wd);
    dQcdqei = reshape(dQcdqei,Nftot,Nftot);
    dQcdw0i = reshape(dQcdw0i,Nftot,6);
    %% BEAM EQUILIBRIUMS
    beameq(1+Nftot*(i-1):Nftot*i,1) = Li*Kad*qei + Qc;
    dbeameqdqa(1+Nftot*(i-1):Nftot*i,2+i) = Kad*qei + dQcdLvi;
    dbeameqdqe(1+Nftot*(i-1):Nftot*i,1+Nftot*(i-1):Nftot*i) = Li*Kad+dQcdqei;
    dbeameqdlambda(1+Nftot*(i-1):Nftot*i,1+nj*(i-1):nj*i) = dQcdw0i*Ci;

    %% CONSTRAINTS
    pp = Rp*endpoints(:,i);
    dppdrot = [dRpda*endpoints(:,i),dRpdb*endpoints(:,i),dRpdc*endpoints(:,i)];
    platconstr = p2-(pplat +pp);
    dplatconstrdLvi = dp2dLvi;
    dplatconstrdpbase = [eye(2);zeros(1,2)];
    dplatconstrdqei = dp2dqei;
    dplatconstrdqp = -[eye(3),dppdrot];
    hJi = eul2quat(endangles(:,i)',geometry.endanglesconvention)';
    RJi = Rp*quat2rotmatrix(hJi);
    dRJida = dRpda*quat2rotmatrix(hJi);
    dRJidb = dRpdb*quat2rotmatrix(hJi);
    dRJidc = dRpdc*quat2rotmatrix(hJi);

    [platangerr,dplatangerrdLvi,dplatangerrdqei,m2] = roterr(RJi,R2,h2,dh2dLvi,dh2dqei,dRJida,dRJidb,dRJidc);
    dplatangerrdqp =  [zeros(3,3),m2];
    constr(1+nj*(i-1):nj*i,1) = Ci'*[platangerr;platconstr;];
    dconstrdqa(1+nj*(i-1):nj*i,1:2) = Ci'*[zeros(3,2);dplatconstrdpbase];
    dconstrdqa(1+nj*(i-1):nj*i,2+i) = Ci'*[dplatangerrdLvi;dplatconstrdLvi;];
    dconstrdqe(1+nj*(i-1):nj*i,1+Nftot*(i-1):Nftot*i) =  Ci'*[dplatangerrdqei;dplatconstrdqei;];
    dconstrdqp(1+nj*(i-1):nj*i,:) = Ci'*[dplatangerrdqp;dplatconstrdqp;];

    %% PLATFORM EQUILIBRIUM
     
    Adg = [R2,zeros(3); zeros(3), R2]; 
    gwrench = -Adg*(twrench); 
    
    [D1,D2,D3] = derivativeColRotMatQuat(h2);
    dgwrenchdLvi = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dh2dLvi;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dh2dLvi];
    dgwrenchdqei = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dh2dqei;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dh2dqei];
    Adg2 = [eye(3),skew(pp); zeros(3),eye(3)]; 
    m2 = [zeros(3,3),skew(dppdrot(:,1))*gwrench(4:6),skew(dppdrot(:,2))*gwrench(4:6),skew(dppdrot(:,3))*gwrench(4:6)];

    wrencheq = wrencheq + Adg2*gwrench; 
    dwrenchdqa(:,2+i) = Adg2*dgwrenchdLvi;
    dwrenchdqe(:,1+Nftot*(i-1):Nftot*i) = Adg2*dgwrenchdqei;
    dwrenchdqp = dwrenchdqp + [m2;zeros(3,6)] ;
    dwrenchdlambda(:,1+nj*(i-1):nj*i) = -Adg2*Adg*Ci;

end

%% Forward problem
forweq = qa-qav;

%% COLLECT
eq = [beameq;wrencheq;constr;forweq];

gradeq = [dbeameqdqa, dbeameqdqe, zeros(4*Nftot,6), dbeameqdlambda;
          dwrenchdqa, dwrenchdqe, dwrenchdqp, dwrenchdlambda;
          dconstrdqa, dconstrdqe, dconstrdqp, zeros(4*nj,4*nj);
          eye(6), zeros(6,4*Nftot), zeros(6,6), zeros(6,4*nj);];

end