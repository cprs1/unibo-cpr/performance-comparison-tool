function h = plotUniboCPR_8(guess,guesst,geometry,Nf,Nsh,hr)
Nftot = 3*Nf;
h = [];
qa = guess(1:6,1);
qe = guess(1+6:6+8*Nftot,1);
xp = qa(1);
yp = qa(2);
pbase  = [xp;yp;hr];
Lv = [qa(3);qa(3);qa(4);qa(4);qa(5);qa(5);qa(6);qa(6)];

Lt = geometry.L_tube;

framepoints = geometry.framepoints;
basepoints = geometry.basepoints;
baseangles = geometry.baseangles;
platangles = geometry.platangles;
platpoints = geometry.platpoints;

% figure()
plot3(xp,yp,hr,'ro','LineWidth',2)
hold on

pend = zeros(3,8);
for i = 1:8
    qei = qe(1+Nftot*(i-1):Nftot*i,1);
    L = Lv(i);
    
    %% FORWARD RECURSION
    p1L = pbase + platpoints(:,i);
    h1L = eul2quat(platangles(:,i)','XYZ')';
    % free beam
    y2F = [p1L;h1L;zeros(3+3*3*Nf,1);zeros(4,1);zeros(4*3*Nf,1)];
    fun = @(s,y) OdefunAssumedForward(s,y,qei,Nf,L,'variable');
    [s1,y] = ode45(fun,[0,1],y2F);

    pos1 = y(:,1:3);
    sspan1 = linspace(0,L,Nsh);
    posbeam1 = spline(L*s1,pos1',sspan1);
    h1 = plot3(posbeam1(1,:),posbeam1(2,:),posbeam1(3,:),'b-','LineWidth',2);
    h2 = plot3(posbeam1(1,end),posbeam1(2,end),posbeam1(3,end),'rs','LineWidth',2);
    h3 = plot3(posbeam1(1,1),posbeam1(2,1),posbeam1(3,1),'bs','LineWidth',2);
    h = [h;h1;h2;h3];
    pend(:,i) = posbeam1(:,end);
end

%% BASE
h1 = line([pend(1,1),pend(1,3)],[pend(2,1),pend(2,3)],[pend(3,1),pend(3,3)],'Color','red','LineWidth',2);
h2 = line([pend(1,3),pend(1,5)],[pend(2,3),pend(2,5)],[pend(3,3),pend(3,5)],'Color','red','LineWidth',2);
h3 = line([pend(1,5),pend(1,7)],[pend(2,5),pend(2,7)],[pend(3,5),pend(3,7)],'Color','red','LineWidth',2);
h4 = line([pend(1,7),pend(1,1)],[pend(2,7),pend(2,1)],[pend(3,7),pend(3,1)],'Color','red','LineWidth',2);

h = [h;h1;h2;h3;h4];

%% PLATFORM
ppoints = pbase + platpoints;
h1 = line([ppoints(1,1),ppoints(1,3)],[ppoints(2,1),ppoints(2,3)],[ppoints(3,1),ppoints(3,3)],'Color','black','LineWidth',2);
h2 = line([ppoints(1,3),ppoints(1,5)],[ppoints(2,3),ppoints(2,5)],[ppoints(3,3),ppoints(3,5)],'Color','black','LineWidth',2);
h3 = line([ppoints(1,5),ppoints(1,7)],[ppoints(2,5),ppoints(2,7)],[ppoints(3,5),ppoints(3,7)],'Color','black','LineWidth',2);
h4 = line([ppoints(1,7),ppoints(1,1)],[ppoints(2,7),ppoints(2,1)],[ppoints(3,7),ppoints(3,1)],'Color','black','LineWidth',2);

h = [h;h1;h2;h3;h4];

%% FRAME

h1 = line([framepoints(1,1),framepoints(1,2)],[framepoints(2,1),framepoints(2,2)],[framepoints(3,1),framepoints(3,2)],'Color','black','LineWidth',2);
h2 = line([framepoints(1,2),framepoints(1,3)],[framepoints(2,2),framepoints(2,3)],[framepoints(3,2),framepoints(3,3)],'Color','black','LineWidth',2);
h3 = line([framepoints(1,3),framepoints(1,4)],[framepoints(2,3),framepoints(2,4)],[framepoints(3,3),framepoints(3,4)],'Color','black','LineWidth',2);
h4 = line([framepoints(1,4),framepoints(1,1)],[framepoints(2,4),framepoints(2,1)],[framepoints(3,4),framepoints(3,1)],'Color','black','LineWidth',2);
h5 = line([framepoints(1,1),framepoints(1,1)],[framepoints(2,1),framepoints(2,1)],[0,framepoints(3,1)],'Color','black','LineWidth',2);
h6 = line([framepoints(1,2),framepoints(1,2)],[framepoints(2,2),framepoints(2,2)],[0,framepoints(3,2)],'Color','black','LineWidth',2);
h7 = line([framepoints(1,3),framepoints(1,3)],[framepoints(2,3),framepoints(2,3)],[0,framepoints(3,3)],'Color','black','LineWidth',2);
h8 = line([framepoints(1,4),framepoints(1,4)],[framepoints(2,4),framepoints(2,4)],[0,framepoints(3,4)],'Color','black','LineWidth',2);

h = [h;h1;h2;h3;h4;h5;h6;h7;h8];


%% TUBES
qet = guesst(1+2:2+4*Nftot,1);
for j = 1:4
    qei = qet(1+Nftot*(j-1):Nftot*j,1);
    
    %% FORWARD RECURSION
    p1L = basepoints(:,j);
    h1L = eul2quat(baseangles(:,j)','XYZ')';
    % SECOND PART: free beam
    y2F = [p1L;h1L;zeros(3+3*3*Nf,1);zeros(4,1);zeros(4*3*Nf,1)];
    fun = @(s,y) OdefunAssumedForward(s,y,qei,Nf,Lt,'variable');
    [s1,y] = ode45(fun,[0,1],y2F);

    pos1 = y(:,1:3);
    sspan1 = linspace(0,Lt,Nsh);
    posbeam1 = spline(Lt*s1,pos1',sspan1);
    h1 = plot3(posbeam1(1,:),posbeam1(2,:),posbeam1(3,:),'m-','LineWidth',2);
%     h2 = plot3(posbeam1(1,end),posbeam1(2,end),posbeam1(3,end),'rs','LineWidth',2);
%     h3 = plot3(posbeam1(1,1),posbeam1(2,1),posbeam1(3,1),'bs','LineWidth',2);
    h = [h;h1];

end

%%
axis equal
grid minor
axis([-.3 .3 -.3 .3 0 .8])

end