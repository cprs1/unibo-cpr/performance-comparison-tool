function output = mechanicsUniboPrototype(guess,guesst,params,geometry)
% initialize:
Fz = zeros(4,1);

% recover wrenches at the XY platform
% extract parameters
Nf = params.Nf;
Nftot = 3*params.Nf;
Ci = geometry.jointmatr;
[~,nj] = size(Ci);
hr = geometry.hr;
platangles = geometry.platangles;
platpoints = geometry.platpoints;
Ct = geometry.basematr;
[~,ntj] = size(Ct);
basepoints = geometry.basepoints;
baseangles = geometry.baseangles;

% extract variables
qet = guesst(1+2:2+4*Nftot,1);
lambdat = guesst(1+2+4*Nftot:2+4*Nftot+4*ntj,1);

L = geometry.L_tube;
g = params.g;
wd = [zeros(3,1);g*params.fg2];

% extract variables
[qa,qe,qp,lambda] = unstack(guess,Nftot,nj);
[Rp,~,~,~] = rotationParametrization(qp(4:6,1),params.rotparams);

pbase = [qa(1);qa(2);hr];
Lv = qa(3:6,1);

robotwrenches = zeros(6,4);
tubewrenches = zeros(6,4);
basewrenches = zeros(6,4);
platR = zeros(4,1);
platM = zeros(4,1);
baseR = zeros(4,1);
baseM = zeros(4,1);
equivalentwrench = zeros(6,1);
strbeam = zeros(4,1);
strtube = zeros(4,1);

for i = 1:4
%% LEGS.
    % extract variables of the th-i beam
    qei = qe(1+Nftot*(i-1):Nftot*i,1);
    lambdai = lambda(1+nj*(i-1):nj*i,1);
    Li = Lv(i);
    p1 = pbase + platpoints(:,i);
    h1 = eul2quat(platangles(:,i)','XYZ')';
    wt = (Ci*lambdai);
    
    % forward recursion
    [~,h2,~,~,~,~]  = forwardRecursion(p1,h1,qei,Li,Nf);
    Rt = quat2rotmatrix(h2);
    % backward recursion
    [wb,~,~,~,~,~,~,~] = backwardRecursion(wt,qei,Nf,Li,wd);
       
    Fz(i,1) = wb(6);
    robotwrenches(:,i) = wb; % wrench at the base joint in RIGID XY FRAME
    
    Adgp = [Rp'*Rt',zeros(3);zeros(3),Rp'*Rt'];
    wt_p = Adgp*wt;
    platR(i,1) = norm(wt_p(4:5)); % magnitude of in-plane joint reaction PLATFORM FRAME
    platM(i,1) = norm(wt_p(1:2)); % magnitude of joint bending moment PLATFORM FRAME
    % strains
    strbeam(i,1)= getMaxStrain(qei,Nf,geometry.beamradius);

%% TUBES
% extract variables of the th-i tube
    qeti = qet(1+Nftot*(i-1):Nftot*i,1);
    lambdati = lambdat(1+ntj*(i-1):ntj*i,1);
    p1 = basepoints(:,i);
    h1 = eul2quat(-baseangles(:,i)','XYZ')';
    wl = (Ct*lambdati);
    
    % forward recursion
    [~,h2,~,~,~,~]  = forwardRecursion(p1,h1,qeti,L,Nf);
    Rl = quat2rotmatrix(h2);
       
    Adgl = [Rl,zeros(3);Rl,zeros(3)];
    wl_0 = Adgl*wl;
    tubewrenches(:,i) = wl_0; % wrench at the base joint in GLOBAL FRAME

    basewrenches(:,i) = robotwrenches(:,i) + tubewrenches(:,i);
    baseR(i,1) = norm(basewrenches(4:5,i)); % magnitude of in-plane joint reaction BASE FRAME
    baseM(i,1) = norm(basewrenches(1:2,i)); % magnitude of joint bending moment BASE FRAME

    pe = platpoints(:,i);
    Adge = [eye(3), skew(pe); zeros(3), eye(3)];
    equivalentwrench = equivalentwrench + Adge*basewrenches(:,i);

    % strains
    strtube(i,1) = getMaxStrain(qeti,Nf,geometry.beamradius);
end

%% LOAD ON RAILS
Lframe = abs(basepoints(1,2)-basepoints(1,1));
bx = abs(qa(1)) + Lframe/2;
by = abs(qa(2)) + Lframe/2;
Mfxd = equivalentwrench(1);
Mfyd = equivalentwrench(2);
Fload = equivalentwrench(6);
Mx = abs(Fload)*bx + Mfxd;
My = abs(Fload)*by + Mfyd;
FrameMoment = max(Mx,My);


%% COLLECT
output.Fz = Fz;
output.Fa = -equivalentwrench(1:2);
output.maxBeamStrain = max(strbeam); 
output.maxTubeStrain = max(strtube); 
output.maxPlatR = max(platR); 
output.maxPlatM = max(platM); 
output.maxBaseR = max(baseR); 
output.maxBaseM = max(baseM);
output.maxFrameMoment = FrameMoment;


end