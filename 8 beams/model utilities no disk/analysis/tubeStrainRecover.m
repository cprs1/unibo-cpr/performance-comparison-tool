function [dmin,dmax] = tubeStrainRecover(fcn,params)

tubefcn = fcn.tubesfcn;
Lt = params.Ltube;
Nf = params.Nf;
d0 = Lt/1.8;

qa0 = [1;0]*Lt/2;
qt0 = [5;zeros(3*Nf-1,1)];
lambdat0 = zeros(5,1);
y00 = [qa0;qt0;lambdat0];
options = optimoptions('fsolve','display','off','SpecifyObjectiveGradient',true,'CheckGradients',false,'MaxIterations',50);

%% step 1
j = 1;
flag=1;
flags=0;
step = 0.01;
maxstrain = 0;
jmax = floor(Lt/step);
pb =  [d0;0];
y0 = y00;
while j<jmax && (flag==1 || flag ==3) && pb(1)>=0 && flags==0 && maxstrain<=params.maxstrain
    fun = @(y) tubefcn(y,pb);
    [y,~,flag,~,~] = fsolve(fun,y0,options);
    maxstrain = getMaxStrain(y(1+2:2+3*Nf,1),Nf,params.beamradius);
    % update
    j = j+1;
    dmin = pb(1);
    pb = pb - [step;0];
    y0 = y;
end

%% step 2
j = 1;
flag=1;
flags=0;
step = 0.01;
maxstrain = 0;
jmax = floor(Lt/step);
pb =  [d0;0];
y0 = y00;
while j<jmax && (flag==1 || flag ==3) && pb(1)>=0 && flags==0 && maxstrain<=params.maxstrain
    fun = @(y) tubefcn(y,pb);
    [y,~,flag,~,~] = fsolve(fun,y0,options);
    maxstrain = getMaxStrain(y(1+2:2+3*Nf,1),Nf,params.beamradius);
    % update
    j = j+1;
    dmax = pb(1);
    pb = pb + [step;0];
    y0 = y;
end
end