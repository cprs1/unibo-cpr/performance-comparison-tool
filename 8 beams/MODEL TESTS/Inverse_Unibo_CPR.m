%% Unibo CPR Structure - inverse problem
% Assumed strain mode approach, forward-backward formulation
% Federico Zaccaria 19 Jan 2022

clear
close all
clc
%% PARAMETERS

[geometry,params,Kbt_1,Kbt_2] = createRobotParams();
params.g = [0;0;0]; % gravity
params.tipwrench = [0;0;0;0;0;-30]; % tip wrench
[~,nj] = size(geometry.jointmatr);  

%% INVERSE PROBLEM
xP = 0; yP = 0; zP = 0.1; alpha = 0*pi/180; beta = 0*pi/180; gamma = 0*pi/180;
qpv = [xP;yP;zP;alpha;beta;gamma];

%% MODEL PARAMETERS
Nf = 4;
params.Nf = Nf;
M = @(s) PhiMatr(s,1,Nf);
Kad1 = integral(@(s) M(s)'*Kbt_1*M(s),0,1,'ArrayValued',true);
Kad2 = integral(@(s) M(s)'*Kbt_2*M(s),0,1,'ArrayValued',true);
params.rotparams = 'XYZ';
%% Solution
qa0 = [0;0;0.4;0.4;0.4;0.4];
% qe0 = repmat([0;zeros(3*Nf-1,1)],8,1);
qe0 = 0.0*rand(8*3*Nf,1);
qp0 = qpv;
lambda0 = rand(8*nj,1);
guess0 = [qa0;qe0;qp0;lambda0];

% load guess0;

options = optimoptions('fsolve','display','iter-detailed','Algorithm','levenberg-marquardt','SpecifyObjectiveGradient',true,'CheckGradients',false,'MaxIterations',5000);
fun = @(guess) inverse_UniboCPR_8(guess,geometry,params,Kad2,qpv);
[sol,~,~,~,jac] = fsolve(fun,guess0,options);
jac = full(jac);

%% TUBE POSITION COMPUTATION
qa0 = sol(1:2);
qt0 = repmat([5.2;zeros(3*Nf-1,1)],4,1);
lambdat0 = zeros(4*5,1);
guesst0 = [qa0;qt0;lambdat0];

funt = @(guess) tubes_UniboCPR(guess,geometry,params,Kad1,sol(1:2));

options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',false,'MaxIterations',5000);

[solt,~,~,~,jact] = fsolve(funt,guesst0,options);

%% PLOT & STRAIN RECOVERY
h = plotUniboCPR_8(sol,solt,geometry,Nf,100,geometry.hr);

%% SINGULARITY AND EQUILIBRIUM STABILITY ANALYSIS

[flag1,flag2] = SingularitiesUniboPrototype(sol,jac,geometry,params);
flag = StabilityUniboPrototype(sol,jac,Nf,nj,params)
