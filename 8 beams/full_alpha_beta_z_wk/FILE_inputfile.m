%% FLOODING ALGORITHM - Spatial CASE
% F.Zaccaria 02 February 2022
% extension to the spatial case of the algorithm provided by "Singularity
% Conditions for Continuum Parallel Robot"

%% INPUT FILE

maxiter = 50;
TOL = 1e-6;
TOL2 = Inf;
% TOL2 = 1e5; 
stepsizeZ = 0.05; %Z
stepsizeA = 2*pi/180; %alpha
stepsizeB = 2*pi/180; %beta
boxsize = [0.09 0.11  -pi,pi, 0,pi];



%% STORE VARIABLES

params.stepsizeZ = stepsizeZ;
params.stepsizeA = stepsizeA;
params.stepsizeB = stepsizeB;
params.maxiter = maxiter;
params.boxsize = boxsize;
params.TOL = TOL;
params.TOL2 = TOL2;
params.solver = 'fsolve';

