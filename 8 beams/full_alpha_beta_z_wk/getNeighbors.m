%% FLOODING ALGORITHM - Spatial CASE
% F.Zaccaria 02 February 2022
% extension to the spatial case of the algorithm provided by "Singularity
% Conditions for Continuum Parallel Robot"

function id2 = getNeighbors(WK,pstart,params,fact)
stepsizeZ = params.stepsizeZ;
stepsizeA = params.stepsizeA;
stepsizeB = params.stepsizeB;

id = ((abs(WK(:,2)-pstart(1))<=fact*stepsizeZ) & (abs(WK(:,3)-pstart(2))<=fact*stepsizeA) & (abs(WK(:,4)-pstart(3))<=fact*stepsizeB));
id2 = WK(id==1,1);
end