clear
close all
clc

load WK_08
load guesses_08


%% ROBOT PARAMETERS
FILE_UniboCPR_robotfile_abz

%% SIMULATION PARAMETERS
FILE_inputfile_modif

params.pstart = [xP;yP;zP];
instantplot = 1;
%% RE-iterate in xyz

WKout = FCN_FloodingSpatialModifWK(fcn,params,instantplot,WK,guesses);

%% PLOT
meanindices = FCN_plotIndices(WKout,geometry,params);

meanindices.volume/0.02
meanindices.orientA*4
meanindices.orientX*180/pi