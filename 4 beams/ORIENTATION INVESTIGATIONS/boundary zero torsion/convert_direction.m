%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to convert exploration direction from unitary to physical.
% each component is reported in the physical domain and then approximated
% to the closer point according to the stepsize.

% INPUT:
% dir_it: direction in unitary space
% params: structure with simulation parameters

% OUTPUT:
% dir_new = direction in the physical space

function dir_new = convert_direction(dir_it,params)
boxsize = params.boxsize;
stepsize_x = params.stepsize_x;
stepsize_y = params.stepsize_y;
stepsize_z = params.stepsize_z;

dir_new = [dir_it(1)*(boxsize(2)-boxsize(1))*stepsize_x;
           dir_it(2)*(boxsize(4)-boxsize(3))*stepsize_y;
           dir_it(3)*(boxsize(6)-boxsize(5))*stepsize_z;];

if abs(dir_new(1))>stepsize_x
    dir_new = stepsize_x*dir_new/abs(dir_new(1));
end

if abs(dir_new(2))>stepsize_x
    dir_new = stepsize_x*dir_new/abs(dir_new(2));
    
end

if abs(dir_new(3))>stepsize_z
    dir_new = stepsize_z*dir_new/abs(dir_new(3));
    
end

end