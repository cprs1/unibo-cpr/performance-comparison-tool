/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * backwardRecursion.h
 *
 * Code generation for function 'backwardRecursion'
 *
 */

#ifndef BACKWARDRECURSION_H
#define BACKWARDRECURSION_H

/* Include files */
#include "inverse_UniboCPR_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void backwardRecursion(const double twrench[6], const double qe_data[],
                       double Nf, double L, double wbase[6],
                       emxArray_real_T *Qc, emxArray_real_T *dwbasedqa,
                       emxArray_real_T *dQcdqa, emxArray_real_T *dwbasedqe,
                       emxArray_real_T *dQcdqe, emxArray_real_T *dwbasedw0,
                       emxArray_real_T *dQcdw0);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (backwardRecursion.h) */
