/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * inverse_UniboCPR.c
 *
 * Code generation for function 'inverse_UniboCPR'
 *
 */

/* Include files */
#include "inverse_UniboCPR.h"
#include "backwardRecursion.h"
#include "derivativeColRotMatQuat.h"
#include "eul2quat.h"
#include "eye.h"
#include "forwardRecursion.h"
#include "inverse_UniboCPR_data.h"
#include "inverse_UniboCPR_emxutil.h"
#include "inverse_UniboCPR_initialize.h"
#include "inverse_UniboCPR_types.h"
#include "mtimes.h"
#include "rotationParametrization.h"
#include "roterr.h"
#include "rt_nonfinite.h"
#include "unstack.h"
#include <string.h>

/* Function Declarations */
static void b_binary_expand_op(emxArray_real_T *in1, int in2, int in3, int in4,
                               const double in5[144], const double in6_data[],
                               int in7, const emxArray_real_T *in9);

static void binary_expand_op(emxArray_real_T *in1, int in2, int in3, int in4,
                             int in5, const double in6[6], int in7,
                             const double in8[144], const emxArray_real_T *in9,
                             double in10);

static void c_binary_expand_op(emxArray_real_T *in1, int in2, int in3,
                               const double in4[6], int in5,
                               const double in6[144], const double in7_data[],
                               int in8, const emxArray_real_T *in10);

static void d_binary_expand_op(emxArray_real_T *in1, const emxArray_real_T *in2,
                               const double in3[6], const double in4[20],
                               const double in5_data[], const int *in5_size,
                               const double in6[6]);

/* Function Definitions */
static void b_binary_expand_op(emxArray_real_T *in1, int in2, int in3, int in4,
                               const double in5[144], const double in6_data[],
                               int in7, const emxArray_real_T *in9)
{
  double b_in5[12];
  const double *in9_data;
  double d;
  double *in1_data;
  int i;
  int loop_ub;
  int stride_0_0;
  in9_data = in9->data;
  in1_data = in1->data;
  for (i = 0; i < 12; i++) {
    d = 0.0;
    for (stride_0_0 = 0; stride_0_0 < 12; stride_0_0++) {
      d += in5[i + 12 * stride_0_0] * in6_data[in7 + stride_0_0];
    }
    b_in5[i] = d;
  }
  stride_0_0 = (in9->size[0] != 1);
  loop_ub = in3 - in2;
  for (i = 0; i < loop_ub; i++) {
    in1_data[(in2 + i) + in1->size[0] * (in4 + 2)] =
        b_in5[i] + in9_data[i * stride_0_0];
  }
}

static void binary_expand_op(emxArray_real_T *in1, int in2, int in3, int in4,
                             int in5, const double in6[6], int in7,
                             const double in8[144], const emxArray_real_T *in9,
                             double in10)
{
  double c_in6[144];
  const double *in9_data;
  double b_in6;
  double *in1_data;
  int aux_0_1;
  int i;
  int i1;
  int in6_tmp;
  int stride_0_0;
  int stride_0_1;
  in9_data = in9->data;
  in1_data = in1->data;
  b_in6 = in6[in7 + 2];
  stride_0_0 = ((int)in10 != 1);
  stride_0_1 = ((int)in10 != 1);
  aux_0_1 = 0;
  for (i = 0; i < 12; i++) {
    for (i1 = 0; i1 < 12; i1++) {
      in6_tmp = i1 + 12 * i;
      c_in6[in6_tmp] = b_in6 * in8[in6_tmp] +
                       in9_data[i1 * stride_0_0 + (int)in10 * aux_0_1];
    }
    aux_0_1 += stride_0_1;
  }
  stride_0_0 = in3 - in2;
  stride_0_1 = in5 - in4;
  for (i = 0; i < stride_0_1; i++) {
    for (i1 = 0; i1 < stride_0_0; i1++) {
      in1_data[(in2 + i1) + in1->size[0] * (in4 + i)] =
          c_in6[i1 + stride_0_0 * i];
    }
  }
}

static void c_binary_expand_op(emxArray_real_T *in1, int in2, int in3,
                               const double in4[6], int in5,
                               const double in6[144], const double in7_data[],
                               int in8, const emxArray_real_T *in10)
{
  double c_in4[12];
  const double *in10_data;
  double b_in4;
  double d;
  double *in1_data;
  int i;
  int loop_ub;
  int stride_0_0;
  in10_data = in10->data;
  in1_data = in1->data;
  b_in4 = in4[in5 + 2];
  for (i = 0; i < 12; i++) {
    d = 0.0;
    for (stride_0_0 = 0; stride_0_0 < 12; stride_0_0++) {
      d += b_in4 * in6[i + 12 * stride_0_0] * in7_data[in8 + stride_0_0];
    }
    c_in4[i] = d;
  }
  stride_0_0 = (in10->size[0] != 1);
  loop_ub = in3 - in2;
  for (i = 0; i < loop_ub; i++) {
    in1_data[in2 + i] = c_in4[i] + in10_data[i * stride_0_0];
  }
}

static void d_binary_expand_op(emxArray_real_T *in1, const emxArray_real_T *in2,
                               const double in3[6], const double in4[20],
                               const double in5_data[], const int *in5_size,
                               const double in6[6])
{
  double in5[6];
  const double *in2_data;
  double *in1_data;
  int i;
  int stride_0_0;
  in2_data = in2->data;
  stride_0_0 = (*in5_size != 1);
  for (i = 0; i < 6; i++) {
    in5[i] = in5_data[i * stride_0_0] - in6[i];
  }
  i = in1->size[0];
  in1->size[0] = in2->size[0] + 32;
  emxEnsureCapacity_real_T(in1, i);
  in1_data = in1->data;
  stride_0_0 = in2->size[0];
  for (i = 0; i < stride_0_0; i++) {
    in1_data[i] = in2_data[i];
  }
  for (i = 0; i < 6; i++) {
    in1_data[i + in2->size[0]] = in3[i];
  }
  for (i = 0; i < 20; i++) {
    in1_data[(i + in2->size[0]) + 6] = in4[i];
  }
  for (i = 0; i < 6; i++) {
    in1_data[(i + in2->size[0]) + 26] = in5[i];
  }
}

void inverse_UniboCPR(const double guess_data[], const int guess_size[1],
                      const struct0_T *geometry, const struct1_T *params,
                      const double Kad[144], const double qpv[6],
                      emxArray_real_T *eq, emxArray_real_T *gradeq)
{
  static const signed char b_iv1[18] = {0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        1, 0, 0, 0, 1, 0, 0, 0, 1};
  static const signed char b[12] = {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0};
  static const signed char b_iv[9] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
  emxArray_real_T b_dQcdw0i;
  emxArray_real_T *Qc;
  emxArray_real_T *a__3;
  emxArray_real_T *a__4;
  emxArray_real_T *a__5;
  emxArray_real_T *b_dconstrdqa;
  emxArray_real_T *b_dplatangerrdqei;
  emxArray_real_T *b_result;
  emxArray_real_T *b_y;
  emxArray_real_T *beameq;
  emxArray_real_T *dQcdLvi;
  emxArray_real_T *dQcdqei;
  emxArray_real_T *dQcdw0i;
  emxArray_real_T *dbeameqdlambda;
  emxArray_real_T *dbeameqdqa;
  emxArray_real_T *dbeameqdqe;
  emxArray_real_T *dconstrdqe;
  emxArray_real_T *dh2dLvi;
  emxArray_real_T *dh2dqei;
  emxArray_real_T *dp2dqei;
  emxArray_real_T *dplatangerrdqei;
  emxArray_real_T *dwrenchdqe;
  emxArray_real_T *r;
  emxArray_real_T *r1;
  emxArray_real_T *varargin_2;
  emxArray_real_T *y;
  double c_qa[144];
  double dconstrdqa[120];
  double dconstrdqp[120];
  double dwrenchdlambda[120];
  double lambda_data[86];
  double qe_data[86];
  double qei_data[86];
  double qp_data[86];
  double Adg[36];
  double Adg2[36];
  double b_Adg[36];
  double dwrenchdqa[36];
  double dwrenchdqp[36];
  double d_y_tmp[30];
  double y_tmp[30];
  double constr[20];
  double D1[12];
  double D2[12];
  double D3[12];
  double b_qa[12];
  double b_y_tmp[10];
  double R2[9];
  double Rp[9];
  double b_Rp[9];
  double b_dRpda[9];
  double b_dRpdb[9];
  double b_tmp[9];
  double c_dRpdc[9];
  double dRpda[9];
  double dRpdb[9];
  double dRpdc[9];
  double dppdrot[9];
  double m2[9];
  double gwrench[6];
  double platangerr[6];
  double qa[6];
  double twrench[6];
  double wrencheq[6];
  double c_y_tmp[5];
  double h2[4];
  double hp[4];
  double b_dRpdc[3];
  double dplatangerrdLvi_data[3];
  double p2[3];
  double pp[3];
  double reshapes_f2[3];
  double Nf;
  double Nftot;
  double R2_tmp;
  double b_R2_tmp;
  double c_R2_tmp;
  double d;
  double d1;
  double d2;
  double d_R2_tmp;
  double e_R2_tmp;
  double f_R2_tmp;
  double g_R2_tmp;
  double h_R2_tmp;
  double pbase_idx_0;
  double pbase_idx_1;
  double pplat_idx_0;
  double pplat_idx_1;
  double pplat_idx_2;
  double *Qc_data;
  double *beameq_data;
  double *dQcdLvi_data;
  double *dbeameqdlambda_data;
  double *dbeameqdqa_data;
  double *dbeameqdqe_data;
  double *dconstrdqe_data;
  double *dh2dLvi_data;
  double *dp2dqei_data;
  double *dplatangerrdqei_data;
  double *dwrenchdqe_data;
  double *eq_data;
  int b_beameq[2];
  int c_result[2];
  int d_result[2];
  int dplatangerrdLvi_size[2];
  int e_result[2];
  int b_i;
  int b_loop_ub;
  int i;
  int i1;
  int i2;
  int i3;
  int i4;
  int i5;
  int input_sizes_idx_1;
  int loop_ub;
  int qp_size;
  int result;
  int sizes_idx_1;
  signed char b_input_sizes_idx_1;
  signed char sizes_idx_0;
  boolean_T empty_non_axis_sizes;
  (void)guess_size;
  if (!isInitialized_inverse_UniboCPR) {
    inverse_UniboCPR_initialize();
  }
  /*  extract parameters */
  Nf = params->Nf;
  Nftot = 3.0 * params->Nf;
  /*  extract variables */
  unstack(guess_data, Nftot, qa, qe_data, &input_sizes_idx_1, qp_data, &qp_size,
          lambda_data, &sizes_idx_1);
  pbase_idx_0 = qa[0];
  pbase_idx_1 = qa[1];
  pplat_idx_0 = qp_data[0];
  pplat_idx_1 = qp_data[1];
  pplat_idx_2 = qp_data[2];
  rotationParametrization(&qp_data[3], params->rotparams.data,
                          params->rotparams.size, Rp, dRpda, dRpdb, dRpdc);
  /*  initialize */
  emxInit_real_T(&beameq, 1);
  loop_ub = (int)(4.0 * Nftot);
  i = beameq->size[0];
  beameq->size[0] = loop_ub;
  emxEnsureCapacity_real_T(beameq, i);
  beameq_data = beameq->data;
  for (i = 0; i < loop_ub; i++) {
    beameq_data[i] = 0.0;
  }
  emxInit_real_T(&dbeameqdqa, 2);
  i = dbeameqdqa->size[0] * dbeameqdqa->size[1];
  dbeameqdqa->size[0] = loop_ub;
  dbeameqdqa->size[1] = 6;
  emxEnsureCapacity_real_T(dbeameqdqa, i);
  dbeameqdqa_data = dbeameqdqa->data;
  sizes_idx_1 = loop_ub * 6;
  for (i = 0; i < sizes_idx_1; i++) {
    dbeameqdqa_data[i] = 0.0;
  }
  emxInit_real_T(&dbeameqdqe, 2);
  i = dbeameqdqe->size[0] * dbeameqdqe->size[1];
  dbeameqdqe->size[0] = loop_ub;
  dbeameqdqe->size[1] = loop_ub;
  emxEnsureCapacity_real_T(dbeameqdqe, i);
  dbeameqdqe_data = dbeameqdqe->data;
  b_loop_ub = loop_ub * loop_ub;
  for (i = 0; i < b_loop_ub; i++) {
    dbeameqdqe_data[i] = 0.0;
  }
  emxInit_real_T(&dbeameqdlambda, 2);
  i = dbeameqdlambda->size[0] * dbeameqdlambda->size[1];
  dbeameqdlambda->size[0] = loop_ub;
  dbeameqdlambda->size[1] = 20;
  emxEnsureCapacity_real_T(dbeameqdlambda, i);
  dbeameqdlambda_data = dbeameqdlambda->data;
  input_sizes_idx_1 = loop_ub * 20;
  for (i = 0; i < input_sizes_idx_1; i++) {
    dbeameqdlambda_data[i] = 0.0;
  }
  for (b_i = 0; b_i < 6; b_i++) {
    wrencheq[b_i] = params->tipwrench[b_i];
  }
  memset(&dwrenchdqa[0], 0, 36U * sizeof(double));
  emxInit_real_T(&dwrenchdqe, 2);
  i = dwrenchdqe->size[0] * dwrenchdqe->size[1];
  dwrenchdqe->size[0] = 6;
  dwrenchdqe->size[1] = loop_ub;
  emxEnsureCapacity_real_T(dwrenchdqe, i);
  dwrenchdqe_data = dwrenchdqe->data;
  for (i = 0; i < sizes_idx_1; i++) {
    dwrenchdqe_data[i] = 0.0;
  }
  memset(&dwrenchdqp[0], 0, 36U * sizeof(double));
  memset(&dwrenchdlambda[0], 0, 120U * sizeof(double));
  memset(&constr[0], 0, 20U * sizeof(double));
  memset(&dconstrdqa[0], 0, 120U * sizeof(double));
  emxInit_real_T(&dconstrdqe, 2);
  i = dconstrdqe->size[0] * dconstrdqe->size[1];
  dconstrdqe->size[0] = 20;
  dconstrdqe->size[1] = loop_ub;
  emxEnsureCapacity_real_T(dconstrdqe, i);
  dconstrdqe_data = dconstrdqe->data;
  for (i = 0; i < input_sizes_idx_1; i++) {
    dconstrdqe_data[i] = 0.0;
  }
  memset(&dconstrdqp[0], 0, 120U * sizeof(double));
  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 5; i1++) {
      y_tmp[i1 + 5 * i] = geometry->jointmatr[i + 6 * i1];
    }
  }
  emxInit_real_T(&a__3, 1);
  emxInit_real_T(&a__4, 1);
  emxInit_real_T(&a__5, 1);
  emxInit_real_T(&dh2dLvi, 1);
  emxInit_real_T(&dp2dqei, 1);
  emxInit_real_T(&dh2dqei, 1);
  emxInit_real_T(&Qc, 1);
  emxInit_real_T(&dQcdLvi, 1);
  emxInit_real_T(&dQcdqei, 1);
  emxInit_real_T(&dQcdw0i, 1);
  emxInit_real_T(&dplatangerrdqei, 2);
  emxInit_real_T(&varargin_2, 2);
  emxInit_real_T(&y, 2);
  emxInit_real_T(&b_y, 2);
  emxInit_real_T(&r, 2);
  emxInit_real_T(&r1, 2);
  emxInit_real_T(&b_dplatangerrdqei, 2);
  for (b_i = 0; b_i < 4; b_i++) {
    /*  extract variables of the th-i beam */
    d = Nftot * (((double)b_i + 1.0) - 1.0) + 1.0;
    d1 = Nftot * ((double)b_i + 1.0);
    if (d > d1) {
      i = 0;
      i1 = -1;
    } else {
      i = (int)d - 1;
      i1 = (int)d1 - 1;
    }
    b_loop_ub = i1 - i;
    for (i1 = 0; i1 <= b_loop_ub; i1++) {
      qei_data[i1] = qe_data[i + i1];
    }
    i1 = 5 * b_i;
    i2 = 5 * (b_i + 1);
    if (i1 + 1 > i2) {
      i3 = 0;
    } else {
      i3 = i1;
    }
    for (i4 = 0; i4 < 6; i4++) {
      d2 = 0.0;
      for (i5 = 0; i5 < 5; i5++) {
        d2 += geometry->jointmatr[i4 + 6 * i5] * lambda_data[i3 + i5];
      }
      twrench[i4] = d2;
    }
    /*  forward recursion */
    eul2quat(&geometry->platangles[3 * b_i], hp);
    pp[0] = pbase_idx_0 + geometry->platpoints[3 * b_i];
    pp[1] = pbase_idx_1 + geometry->platpoints[3 * b_i + 1];
    pp[2] = geometry->hr + geometry->platpoints[3 * b_i + 2];
    d2 = qa[b_i + 2];
    forwardRecursion(pp, hp, qei_data, d2, Nf, p2, h2, reshapes_f2, dh2dLvi,
                     dp2dqei, dh2dqei);
    dp2dqei_data = dp2dqei->data;
    dh2dLvi_data = dh2dLvi->data;
    /*  CONVERT QUATERNION HP INTO ROTATION MATRIX */
    /*  hP = h1 + h2 i + h3 j + h4 k (h1 = scalar component) */
    R2_tmp = h2[0] * h2[0];
    b_R2_tmp = h2[1] * h2[1];
    c_R2_tmp = h2[2] * h2[2];
    d_R2_tmp = h2[3] * h2[3];
    R2[0] = ((R2_tmp + b_R2_tmp) - c_R2_tmp) - d_R2_tmp;
    e_R2_tmp = h2[1] * h2[2];
    f_R2_tmp = h2[0] * h2[3];
    R2[1] = 2.0 * (f_R2_tmp + e_R2_tmp);
    g_R2_tmp = h2[0] * h2[2];
    h_R2_tmp = h2[1] * h2[3];
    R2[2] = 2.0 * (h_R2_tmp - g_R2_tmp);
    R2[3] = 2.0 * (e_R2_tmp - f_R2_tmp);
    R2_tmp -= b_R2_tmp;
    R2[4] = (R2_tmp + c_R2_tmp) - d_R2_tmp;
    b_R2_tmp = h2[2] * h2[3];
    e_R2_tmp = h2[0] * h2[1];
    R2[5] = 2.0 * (e_R2_tmp + b_R2_tmp);
    R2[6] = 2.0 * (g_R2_tmp + h_R2_tmp);
    R2[7] = 2.0 * (b_R2_tmp - e_R2_tmp);
    R2[8] = (R2_tmp - c_R2_tmp) + d_R2_tmp;
    /*  backward recursion */
    backwardRecursion(twrench, qei_data, Nf, d2, gwrench, Qc, a__3, dQcdLvi,
                      a__4, dQcdqei, a__5, dQcdw0i);
    eq_data = dQcdqei->data;
    dQcdLvi_data = dQcdLvi->data;
    Qc_data = Qc->data;
    /*     %% BEAM EQUILIBRIUMS */
    if (d > d1) {
      i3 = 0;
      i4 = 0;
    } else {
      i3 = (int)d - 1;
      i4 = (int)d1;
    }
    if (Qc->size[0] == 12) {
      for (i5 = 0; i5 < 12; i5++) {
        R2_tmp = 0.0;
        for (input_sizes_idx_1 = 0; input_sizes_idx_1 < 12;
             input_sizes_idx_1++) {
          R2_tmp += d2 * Kad[i5 + 12 * input_sizes_idx_1] *
                    qe_data[i + input_sizes_idx_1];
        }
        b_qa[i5] = R2_tmp + Qc_data[i5];
      }
      b_loop_ub = i4 - i3;
      for (i4 = 0; i4 < b_loop_ub; i4++) {
        beameq_data[i3 + i4] = b_qa[i4];
      }
    } else {
      c_binary_expand_op(beameq, i3, i4, qa, b_i, Kad, qe_data, i, Qc);
      beameq_data = beameq->data;
    }
    if (d > d1) {
      i3 = 0;
      i4 = 0;
    } else {
      i3 = (int)d - 1;
      i4 = (int)d1;
    }
    if (dQcdLvi->size[0] == 12) {
      for (i5 = 0; i5 < 12; i5++) {
        R2_tmp = 0.0;
        for (input_sizes_idx_1 = 0; input_sizes_idx_1 < 12;
             input_sizes_idx_1++) {
          R2_tmp +=
              Kad[i5 + 12 * input_sizes_idx_1] * qe_data[i + input_sizes_idx_1];
        }
        b_qa[i5] = R2_tmp + dQcdLvi_data[i5];
      }
      b_loop_ub = i4 - i3;
      for (i = 0; i < b_loop_ub; i++) {
        dbeameqdqa_data[(i3 + i) + dbeameqdqa->size[0] * (b_i + 2)] = b_qa[i];
      }
    } else {
      b_binary_expand_op(dbeameqdqa, i3, i4, b_i, Kad, qe_data, i, dQcdLvi);
      dbeameqdqa_data = dbeameqdqa->data;
    }
    if (d > d1) {
      i = 0;
      i3 = 0;
      i4 = 0;
      i5 = 0;
    } else {
      i = (int)d - 1;
      i3 = (int)d1;
      i4 = (int)d - 1;
      i5 = (int)d1;
    }
    if ((int)Nftot == 12) {
      for (input_sizes_idx_1 = 0; input_sizes_idx_1 < 144;
           input_sizes_idx_1++) {
        c_qa[input_sizes_idx_1] =
            d2 * Kad[input_sizes_idx_1] + eq_data[input_sizes_idx_1];
      }
      input_sizes_idx_1 = i3 - i;
      sizes_idx_1 = i5 - i4;
      for (i3 = 0; i3 < sizes_idx_1; i3++) {
        for (i5 = 0; i5 < input_sizes_idx_1; i5++) {
          dbeameqdqe_data[(i + i5) + dbeameqdqe->size[0] * (i4 + i3)] =
              c_qa[i5 + input_sizes_idx_1 * i3];
        }
      }
    } else {
      binary_expand_op(dbeameqdqe, i, i3, i4, i5, qa, b_i, Kad, dQcdqei, Nftot);
      dbeameqdqe_data = dbeameqdqe->data;
    }
    if (d > d1) {
      i = 1;
    } else {
      i = (int)d;
    }
    if (i1 + 1 > i2) {
      i3 = 0;
    } else {
      i3 = i1;
    }
    b_dQcdw0i = *dQcdw0i;
    b_beameq[0] = (int)Nftot;
    b_beameq[1] = 6;
    b_dQcdw0i.size = &b_beameq[0];
    b_dQcdw0i.numDimensions = 2;
    d_mtimes(&b_dQcdw0i, geometry->jointmatr, r);
    dQcdLvi_data = r->data;
    b_loop_ub = r->size[0];
    for (i4 = 0; i4 < 5; i4++) {
      for (i5 = 0; i5 < b_loop_ub; i5++) {
        dbeameqdlambda_data[((i + i5) + dbeameqdlambda->size[0] * (i3 + i4)) -
                            1] = dQcdLvi_data[i5 + r->size[0] * i4];
      }
    }
    /*     %% CONSTRAINTS */
    b_eul2quat(&geometry->endangles[3 * b_i], geometry->endanglesconvention,
               hp);
    /*  CONVERT QUATERNION HP INTO ROTATION MATRIX */
    /*  hP = h1 + h2 i + h3 j + h4 k (h1 = scalar component) */
    R2_tmp = hp[0] * hp[0];
    b_R2_tmp = hp[1] * hp[1];
    c_R2_tmp = hp[2] * hp[2];
    d_R2_tmp = hp[3] * hp[3];
    b_tmp[0] = ((R2_tmp + b_R2_tmp) - c_R2_tmp) - d_R2_tmp;
    e_R2_tmp = hp[1] * hp[2];
    f_R2_tmp = hp[0] * hp[3];
    b_tmp[1] = 2.0 * (f_R2_tmp + e_R2_tmp);
    g_R2_tmp = hp[0] * hp[2];
    h_R2_tmp = hp[1] * hp[3];
    b_tmp[2] = 2.0 * (h_R2_tmp - g_R2_tmp);
    b_tmp[3] = 2.0 * (e_R2_tmp - f_R2_tmp);
    R2_tmp -= b_R2_tmp;
    b_tmp[4] = (R2_tmp + c_R2_tmp) - d_R2_tmp;
    b_R2_tmp = hp[2] * hp[3];
    e_R2_tmp = hp[0] * hp[1];
    b_tmp[5] = 2.0 * (e_R2_tmp + b_R2_tmp);
    b_tmp[6] = 2.0 * (g_R2_tmp + h_R2_tmp);
    b_tmp[7] = 2.0 * (b_R2_tmp - e_R2_tmp);
    b_tmp[8] = (R2_tmp - c_R2_tmp) + d_R2_tmp;
    for (i = 0; i < 3; i++) {
      d2 = 0.0;
      R2_tmp = 0.0;
      b_R2_tmp = 0.0;
      c_R2_tmp = 0.0;
      for (i3 = 0; i3 < 3; i3++) {
        d_R2_tmp = geometry->endpoints[i3 + 3 * b_i];
        sizes_idx_1 = i + 3 * i3;
        d2 += Rp[sizes_idx_1] * d_R2_tmp;
        R2_tmp += dRpda[sizes_idx_1] * d_R2_tmp;
        b_R2_tmp += dRpdb[sizes_idx_1] * d_R2_tmp;
        c_R2_tmp += dRpdc[sizes_idx_1] * d_R2_tmp;
        d_R2_tmp = b_tmp[3 * i3];
        e_R2_tmp = Rp[i] * d_R2_tmp;
        f_R2_tmp = dRpda[i] * d_R2_tmp;
        g_R2_tmp = dRpdb[i] * d_R2_tmp;
        h_R2_tmp = dRpdc[i] * d_R2_tmp;
        d_R2_tmp = b_tmp[3 * i3 + 1];
        e_R2_tmp += Rp[i + 3] * d_R2_tmp;
        f_R2_tmp += dRpda[i + 3] * d_R2_tmp;
        g_R2_tmp += dRpdb[i + 3] * d_R2_tmp;
        h_R2_tmp += dRpdc[i + 3] * d_R2_tmp;
        d_R2_tmp = b_tmp[3 * i3 + 2];
        e_R2_tmp += Rp[i + 6] * d_R2_tmp;
        f_R2_tmp += dRpda[i + 6] * d_R2_tmp;
        g_R2_tmp += dRpdb[i + 6] * d_R2_tmp;
        h_R2_tmp += dRpdc[i + 6] * d_R2_tmp;
        c_dRpdc[sizes_idx_1] = h_R2_tmp;
        b_dRpdb[sizes_idx_1] = g_R2_tmp;
        b_dRpda[sizes_idx_1] = f_R2_tmp;
        b_Rp[sizes_idx_1] = e_R2_tmp;
      }
      b_dRpdc[i] = c_R2_tmp;
      pp[i] = d2;
      dppdrot[i] = R2_tmp;
      dppdrot[i + 3] = b_R2_tmp;
      dppdrot[i + 6] = c_R2_tmp;
    }
    b_dQcdw0i = *dh2dqei;
    c_result[0] = 4;
    c_result[1] = (int)Nftot;
    b_dQcdw0i.size = &c_result[0];
    b_dQcdw0i.numDimensions = 2;
    roterr(b_Rp, R2, h2, dh2dLvi, &b_dQcdw0i, b_dRpda, b_dRpdb, c_dRpdc,
           b_dRpdc, dplatangerrdLvi_data, dplatangerrdLvi_size, dplatangerrdqei,
           m2);
    dQcdLvi_data = dplatangerrdqei->data;
    if (i1 + 1 > i2) {
      i = 0;
      i3 = 0;
    } else {
      i = i1;
      i3 = i2;
    }
    platangerr[0] = b_dRpdc[0];
    platangerr[3] = p2[0] - (pplat_idx_0 + pp[0]);
    platangerr[1] = b_dRpdc[1];
    platangerr[4] = p2[1] - (pplat_idx_1 + pp[1]);
    platangerr[2] = b_dRpdc[2];
    platangerr[5] = p2[2] - (pplat_idx_2 + pp[2]);
    for (i4 = 0; i4 < 5; i4++) {
      d2 = 0.0;
      for (i5 = 0; i5 < 6; i5++) {
        d2 += y_tmp[i4 + 5 * i5] * platangerr[i5];
      }
      c_y_tmp[i4] = d2;
    }
    b_loop_ub = i3 - i;
    for (i3 = 0; i3 < b_loop_ub; i3++) {
      constr[i + i3] = c_y_tmp[i3];
    }
    if (i1 + 1 > i2) {
      i = 0;
      i3 = 0;
    } else {
      i = i1;
      i3 = i2;
    }
    for (i4 = 0; i4 < 5; i4++) {
      for (i5 = 0; i5 < 2; i5++) {
        d2 = 0.0;
        for (input_sizes_idx_1 = 0; input_sizes_idx_1 < 6;
             input_sizes_idx_1++) {
          d2 += y_tmp[i4 + 5 * input_sizes_idx_1] *
                (double)b[input_sizes_idx_1 + 6 * i5];
        }
        b_y_tmp[i4 + 5 * i5] = d2;
      }
    }
    input_sizes_idx_1 = i3 - i;
    for (i3 = 0; i3 < 2; i3++) {
      for (i4 = 0; i4 < input_sizes_idx_1; i4++) {
        dconstrdqa[(i + i4) + 20 * i3] = b_y_tmp[i4 + input_sizes_idx_1 * i3];
      }
    }
    if (i1 + 1 > i2) {
      i = 0;
      i3 = 0;
    } else {
      i = i1;
      i3 = i2;
    }
    gwrench[0] = dplatangerrdLvi_data[0];
    gwrench[3] = reshapes_f2[0];
    gwrench[1] = dplatangerrdLvi_data[1];
    gwrench[4] = reshapes_f2[1];
    gwrench[2] = dplatangerrdLvi_data[2];
    gwrench[5] = reshapes_f2[2];
    for (i4 = 0; i4 < 5; i4++) {
      d2 = 0.0;
      for (i5 = 0; i5 < 6; i5++) {
        d2 += y_tmp[i4 + 5 * i5] * gwrench[i5];
      }
      c_y_tmp[i4] = d2;
    }
    b_loop_ub = i3 - i;
    for (i3 = 0; i3 < b_loop_ub; i3++) {
      dconstrdqa[(i + i3) + 20 * (b_i + 2)] = c_y_tmp[i3];
    }
    if (i1 + 1 > i2) {
      i = 0;
    } else {
      i = i1;
    }
    if (d > d1) {
      i3 = 1;
    } else {
      i3 = (int)d;
    }
    if (dplatangerrdqei->size[1] != 0) {
      result = dplatangerrdqei->size[1];
    } else if ((int)Nftot != 0) {
      result = (int)Nftot;
    } else {
      result = 0;
      if ((int)Nftot > 0) {
        result = (int)Nftot;
      }
    }
    empty_non_axis_sizes = (result == 0);
    if (empty_non_axis_sizes || (dplatangerrdqei->size[1] != 0)) {
      b_input_sizes_idx_1 = 3;
    } else {
      b_input_sizes_idx_1 = 0;
    }
    if (empty_non_axis_sizes || ((int)Nftot != 0)) {
      b_loop_ub = 3;
    } else {
      b_loop_ub = 0;
    }
    sizes_idx_1 = b_input_sizes_idx_1;
    i4 = b_dplatangerrdqei->size[0] * b_dplatangerrdqei->size[1];
    b_dplatangerrdqei->size[0] = b_input_sizes_idx_1 + b_loop_ub;
    b_dplatangerrdqei->size[1] = result;
    emxEnsureCapacity_real_T(b_dplatangerrdqei, i4);
    dplatangerrdqei_data = b_dplatangerrdqei->data;
    for (i4 = 0; i4 < result; i4++) {
      for (i5 = 0; i5 < sizes_idx_1; i5++) {
        dplatangerrdqei_data[i5 + b_dplatangerrdqei->size[0] * i4] =
            dQcdLvi_data[i5 + b_input_sizes_idx_1 * i4];
      }
    }
    for (i4 = 0; i4 < result; i4++) {
      for (i5 = 0; i5 < b_loop_ub; i5++) {
        dplatangerrdqei_data[(i5 + b_input_sizes_idx_1) +
                             b_dplatangerrdqei->size[0] * i4] =
            dp2dqei_data[i5 + b_loop_ub * i4];
      }
    }
    e_mtimes(geometry->jointmatr, b_dplatangerrdqei, r1);
    dQcdLvi_data = r1->data;
    b_loop_ub = r1->size[1];
    for (i4 = 0; i4 < b_loop_ub; i4++) {
      for (i5 = 0; i5 < 5; i5++) {
        dconstrdqe_data[(i + i5) + 20 * ((i3 + i4) - 1)] =
            dQcdLvi_data[i5 + 5 * i4];
      }
    }
    if (i1 + 1 > i2) {
      i = 0;
      i3 = 0;
    } else {
      i = i1;
      i3 = i2;
    }
    for (i4 = 0; i4 < 3; i4++) {
      Adg[6 * i4] = 0.0;
      input_sizes_idx_1 = 6 * (i4 + 3);
      Adg[input_sizes_idx_1] = m2[3 * i4];
      Adg[6 * i4 + 3] = -(double)b_iv[3 * i4];
      Adg[input_sizes_idx_1 + 3] = -dppdrot[3 * i4];
      Adg[6 * i4 + 1] = 0.0;
      sizes_idx_1 = 3 * i4 + 1;
      Adg[input_sizes_idx_1 + 1] = m2[sizes_idx_1];
      Adg[6 * i4 + 4] = -(double)b_iv[sizes_idx_1];
      Adg[input_sizes_idx_1 + 4] = -dppdrot[sizes_idx_1];
      Adg[6 * i4 + 2] = 0.0;
      sizes_idx_1 = 3 * i4 + 2;
      Adg[input_sizes_idx_1 + 2] = m2[sizes_idx_1];
      Adg[6 * i4 + 5] = -(double)b_iv[sizes_idx_1];
      Adg[input_sizes_idx_1 + 5] = -dppdrot[sizes_idx_1];
    }
    for (i4 = 0; i4 < 5; i4++) {
      for (i5 = 0; i5 < 6; i5++) {
        d2 = 0.0;
        for (input_sizes_idx_1 = 0; input_sizes_idx_1 < 6;
             input_sizes_idx_1++) {
          d2 += y_tmp[i4 + 5 * input_sizes_idx_1] *
                Adg[input_sizes_idx_1 + 6 * i5];
        }
        d_y_tmp[i4 + 5 * i5] = d2;
      }
    }
    input_sizes_idx_1 = i3 - i;
    for (i3 = 0; i3 < 6; i3++) {
      for (i4 = 0; i4 < input_sizes_idx_1; i4++) {
        dconstrdqp[(i + i4) + 20 * i3] = d_y_tmp[i4 + input_sizes_idx_1 * i3];
      }
    }
    /*     %% PLATFORM EQUILIBRIUM */
    for (i = 0; i < 3; i++) {
      d2 = R2[3 * i];
      b_Adg[6 * i] = d2;
      input_sizes_idx_1 = 6 * (i + 3);
      b_Adg[input_sizes_idx_1] = 0.0;
      b_Adg[6 * i + 3] = 0.0;
      b_Adg[input_sizes_idx_1 + 3] = d2;
      d2 = R2[3 * i + 1];
      b_Adg[6 * i + 1] = d2;
      b_Adg[input_sizes_idx_1 + 1] = 0.0;
      b_Adg[6 * i + 4] = 0.0;
      b_Adg[input_sizes_idx_1 + 4] = d2;
      d2 = R2[3 * i + 2];
      b_Adg[6 * i + 2] = d2;
      b_Adg[input_sizes_idx_1 + 2] = 0.0;
      b_Adg[6 * i + 5] = 0.0;
      b_Adg[input_sizes_idx_1 + 5] = d2;
    }
    for (i = 0; i < 36; i++) {
      Adg[i] = -b_Adg[i];
    }
    for (i = 0; i < 6; i++) {
      d2 = 0.0;
      for (i3 = 0; i3 < 6; i3++) {
        d2 += Adg[i + 6 * i3] * twrench[i3];
      }
      gwrench[i] = d2;
    }
    derivativeColRotMatQuat(h2, b_qa, D2, D3);
    d2 = twrench[0];
    R2_tmp = twrench[1];
    b_R2_tmp = twrench[2];
    for (i = 0; i < 12; i++) {
      D1[i] = (b_qa[i] * d2 + D2[i] * R2_tmp) + D3[i] * b_R2_tmp;
    }
    b_dQcdw0i = *dh2dqei;
    d_result[0] = 4;
    d_result[1] = (int)Nftot;
    b_dQcdw0i.size = &d_result[0];
    b_dQcdw0i.numDimensions = 2;
    mtimes(D1, &b_dQcdw0i, y);
    eq_data = y->data;
    d2 = twrench[3];
    R2_tmp = twrench[4];
    b_R2_tmp = twrench[5];
    for (i = 0; i < 12; i++) {
      D1[i] = (b_qa[i] * d2 + D2[i] * R2_tmp) + D3[i] * b_R2_tmp;
    }
    b_dQcdw0i = *dh2dqei;
    e_result[0] = 4;
    e_result[1] = (int)Nftot;
    b_dQcdw0i.size = &e_result[0];
    b_dQcdw0i.numDimensions = 2;
    mtimes(D1, &b_dQcdw0i, b_y);
    Qc_data = b_y->data;
    if (y->size[1] != 0) {
      result = y->size[1];
    } else if (b_y->size[1] != 0) {
      result = b_y->size[1];
    } else {
      result = 0;
    }
    empty_non_axis_sizes = (result == 0);
    if (empty_non_axis_sizes || (y->size[1] != 0)) {
      b_input_sizes_idx_1 = 3;
    } else {
      b_input_sizes_idx_1 = 0;
    }
    if (empty_non_axis_sizes || (b_y->size[1] != 0)) {
      sizes_idx_0 = 3;
    } else {
      sizes_idx_0 = 0;
    }
    for (i = 0; i < 3; i++) {
      Adg2[6 * i] = b_iv[3 * i];
      Adg2[6 * i + 1] = b_iv[3 * i + 1];
      Adg2[6 * i + 2] = b_iv[3 * i + 2];
    }
    Adg2[18] = 0.0;
    Adg2[24] = -pp[2];
    Adg2[30] = pp[1];
    Adg2[19] = pp[2];
    Adg2[25] = 0.0;
    Adg2[31] = -pp[0];
    Adg2[20] = -pp[1];
    Adg2[26] = pp[0];
    Adg2[32] = 0.0;
    for (i = 0; i < 6; i++) {
      Adg2[6 * i + 3] = b_iv1[3 * i];
      Adg2[6 * i + 4] = b_iv1[3 * i + 1];
      Adg2[6 * i + 5] = b_iv1[3 * i + 2];
    }
    for (i = 0; i < 6; i++) {
      d2 = 0.0;
      for (i3 = 0; i3 < 6; i3++) {
        d2 += Adg2[i + 6 * i3] * gwrench[i3];
      }
      wrencheq[i] += d2;
    }
    d2 = twrench[0];
    R2_tmp = twrench[1];
    b_R2_tmp = twrench[2];
    c_R2_tmp = twrench[3];
    d_R2_tmp = twrench[4];
    e_R2_tmp = twrench[5];
    for (i = 0; i < 12; i++) {
      f_R2_tmp = b_qa[i];
      g_R2_tmp = D2[i];
      h_R2_tmp = D3[i];
      D1[i] = (f_R2_tmp * d2 + g_R2_tmp * R2_tmp) + h_R2_tmp * b_R2_tmp;
      f_R2_tmp =
          (f_R2_tmp * c_R2_tmp + g_R2_tmp * d_R2_tmp) + h_R2_tmp * e_R2_tmp;
      b_qa[i] = f_R2_tmp;
    }
    for (i = 0; i < 3; i++) {
      platangerr[i] =
          -(((D1[i] * dh2dLvi_data[0] + D1[i + 3] * dh2dLvi_data[1]) +
             D1[i + 6] * dh2dLvi_data[2]) +
            D1[i + 9] * dh2dLvi_data[3]);
      platangerr[i + 3] =
          -(((b_qa[i] * dh2dLvi_data[0] + b_qa[i + 3] * dh2dLvi_data[1]) +
             b_qa[i + 6] * dh2dLvi_data[2]) +
            b_qa[i + 9] * dh2dLvi_data[3]);
    }
    for (i = 0; i < 6; i++) {
      d2 = 0.0;
      for (i3 = 0; i3 < 6; i3++) {
        d2 += Adg2[i + 6 * i3] * platangerr[i3];
      }
      dwrenchdqa[i + 6 * (b_i + 2)] = d2;
    }
    if (d > d1) {
      i = 1;
    } else {
      i = (int)d;
    }
    sizes_idx_1 = b_input_sizes_idx_1;
    input_sizes_idx_1 = sizes_idx_0;
    i3 = b_dplatangerrdqei->size[0] * b_dplatangerrdqei->size[1];
    b_dplatangerrdqei->size[0] = b_input_sizes_idx_1 + sizes_idx_0;
    b_dplatangerrdqei->size[1] = result;
    emxEnsureCapacity_real_T(b_dplatangerrdqei, i3);
    dplatangerrdqei_data = b_dplatangerrdqei->data;
    for (i3 = 0; i3 < result; i3++) {
      for (i4 = 0; i4 < sizes_idx_1; i4++) {
        dplatangerrdqei_data[i4 + b_dplatangerrdqei->size[0] * i3] =
            -eq_data[i4 + b_input_sizes_idx_1 * i3];
      }
    }
    for (i3 = 0; i3 < result; i3++) {
      for (i4 = 0; i4 < input_sizes_idx_1; i4++) {
        dplatangerrdqei_data[(i4 + b_input_sizes_idx_1) +
                             b_dplatangerrdqei->size[0] * i3] =
            -Qc_data[i4 + sizes_idx_0 * i3];
      }
    }
    f_mtimes(Adg2, b_dplatangerrdqei, varargin_2);
    dplatangerrdqei_data = varargin_2->data;
    b_loop_ub = varargin_2->size[1];
    for (i3 = 0; i3 < b_loop_ub; i3++) {
      for (i4 = 0; i4 < 6; i4++) {
        dwrenchdqe_data[i4 + 6 * ((i + i3) - 1)] =
            dplatangerrdqei_data[i4 + 6 * i3];
      }
    }
    b_tmp[0] = 0.0;
    b_tmp[3] = -dppdrot[2];
    b_tmp[6] = dppdrot[1];
    b_tmp[1] = dppdrot[2];
    b_tmp[4] = 0.0;
    b_tmp[7] = -dppdrot[0];
    b_tmp[2] = -dppdrot[1];
    b_tmp[5] = dppdrot[0];
    b_tmp[8] = 0.0;
    m2[0] = 0.0;
    m2[3] = -dppdrot[5];
    m2[6] = dppdrot[4];
    m2[1] = dppdrot[5];
    m2[4] = 0.0;
    m2[7] = -dppdrot[3];
    m2[2] = -dppdrot[4];
    m2[5] = dppdrot[3];
    m2[8] = 0.0;
    R2[0] = 0.0;
    R2[3] = -dppdrot[8];
    R2[6] = dppdrot[7];
    R2[1] = dppdrot[8];
    R2[4] = 0.0;
    R2[7] = -dppdrot[6];
    R2[2] = -dppdrot[7];
    R2[5] = dppdrot[6];
    R2[8] = 0.0;
    d = gwrench[3];
    d1 = gwrench[4];
    d2 = gwrench[5];
    for (i = 0; i < 3; i++) {
      Adg[6 * i] = dwrenchdqp[6 * i];
      input_sizes_idx_1 = 6 * i + 1;
      Adg[input_sizes_idx_1] = dwrenchdqp[input_sizes_idx_1];
      input_sizes_idx_1 = 6 * i + 2;
      Adg[input_sizes_idx_1] = dwrenchdqp[input_sizes_idx_1];
      Adg[i + 18] = dwrenchdqp[i + 18] +
                    ((b_tmp[i] * d + b_tmp[i + 3] * d1) + b_tmp[i + 6] * d2);
      Adg[i + 24] =
          dwrenchdqp[i + 24] + ((m2[i] * d + m2[i + 3] * d1) + m2[i + 6] * d2);
      Adg[i + 30] =
          dwrenchdqp[i + 30] + ((R2[i] * d + R2[i + 3] * d1) + R2[i + 6] * d2);
    }
    for (i = 0; i < 6; i++) {
      input_sizes_idx_1 = 6 * i + 3;
      Adg[input_sizes_idx_1] = dwrenchdqp[input_sizes_idx_1];
      input_sizes_idx_1 = 6 * i + 4;
      Adg[input_sizes_idx_1] = dwrenchdqp[input_sizes_idx_1];
      input_sizes_idx_1 = 6 * i + 5;
      Adg[input_sizes_idx_1] = dwrenchdqp[input_sizes_idx_1];
    }
    memcpy(&dwrenchdqp[0], &Adg[0], 36U * sizeof(double));
    if (i1 + 1 > i2) {
      i1 = 0;
      i2 = 0;
    }
    for (i = 0; i < 36; i++) {
      Adg2[i] = -Adg2[i];
    }
    for (i = 0; i < 6; i++) {
      for (i3 = 0; i3 < 6; i3++) {
        d = 0.0;
        for (i4 = 0; i4 < 6; i4++) {
          d += Adg2[i + 6 * i4] * b_Adg[i4 + 6 * i3];
        }
        Adg[i + 6 * i3] = d;
      }
      for (i3 = 0; i3 < 5; i3++) {
        d = 0.0;
        for (i4 = 0; i4 < 6; i4++) {
          d += Adg[i + 6 * i4] * geometry->jointmatr[i4 + 6 * i3];
        }
        d_y_tmp[i + 6 * i3] = d;
      }
    }
    sizes_idx_1 = i2 - i1;
    for (i = 0; i < sizes_idx_1; i++) {
      for (i2 = 0; i2 < 6; i2++) {
        dwrenchdlambda[i2 + 6 * (i1 + i)] = d_y_tmp[i2 + 6 * i];
      }
    }
  }
  emxFree_real_T(&b_dplatangerrdqei);
  emxFree_real_T(&r1);
  emxFree_real_T(&r);
  emxFree_real_T(&b_y);
  emxFree_real_T(&y);
  emxFree_real_T(&dplatangerrdqei);
  emxFree_real_T(&dQcdw0i);
  emxFree_real_T(&dQcdqei);
  emxFree_real_T(&dQcdLvi);
  emxFree_real_T(&Qc);
  emxFree_real_T(&dh2dqei);
  emxFree_real_T(&dp2dqei);
  emxFree_real_T(&dh2dLvi);
  emxFree_real_T(&a__5);
  emxFree_real_T(&a__4);
  emxFree_real_T(&a__3);
  /*  Forward problem */
  /*  COLLECT */
  if (qp_size == 6) {
    i = eq->size[0];
    eq->size[0] = beameq->size[0] + 32;
    emxEnsureCapacity_real_T(eq, i);
    eq_data = eq->data;
    b_loop_ub = beameq->size[0];
    for (i = 0; i < b_loop_ub; i++) {
      eq_data[i] = beameq_data[i];
    }
    for (i = 0; i < 6; i++) {
      eq_data[i + beameq->size[0]] = wrencheq[i];
    }
    for (i = 0; i < 20; i++) {
      eq_data[(i + beameq->size[0]) + 6] = constr[i];
    }
    for (i = 0; i < 6; i++) {
      eq_data[(i + beameq->size[0]) + 26] = qp_data[i] - qpv[i];
    }
  } else {
    d_binary_expand_op(eq, beameq, wrencheq, constr, qp_data, &qp_size, qpv);
  }
  emxFree_real_T(&beameq);
  if (dbeameqdqa->size[0] != 0) {
    result = dbeameqdqa->size[0];
  } else if ((dbeameqdqe->size[0] != 0) && (dbeameqdqe->size[1] != 0)) {
    result = dbeameqdqe->size[0];
  } else if (loop_ub != 0) {
    result = loop_ub;
  } else if (dbeameqdlambda->size[0] != 0) {
    result = dbeameqdlambda->size[0];
  } else {
    result = 0;
    if (dbeameqdqe->size[0] > 0) {
      result = dbeameqdqe->size[0];
    }
    if (result < 0) {
      result = (int)(4.0 * Nftot);
    }
    if (result < 0) {
      result = 0;
    }
  }
  empty_non_axis_sizes = (result == 0);
  if (empty_non_axis_sizes || (dbeameqdqa->size[0] != 0)) {
    b_input_sizes_idx_1 = 6;
  } else {
    b_input_sizes_idx_1 = 0;
  }
  if (empty_non_axis_sizes ||
      ((dbeameqdqe->size[0] != 0) && (dbeameqdqe->size[1] != 0))) {
    input_sizes_idx_1 = dbeameqdqe->size[1];
  } else {
    input_sizes_idx_1 = 0;
  }
  if (empty_non_axis_sizes || (loop_ub != 0)) {
    sizes_idx_0 = 6;
  } else {
    sizes_idx_0 = 0;
  }
  if (empty_non_axis_sizes || (dbeameqdlambda->size[0] != 0)) {
    sizes_idx_1 = 20;
  } else {
    sizes_idx_1 = 0;
  }
  emxInit_real_T(&b_result, 2);
  i = b_result->size[0] * b_result->size[1];
  b_result->size[0] = result;
  b_result->size[1] =
      ((b_input_sizes_idx_1 + input_sizes_idx_1) + sizes_idx_0) + sizes_idx_1;
  emxEnsureCapacity_real_T(b_result, i);
  Qc_data = b_result->data;
  b_loop_ub = b_input_sizes_idx_1;
  for (i = 0; i < b_loop_ub; i++) {
    for (i1 = 0; i1 < result; i1++) {
      Qc_data[i1 + b_result->size[0] * i] = dbeameqdqa_data[i1 + result * i];
    }
  }
  emxFree_real_T(&dbeameqdqa);
  for (i = 0; i < input_sizes_idx_1; i++) {
    for (i1 = 0; i1 < result; i1++) {
      Qc_data[i1 + b_result->size[0] * (i + b_input_sizes_idx_1)] =
          dbeameqdqe_data[i1 + result * i];
    }
  }
  emxFree_real_T(&dbeameqdqe);
  b_loop_ub = sizes_idx_0;
  for (i = 0; i < b_loop_ub; i++) {
    for (i1 = 0; i1 < result; i1++) {
      Qc_data[i1 + b_result->size[0] *
                       ((i + b_input_sizes_idx_1) + input_sizes_idx_1)] = 0.0;
    }
  }
  for (i = 0; i < sizes_idx_1; i++) {
    for (i1 = 0; i1 < result; i1++) {
      Qc_data[i1 + b_result->size[0] *
                       (((i + b_input_sizes_idx_1) + input_sizes_idx_1) +
                        sizes_idx_0)] = dbeameqdlambda_data[i1 + result * i];
    }
  }
  emxFree_real_T(&dbeameqdlambda);
  i = varargin_2->size[0] * varargin_2->size[1];
  varargin_2->size[0] = 6;
  varargin_2->size[1] = dwrenchdqe->size[1] + 32;
  emxEnsureCapacity_real_T(varargin_2, i);
  dplatangerrdqei_data = varargin_2->data;
  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      i2 = i1 + 6 * i;
      dplatangerrdqei_data[i2] = dwrenchdqa[i2];
    }
  }
  b_loop_ub = dwrenchdqe->size[1];
  for (i = 0; i < b_loop_ub; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      dplatangerrdqei_data[i1 + 6 * (i + 6)] = dwrenchdqe_data[i1 + 6 * i];
    }
  }
  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      dplatangerrdqei_data[i1 + 6 * ((i + dwrenchdqe->size[1]) + 6)] =
          dwrenchdqp[i1 + 6 * i];
    }
  }
  for (i = 0; i < 20; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      dplatangerrdqei_data[i1 + 6 * ((i + dwrenchdqe->size[1]) + 12)] =
          dwrenchdlambda[i1 + 6 * i];
    }
  }
  eye(dwrenchdqa);
  i = dwrenchdqe->size[0] * dwrenchdqe->size[1];
  dwrenchdqe->size[0] = 6;
  dwrenchdqe->size[1] = loop_ub + 32;
  emxEnsureCapacity_real_T(dwrenchdqe, i);
  dwrenchdqe_data = dwrenchdqe->data;
  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      dwrenchdqe_data[i1 + 6 * i] = 0.0;
    }
  }
  for (i = 0; i < loop_ub; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      dwrenchdqe_data[i1 + 6 * (i + 6)] = 0.0;
    }
  }
  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      dwrenchdqe_data[i1 + 6 * ((i + loop_ub) + 6)] = dwrenchdqa[i1 + 6 * i];
    }
  }
  for (i = 0; i < 20; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      dwrenchdqe_data[i1 + 6 * ((i + loop_ub) + 12)] = 0.0;
    }
  }
  if ((b_result->size[0] != 0) && (b_result->size[1] != 0)) {
    sizes_idx_1 = b_result->size[1];
  } else {
    sizes_idx_1 = varargin_2->size[1];
  }
  if ((b_result->size[0] != 0) && (b_result->size[1] != 0)) {
    input_sizes_idx_1 = b_result->size[0];
  } else {
    input_sizes_idx_1 = 0;
  }
  emxInit_real_T(&b_dconstrdqa, 2);
  i = b_dconstrdqa->size[0] * b_dconstrdqa->size[1];
  b_dconstrdqa->size[0] = 20;
  b_dconstrdqa->size[1] = dconstrdqe->size[1] + 32;
  emxEnsureCapacity_real_T(b_dconstrdqa, i);
  dQcdLvi_data = b_dconstrdqa->data;
  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 20; i1++) {
      i2 = i1 + 20 * i;
      dQcdLvi_data[i2] = dconstrdqa[i2];
    }
  }
  loop_ub = dconstrdqe->size[1];
  for (i = 0; i < loop_ub; i++) {
    for (i1 = 0; i1 < 20; i1++) {
      dQcdLvi_data[i1 + 20 * (i + 6)] = dconstrdqe_data[i1 + 20 * i];
    }
  }
  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 20; i1++) {
      dQcdLvi_data[i1 + 20 * ((i + dconstrdqe->size[1]) + 6)] =
          dconstrdqp[i1 + 20 * i];
    }
  }
  for (i = 0; i < 20; i++) {
    for (i1 = 0; i1 < 20; i1++) {
      dQcdLvi_data[i1 + 20 * ((i + dconstrdqe->size[1]) + 12)] = 0.0;
    }
  }
  emxFree_real_T(&dconstrdqe);
  i = gradeq->size[0] * gradeq->size[1];
  if ((b_result->size[0] != 0) && (b_result->size[1] != 0)) {
    i1 = b_result->size[0];
  } else {
    i1 = 0;
  }
  gradeq->size[0] = i1 + 32;
  gradeq->size[1] = sizes_idx_1;
  emxEnsureCapacity_real_T(gradeq, i);
  eq_data = gradeq->data;
  for (i = 0; i < sizes_idx_1; i++) {
    for (i1 = 0; i1 < input_sizes_idx_1; i1++) {
      eq_data[i1 + gradeq->size[0] * i] = Qc_data[i1 + input_sizes_idx_1 * i];
    }
  }
  emxFree_real_T(&b_result);
  for (i = 0; i < sizes_idx_1; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      eq_data[(i1 + input_sizes_idx_1) + gradeq->size[0] * i] =
          dplatangerrdqei_data[i1 + 6 * i];
    }
  }
  emxFree_real_T(&varargin_2);
  for (i = 0; i < sizes_idx_1; i++) {
    for (i1 = 0; i1 < 20; i1++) {
      eq_data[((i1 + input_sizes_idx_1) + gradeq->size[0] * i) + 6] =
          dQcdLvi_data[i1 + 20 * i];
    }
  }
  emxFree_real_T(&b_dconstrdqa);
  for (i = 0; i < sizes_idx_1; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      eq_data[((i1 + input_sizes_idx_1) + gradeq->size[0] * i) + 26] =
          dwrenchdqe_data[i1 + 6 * i];
    }
  }
  emxFree_real_T(&dwrenchdqe);
}

/* End of code generation (inverse_UniboCPR.c) */
