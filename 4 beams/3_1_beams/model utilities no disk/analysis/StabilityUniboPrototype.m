function flag = StabilityUniboPrototype(sol,jac,Nf,nj,params)
Nftot = 3*Nf;
%% First set of eqns: beams
rotparams = params.rotparams;
eul = sol(1+6+4*Nftot+3:6+4*Nftot+6,1);
D = rot2twist(eul,rotparams);
jac(1+4*Nftot+3:4*Nftot+6,:) = D'*jac(1+4*Nftot+3:4*Nftot+6,:);

% beam eq and plat eq
U1 = jac(1:4*Nftot+6,1+6:6+4*Nftot); % wrt elastic coordinate of beams
P1 = jac(1:4*Nftot+6,1+6+4*Nftot:6+4*Nftot+6); % wrt plat vars
G1 = jac(1:4*Nftot+6,1+6+4*Nftot+6:6+4*Nftot+6+3+3*nj); % wrt multipliers

%% STABILITY
H = [U1,P1];
Z = null(G1');
Hr = Z'*H*Z;

[~,flag] = chol(Hr);

end