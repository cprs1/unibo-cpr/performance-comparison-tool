%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% MAIN FILE
clear
close all
clc

%% ROBOT PARAMETERS
FILE_UniboCPR_robotfile_transl

%% SIMULATION PARAMETERS
FILE_inputfile

%%
instantplot = 0; % set 1 to see how it works during computation
[WK,outstruct] = FCN_BoundaryFloodingSpatialWK(fcn,params,instantplot);

% % symmetry
% 
% WK1 = [WK(:,1),-WK(:,2),WK(:,3:7)];
% WK2 = [WK;WK1];
% WK3 = [WK2(:,1:2),-WK2(:,3),WK2(:,4:7)];
% WK4 = [WK2;WK3];
% 
% id = numel(WK4(:,1));
% arr = 1:1:id;
% WK4(:,1) = arr;
% 
% WK = WK4;

%%

volumetot = FCN_plot_space(WK,params);

%% slice

slice_coord = 0.55;
str = 'XY';

PlotSlice(WK,str,slice_coord);
