%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to plot a workspace slice
% INPUT:
% WK_full: 3d workspace
% str: string to define kind of slice (XY,XZ,YZ)
% slice coordinate: coordinate of the slice to plot

% OUTPUT: 
% h: plot identifier to clear the plot if necessary

function h = PlotSlice(WK_full,str,slice_coordinate)

% slice coordinate: if a XY is desired, select the Z coordinateS
switch str
    case 'XY'
        idslice = 4;
        id1 = 2;
        id2 = 3;
    case 'XZ'
        idslice = 3;
        id1 = 2;
        id2 = 4;
    case 'YZ'
        idslice = 2;
        id1 = 3;
        id2 = 4;
    otherwise
        error('Uncorrect Slice Formatting')        
end

%% find the correct slice among the WK
coord_values = WK_full(:,idslice);
[~,id] = min(abs(coord_values-slice_coordinate));
correct_coordinate = WK_full(id,idslice);
idok = (WK_full(:,idslice) == correct_coordinate);
WK = WK_full(idok==1,:);
%% NUMERICAL RESULT

idwk = (WK(:,5) == 1 & WK(:,6) ~=0);
idT1 = (WK(:,5) == 2) | (WK(:,5) == 3) | (WK(:,5) == 4);
idT2 = (WK(:,5) == 5) | (WK(:,5) == 6);
idmech = (WK(:,5) == 7);

% figure()
h1 = plot(WK(idwk,id1),WK(idwk,id2),'b.');
hold on
h2 = plot(WK(idT1,id1),WK(idT1,id2),'r.');
h3 = plot(WK(idT2,id1),WK(idT2,id2),'k.');
h4 = plot(WK(idmech,id1),WK(idmech,id2),'g.');

grid on
axis equal
axis([-1 1 -1 1])
title('Slice - Workspace Computation')

h = [h1;h2;h3;h4];
end