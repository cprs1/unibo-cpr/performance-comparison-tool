%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to get plots

% INPUT:
% WK: array to containt wk points
% parameters: structure with simulation parameters
% n_rad: number of radiating directions


function volumetot = FCN_plot_space(WK,parameters)


boxsize = parameters.boxsize;
n_rad = parameters.n_rad;

%% NUMERICAL RESULT

idwk = (WK(:,5) == 1 );
idT1 = (WK(:,5) == 2) | (WK(:,5) == 3) | (WK(:,5) == 4);
idT2 = (WK(:,5) == 5) | (WK(:,5) == 6);
idmech = (WK(:,5) == 7);

figure()
plot3(WK(idwk,2),WK(idwk,3),WK(idwk,4),'b.')
hold on
plot3(WK(idT1,2),WK(idT1,3),WK(idT1,4),'r.')
plot3(WK(idT2,2),WK(idT2,3),WK(idT2,4),'k.')
plot3(WK(idmech,2),WK(idmech,3),WK(idmech,4),'g.')

xlabel('x_{[m]}')
ylabel('y_{[m]}')
zlabel('z_{[m]}')
grid on
axis equal
axis(boxsize)
title('Workspace Computation')

%% REFINED BOUNDARY
volumep = zeros(n_rad,1);
figure()
hold on
for i = 1:n_rad
    idwk_i = (WK(:,5) == 1 & WK(:,6) == i );
    points = [WK(idwk_i,2),WK(idwk_i,3),WK(idwk_i,4)];
    [k,volumep(i)] = boundary(points(:,1),points(:,2),points(:,3),0.99);
    trisurf(k,points(:,1),points(:,2),points(:,3),'Facecolor','cyan','FaceAlpha',0.5)
end
xlabel('x_{[m]}')
ylabel('y_{[m]}')
grid on
axis equal
axis(boxsize)
title('Workspace Boundary')

volumep = flip(sort(volumep));
if numel(volumep)>1
    volumetot = volumep(1)-sum(volumep(2:end));
else
    volumetot = volumep;
end
end