%% Robot Geometry Parameters

%% PARAMETERS

[geometry,params,Kbt_1,Kbt_2] = createRobotParams();
params.g = [0;0;0]; % gravity
params.tipwrench = [zeros(3,1);0;0;-10]; % tip wrench
[~,nj] = size(geometry.jointmatr);  

%% INVERSE PROBLEM
xP = 0; yP = 0; zP = 0.2; alpha = 0*pi/180; beta = 0*pi/180; gamma = 0*pi/180;
qpv = [xP;yP;zP;alpha;beta;gamma];

%% MODEL PARAMETERS
Nf = 4;
params.Nf = Nf;
M = @(s) PhiMatr(s,1,Nf);
Kad1 = integral(@(s) M(s)'*Kbt_1*M(s),0,1,'ArrayValued',true);
Kad2 = integral(@(s) M(s)'*Kbt_2*M(s),0,1,'ArrayValued',true);
params.rotparams = 'XYZ';

hp = [alpha,beta,gamma]';

pstart = [0;0;0.2];
params.pstart = pstart;


%% Simulation functions
fcn.objetivefcn = @(y,pend) inverse_UniboCPR_mex(y,geometry,params,Kad2,[pend;hp]);
fcn.mechconstrfcn = @(y) mechconstrUniboCPR(y,params.actlims,params.displims,geometry.beamradius,params.Nf,params.maxstrain);
fcn.singufcn = @(jac,y) SingularitiesUniboPrototype(y,jac,geometry,params);
fcn.stabilityfcn = @(jac,y) StabilityUniboPrototype(y,jac,Nf,nj,params);


%% First Initial Guess

qa0 = [0;0;0.4;0.4;0.4;0.4];
qe0 = repmat([.5;zeros(3*Nf-1,1)],4,1);
qp0 = qpv;
lambda0 = zeros(4*nj,1);
guess0 = [qa0;qe0;qp0;lambda0];

options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',false);
fun = @(guess) inverse_UniboCPR(guess,geometry,params,Kad2,qpv);
[sol,~,~,~,jac] = fsolve(fun,guess0,options);
params.y0 = sol;

%% TUBE POSITION COMPUTATION
qa0 = sol(1:2);
qt0 = repmat([5.2;zeros(3*Nf-1,1)],4,1);
lambdat0 = zeros(4*5,1);
guesst0 = [qa0;qt0;lambdat0];

funt = @(guess) tubes_UniboCPR(guess,geometry,params,Kad1,sol(1:2));

options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',false);

[solt,~,~,~,jact] = fsolve(funt,guesst0,options);

%% PLOT & STRAIN RECOVERY
h = plotUniboCPR(sol,solt,geometry,Nf,100,geometry.hr);


drawnow

