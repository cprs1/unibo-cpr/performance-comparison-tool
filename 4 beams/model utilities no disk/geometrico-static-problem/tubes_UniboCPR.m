function [eq,gradeq] = tubes_UniboCPR(guess,geometry,params,Kad,pb)

% extract parameters
Nf = params.Nf;
Nftot = 3*params.Nf;
Ci = geometry.basematr;
[~,nj] = size(Ci);
hr = geometry.hr;
basepoints = geometry.basepoints;
baseangles = geometry.baseangles;
platangles = geometry.platangles;
platpoints = geometry.platpoints;
g = params.g;
wd = [zeros(3,1);g*params.fg2];

% extract variables
qa = guess(1:2,1);
qe = guess(1+2:2+4*Nftot,1);
lambda = guess(1+2+4*Nftot:2+4*Nftot+4*nj,1);

pplat = [qa;hr];

L = geometry.L_tube;


% initialize
beameq = zeros(4*Nftot,1);
dbeameqdqe = zeros(4*Nftot,4*Nftot);
dbeameqdlambda = zeros(4*Nftot,4*nj);

constr = zeros(4*nj,1);
dconstrdqe = zeros(4*nj,4*Nftot);

for i = 1:4
    % extract variables of the th-i beam
    qei = qe(1+Nftot*(i-1):Nftot*i,1);
    lambdai = lambda(1+nj*(i-1):nj*i,1);
    p1 = basepoints(:,i);
    h1 = eul2quat(baseangles(:,i)','XYZ')';
    twrench = (Ci*lambdai);
    [Rj,dRpda,dRpdb,dRpdc] = rotationParametrization(platangles(:,i),'XYZ');

    % forward recursion
    [p2,h2,~,~,dp2dqei,dh2dqei]  = forwardRecursion(p1,h1,qei,L,Nf);
    dp2dqei = reshape(dp2dqei,3,Nftot);
    dh2dqei = reshape(dh2dqei,4,Nftot);
    R2 = quat2rotmatrix(h2);
    % backward recursion
    [~,Qc,~,~,~,dQcdqei,~,dQcdw0i] = backwardRecursion(twrench,qei,Nf,L,wd);
    dQcdqei = reshape(dQcdqei,Nftot,Nftot);
    dQcdw0i = reshape(dQcdw0i,Nftot,6);

    %% BEAM EQUILIBRIUMS
    beameq(1+Nftot*(i-1):Nftot*i,1) = L*Kad*qei + Qc;
    dbeameqdqe(1+Nftot*(i-1):Nftot*i,1+Nftot*(i-1):Nftot*i) = L*Kad+dQcdqei;
    dbeameqdlambda(1+Nftot*(i-1):Nftot*i,1+nj*(i-1):nj*i) = dQcdw0i*Ci;

    %% CONSTRAINTS
    pp = platpoints(:,i); % to check if points are assembled and rotated ok
    platconstr = p2-(pplat +pp);
    dplatconstrdqei = dp2dqei;
    dplatconstrdqa = -[eye(2);zeros(1,2)];
    
    [platangerr,~,dplatangerrdqei,~] = roterr(Rj,R2,h2,zeros(4,1),dh2dqei,dRpda,dRpdb,dRpdc);
    constr(1+nj*(i-1):nj*i,1) = Ci'*[platangerr;platconstr;];
    dconstrdqe(1+nj*(i-1):nj*i,1+Nftot*(i-1):Nftot*i) =  Ci'*[dplatangerrdqei;dplatconstrdqei;];
    dconstrdqa(1+nj*(i-1):nj*i,:) = Ci'*[zeros(3,2);dplatconstrdqa];

end


%% COLLECT
inv = qa-pb;
eq = [beameq;constr;inv];

gradeq = [zeros(4*Nftot,2), dbeameqdqe, dbeameqdlambda;
          dconstrdqa, dconstrdqe, zeros(4*nj,4*nj);
          eye(2), zeros(2,4*Nftot), zeros(2,4*nj)];

end
