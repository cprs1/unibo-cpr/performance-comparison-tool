/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * inverse_UniboCPR_data.h
 *
 * Code generation for function 'inverse_UniboCPR_data'
 *
 */

#ifndef INVERSE_UNIBOCPR_DATA_H
#define INVERSE_UNIBOCPR_DATA_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

/* Variable Declarations */
extern const double dv[21];
extern const double dv1[6];
extern const double dv2[7];
extern boolean_T isInitialized_inverse_UniboCPR;

#endif
/* End of code generation (inverse_UniboCPR_data.h) */
