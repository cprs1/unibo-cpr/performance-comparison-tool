/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * explicitRungeKutta.c
 *
 * Code generation for function 'explicitRungeKutta'
 *
 */

/* Include files */
#include "explicitRungeKutta.h"
#include "inverse_UniboCPR_types.h"
#include "rt_nonfinite.h"
#include "rt_nonfinite.h"
#include <math.h>
#include <string.h>

/* Function Definitions */
double maxScaledError(const emxArray_real_T *fE, const emxArray_real_T *y,
                      const emxArray_real_T *ynew)
{
  const double *fE_data;
  const double *y_data;
  const double *ynew_data;
  double d1;
  double d2;
  double mxerr;
  double num;
  int k;
  int n;
  ynew_data = ynew->data;
  y_data = y->data;
  fE_data = fE->data;
  n = fE->size[0];
  mxerr = 0.0;
  for (k = 0; k < n; k++) {
    num = fabs(fE_data[k]);
    d1 = fabs(y_data[k]);
    d2 = fabs(ynew_data[k]);
    if ((d1 > d2) || rtIsNaN(d2)) {
      if (d1 > 0.001) {
        num /= d1;
      } else {
        num /= 0.001;
      }
    } else if (d2 > 0.001) {
      num /= d2;
    } else {
      num /= 0.001;
    }
    if ((num > mxerr) || rtIsNaN(num)) {
      mxerr = num;
    }
  }
  return mxerr;
}

/* End of code generation (explicitRungeKutta.c) */
