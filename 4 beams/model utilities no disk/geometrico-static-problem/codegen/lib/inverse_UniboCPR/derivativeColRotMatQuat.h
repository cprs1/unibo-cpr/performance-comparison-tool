/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * derivativeColRotMatQuat.h
 *
 * Code generation for function 'derivativeColRotMatQuat'
 *
 */

#ifndef DERIVATIVECOLROTMATQUAT_H
#define DERIVATIVECOLROTMATQUAT_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void derivativeColRotMatQuat(const double h[4], double D1[12], double D2[12],
                             double D3[12]);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (derivativeColRotMatQuat.h) */
