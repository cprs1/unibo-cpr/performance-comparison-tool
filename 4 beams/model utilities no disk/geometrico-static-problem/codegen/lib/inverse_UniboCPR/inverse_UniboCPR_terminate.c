/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * inverse_UniboCPR_terminate.c
 *
 * Code generation for function 'inverse_UniboCPR_terminate'
 *
 */

/* Include files */
#include "inverse_UniboCPR_terminate.h"
#include "inverse_UniboCPR_data.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */
void inverse_UniboCPR_terminate(void)
{
  isInitialized_inverse_UniboCPR = false;
}

/* End of code generation (inverse_UniboCPR_terminate.c) */
