/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * backwardRecursion.c
 *
 * Code generation for function 'backwardRecursion'
 *
 */

/* Include files */
#include "backwardRecursion.h"
#include "OdefunAssumeBackward.h"
#include "PhiMatr.h"
#include "explicitRungeKutta.h"
#include "inverse_UniboCPR_data.h"
#include "inverse_UniboCPR_emxutil.h"
#include "inverse_UniboCPR_rtwutil.h"
#include "inverse_UniboCPR_types.h"
#include "mtimes.h"
#include "ntrp45.h"
#include "rt_nonfinite.h"
#include "rt_nonfinite.h"
#include <math.h>
#include <string.h>

/* Variable Definitions */
static const signed char iv[9] = {0, 1, 0, -1, 0, 0, 0, 0, 0};

static const signed char iv1[18] = {1, 0, 0, 0, 0, 0, 0, 1, 0,
                                    0, 0, 0, 0, 0, 1, 0, 0, 0};

/* Function Declarations */
static void b_backwardRecursion_anonFcn1(const double qe_data[], double Nf,
                                         double L, double s,
                                         const emxArray_real_T *y,
                                         emxArray_real_T *varargout_1);

static void backwardRecursion_anonFcn1(const double qe_data[], double Nf,
                                       double L, const emxArray_real_T *y,
                                       emxArray_real_T *varargout_1);

/* Function Definitions */
static void b_backwardRecursion_anonFcn1(const double qe_data[], double Nf,
                                         double L, double s,
                                         const emxArray_real_T *y,
                                         emxArray_real_T *varargout_1)
{
  emxArray_real_T c_y;
  emxArray_real_T *A;
  emxArray_real_T *C;
  emxArray_real_T *Phi;
  emxArray_real_T *b_C;
  emxArray_real_T *b_L;
  emxArray_real_T *b_Phi;
  emxArray_real_T *b_y;
  emxArray_real_T *b_y_tmp;
  emxArray_real_T *c_C;
  emxArray_real_T *r;
  emxArray_real_T *r2;
  double adxi[36];
  double b_B[36];
  double y_tmp[36];
  double sk[9];
  double B[6];
  double c_L[6];
  const double *y_data;
  double Nftot;
  double d;
  double k_idx_0;
  double k_idx_1;
  double k_idx_2;
  double *A_data;
  double *C_data;
  double *Phi_data;
  double *b_C_data;
  double *c_C_data;
  double *r1;
  double *r3;
  double *y_tmp_data;
  int b_iv[2];
  int b_iv1[2];
  int aoffset;
  int b_i;
  int boffset;
  int i;
  int i1;
  int i2;
  int i3;
  int i4;
  int i5;
  int inner;
  int j;
  int k;
  y_data = y->data;
  /*  BACKWARD ODEs of a single beam */
  /*  for assumed strain mode approach */
  Nftot = 3.0 * Nf;
  if (Nftot + 7.0 > (Nftot + 6.0) + 6.0) {
    i = 0;
    i1 = -1;
  } else {
    i = (int)(Nftot + 7.0) - 1;
    i1 = (int)((Nftot + 6.0) + 6.0) - 1;
  }
  d = ((Nftot + 7.0) + 6.0) + Nftot;
  k_idx_0 = ((Nftot + 6.0) + 6.0) + Nftot;
  k_idx_1 = k_idx_0 + 6.0 * Nftot;
  if (d > k_idx_1) {
    i2 = 0;
    i3 = 0;
  } else {
    i2 = (int)d - 1;
    i3 = (int)k_idx_1;
  }
  k_idx_1 = Nftot * (Nftot + 6.0);
  d += k_idx_1;
  k_idx_0 = (k_idx_0 + k_idx_1) + 36.0;
  if (d > k_idx_0) {
    i4 = 0;
    i5 = 0;
  } else {
    i4 = (int)d - 1;
    i5 = (int)k_idx_0;
  }
  emxInit_real_T(&Phi, 2);
  PhiMatr(s, Nf, Phi);
  Phi_data = Phi->data;
  inner = Phi->size[1];
  k_idx_0 = 0.0;
  k_idx_1 = 0.0;
  k_idx_2 = 0.0;
  for (k = 0; k < inner; k++) {
    aoffset = k * 3;
    d = qe_data[k];
    k_idx_0 += Phi_data[aoffset] * d;
    k_idx_1 += Phi_data[aoffset + 1] * d;
    k_idx_2 += Phi_data[aoffset + 2] * d;
  }
  sk[0] = 0.0;
  sk[3] = -k_idx_2;
  sk[6] = k_idx_1;
  sk[1] = k_idx_2;
  sk[4] = 0.0;
  sk[7] = -k_idx_0;
  sk[2] = -k_idx_1;
  sk[5] = k_idx_0;
  sk[8] = 0.0;
  for (j = 0; j < 3; j++) {
    d = sk[3 * j];
    adxi[6 * j] = d;
    aoffset = 6 * (j + 3);
    adxi[aoffset] = 0.0;
    adxi[6 * j + 3] = iv[3 * j];
    adxi[aoffset + 3] = d;
    inner = 3 * j + 1;
    d = sk[inner];
    adxi[6 * j + 1] = d;
    adxi[aoffset + 1] = 0.0;
    adxi[6 * j + 4] = iv[inner];
    adxi[aoffset + 4] = d;
    inner = 3 * j + 2;
    d = sk[inner];
    adxi[6 * j + 2] = d;
    adxi[aoffset + 2] = 0.0;
    adxi[6 * j + 5] = iv[inner];
    adxi[aoffset + 5] = d;
  }
  /*  these infos should be provided by forward integration */
  /*  ys = deval(solf,s); */
  /*  hs = ys(4:7,1); */
  /*  Rs = quat2rotmatrix(hs); */
  /*  Adgfinv = [Rs', zeros(3) ; zeros(3), Rs']; */
  /*  wd = Adgfinv*wd;  */
  /*  you need it also for the derivatives of the gravitational terms for the */
  /*  jacobian */
  /*   */
  for (j = 0; j < 6; j++) {
    for (inner = 0; inner < 6; inner++) {
      y_tmp[inner + 6 * j] = adxi[j + 6 * inner];
    }
  }
  inner = Phi->size[1];
  emxInit_real_T(&r, 2);
  j = r->size[0] * r->size[1];
  r->size[0] = 6;
  r->size[1] = Phi->size[1];
  emxEnsureCapacity_real_T(r, j);
  r1 = r->data;
  for (j = 0; j < inner; j++) {
    aoffset = j * 6;
    boffset = j * 3;
    for (b_i = 0; b_i < 6; b_i++) {
      r1[aoffset + b_i] = ((double)iv1[b_i] * Phi_data[boffset] +
                           (double)iv1[b_i + 6] * Phi_data[boffset + 1]) +
                          (double)iv1[b_i + 12] * Phi_data[boffset + 2];
    }
  }
  emxInit_real_T(&b_y_tmp, 2);
  j = b_y_tmp->size[0] * b_y_tmp->size[1];
  b_y_tmp->size[0] = r->size[1];
  b_y_tmp->size[1] = 6;
  emxEnsureCapacity_real_T(b_y_tmp, j);
  y_tmp_data = b_y_tmp->data;
  boffset = r->size[1];
  for (j = 0; j < 6; j++) {
    for (inner = 0; inner < boffset; inner++) {
      y_tmp_data[inner + b_y_tmp->size[0] * j] = r1[j + 6 * inner];
    }
  }
  emxInit_real_T(&A, 2);
  j = A->size[0] * A->size[1];
  A->size[0] = b_y_tmp->size[0];
  A->size[1] = 6;
  emxEnsureCapacity_real_T(A, j);
  A_data = A->data;
  boffset = b_y_tmp->size[0] * 6;
  for (j = 0; j < boffset; j++) {
    A_data[j] = L * y_tmp_data[j];
  }
  inner = A->size[0];
  emxInit_real_T(&C, 1);
  j = C->size[0];
  C->size[0] = A->size[0];
  emxEnsureCapacity_real_T(C, j);
  C_data = C->data;
  for (b_i = 0; b_i < inner; b_i++) {
    k_idx_0 = 0.0;
    for (k = 0; k < 6; k++) {
      k_idx_0 += A_data[k * A->size[0] + b_i] * y_data[k];
    }
    C_data[b_i] = k_idx_0;
  }
  boffset = i1 - i;
  if (boffset + 1 == 6) {
    for (i1 = 0; i1 <= boffset; i1++) {
      B[i1] = L * y_data[i + i1] + y_data[i1];
    }
  } else {
    g_binary_expand_op(B, L, y, i, i1);
  }
  inner = r->size[1];
  emxInit_real_T(&b_C, 1);
  i1 = b_C->size[0];
  b_C->size[0] = r->size[1];
  emxEnsureCapacity_real_T(b_C, i1);
  b_C_data = b_C->data;
  for (b_i = 0; b_i < inner; b_i++) {
    aoffset = b_i * 6;
    k_idx_0 = 0.0;
    for (k = 0; k < 6; k++) {
      k_idx_0 += r1[aoffset + k] * B[k];
    }
    b_C_data[b_i] = k_idx_0;
  }
  i1 = A->size[0] * A->size[1];
  A->size[0] = b_y_tmp->size[0];
  A->size[1] = 6;
  emxEnsureCapacity_real_T(A, i1);
  A_data = A->data;
  boffset = b_y_tmp->size[0] * 6;
  for (i1 = 0; i1 < boffset; i1++) {
    A_data[i1] = L * y_tmp_data[i1];
  }
  for (i1 = 0; i1 < 36; i1++) {
    b_B[i1] = y_data[i4 + i1];
  }
  inner = A->size[0];
  emxInit_real_T(&c_C, 2);
  i1 = c_C->size[0] * c_C->size[1];
  c_C->size[0] = A->size[0];
  c_C->size[1] = 6;
  emxEnsureCapacity_real_T(c_C, i1);
  c_C_data = c_C->data;
  for (j = 0; j < 6; j++) {
    aoffset = j * inner;
    boffset = j * 6;
    for (b_i = 0; b_i < inner; b_i++) {
      k_idx_0 = 0.0;
      for (k = 0; k < 6; k++) {
        k_idx_0 += A_data[k * A->size[0] + b_i] * b_B[boffset + k];
      }
      c_C_data[aoffset + b_i] = k_idx_0;
    }
  }
  emxInit_real_T(&b_y, 1);
  boffset = i3 - i2;
  i1 = b_y->size[0];
  b_y->size[0] = boffset;
  emxEnsureCapacity_real_T(b_y, i1);
  A_data = b_y->data;
  for (i1 = 0; i1 < boffset; i1++) {
    A_data[i1] = y_data[i2 + i1];
  }
  c_y = *b_y;
  b_iv[0] = 6;
  b_iv[1] = (int)Nftot;
  c_y.size = &b_iv[0];
  c_y.numDimensions = 2;
  b_mtimes(adxi, &c_y, r);
  r1 = r->data;
  i1 = b_y->size[0];
  b_y->size[0] = boffset;
  emxEnsureCapacity_real_T(b_y, i1);
  A_data = b_y->data;
  for (i1 = 0; i1 < boffset; i1++) {
    A_data[i1] = y_data[i2 + i1];
  }
  i1 = A->size[0] * A->size[1];
  A->size[0] = b_y_tmp->size[0];
  A->size[1] = 6;
  emxEnsureCapacity_real_T(A, i1);
  A_data = A->data;
  boffset = b_y_tmp->size[0] * 6;
  for (i1 = 0; i1 < boffset; i1++) {
    A_data[i1] = L * y_tmp_data[i1];
  }
  emxFree_real_T(&b_y_tmp);
  c_y = *b_y;
  b_iv1[0] = 6;
  b_iv1[1] = (int)Nftot;
  c_y.size = &b_iv1[0];
  c_y.numDimensions = 2;
  emxInit_real_T(&r2, 2);
  c_mtimes(A, &c_y, r2);
  r3 = r2->data;
  emxFree_real_T(&A);
  if (Phi->size[1] == 1) {
    i1 = r->size[1];
  } else {
    i1 = Phi->size[1];
  }
  if ((Phi->size[1] == r->size[1]) && (i1 == (int)Nftot)) {
    for (i1 = 0; i1 < 6; i1++) {
      d = 0.0;
      for (i2 = 0; i2 < 6; i2++) {
        d += y_tmp[i1 + 6 * i2] * y_data[i + i2];
      }
      B[i1] = d;
    }
    emxInit_real_T(&b_Phi, 2);
    i = b_Phi->size[0] * b_Phi->size[1];
    b_Phi->size[0] = 6;
    b_Phi->size[1] = Phi->size[1];
    emxEnsureCapacity_real_T(b_Phi, i);
    A_data = b_Phi->data;
    boffset = Phi->size[1];
    for (i = 0; i < boffset; i++) {
      A_data[6 * i] =
          Phi_data[3 * i + 2] * y_data[1] - Phi_data[3 * i + 1] * y_data[2];
    }
    boffset = Phi->size[1];
    for (i = 0; i < boffset; i++) {
      A_data[6 * i + 1] =
          -Phi_data[3 * i + 2] * y_data[0] + Phi_data[3 * i] * y_data[2];
    }
    boffset = Phi->size[1];
    for (i = 0; i < boffset; i++) {
      A_data[6 * i + 2] =
          Phi_data[3 * i + 1] * y_data[0] - Phi_data[3 * i] * y_data[1];
    }
    boffset = Phi->size[1];
    for (i = 0; i < boffset; i++) {
      A_data[6 * i + 3] =
          Phi_data[3 * i + 2] * y_data[4] - Phi_data[3 * i + 1] * y_data[5];
    }
    boffset = Phi->size[1];
    for (i = 0; i < boffset; i++) {
      A_data[6 * i + 4] =
          -Phi_data[3 * i + 2] * y_data[3] + Phi_data[3 * i] * y_data[5];
    }
    boffset = Phi->size[1];
    for (i = 0; i < boffset; i++) {
      A_data[6 * i + 5] =
          Phi_data[3 * i + 1] * y_data[3] - Phi_data[3 * i] * y_data[4];
    }
    emxInit_real_T(&b_L, 2);
    i = b_L->size[0] * b_L->size[1];
    b_L->size[0] = 6;
    b_L->size[1] = b_Phi->size[1];
    emxEnsureCapacity_real_T(b_L, i);
    y_tmp_data = b_L->data;
    boffset = 6 * b_Phi->size[1];
    for (i = 0; i < boffset; i++) {
      y_tmp_data[i] = L * (A_data[i] + r1[i]);
    }
    emxFree_real_T(&b_Phi);
    inner = (int)(6.0 * Nftot);
    aoffset = (int)(Nftot * Nftot);
    boffset = i5 - i4;
    i = b_y->size[0];
    b_y->size[0] = boffset;
    emxEnsureCapacity_real_T(b_y, i);
    A_data = b_y->data;
    for (i = 0; i < boffset; i++) {
      A_data[i] = y_data[i4 + i];
    }
    for (i = 0; i < 6; i++) {
      for (i1 = 0; i1 < 6; i1++) {
        d = 0.0;
        for (i2 = 0; i2 < 6; i2++) {
          d += y_tmp[i + 6 * i2] * A_data[i2 + 6 * i1];
        }
        adxi[i + 6 * i1] = d;
      }
    }
    for (i = 0; i < 36; i++) {
      adxi[i] *= L;
    }
    i = varargout_1->size[0];
    varargout_1->size[0] =
        ((((C->size[0] + b_C->size[0]) + inner) + aoffset) + inner) + 48;
    emxEnsureCapacity_real_T(varargout_1, i);
    A_data = varargout_1->data;
    for (i = 0; i < 6; i++) {
      d = 0.0;
      k_idx_0 = 0.0;
      for (i1 = 0; i1 < 6; i1++) {
        k_idx_1 = y_tmp[i + 6 * i1];
        d += L * k_idx_1 * y_data[i1];
        k_idx_0 += k_idx_1 * y_data[i1];
      }
      c_L[i] = L * B[i] + k_idx_0;
      A_data[i] = d;
    }
    boffset = C->size[0];
    for (i = 0; i < boffset; i++) {
      A_data[i + 6] = C_data[i];
    }
    for (i = 0; i < 6; i++) {
      A_data[(i + C->size[0]) + 6] = c_L[i];
    }
    boffset = b_C->size[0];
    for (i = 0; i < boffset; i++) {
      A_data[(i + C->size[0]) + 12] = b_C_data[i];
    }
    for (i = 0; i < inner; i++) {
      A_data[((i + C->size[0]) + b_C->size[0]) + 12] = y_tmp_data[i];
    }
    emxFree_real_T(&b_L);
    for (i = 0; i < aoffset; i++) {
      A_data[(((i + C->size[0]) + b_C->size[0]) + inner) + 12] = r3[i];
    }
    for (i = 0; i < 36; i++) {
      A_data[((((i + C->size[0]) + b_C->size[0]) + inner) + aoffset) + 12] =
          adxi[i];
    }
    for (i = 0; i < inner; i++) {
      A_data[((((i + C->size[0]) + b_C->size[0]) + inner) + aoffset) + 48] =
          c_C_data[i];
    }
  } else {
    h_binary_expand_op(varargout_1, L, y_tmp, y, C, i, b_C, Phi, r, Nftot, r2,
                       i4, i5 - 1, c_C);
  }
  emxFree_real_T(&b_y);
  emxFree_real_T(&r2);
  emxFree_real_T(&c_C);
  emxFree_real_T(&b_C);
  emxFree_real_T(&C);
  emxFree_real_T(&r);
  emxFree_real_T(&Phi);
}

static void backwardRecursion_anonFcn1(const double qe_data[], double Nf,
                                       double L, const emxArray_real_T *y,
                                       emxArray_real_T *varargout_1)
{
  emxArray_int8_T *Phi;
  emxArray_real_T c_y;
  emxArray_real_T *A;
  emxArray_real_T *C;
  emxArray_real_T *b_C;
  emxArray_real_T *b_L;
  emxArray_real_T *b_Phi;
  emxArray_real_T *b_y;
  emxArray_real_T *b_y_tmp;
  emxArray_real_T *c_C;
  emxArray_real_T *r;
  emxArray_real_T *r2;
  double adxi[36];
  double b_B[36];
  double y_tmp[36];
  double sk[9];
  double B[6];
  double c_L[6];
  const double *y_data;
  double Nftot;
  double d;
  double k_idx_0;
  double k_idx_1;
  double k_idx_2;
  double *A_data;
  double *C_data;
  double *L_data;
  double *b_C_data;
  double *c_C_data;
  double *r1;
  double *y_tmp_data;
  int b_iv[2];
  int b_iv1[2];
  int aoffset;
  int b_i;
  int boffset;
  int i;
  int i1;
  int i2;
  int i3;
  int i4;
  int i5;
  int inner;
  int j;
  int k;
  signed char *Phi_data;
  y_data = y->data;
  /*  BACKWARD ODEs of a single beam */
  /*  for assumed strain mode approach */
  Nftot = 3.0 * Nf;
  if (Nftot + 7.0 > (Nftot + 6.0) + 6.0) {
    i = 0;
    i1 = -1;
  } else {
    i = (int)(Nftot + 7.0) - 1;
    i1 = (int)((Nftot + 6.0) + 6.0) - 1;
  }
  d = ((Nftot + 7.0) + 6.0) + Nftot;
  k_idx_0 = ((Nftot + 6.0) + 6.0) + Nftot;
  k_idx_1 = k_idx_0 + 6.0 * Nftot;
  if (d > k_idx_1) {
    i2 = 0;
    i3 = 0;
  } else {
    i2 = (int)d - 1;
    i3 = (int)k_idx_1;
  }
  k_idx_1 = Nftot * (Nftot + 6.0);
  d += k_idx_1;
  k_idx_0 = (k_idx_0 + k_idx_1) + 36.0;
  if (d > k_idx_0) {
    i4 = 0;
    i5 = 0;
  } else {
    i4 = (int)d - 1;
    i5 = (int)k_idx_0;
  }
  /*  vect =   [ones(1,numel(s)) ; */
  /*           s ; */
  /*           s.^2 ;  */
  /*           s.^3; */
  /*           s.^4; */
  /*           s.^5; */
  /*           ]; */
  if (Nf < 1.0) {
    boffset = 0;
  } else {
    boffset = (int)Nf;
  }
  emxInit_int8_T(&Phi);
  j = Phi->size[0] * Phi->size[1];
  Phi->size[0] = 3;
  Phi->size[1] = (boffset + (int)Nf) + (int)Nf;
  emxEnsureCapacity_int8_T(Phi, j);
  Phi_data = Phi->data;
  for (j = 0; j < boffset; j++) {
    Phi_data[3 * j] = 1;
  }
  aoffset = (int)Nf;
  for (j = 0; j < aoffset; j++) {
    Phi_data[3 * (j + boffset)] = 0;
  }
  aoffset = (int)Nf;
  for (j = 0; j < aoffset; j++) {
    Phi_data[3 * ((j + boffset) + (int)Nf)] = 0;
  }
  aoffset = (int)Nf;
  for (j = 0; j < aoffset; j++) {
    Phi_data[3 * j + 1] = 0;
  }
  for (j = 0; j < boffset; j++) {
    Phi_data[3 * (j + (int)Nf) + 1] = 1;
  }
  aoffset = (int)Nf;
  for (j = 0; j < aoffset; j++) {
    Phi_data[3 * ((j + (int)Nf) + boffset) + 1] = 0;
  }
  aoffset = (int)Nf;
  for (j = 0; j < aoffset; j++) {
    Phi_data[3 * j + 2] = 0;
  }
  aoffset = (int)Nf;
  for (j = 0; j < aoffset; j++) {
    Phi_data[3 * (j + (int)Nf) + 2] = 0;
  }
  for (j = 0; j < boffset; j++) {
    Phi_data[3 * ((j + (int)Nf) + (int)Nf) + 2] = 1;
  }
  inner = Phi->size[1];
  k_idx_0 = 0.0;
  k_idx_1 = 0.0;
  k_idx_2 = 0.0;
  for (k = 0; k < inner; k++) {
    aoffset = k * 3;
    d = qe_data[k];
    k_idx_0 += (double)Phi_data[aoffset] * d;
    k_idx_1 += (double)Phi_data[aoffset + 1] * d;
    k_idx_2 += (double)Phi_data[aoffset + 2] * d;
  }
  sk[0] = 0.0;
  sk[3] = -k_idx_2;
  sk[6] = k_idx_1;
  sk[1] = k_idx_2;
  sk[4] = 0.0;
  sk[7] = -k_idx_0;
  sk[2] = -k_idx_1;
  sk[5] = k_idx_0;
  sk[8] = 0.0;
  for (j = 0; j < 3; j++) {
    d = sk[3 * j];
    adxi[6 * j] = d;
    aoffset = 6 * (j + 3);
    adxi[aoffset] = 0.0;
    adxi[6 * j + 3] = iv[3 * j];
    adxi[aoffset + 3] = d;
    inner = 3 * j + 1;
    d = sk[inner];
    adxi[6 * j + 1] = d;
    adxi[aoffset + 1] = 0.0;
    adxi[6 * j + 4] = iv[inner];
    adxi[aoffset + 4] = d;
    inner = 3 * j + 2;
    d = sk[inner];
    adxi[6 * j + 2] = d;
    adxi[aoffset + 2] = 0.0;
    adxi[6 * j + 5] = iv[inner];
    adxi[aoffset + 5] = d;
  }
  /*  these infos should be provided by forward integration */
  /*  ys = deval(solf,s); */
  /*  hs = ys(4:7,1); */
  /*  Rs = quat2rotmatrix(hs); */
  /*  Adgfinv = [Rs', zeros(3) ; zeros(3), Rs']; */
  /*  wd = Adgfinv*wd;  */
  /*  you need it also for the derivatives of the gravitational terms for the */
  /*  jacobian */
  /*   */
  for (j = 0; j < 6; j++) {
    for (inner = 0; inner < 6; inner++) {
      y_tmp[inner + 6 * j] = adxi[j + 6 * inner];
    }
  }
  inner = Phi->size[1];
  emxInit_real_T(&r, 2);
  j = r->size[0] * r->size[1];
  r->size[0] = 6;
  r->size[1] = Phi->size[1];
  emxEnsureCapacity_real_T(r, j);
  r1 = r->data;
  for (j = 0; j < inner; j++) {
    aoffset = j * 6;
    boffset = j * 3;
    for (b_i = 0; b_i < 6; b_i++) {
      r1[aoffset + b_i] = (iv1[b_i] * Phi_data[boffset] +
                           iv1[b_i + 6] * Phi_data[boffset + 1]) +
                          iv1[b_i + 12] * Phi_data[boffset + 2];
    }
  }
  emxInit_real_T(&b_y_tmp, 2);
  j = b_y_tmp->size[0] * b_y_tmp->size[1];
  b_y_tmp->size[0] = r->size[1];
  b_y_tmp->size[1] = 6;
  emxEnsureCapacity_real_T(b_y_tmp, j);
  y_tmp_data = b_y_tmp->data;
  boffset = r->size[1];
  for (j = 0; j < 6; j++) {
    for (inner = 0; inner < boffset; inner++) {
      y_tmp_data[inner + b_y_tmp->size[0] * j] = r1[j + 6 * inner];
    }
  }
  emxInit_real_T(&A, 2);
  j = A->size[0] * A->size[1];
  A->size[0] = b_y_tmp->size[0];
  A->size[1] = 6;
  emxEnsureCapacity_real_T(A, j);
  A_data = A->data;
  boffset = b_y_tmp->size[0] * 6;
  for (j = 0; j < boffset; j++) {
    A_data[j] = L * y_tmp_data[j];
  }
  inner = A->size[0];
  emxInit_real_T(&C, 1);
  j = C->size[0];
  C->size[0] = A->size[0];
  emxEnsureCapacity_real_T(C, j);
  C_data = C->data;
  for (b_i = 0; b_i < inner; b_i++) {
    k_idx_0 = 0.0;
    for (k = 0; k < 6; k++) {
      k_idx_0 += A_data[k * A->size[0] + b_i] * y_data[k];
    }
    C_data[b_i] = k_idx_0;
  }
  boffset = i1 - i;
  if (boffset + 1 == 6) {
    for (i1 = 0; i1 <= boffset; i1++) {
      B[i1] = L * y_data[i + i1] + y_data[i1];
    }
  } else {
    g_binary_expand_op(B, L, y, i, i1);
  }
  inner = r->size[1];
  emxInit_real_T(&b_C, 1);
  i1 = b_C->size[0];
  b_C->size[0] = r->size[1];
  emxEnsureCapacity_real_T(b_C, i1);
  b_C_data = b_C->data;
  for (b_i = 0; b_i < inner; b_i++) {
    aoffset = b_i * 6;
    k_idx_0 = 0.0;
    for (k = 0; k < 6; k++) {
      k_idx_0 += r1[aoffset + k] * B[k];
    }
    b_C_data[b_i] = k_idx_0;
  }
  i1 = A->size[0] * A->size[1];
  A->size[0] = b_y_tmp->size[0];
  A->size[1] = 6;
  emxEnsureCapacity_real_T(A, i1);
  A_data = A->data;
  boffset = b_y_tmp->size[0] * 6;
  for (i1 = 0; i1 < boffset; i1++) {
    A_data[i1] = L * y_tmp_data[i1];
  }
  for (i1 = 0; i1 < 36; i1++) {
    b_B[i1] = y_data[i4 + i1];
  }
  inner = A->size[0];
  emxInit_real_T(&c_C, 2);
  i1 = c_C->size[0] * c_C->size[1];
  c_C->size[0] = A->size[0];
  c_C->size[1] = 6;
  emxEnsureCapacity_real_T(c_C, i1);
  c_C_data = c_C->data;
  for (j = 0; j < 6; j++) {
    aoffset = j * inner;
    boffset = j * 6;
    for (b_i = 0; b_i < inner; b_i++) {
      k_idx_0 = 0.0;
      for (k = 0; k < 6; k++) {
        k_idx_0 += A_data[k * A->size[0] + b_i] * b_B[boffset + k];
      }
      c_C_data[aoffset + b_i] = k_idx_0;
    }
  }
  emxInit_real_T(&b_y, 1);
  boffset = i3 - i2;
  i1 = b_y->size[0];
  b_y->size[0] = boffset;
  emxEnsureCapacity_real_T(b_y, i1);
  A_data = b_y->data;
  for (i1 = 0; i1 < boffset; i1++) {
    A_data[i1] = y_data[i2 + i1];
  }
  c_y = *b_y;
  b_iv[0] = 6;
  b_iv[1] = (int)Nftot;
  c_y.size = &b_iv[0];
  c_y.numDimensions = 2;
  b_mtimes(adxi, &c_y, r);
  r1 = r->data;
  i1 = b_y->size[0];
  b_y->size[0] = boffset;
  emxEnsureCapacity_real_T(b_y, i1);
  A_data = b_y->data;
  for (i1 = 0; i1 < boffset; i1++) {
    A_data[i1] = y_data[i2 + i1];
  }
  i1 = A->size[0] * A->size[1];
  A->size[0] = b_y_tmp->size[0];
  A->size[1] = 6;
  emxEnsureCapacity_real_T(A, i1);
  A_data = A->data;
  boffset = b_y_tmp->size[0] * 6;
  for (i1 = 0; i1 < boffset; i1++) {
    A_data[i1] = L * y_tmp_data[i1];
  }
  emxFree_real_T(&b_y_tmp);
  c_y = *b_y;
  b_iv1[0] = 6;
  b_iv1[1] = (int)Nftot;
  c_y.size = &b_iv1[0];
  c_y.numDimensions = 2;
  emxInit_real_T(&r2, 2);
  c_mtimes(A, &c_y, r2);
  y_tmp_data = r2->data;
  emxFree_real_T(&A);
  if (Phi->size[1] == 1) {
    i1 = r->size[1];
  } else {
    i1 = Phi->size[1];
  }
  if ((Phi->size[1] == r->size[1]) && (i1 == (int)Nftot)) {
    for (i1 = 0; i1 < 6; i1++) {
      d = 0.0;
      for (i2 = 0; i2 < 6; i2++) {
        d += y_tmp[i1 + 6 * i2] * y_data[i + i2];
      }
      B[i1] = d;
    }
    emxInit_real_T(&b_Phi, 2);
    i = b_Phi->size[0] * b_Phi->size[1];
    b_Phi->size[0] = 6;
    b_Phi->size[1] = Phi->size[1];
    emxEnsureCapacity_real_T(b_Phi, i);
    A_data = b_Phi->data;
    boffset = Phi->size[1];
    for (i = 0; i < boffset; i++) {
      A_data[6 * i] = (double)Phi_data[3 * i + 2] * y_data[1] -
                      (double)Phi_data[3 * i + 1] * y_data[2];
    }
    boffset = Phi->size[1];
    for (i = 0; i < boffset; i++) {
      A_data[6 * i + 1] = -(double)Phi_data[3 * i + 2] * y_data[0] +
                          (double)Phi_data[3 * i] * y_data[2];
    }
    boffset = Phi->size[1];
    for (i = 0; i < boffset; i++) {
      A_data[6 * i + 2] = (double)Phi_data[3 * i + 1] * y_data[0] -
                          (double)Phi_data[3 * i] * y_data[1];
    }
    boffset = Phi->size[1];
    for (i = 0; i < boffset; i++) {
      A_data[6 * i + 3] = (double)Phi_data[3 * i + 2] * y_data[4] -
                          (double)Phi_data[3 * i + 1] * y_data[5];
    }
    boffset = Phi->size[1];
    for (i = 0; i < boffset; i++) {
      A_data[6 * i + 4] = -(double)Phi_data[3 * i + 2] * y_data[3] +
                          (double)Phi_data[3 * i] * y_data[5];
    }
    boffset = Phi->size[1];
    for (i = 0; i < boffset; i++) {
      A_data[6 * i + 5] = (double)Phi_data[3 * i + 1] * y_data[3] -
                          (double)Phi_data[3 * i] * y_data[4];
    }
    emxInit_real_T(&b_L, 2);
    i = b_L->size[0] * b_L->size[1];
    b_L->size[0] = 6;
    b_L->size[1] = b_Phi->size[1];
    emxEnsureCapacity_real_T(b_L, i);
    L_data = b_L->data;
    boffset = 6 * b_Phi->size[1];
    for (i = 0; i < boffset; i++) {
      L_data[i] = L * (A_data[i] + r1[i]);
    }
    emxFree_real_T(&b_Phi);
    aoffset = (int)(6.0 * Nftot);
    inner = (int)(Nftot * Nftot);
    boffset = i5 - i4;
    i = b_y->size[0];
    b_y->size[0] = boffset;
    emxEnsureCapacity_real_T(b_y, i);
    A_data = b_y->data;
    for (i = 0; i < boffset; i++) {
      A_data[i] = y_data[i4 + i];
    }
    for (i = 0; i < 6; i++) {
      for (i1 = 0; i1 < 6; i1++) {
        d = 0.0;
        for (i2 = 0; i2 < 6; i2++) {
          d += y_tmp[i + 6 * i2] * A_data[i2 + 6 * i1];
        }
        adxi[i + 6 * i1] = d;
      }
    }
    for (i = 0; i < 36; i++) {
      adxi[i] *= L;
    }
    i = varargout_1->size[0];
    varargout_1->size[0] =
        ((((C->size[0] + b_C->size[0]) + aoffset) + inner) + aoffset) + 48;
    emxEnsureCapacity_real_T(varargout_1, i);
    A_data = varargout_1->data;
    for (i = 0; i < 6; i++) {
      d = 0.0;
      k_idx_0 = 0.0;
      for (i1 = 0; i1 < 6; i1++) {
        k_idx_1 = y_tmp[i + 6 * i1];
        d += L * k_idx_1 * y_data[i1];
        k_idx_0 += k_idx_1 * y_data[i1];
      }
      c_L[i] = L * B[i] + k_idx_0;
      A_data[i] = d;
    }
    boffset = C->size[0];
    for (i = 0; i < boffset; i++) {
      A_data[i + 6] = C_data[i];
    }
    for (i = 0; i < 6; i++) {
      A_data[(i + C->size[0]) + 6] = c_L[i];
    }
    boffset = b_C->size[0];
    for (i = 0; i < boffset; i++) {
      A_data[(i + C->size[0]) + 12] = b_C_data[i];
    }
    for (i = 0; i < aoffset; i++) {
      A_data[((i + C->size[0]) + b_C->size[0]) + 12] = L_data[i];
    }
    emxFree_real_T(&b_L);
    for (i = 0; i < inner; i++) {
      A_data[(((i + C->size[0]) + b_C->size[0]) + aoffset) + 12] =
          y_tmp_data[i];
    }
    for (i = 0; i < 36; i++) {
      A_data[((((i + C->size[0]) + b_C->size[0]) + aoffset) + inner) + 12] =
          adxi[i];
    }
    for (i = 0; i < aoffset; i++) {
      A_data[((((i + C->size[0]) + b_C->size[0]) + aoffset) + inner) + 48] =
          c_C_data[i];
    }
  } else {
    f_binary_expand_op(varargout_1, L, y_tmp, y, C, i, b_C, Phi, r, Nftot, r2,
                       i4, i5 - 1, c_C);
  }
  emxFree_real_T(&b_y);
  emxFree_real_T(&r2);
  emxFree_real_T(&c_C);
  emxFree_real_T(&b_C);
  emxFree_real_T(&C);
  emxFree_real_T(&r);
  emxFree_int8_T(&Phi);
}

void backwardRecursion(const double twrench[6], const double qe_data[],
                       double Nf, double L, double wbase[6],
                       emxArray_real_T *Qc, emxArray_real_T *dwbasedqa,
                       emxArray_real_T *dQcdqa, emxArray_real_T *dwbasedqe,
                       emxArray_real_T *dQcdqe, emxArray_real_T *dwbasedw0,
                       emxArray_real_T *dQcdw0)
{
  static const signed char b_iv[36] = {1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
                                       0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0,
                                       0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1};
  emxArray_real_T *f;
  emxArray_real_T *f0;
  emxArray_real_T *tout;
  emxArray_real_T *varargin_1;
  emxArray_real_T *y;
  emxArray_real_T *y2B;
  emxArray_real_T *ynew;
  emxArray_real_T *yout;
  emxArray_real_T *youtnew;
  double toutnew[4];
  double tref[3];
  double Nftot;
  double absh;
  double absx;
  double d;
  double hmin;
  double rh;
  double t;
  double tnew;
  double *f0_data;
  double *f_data;
  double *tout_data;
  double *y2B_data;
  double *ynew_data;
  double *yout_data;
  int Bcolidx;
  int c;
  int exitg1;
  int exitg2;
  int exponent;
  int i;
  int i1;
  int i2;
  int ia;
  int j;
  int loop_ub;
  int neq;
  int nout;
  int nrows;
  int nx;
  int outidx;
  boolean_T Done;
  boolean_T MinStepExit;
  boolean_T NoFailedAttempts;
  Nftot = 3.0 * Nf;
  emxInit_real_T(&y2B, 1);
  loop_ub = (int)(Nftot * (Nftot + 6.0));
  nrows = (int)(6.0 * Nftot);
  i = y2B->size[0];
  y2B->size[0] = ((((int)Nftot + (int)(Nftot + 6.0)) + loop_ub) + nrows) + 42;
  emxEnsureCapacity_real_T(y2B, i);
  y2B_data = y2B->data;
  for (i = 0; i < 6; i++) {
    y2B_data[i] = twrench[i];
  }
  nx = (int)Nftot;
  for (i = 0; i < nx; i++) {
    y2B_data[i + 6] = 0.0;
  }
  nx = (int)(Nftot + 6.0);
  for (i = 0; i < nx; i++) {
    y2B_data[(i + (int)Nftot) + 6] = 0.0;
  }
  for (i = 0; i < loop_ub; i++) {
    y2B_data[((i + (int)Nftot) + (int)(Nftot + 6.0)) + 6] = 0.0;
  }
  for (i = 0; i < 36; i++) {
    y2B_data[(((i + (int)Nftot) + (int)(Nftot + 6.0)) + loop_ub) + 6] = b_iv[i];
  }
  for (i = 0; i < nrows; i++) {
    y2B_data[(((i + (int)Nftot) + (int)(Nftot + 6.0)) + loop_ub) + 42] = 0.0;
  }
  neq = y2B->size[0];
  emxInit_real_T(&f0, 1);
  backwardRecursion_anonFcn1(qe_data, Nf, L, y2B, f0);
  f0_data = f0->data;
  if ((unsigned int)y2B->size[0] == 0U) {
    i = MAX_int32_T;
  } else {
    i = (int)(8192U / (unsigned int)y2B->size[0]);
  }
  c = i + 4;
  emxInit_real_T(&tout, 2);
  i1 = tout->size[0] * tout->size[1];
  tout->size[0] = 1;
  tout->size[1] = i + 4;
  emxEnsureCapacity_real_T(tout, i1);
  tout_data = tout->data;
  for (i1 = 0; i1 < c; i1++) {
    tout_data[i1] = 0.0;
  }
  emxInit_real_T(&yout, 2);
  i1 = yout->size[0] * yout->size[1];
  yout->size[0] = y2B->size[0];
  yout->size[1] = i + 4;
  emxEnsureCapacity_real_T(yout, i1);
  yout_data = yout->data;
  loop_ub = y2B->size[0] * (i + 4);
  for (i1 = 0; i1 < loop_ub; i1++) {
    yout_data[i1] = 0.0;
  }
  nout = 0;
  loop_ub = y2B->size[0];
  for (i1 = 0; i1 < loop_ub; i1++) {
    yout_data[i1] = y2B_data[i1];
  }
  absh = 0.1;
  nx = y2B->size[0];
  emxInit_real_T(&ynew, 1);
  i1 = ynew->size[0];
  ynew->size[0] = y2B->size[0];
  emxEnsureCapacity_real_T(ynew, i1);
  ynew_data = ynew->data;
  for (nrows = 0; nrows < nx; nrows++) {
    ynew_data[nrows] = fabs(y2B_data[nrows]);
  }
  loop_ub = ynew->size[0];
  for (i1 = 0; i1 < loop_ub; i1++) {
    rh = ynew_data[i1];
    if (rh >= 0.001) {
      ynew_data[i1] = rh;
    } else {
      ynew_data[i1] = 0.001;
    }
  }
  i1 = ynew->size[0];
  ynew->size[0] = f0->size[0];
  emxEnsureCapacity_real_T(ynew, i1);
  ynew_data = ynew->data;
  loop_ub = f0->size[0];
  for (i1 = 0; i1 < loop_ub; i1++) {
    ynew_data[i1] = f0_data[i1] / ynew_data[i1];
  }
  rh = 0.0;
  i1 = ynew->size[0];
  for (nrows = 0; nrows < i1; nrows++) {
    absx = fabs(ynew_data[nrows]);
    if (rtIsNaN(absx) || (absx > rh)) {
      rh = absx;
    }
  }
  rh /= 0.20095091452076641;
  if (0.1 * rh > 1.0) {
    absh = 1.0 / rh;
  }
  if (!(absh >= 3.5527136788005009E-15)) {
    absh = 3.5527136788005009E-15;
  }
  t = 1.0;
  emxInit_real_T(&f, 2);
  i1 = f->size[0] * f->size[1];
  f->size[0] = y2B->size[0];
  f->size[1] = 7;
  emxEnsureCapacity_real_T(f, i1);
  f_data = f->data;
  loop_ub = y2B->size[0] * 7;
  for (i1 = 0; i1 < loop_ub; i1++) {
    f_data[i1] = 0.0;
  }
  loop_ub = f0->size[0];
  for (i1 = 0; i1 < loop_ub; i1++) {
    f_data[i1] = f0_data[i1];
  }
  MinStepExit = false;
  Done = false;
  emxInit_real_T(&youtnew, 2);
  emxInit_real_T(&varargin_1, 2);
  do {
    exitg1 = 0;
    absx = fabs(t);
    if (rtIsInf(absx) || rtIsNaN(absx)) {
      rh = rtNaN;
    } else if (absx < 4.4501477170144028E-308) {
      rh = 4.94065645841247E-324;
    } else {
      frexp(absx, &exponent);
      rh = ldexp(1.0, exponent - 53);
    }
    hmin = 16.0 * rh;
    if ((hmin >= absh) || rtIsNaN(absh)) {
      rh = hmin;
    } else {
      rh = absh;
    }
    if ((rh >= 0.1) || rtIsNaN(rh)) {
      absh = 0.1;
    } else {
      absh = rh;
    }
    absx = -absh;
    d = fabs(0.0 - t);
    if (1.1 * absh >= d) {
      absx = 0.0 - t;
      absh = d;
      Done = true;
    }
    NoFailedAttempts = true;
    do {
      exitg2 = 0;
      Bcolidx = 0;
      loop_ub = y2B->size[0];
      for (j = 0; j < 5; j++) {
        Bcolidx += j;
        i1 = ynew->size[0];
        ynew->size[0] = y2B->size[0];
        emxEnsureCapacity_real_T(ynew, i1);
        ynew_data = ynew->data;
        for (i1 = 0; i1 < loop_ub; i1++) {
          ynew_data[i1] = y2B_data[i1];
        }
        if (!(absx == 0.0)) {
          nx = Bcolidx;
          i1 = neq * j + 1;
          for (outidx = 1; neq < 0 ? outidx >= i1 : outidx <= i1;
               outidx += neq) {
            rh = absx * dv[nx];
            i2 = (outidx + neq) - 1;
            for (ia = outidx; ia <= i2; ia++) {
              nrows = ia - outidx;
              ynew_data[nrows] += f_data[ia - 1] * rh;
            }
            nx++;
          }
        }
        b_backwardRecursion_anonFcn1(qe_data, Nf, L, t + absx * dv1[j], ynew,
                                     f0);
        f0_data = f0->data;
        nrows = f0->size[0];
        for (i1 = 0; i1 < nrows; i1++) {
          f_data[i1 + f->size[0] * (j + 1)] = f0_data[i1];
        }
      }
      tnew = t + absx;
      i1 = ynew->size[0];
      ynew->size[0] = y2B->size[0];
      emxEnsureCapacity_real_T(ynew, i1);
      ynew_data = ynew->data;
      loop_ub = y2B->size[0];
      for (i1 = 0; i1 < loop_ub; i1++) {
        ynew_data[i1] = y2B_data[i1];
      }
      if (!(absx == 0.0)) {
        i1 = neq * 5 + 1;
        for (outidx = 1; neq < 0 ? outidx >= i1 : outidx <= i1; outidx += neq) {
          rh = absx * dv[Bcolidx + 5];
          i2 = (outidx + neq) - 1;
          for (ia = outidx; ia <= i2; ia++) {
            nrows = ia - outidx;
            ynew_data[nrows] += f_data[ia - 1] * rh;
          }
          Bcolidx++;
        }
      }
      b_backwardRecursion_anonFcn1(qe_data, Nf, L, tnew, ynew, f0);
      f0_data = f0->data;
      loop_ub = f0->size[0];
      for (i1 = 0; i1 < loop_ub; i1++) {
        f_data[i1 + f->size[0] * 6] = f0_data[i1];
      }
      if (Done) {
        tnew = 0.0;
      }
      nx = f->size[0];
      i1 = f0->size[0];
      f0->size[0] = f->size[0];
      emxEnsureCapacity_real_T(f0, i1);
      f0_data = f0->data;
      for (loop_ub = 0; loop_ub < nx; loop_ub++) {
        rh = 0.0;
        for (nrows = 0; nrows < 7; nrows++) {
          rh += f_data[nrows * f->size[0] + loop_ub] * dv2[nrows];
        }
        f0_data[loop_ub] = rh;
      }
      absx = absh * maxScaledError(f0, y2B, ynew);
      if (!(absx <= 0.001)) {
        if (absh <= hmin) {
          MinStepExit = true;
          exitg2 = 1;
        } else {
          if (NoFailedAttempts) {
            NoFailedAttempts = false;
            rh = 0.8 * rt_powd_snf(0.001 / absx, 0.2);
            if ((rh <= 0.1) || rtIsNaN(rh)) {
              rh = 0.1;
            }
            rh *= absh;
            if ((hmin >= rh) || rtIsNaN(rh)) {
              absh = hmin;
            } else {
              absh = rh;
            }
          } else {
            rh = 0.5 * absh;
            if ((hmin >= rh) || rtIsNaN(rh)) {
              absh = hmin;
            } else {
              absh = rh;
            }
          }
          absx = -absh;
          Done = false;
        }
      } else {
        exitg2 = 1;
      }
    } while (exitg2 == 0);
    if (MinStepExit) {
      exitg1 = 1;
    } else {
      outidx = nout + 1;
      rh = tnew - t;
      d = t + rh * 0.25;
      tref[0] = d;
      toutnew[0] = d;
      d = t + rh * 0.5;
      tref[1] = d;
      toutnew[1] = d;
      d = t + rh * 0.75;
      tref[2] = d;
      toutnew[2] = d;
      toutnew[3] = tnew;
      ntrp45(tref, t, y2B, rh, f, varargin_1);
      y2B_data = varargin_1->data;
      nx = varargin_1->size[0];
      i1 = youtnew->size[0] * youtnew->size[1];
      youtnew->size[0] = varargin_1->size[0];
      youtnew->size[1] = 4;
      emxEnsureCapacity_real_T(youtnew, i1);
      f0_data = youtnew->data;
      loop_ub = varargin_1->size[0];
      for (i1 = 0; i1 < 3; i1++) {
        for (i2 = 0; i2 < loop_ub; i2++) {
          f0_data[i2 + youtnew->size[0] * i1] =
              y2B_data[i2 + varargin_1->size[0] * i1];
        }
      }
      for (i1 = 0; i1 < nx; i1++) {
        f0_data[i1 + youtnew->size[0] * 3] = ynew_data[i1];
      }
      nout += 4;
      if (nout + 1 > tout->size[1]) {
        nx = tout->size[1];
        i1 = tout->size[0] * tout->size[1];
        tout->size[0] = 1;
        tout->size[1] = (tout->size[1] + i) + 4;
        emxEnsureCapacity_real_T(tout, i1);
        tout_data = tout->data;
        nrows = yout->size[0];
        Bcolidx = yout->size[1];
        i1 = yout->size[0] * yout->size[1];
        yout->size[1] = (yout->size[1] + i) + 4;
        emxEnsureCapacity_real_T(yout, i1);
        yout_data = yout->data;
        for (j = 0; j < c; j++) {
          tout_data[nx + j] = 0.0;
          for (loop_ub = 0; loop_ub < nrows; loop_ub++) {
            yout_data[loop_ub + yout->size[0] * (Bcolidx + j)] = 0.0;
          }
        }
      }
      for (nrows = 0; nrows < 4; nrows++) {
        nx = nrows + outidx;
        tout_data[nx] = toutnew[nrows];
        for (j = 0; j < neq; j++) {
          yout_data[j + yout->size[0] * nx] =
              f0_data[j + youtnew->size[0] * nrows];
        }
      }
      if (Done) {
        exitg1 = 1;
      } else {
        if (NoFailedAttempts) {
          rh = 1.25 * rt_powd_snf(absx / 0.001, 0.2);
          if (rh > 0.2) {
            absh /= rh;
          } else {
            absh *= 5.0;
          }
        }
        t = tnew;
        i1 = y2B->size[0];
        y2B->size[0] = ynew->size[0];
        emxEnsureCapacity_real_T(y2B, i1);
        y2B_data = y2B->data;
        loop_ub = ynew->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          y2B_data[i1] = ynew_data[i1];
        }
        i1 = f0->size[0];
        f0->size[0] = f->size[0];
        emxEnsureCapacity_real_T(f0, i1);
        f0_data = f0->data;
        loop_ub = f->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          f0_data[i1] = f_data[i1 + f->size[0] * 6];
        }
        loop_ub = f0->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          f_data[i1] = f0_data[i1];
        }
      }
    }
  } while (exitg1 == 0);
  emxFree_real_T(&varargin_1);
  emxFree_real_T(&youtnew);
  emxFree_real_T(&f);
  emxFree_real_T(&tout);
  emxFree_real_T(&f0);
  emxFree_real_T(&y2B);
  if (nout + 1 < 1) {
    loop_ub = -1;
  } else {
    loop_ub = nout;
  }
  emxInit_real_T(&y, 2);
  i = y->size[0] * y->size[1];
  y->size[0] = loop_ub + 1;
  y->size[1] = yout->size[0];
  emxEnsureCapacity_real_T(y, i);
  y2B_data = y->data;
  nrows = yout->size[0];
  for (i = 0; i < nrows; i++) {
    for (i1 = 0; i1 <= loop_ub; i1++) {
      y2B_data[i1 + y->size[0] * i] = yout_data[i + yout->size[0] * i1];
    }
  }
  emxFree_real_T(&yout);
  i = ynew->size[0];
  ynew->size[0] = y->size[1];
  emxEnsureCapacity_real_T(ynew, i);
  ynew_data = ynew->data;
  loop_ub = y->size[1];
  for (i = 0; i < loop_ub; i++) {
    ynew_data[i] = y2B_data[(y->size[0] + y->size[0] * i) - 1];
  }
  emxFree_real_T(&y);
  /*  primar variables */
  for (i = 0; i < 6; i++) {
    wbase[i] = ynew_data[i];
  }
  if (Nftot + 6.0 < 7.0) {
    i = 0;
    i1 = 0;
  } else {
    i = 6;
    i1 = (int)(Nftot + 6.0);
  }
  loop_ub = i1 - i;
  i1 = Qc->size[0];
  Qc->size[0] = loop_ub;
  emxEnsureCapacity_real_T(Qc, i1);
  y2B_data = Qc->data;
  for (i1 = 0; i1 < loop_ub; i1++) {
    y2B_data[i1] = ynew_data[i + i1];
  }
  /*  derivatives */
  if (Nftot + 7.0 > (Nftot + 6.0) + 6.0) {
    i = 0;
    i1 = 0;
  } else {
    i = (int)(Nftot + 7.0) - 1;
    i1 = (int)((Nftot + 6.0) + 6.0);
  }
  loop_ub = i1 - i;
  i1 = dwbasedqa->size[0];
  dwbasedqa->size[0] = loop_ub;
  emxEnsureCapacity_real_T(dwbasedqa, i1);
  y2B_data = dwbasedqa->data;
  for (i1 = 0; i1 < loop_ub; i1++) {
    y2B_data[i1] = ynew_data[i + i1];
  }
  d = ((Nftot + 6.0) + 6.0) + Nftot;
  if ((Nftot + 7.0) + 6.0 > d) {
    i = 0;
    i1 = 0;
  } else {
    i = (int)((Nftot + 7.0) + 6.0) - 1;
    i1 = (int)d;
  }
  loop_ub = i1 - i;
  i1 = dQcdqa->size[0];
  dQcdqa->size[0] = loop_ub;
  emxEnsureCapacity_real_T(dQcdqa, i1);
  y2B_data = dQcdqa->data;
  for (i1 = 0; i1 < loop_ub; i1++) {
    y2B_data[i1] = ynew_data[i + i1];
  }
  rh = ((Nftot + 7.0) + 6.0) + Nftot;
  d += 6.0 * Nftot;
  if (rh > d) {
    i = 0;
    i1 = 0;
  } else {
    i = (int)rh - 1;
    i1 = (int)d;
  }
  loop_ub = i1 - i;
  i1 = dwbasedqe->size[0];
  dwbasedqe->size[0] = loop_ub;
  emxEnsureCapacity_real_T(dwbasedqe, i1);
  y2B_data = dwbasedqe->data;
  for (i1 = 0; i1 < loop_ub; i1++) {
    y2B_data[i1] = ynew_data[i + i1];
  }
  rh += 6.0 * Nftot;
  absx = Nftot * Nftot;
  d += absx;
  if (rh > d) {
    i = 0;
    i1 = 0;
  } else {
    i = (int)rh - 1;
    i1 = (int)d;
  }
  loop_ub = i1 - i;
  i1 = dQcdqe->size[0];
  dQcdqe->size[0] = loop_ub;
  emxEnsureCapacity_real_T(dQcdqe, i1);
  y2B_data = dQcdqe->data;
  for (i1 = 0; i1 < loop_ub; i1++) {
    y2B_data[i1] = ynew_data[i + i1];
  }
  rh += absx;
  if (rh > d + 36.0) {
    i = 0;
    i1 = 0;
  } else {
    i = (int)rh - 1;
    i1 = (int)(d + 36.0);
  }
  loop_ub = i1 - i;
  i1 = dwbasedw0->size[0];
  dwbasedw0->size[0] = loop_ub;
  emxEnsureCapacity_real_T(dwbasedw0, i1);
  y2B_data = dwbasedw0->data;
  for (i1 = 0; i1 < loop_ub; i1++) {
    y2B_data[i1] = ynew_data[i + i1];
  }
  d = (d + 36.0) + 6.0 * Nftot;
  if (rh + 36.0 > d) {
    i = 0;
    i1 = 0;
  } else {
    i = (int)(rh + 36.0) - 1;
    i1 = (int)d;
  }
  loop_ub = i1 - i;
  i1 = dQcdw0->size[0];
  dQcdw0->size[0] = loop_ub;
  emxEnsureCapacity_real_T(dQcdw0, i1);
  y2B_data = dQcdw0->data;
  for (i1 = 0; i1 < loop_ub; i1++) {
    y2B_data[i1] = ynew_data[i + i1];
  }
  emxFree_real_T(&ynew);
}

/* End of code generation (backwardRecursion.c) */
