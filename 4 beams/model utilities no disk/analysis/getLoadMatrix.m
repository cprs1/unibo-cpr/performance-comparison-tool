function [W1,W2] = getLoadMatrix(geometry,params)

Nftot = 3*params.Nf;
Ci = geometry.jointmatr;
[~,nj] = size(Ci);
loadm = [eye(3),zeros(3);zeros(3),eye(3);];
W1 = [zeros(4*Nftot,6);loadm];
W2 = zeros(4*nj,6);
end