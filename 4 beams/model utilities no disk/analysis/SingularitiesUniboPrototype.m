function [t1,t2] = SingularitiesUniboPrototype(sol,jac,geometry,params)
Ci = geometry.jointmatr;
[~,nj] = size(Ci);
Nftot = 3*params.Nf;
%% First set of eqns: beams
rotparams = params.rotparams;
eul = sol(1+6+4*Nftot+3:6+4*Nftot+6,1);
D = rot2twist(eul,rotparams);
jac(1+4*Nftot:4*Nftot+3,:) = D'*jac(1+4*Nftot:4*Nftot+3,:);

% beam eq and plat eq
Ab1 = jac(1:4*Nftot+6,1:6); % wrt act var
Ub1 = jac(1:4*Nftot+6,1+6:6+4*Nftot); % wrt elastic coordinate of beams
Pb1 = jac(1:4*Nftot+6,1+6+4*Nftot:6+4*Nftot+6); % wrt plat vars
Gb1 = jac(1:4*Nftot+6,1+6+4*Nftot+6:6+4*Nftot+6+4*nj); % wrt beams multipliers
% constraint eq.
Ab2 = jac(1+4*Nftot+6:4*Nftot+6+4*nj,1:6); % wrt act var
Ub2 = jac(1+4*Nftot+6:4*Nftot+6+4*nj,1+6:6+4*Nftot); % wrt elastic coordinate
Pb2 = jac(1+4*Nftot+6:4*Nftot+6+4*nj,1+6+4*Nftot:6+4*Nftot+6); % wrt plat vars

Z = null(Gb1');

%% COLLECT
A = [Z'*Ab1;Ab2];
U = [Z'*Ub1;Ub2];
P = [Z'*Pb1;Pb2];

%% SINGULARITIES
T1 = [A,U];
T2 = [P,U];

t1 = rcond(T1);
t2 = rcond(T2);

end