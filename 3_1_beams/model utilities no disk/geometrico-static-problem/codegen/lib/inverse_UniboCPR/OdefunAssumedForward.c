/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * OdefunAssumedForward.c
 *
 * Code generation for function 'OdefunAssumedForward'
 *
 */

/* Include files */
#include "OdefunAssumedForward.h"
#include "inverse_UniboCPR_emxutil.h"
#include "inverse_UniboCPR_types.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */
void e_binary_expand_op(emxArray_real_T *in1, double in2, const double in3[9],
                        double in4, const double in5[16],
                        const emxArray_real_T *in6, const double in7[12],
                        int in8, const emxArray_real_T *in10, double in11,
                        double in12, const emxArray_real_T *in13,
                        const emxArray_real_T *in14)
{
  emxArray_real_T *b_in12;
  double b_in2[4];
  double b_in4[4];
  double b_in5[4];
  double c_in2[3];
  const double *in10_data;
  const double *in13_data;
  const double *in14_data;
  const double *in6_data;
  double d;
  double d1;
  double d2;
  double *in12_data;
  double *in1_data;
  int aux_0_1;
  int aux_1_1;
  int b_loop_ub;
  int i;
  int loop_ub;
  int stride_0_1;
  int stride_1_1;
  in14_data = in14->data;
  in13_data = in13->data;
  in10_data = in10->data;
  in6_data = in6->data;
  loop_ub = (int)(3.0 * in11);
  for (i = 0; i < 4; i++) {
    d = in5[i];
    d1 = in2 * d * in6_data[in8];
    d2 = d * in6_data[3];
    d = in5[i + 4];
    d1 += in2 * d * in6_data[in8 + 1];
    d2 += d * in6_data[4];
    d = in5[i + 8];
    d1 += in2 * d * in6_data[in8 + 2];
    d2 += d * in6_data[5];
    d = in5[i + 12];
    d1 += in2 * d * in6_data[in8 + 3];
    d2 += d * in6_data[6];
    b_in5[i] = d2;
    b_in2[i] = d1;
  }
  emxInit_real_T(&b_in12, 2);
  i = b_in12->size[0] * b_in12->size[1];
  b_in12->size[0] = 4;
  if (in14->size[1] == 1) {
    b_in12->size[1] = in13->size[1];
  } else {
    b_in12->size[1] = in14->size[1];
  }
  emxEnsureCapacity_real_T(b_in12, i);
  in12_data = b_in12->data;
  stride_0_1 = (in13->size[1] != 1);
  stride_1_1 = (in14->size[1] != 1);
  aux_0_1 = 0;
  aux_1_1 = 0;
  if (in14->size[1] == 1) {
    b_loop_ub = in13->size[1];
  } else {
    b_loop_ub = in14->size[1];
  }
  for (i = 0; i < b_loop_ub; i++) {
    in12_data[4 * i] = in12 * (in13_data[4 * aux_0_1] + in14_data[4 * aux_1_1]);
    in12_data[4 * i + 1] =
        in12 * (in13_data[4 * aux_0_1 + 1] + in14_data[4 * aux_1_1 + 1]);
    in12_data[4 * i + 2] =
        in12 * (in13_data[4 * aux_0_1 + 2] + in14_data[4 * aux_1_1 + 2]);
    in12_data[4 * i + 3] =
        in12 * (in13_data[4 * aux_0_1 + 3] + in14_data[4 * aux_1_1 + 3]);
    aux_1_1 += stride_1_1;
    aux_0_1 += stride_0_1;
  }
  b_loop_ub = (int)(4.0 * in11);
  for (i = 0; i < 4; i++) {
    b_in4[i] = ((in4 * in5[i] * in6_data[3] + in4 * in5[i + 4] * in6_data[4]) +
                in4 * in5[i + 8] * in6_data[5]) +
               in4 * in5[i + 12] * in6_data[6];
  }
  i = in1->size[0];
  in1->size[0] = (loop_ub + b_loop_ub) + 14;
  emxEnsureCapacity_real_T(in1, i);
  in1_data = in1->data;
  for (i = 0; i < 3; i++) {
    d = in3[i + 6];
    c_in2[i] = (((in2 * in7[i] * in6_data[in8] +
                  in2 * in7[i + 3] * in6_data[in8 + 1]) +
                 in2 * in7[i + 6] * in6_data[in8 + 2]) +
                in2 * in7[i + 9] * in6_data[in8 + 3]) +
               d;
    in1_data[i] = in2 * d;
  }
  in1_data[3] = b_in4[0];
  in1_data[4] = b_in4[1];
  in1_data[5] = b_in4[2];
  in1_data[6] = b_in4[3];
  in1_data[7] = c_in2[0];
  in1_data[8] = c_in2[1];
  in1_data[9] = c_in2[2];
  for (i = 0; i < loop_ub; i++) {
    in1_data[i + 10] = in10_data[i];
  }
  in1_data[loop_ub + 10] = 0.5 * (b_in2[0] + b_in5[0]);
  in1_data[loop_ub + 11] = 0.5 * (b_in2[1] + b_in5[1]);
  in1_data[loop_ub + 12] = 0.5 * (b_in2[2] + b_in5[2]);
  in1_data[loop_ub + 13] = 0.5 * (b_in2[3] + b_in5[3]);
  for (i = 0; i < b_loop_ub; i++) {
    in1_data[(i + loop_ub) + 14] = in12_data[i];
  }
  emxFree_real_T(&b_in12);
}

/* End of code generation (OdefunAssumedForward.c) */
