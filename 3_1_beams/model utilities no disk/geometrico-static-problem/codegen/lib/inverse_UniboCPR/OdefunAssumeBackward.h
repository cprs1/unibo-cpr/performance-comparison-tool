/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * OdefunAssumeBackward.h
 *
 * Code generation for function 'OdefunAssumeBackward'
 *
 */

#ifndef ODEFUNASSUMEBACKWARD_H
#define ODEFUNASSUMEBACKWARD_H

/* Include files */
#include "inverse_UniboCPR_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void f_binary_expand_op(emxArray_real_T *in1, double in2, const double in3[36],
                        const emxArray_real_T *in4, const emxArray_real_T *in5,
                        int in6, const emxArray_real_T *in8,
                        const emxArray_int8_T *in9, const emxArray_real_T *in10,
                        double in11, const emxArray_real_T *in12, int in13,
                        int in14, const emxArray_real_T *in15);

void g_binary_expand_op(double in1[6], double in2, const emxArray_real_T *in3,
                        int in4, int in5);

void h_binary_expand_op(emxArray_real_T *in1, double in2, const double in3[36],
                        const emxArray_real_T *in4, const emxArray_real_T *in5,
                        int in6, const emxArray_real_T *in8,
                        const emxArray_real_T *in9, const emxArray_real_T *in10,
                        double in11, const emxArray_real_T *in12, int in13,
                        int in14, const emxArray_real_T *in15);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (OdefunAssumeBackward.h) */
