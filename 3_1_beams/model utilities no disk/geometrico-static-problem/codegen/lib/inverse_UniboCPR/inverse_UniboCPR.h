/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * inverse_UniboCPR.h
 *
 * Code generation for function 'inverse_UniboCPR'
 *
 */

#ifndef INVERSE_UNIBOCPR_H
#define INVERSE_UNIBOCPR_H

/* Include files */
#include "inverse_UniboCPR_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
extern void inverse_UniboCPR(const double guess_data[], const int guess_size[1],
                             const struct0_T *geometry, const struct1_T *params,
                             const double Kad[144], const double qpv[6],
                             emxArray_real_T *eq, emxArray_real_T *gradeq);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (inverse_UniboCPR.h) */
