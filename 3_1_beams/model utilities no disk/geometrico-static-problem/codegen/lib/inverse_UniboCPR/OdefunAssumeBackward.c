/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * OdefunAssumeBackward.c
 *
 * Code generation for function 'OdefunAssumeBackward'
 *
 */

/* Include files */
#include "OdefunAssumeBackward.h"
#include "inverse_UniboCPR_emxutil.h"
#include "inverse_UniboCPR_types.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */
void f_binary_expand_op(emxArray_real_T *in1, double in2, const double in3[36],
                        const emxArray_real_T *in4, const emxArray_real_T *in5,
                        int in6, const emxArray_real_T *in8,
                        const emxArray_int8_T *in9, const emxArray_real_T *in10,
                        double in11, const emxArray_real_T *in12, int in13,
                        int in14, const emxArray_real_T *in15)
{
  emxArray_real_T *b_in2;
  emxArray_real_T *b_in9;
  emxArray_real_T *n_in4;
  double c_in3[36];
  double b_in3[6];
  double c_in2[6];
  const double *in10_data;
  const double *in12_data;
  const double *in15_data;
  const double *in4_data;
  const double *in5_data;
  const double *in8_data;
  double b_in4;
  double c_in4;
  double d_in4;
  double e_in4;
  double f_in4;
  double g_in4;
  double h_in4;
  double i_in4;
  double j_in4;
  double k_in4;
  double l_in4;
  double m_in4;
  double *b_in9_data;
  double *in1_data;
  int aux_0_1;
  int aux_1_1;
  int i;
  int i1;
  int loop_ub;
  int stride_0_1;
  int stride_1_1;
  const signed char *in9_data;
  in15_data = in15->data;
  in12_data = in12->data;
  in10_data = in10->data;
  in9_data = in9->data;
  in8_data = in8->data;
  in5_data = in5->data;
  in4_data = in4->data;
  for (i = 0; i < 6; i++) {
    b_in4 = 0.0;
    for (i1 = 0; i1 < 6; i1++) {
      b_in4 += in3[i + 6 * i1] * in4_data[in6 + i1];
    }
    b_in3[i] = b_in4;
  }
  b_in4 = in4_data[1];
  c_in4 = in4_data[2];
  d_in4 = in4_data[0];
  e_in4 = in4_data[2];
  f_in4 = in4_data[0];
  g_in4 = in4_data[1];
  h_in4 = in4_data[4];
  i_in4 = in4_data[5];
  j_in4 = in4_data[3];
  k_in4 = in4_data[5];
  l_in4 = in4_data[3];
  m_in4 = in4_data[4];
  emxInit_real_T(&b_in9, 2);
  i = b_in9->size[0] * b_in9->size[1];
  b_in9->size[0] = 6;
  b_in9->size[1] = in9->size[1];
  emxEnsureCapacity_real_T(b_in9, i);
  b_in9_data = b_in9->data;
  loop_ub = in9->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_in9_data[6 * i] = (double)in9_data[3 * i + 2] * b_in4 -
                        (double)in9_data[3 * i + 1] * c_in4;
  }
  loop_ub = in9->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_in9_data[6 * i + 1] =
        -(double)in9_data[3 * i + 2] * d_in4 + (double)in9_data[3 * i] * e_in4;
  }
  loop_ub = in9->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_in9_data[6 * i + 2] =
        (double)in9_data[3 * i + 1] * f_in4 - (double)in9_data[3 * i] * g_in4;
  }
  loop_ub = in9->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_in9_data[6 * i + 3] = (double)in9_data[3 * i + 2] * h_in4 -
                            (double)in9_data[3 * i + 1] * i_in4;
  }
  loop_ub = in9->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_in9_data[6 * i + 4] =
        -(double)in9_data[3 * i + 2] * j_in4 + (double)in9_data[3 * i] * k_in4;
  }
  loop_ub = in9->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_in9_data[6 * i + 5] =
        (double)in9_data[3 * i + 1] * l_in4 - (double)in9_data[3 * i] * m_in4;
  }
  emxInit_real_T(&b_in2, 2);
  i = b_in2->size[0] * b_in2->size[1];
  b_in2->size[0] = 6;
  if ((int)in11 == 1) {
    if (in10->size[1] == 1) {
      b_in2->size[1] = b_in9->size[1];
    } else {
      b_in2->size[1] = in10->size[1];
    }
  } else {
    b_in2->size[1] = (int)in11;
  }
  emxEnsureCapacity_real_T(b_in2, i);
  in1_data = b_in2->data;
  stride_0_1 = (b_in9->size[1] != 1);
  stride_1_1 = (in10->size[1] != 1);
  aux_0_1 = 0;
  aux_1_1 = 0;
  if ((int)in11 == 1) {
    if (in10->size[1] == 1) {
      loop_ub = b_in9->size[1];
    } else {
      loop_ub = in10->size[1];
    }
  } else {
    loop_ub = (int)in11;
  }
  for (i = 0; i < loop_ub; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      in1_data[i1 + 6 * i] =
          in2 * (b_in9_data[i1 + 6 * aux_0_1] + in10_data[i1 + 6 * aux_1_1]);
    }
    aux_1_1 += stride_1_1;
    aux_0_1 += stride_0_1;
  }
  i = b_in9->size[0] * b_in9->size[1];
  b_in9->size[0] = 6;
  b_in9->size[1] = b_in2->size[1];
  emxEnsureCapacity_real_T(b_in9, i);
  b_in9_data = b_in9->data;
  loop_ub = b_in2->size[1];
  for (i = 0; i < loop_ub; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      stride_0_1 = i1 + 6 * i;
      b_in9_data[stride_0_1] = in1_data[stride_0_1];
    }
  }
  emxFree_real_T(&b_in2);
  loop_ub = (int)(6.0 * in11);
  stride_1_1 = (int)(in11 * in11);
  emxInit_real_T(&n_in4, 1);
  stride_0_1 = in14 - in13;
  i = n_in4->size[0];
  n_in4->size[0] = stride_0_1 + 1;
  emxEnsureCapacity_real_T(n_in4, i);
  in1_data = n_in4->data;
  for (i = 0; i <= stride_0_1; i++) {
    in1_data[i] = in4_data[in13 + i];
  }
  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      b_in4 = 0.0;
      for (stride_0_1 = 0; stride_0_1 < 6; stride_0_1++) {
        b_in4 += in3[i + 6 * stride_0_1] * in1_data[stride_0_1 + 6 * i1];
      }
      c_in3[i + 6 * i1] = b_in4;
    }
  }
  emxFree_real_T(&n_in4);
  for (i = 0; i < 36; i++) {
    c_in3[i] *= in2;
  }
  i = in1->size[0];
  in1->size[0] =
      ((((in5->size[0] + in8->size[0]) + loop_ub) + stride_1_1) + loop_ub) + 48;
  emxEnsureCapacity_real_T(in1, i);
  in1_data = in1->data;
  for (i = 0; i < 6; i++) {
    b_in4 = 0.0;
    c_in4 = 0.0;
    for (i1 = 0; i1 < 6; i1++) {
      d_in4 = in3[i + 6 * i1];
      e_in4 = in4_data[i1];
      b_in4 += in2 * d_in4 * e_in4;
      c_in4 += d_in4 * e_in4;
    }
    c_in2[i] = in2 * b_in3[i] + c_in4;
    in1_data[i] = b_in4;
  }
  stride_0_1 = in5->size[0];
  for (i = 0; i < stride_0_1; i++) {
    in1_data[i + 6] = in5_data[i];
  }
  for (i = 0; i < 6; i++) {
    in1_data[(i + in5->size[0]) + 6] = c_in2[i];
  }
  stride_0_1 = in8->size[0];
  for (i = 0; i < stride_0_1; i++) {
    in1_data[(i + in5->size[0]) + 12] = in8_data[i];
  }
  for (i = 0; i < loop_ub; i++) {
    in1_data[((i + in5->size[0]) + in8->size[0]) + 12] = b_in9_data[i];
  }
  emxFree_real_T(&b_in9);
  for (i = 0; i < stride_1_1; i++) {
    in1_data[(((i + in5->size[0]) + in8->size[0]) + loop_ub) + 12] =
        in12_data[i];
  }
  for (i = 0; i < 36; i++) {
    in1_data[((((i + in5->size[0]) + in8->size[0]) + loop_ub) + stride_1_1) +
             12] = c_in3[i];
  }
  for (i = 0; i < loop_ub; i++) {
    in1_data[((((i + in5->size[0]) + in8->size[0]) + loop_ub) + stride_1_1) +
             48] = in15_data[i];
  }
}

void g_binary_expand_op(double in1[6], double in2, const emxArray_real_T *in3,
                        int in4, int in5)
{
  const double *in3_data;
  int i;
  int stride_0_0;
  in3_data = in3->data;
  stride_0_0 = ((in5 - in4) + 1 != 1);
  for (i = 0; i < 6; i++) {
    in1[i] = in2 * in3_data[in4 + i * stride_0_0] + in3_data[i];
  }
}

void h_binary_expand_op(emxArray_real_T *in1, double in2, const double in3[36],
                        const emxArray_real_T *in4, const emxArray_real_T *in5,
                        int in6, const emxArray_real_T *in8,
                        const emxArray_real_T *in9, const emxArray_real_T *in10,
                        double in11, const emxArray_real_T *in12, int in13,
                        int in14, const emxArray_real_T *in15)
{
  emxArray_real_T *b_in2;
  emxArray_real_T *b_in9;
  emxArray_real_T *n_in4;
  double c_in3[36];
  double b_in3[6];
  double c_in2[6];
  const double *in10_data;
  const double *in12_data;
  const double *in15_data;
  const double *in4_data;
  const double *in5_data;
  const double *in8_data;
  const double *in9_data;
  double b_in4;
  double c_in4;
  double d_in4;
  double e_in4;
  double f_in4;
  double g_in4;
  double h_in4;
  double i_in4;
  double j_in4;
  double k_in4;
  double l_in4;
  double m_in4;
  double *b_in9_data;
  double *in1_data;
  int aux_0_1;
  int aux_1_1;
  int i;
  int i1;
  int loop_ub;
  int stride_0_1;
  int stride_1_1;
  in15_data = in15->data;
  in12_data = in12->data;
  in10_data = in10->data;
  in9_data = in9->data;
  in8_data = in8->data;
  in5_data = in5->data;
  in4_data = in4->data;
  for (i = 0; i < 6; i++) {
    b_in4 = 0.0;
    for (i1 = 0; i1 < 6; i1++) {
      b_in4 += in3[i + 6 * i1] * in4_data[in6 + i1];
    }
    b_in3[i] = b_in4;
  }
  b_in4 = in4_data[1];
  c_in4 = in4_data[2];
  d_in4 = in4_data[0];
  e_in4 = in4_data[2];
  f_in4 = in4_data[0];
  g_in4 = in4_data[1];
  h_in4 = in4_data[4];
  i_in4 = in4_data[5];
  j_in4 = in4_data[3];
  k_in4 = in4_data[5];
  l_in4 = in4_data[3];
  m_in4 = in4_data[4];
  emxInit_real_T(&b_in9, 2);
  i = b_in9->size[0] * b_in9->size[1];
  b_in9->size[0] = 6;
  b_in9->size[1] = in9->size[1];
  emxEnsureCapacity_real_T(b_in9, i);
  b_in9_data = b_in9->data;
  loop_ub = in9->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_in9_data[6 * i] =
        in9_data[3 * i + 2] * b_in4 - in9_data[3 * i + 1] * c_in4;
  }
  loop_ub = in9->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_in9_data[6 * i + 1] =
        -in9_data[3 * i + 2] * d_in4 + in9_data[3 * i] * e_in4;
  }
  loop_ub = in9->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_in9_data[6 * i + 2] =
        in9_data[3 * i + 1] * f_in4 - in9_data[3 * i] * g_in4;
  }
  loop_ub = in9->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_in9_data[6 * i + 3] =
        in9_data[3 * i + 2] * h_in4 - in9_data[3 * i + 1] * i_in4;
  }
  loop_ub = in9->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_in9_data[6 * i + 4] =
        -in9_data[3 * i + 2] * j_in4 + in9_data[3 * i] * k_in4;
  }
  loop_ub = in9->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_in9_data[6 * i + 5] =
        in9_data[3 * i + 1] * l_in4 - in9_data[3 * i] * m_in4;
  }
  emxInit_real_T(&b_in2, 2);
  i = b_in2->size[0] * b_in2->size[1];
  b_in2->size[0] = 6;
  if ((int)in11 == 1) {
    if (in10->size[1] == 1) {
      b_in2->size[1] = b_in9->size[1];
    } else {
      b_in2->size[1] = in10->size[1];
    }
  } else {
    b_in2->size[1] = (int)in11;
  }
  emxEnsureCapacity_real_T(b_in2, i);
  in1_data = b_in2->data;
  stride_0_1 = (b_in9->size[1] != 1);
  stride_1_1 = (in10->size[1] != 1);
  aux_0_1 = 0;
  aux_1_1 = 0;
  if ((int)in11 == 1) {
    if (in10->size[1] == 1) {
      loop_ub = b_in9->size[1];
    } else {
      loop_ub = in10->size[1];
    }
  } else {
    loop_ub = (int)in11;
  }
  for (i = 0; i < loop_ub; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      in1_data[i1 + 6 * i] =
          in2 * (b_in9_data[i1 + 6 * aux_0_1] + in10_data[i1 + 6 * aux_1_1]);
    }
    aux_1_1 += stride_1_1;
    aux_0_1 += stride_0_1;
  }
  i = b_in9->size[0] * b_in9->size[1];
  b_in9->size[0] = 6;
  b_in9->size[1] = b_in2->size[1];
  emxEnsureCapacity_real_T(b_in9, i);
  b_in9_data = b_in9->data;
  loop_ub = b_in2->size[1];
  for (i = 0; i < loop_ub; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      stride_0_1 = i1 + 6 * i;
      b_in9_data[stride_0_1] = in1_data[stride_0_1];
    }
  }
  emxFree_real_T(&b_in2);
  loop_ub = (int)(6.0 * in11);
  stride_1_1 = (int)(in11 * in11);
  emxInit_real_T(&n_in4, 1);
  stride_0_1 = in14 - in13;
  i = n_in4->size[0];
  n_in4->size[0] = stride_0_1 + 1;
  emxEnsureCapacity_real_T(n_in4, i);
  in1_data = n_in4->data;
  for (i = 0; i <= stride_0_1; i++) {
    in1_data[i] = in4_data[in13 + i];
  }
  for (i = 0; i < 6; i++) {
    for (i1 = 0; i1 < 6; i1++) {
      b_in4 = 0.0;
      for (stride_0_1 = 0; stride_0_1 < 6; stride_0_1++) {
        b_in4 += in3[i + 6 * stride_0_1] * in1_data[stride_0_1 + 6 * i1];
      }
      c_in3[i + 6 * i1] = b_in4;
    }
  }
  emxFree_real_T(&n_in4);
  for (i = 0; i < 36; i++) {
    c_in3[i] *= in2;
  }
  i = in1->size[0];
  in1->size[0] =
      ((((in5->size[0] + in8->size[0]) + loop_ub) + stride_1_1) + loop_ub) + 48;
  emxEnsureCapacity_real_T(in1, i);
  in1_data = in1->data;
  for (i = 0; i < 6; i++) {
    b_in4 = 0.0;
    c_in4 = 0.0;
    for (i1 = 0; i1 < 6; i1++) {
      d_in4 = in3[i + 6 * i1];
      e_in4 = in4_data[i1];
      b_in4 += in2 * d_in4 * e_in4;
      c_in4 += d_in4 * e_in4;
    }
    c_in2[i] = in2 * b_in3[i] + c_in4;
    in1_data[i] = b_in4;
  }
  stride_0_1 = in5->size[0];
  for (i = 0; i < stride_0_1; i++) {
    in1_data[i + 6] = in5_data[i];
  }
  for (i = 0; i < 6; i++) {
    in1_data[(i + in5->size[0]) + 6] = c_in2[i];
  }
  stride_0_1 = in8->size[0];
  for (i = 0; i < stride_0_1; i++) {
    in1_data[(i + in5->size[0]) + 12] = in8_data[i];
  }
  for (i = 0; i < loop_ub; i++) {
    in1_data[((i + in5->size[0]) + in8->size[0]) + 12] = b_in9_data[i];
  }
  emxFree_real_T(&b_in9);
  for (i = 0; i < stride_1_1; i++) {
    in1_data[(((i + in5->size[0]) + in8->size[0]) + loop_ub) + 12] =
        in12_data[i];
  }
  for (i = 0; i < 36; i++) {
    in1_data[((((i + in5->size[0]) + in8->size[0]) + loop_ub) + stride_1_1) +
             12] = c_in3[i];
  }
  for (i = 0; i < loop_ub; i++) {
    in1_data[((((i + in5->size[0]) + in8->size[0]) + loop_ub) + stride_1_1) +
             48] = in15_data[i];
  }
}

/* End of code generation (OdefunAssumeBackward.c) */
