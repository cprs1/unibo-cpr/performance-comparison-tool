/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * eul2quat.c
 *
 * Code generation for function 'eul2quat'
 *
 */

/* Include files */
#include "eul2quat.h"
#include "rt_nonfinite.h"
#include <math.h>
#include <string.h>

/* Function Definitions */
void b_eul2quat(const double eul[3], const char seq[3], double q[4])
{
  static const char b[3] = {'Z', 'Y', 'X'};
  static const char b_b[3] = {'Z', 'Y', 'Z'};
  static const char c_b[3] = {'X', 'Y', 'Z'};
  double b_q_tmp;
  double c_idx_0;
  double c_idx_1;
  double c_idx_2;
  double d;
  double q_tmp;
  double s_idx_0;
  double s_idx_1;
  int ret;
  /* EUL2QUAT Convert Euler angles to quaternion */
  /*    Q = EUL2QUAT(EUL) converts a given set of 3D Euler angles, EUL, into */
  /*    the corresponding unit quaternion, Q. EUL is an N-by-3 matrix of Euler
   */
  /*    rotation angles. */
  /*    The output, Q, is an N-by-4 matrix containing N quaternions. Each */
  /*    quaternion is of the form q = [w x y z], with w as the scalar number. */
  /*  */
  /*    Q = EUL2QUAT(EUL, SEQ) converts a set of 3D Euler angles into a unit */
  /*    quaternion. The Euler angles are specified by the body-fixed */
  /*    (intrinsic) axis rotation sequence, SEQ. */
  /*  */
  /*    The default rotation sequence is 'ZYX', where the order of rotation */
  /*    angles is Z Axis Rotation, Y Axis Rotation, and X Axis Rotation. */
  /*  */
  /*    The following rotation sequences, SEQ, are supported: 'ZYX', 'ZYZ', and
   */
  /*    'XYZ'. */
  /*  */
  /*    Example: */
  /*       % Calculate the quaternion for a set of Euler angles */
  /*       % By default, the ZYX axis order will be used. */
  /*       angles = [0 pi/2 0]; */
  /*       q = eul2quat(angles) */
  /*  */
  /*       % Calculate the quaternion based on a ZYZ rotation */
  /*       qzyz = eul2quat(angles, 'ZYZ') */
  /*  */
  /*    See also quat2eul */
  /*    Copyright 2014-2018 The MathWorks, Inc. */
  /*  Pre-allocate output */
  q[0] = 0.0;
  q[1] = 0.0;
  q[2] = 0.0;
  q[3] = 0.0;
  /*  Compute sines and cosines of half angles */
  d = eul[0] / 2.0;
  c_idx_0 = cos(d);
  d = sin(d);
  s_idx_0 = d;
  d = eul[1] / 2.0;
  c_idx_1 = cos(d);
  d = sin(d);
  s_idx_1 = d;
  d = eul[2] / 2.0;
  c_idx_2 = cos(d);
  d = sin(d);
  /*  The parsed sequence will be in all upper-case letters and validated */
  ret = memcmp(&seq[0], &b[0], 3);
  if (ret == 0) {
    ret = 0;
  } else {
    ret = memcmp(&seq[0], &b_b[0], 3);
    if (ret == 0) {
      ret = 1;
    } else {
      ret = memcmp(&seq[0], &c_b[0], 3);
      if (ret == 0) {
        ret = 2;
      } else {
        ret = -1;
      }
    }
  }
  switch (ret) {
  case 0:
    /*  Construct quaternion */
    q_tmp = c_idx_0 * c_idx_1;
    b_q_tmp = s_idx_0 * s_idx_1;
    q[0] = q_tmp * c_idx_2 + b_q_tmp * d;
    q[1] = q_tmp * d - b_q_tmp * c_idx_2;
    q[2] = c_idx_0 * s_idx_1 * c_idx_2 + s_idx_0 * c_idx_1 * d;
    q[3] = s_idx_0 * c_idx_1 * c_idx_2 - c_idx_0 * s_idx_1 * d;
    break;
  case 1:
    /*  Construct quaternion */
    q[0] = c_idx_0 * c_idx_1 * c_idx_2 - s_idx_0 * c_idx_1 * d;
    q_tmp = c_idx_0 * s_idx_1;
    q[1] = q_tmp * d - s_idx_0 * s_idx_1 * c_idx_2;
    q[2] = q_tmp * c_idx_2 + s_idx_0 * s_idx_1 * d;
    q[3] = s_idx_0 * c_idx_1 * c_idx_2 + c_idx_0 * c_idx_1 * d;
    break;
  case 2:
    /*  Construct quaternion */
    q[0] = c_idx_0 * c_idx_1 * c_idx_2 - s_idx_0 * s_idx_1 * d;
    q[1] = s_idx_0 * c_idx_1 * c_idx_2 + c_idx_0 * s_idx_1 * d;
    q[2] = -s_idx_0 * c_idx_1 * d + c_idx_0 * s_idx_1 * c_idx_2;
    q[3] = c_idx_0 * c_idx_1 * d + s_idx_0 * s_idx_1 * c_idx_2;
    break;
  }
}

void eul2quat(const double eul[3], double q[4])
{
  double b_q_tmp;
  double c_idx_0;
  double c_idx_1;
  double c_idx_2;
  double d;
  double q_tmp;
  double s_idx_0;
  double s_idx_1;
  /* EUL2QUAT Convert Euler angles to quaternion */
  /*    Q = EUL2QUAT(EUL) converts a given set of 3D Euler angles, EUL, into */
  /*    the corresponding unit quaternion, Q. EUL is an N-by-3 matrix of Euler
   */
  /*    rotation angles. */
  /*    The output, Q, is an N-by-4 matrix containing N quaternions. Each */
  /*    quaternion is of the form q = [w x y z], with w as the scalar number. */
  /*  */
  /*    Q = EUL2QUAT(EUL, SEQ) converts a set of 3D Euler angles into a unit */
  /*    quaternion. The Euler angles are specified by the body-fixed */
  /*    (intrinsic) axis rotation sequence, SEQ. */
  /*  */
  /*    The default rotation sequence is 'ZYX', where the order of rotation */
  /*    angles is Z Axis Rotation, Y Axis Rotation, and X Axis Rotation. */
  /*  */
  /*    The following rotation sequences, SEQ, are supported: 'ZYX', 'ZYZ', and
   */
  /*    'XYZ'. */
  /*  */
  /*    Example: */
  /*       % Calculate the quaternion for a set of Euler angles */
  /*       % By default, the ZYX axis order will be used. */
  /*       angles = [0 pi/2 0]; */
  /*       q = eul2quat(angles) */
  /*  */
  /*       % Calculate the quaternion based on a ZYZ rotation */
  /*       qzyz = eul2quat(angles, 'ZYZ') */
  /*  */
  /*    See also quat2eul */
  /*    Copyright 2014-2018 The MathWorks, Inc. */
  /*  Pre-allocate output */
  /*  Compute sines and cosines of half angles */
  d = eul[0] / 2.0;
  c_idx_0 = cos(d);
  d = sin(d);
  s_idx_0 = d;
  d = eul[1] / 2.0;
  c_idx_1 = cos(d);
  d = sin(d);
  s_idx_1 = d;
  d = eul[2] / 2.0;
  c_idx_2 = cos(d);
  d = sin(d);
  /*  The parsed sequence will be in all upper-case letters and validated */
  /*  Construct quaternion */
  q_tmp = c_idx_0 * c_idx_1;
  b_q_tmp = s_idx_0 * s_idx_1;
  q[0] = q_tmp * c_idx_2 - b_q_tmp * d;
  c_idx_0 *= s_idx_1;
  q[1] = s_idx_0 * c_idx_1 * c_idx_2 + c_idx_0 * d;
  q[2] = -s_idx_0 * c_idx_1 * d + c_idx_0 * c_idx_2;
  q[3] = q_tmp * d + b_q_tmp * c_idx_2;
}

/* End of code generation (eul2quat.c) */
