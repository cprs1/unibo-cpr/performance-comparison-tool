function [qa,qe,qp,lambda] = unstack(guess,Nftot,nj)
qa = guess(1:6,1);
qe = guess(1+6:6+4*Nftot,1);
qp = guess(1+6+4*Nftot:6+4*Nftot+6,1);
% lambda = guess(1+6+4*Nftot+6:6+4*Nftot+6+6+nj*3,1);
lambda = guess(1+6+4*Nftot+6:6+4*Nftot+3+6+nj*3,1);

end