function [eq,gradeq] = forward_UniboCPR_31(guess,geometry,params,Kad,qpv)

% extract parameters
% Ci0 = eye(6);
Ci0 = [zeros(3,3);eye(3)];

Nf = params.Nf;
Nftot = 3*Nf;
rotparams = params.rotparams;
Ci = geometry.jointmatr;
[~,nj] = size(Ci);
hr = geometry.hr;
platangles = geometry.platangles;
platpoints = geometry.platpoints;
endpoints = geometry.endpoints;
endangles = geometry.endangles;
g = params.g;
wd = [zeros(3,1);g*params.fg2];

% extract variables
[qa,qe,qp,lambda] = unstack(guess,Nftot,nj);

pbase = [qa(1);qa(2);hr];
Lv = qa(3:6,1);
pplat = qp(1:3,1);
[Rp,dRpda,dRpdb,dRpdc] = rotationParametrization(qp(4:6,1),rotparams);

dimf = 3;
% initialize
beameq = zeros(4*Nftot,1);
dbeameqdqa = zeros(4*Nftot,6);
dbeameqdqe = zeros(4*Nftot,4*Nftot);
dbeameqdlambda = zeros(4*Nftot,dimf+3*nj);
wrencheq = params.tipwrench;
dwrenchdqa = zeros(6,6);
dwrenchdqe = zeros(6,4*Nftot);
dwrenchdqp = zeros(6,6);
dwrenchdlambda = zeros(6,dimf+3*nj);
constr = zeros(dimf+3*nj,1);
dconstrdqa = zeros(dimf+3*nj,6);
dconstrdqe = zeros(dimf+3*nj,4*Nftot);
dconstrdqp = zeros(dimf+3*nj,6);

for i = 1:4
    if i == 1
        % extract variables of the th-i beam
        qei = qe(1+Nftot*(i-1):Nftot*i,1);
        lambdai = lambda(1:dimf,1);
        Li = Lv(i);
        p1 = pbase + platpoints(:,i);
        h1 = eul2quat(platangles(:,i)','XYZ')';
        twrench = (Ci0*lambdai);
        
        % forward recursion
        [p2,h2,dp2dLvi,dh2dLvi,dp2dqei,dh2dqei]  = forwardRecursion(p1,h1,qei,Li,Nf);
        dp2dqei = reshape(dp2dqei,3,Nftot);
        dh2dqei = reshape(dh2dqei,4,Nftot);
        R2 = quat2rotmatrix(h2);
        % backward recursion
        [~,Qc,~,dQcdLvi,~,dQcdqei,~,dQcdw0i] = backwardRecursion(twrench,qei,Nf,Li,wd);
        dQcdqei = reshape(dQcdqei,Nftot,Nftot);
        dQcdw0i = reshape(dQcdw0i,Nftot,6);
        %% BEAM EQUILIBRIUMS
        beameq(1+Nftot*(i-1):Nftot*i,1) = Li*Kad*qei + Qc;
        dbeameqdqa(1+Nftot*(i-1):Nftot*i,2+i) = Kad*qei + dQcdLvi;
        dbeameqdqe(1+Nftot*(i-1):Nftot*i,1+Nftot*(i-1):Nftot*i) = Li*Kad+dQcdqei;
        dbeameqdlambda(1+Nftot*(i-1):Nftot*i,1:dimf) = dQcdw0i*Ci0;
    
        %% CONSTRAINTS
        pp = Rp*endpoints(:,i);
        dppdrot = [dRpda*endpoints(:,i),dRpdb*endpoints(:,i),dRpdc*endpoints(:,i)];
        platconstr = p2-(pplat +pp);
        dplatconstrdLvi = dp2dLvi;
        dplatconstrdpbase = [eye(2);zeros(1,2)];
        dplatconstrdqei = dp2dqei;
        dplatconstrdqp = -[eye(3),dppdrot];
        hJi = eul2quat(endangles(:,i)',geometry.endanglesconvention)';
        RJi = Rp*quat2rotmatrix(hJi);
        dRJida = dRpda*quat2rotmatrix(hJi);
        dRJidb = dRpdb*quat2rotmatrix(hJi);
        dRJidc = dRpdc*quat2rotmatrix(hJi);
    
        [platangerr,dplatangerrdLvi,dplatangerrdqei,m2] = roterr(RJi,R2,h2,dh2dLvi,dh2dqei,dRJida,dRJidb,dRJidc);
        dplatangerrdqp =  [zeros(3,3),m2];
        constr(1:dimf,1) = Ci0'*[platangerr;platconstr;];
        dconstrdqa(1:dimf,1:2) = Ci0'*[zeros(3,2);dplatconstrdpbase];
        dconstrdqa(1:dimf,2+i) = Ci0'*[dplatangerrdLvi;dplatconstrdLvi;];
        dconstrdqe(1:dimf,1+Nftot*(i-1):Nftot*i) =  Ci0'*[dplatangerrdqei;dplatconstrdqei;];
        dconstrdqp(1:dimf,:) = Ci0'*[dplatangerrdqp;dplatconstrdqp;];
    
        %% PLATFORM EQUILIBRIUM
         
        Adg = [R2,zeros(3); zeros(3), R2]; 
        gwrench = -Adg*(twrench); 
        
        [D1,D2,D3] = derivativeColRotMatQuat(h2);
        dgwrenchdLvi = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dh2dLvi;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dh2dLvi];
        dgwrenchdqei = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dh2dqei;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dh2dqei];
        Adg2 = [eye(3),skew(pp); zeros(3),eye(3)]; 
        m2 = [zeros(3,3),skew(dppdrot(:,1))*gwrench(4:6),skew(dppdrot(:,2))*gwrench(4:6),skew(dppdrot(:,3))*gwrench(4:6)];
    
        wrencheq = wrencheq + Adg2*gwrench; 
        dwrenchdqa(:,2+i) = Adg2*dgwrenchdLvi;
        dwrenchdqe(:,1+Nftot*(i-1):Nftot*i) = Adg2*dgwrenchdqei;
        dwrenchdqp = dwrenchdqp + [m2;zeros(3,6)] ;
        dwrenchdlambda(:,1:dimf) = -Adg2*Adg*Ci0;
    else
        % extract variables of the th-i beam
        qei = qe(1+Nftot*(i-1):Nftot*i,1);
        lambdai = lambda(1+dimf+nj*(i-2):dimf+nj*(i-1),1);
        Li = Lv(i);
        p1 = pbase + platpoints(:,i);
        h1 = eul2quat(platangles(:,i)','XYZ')';
        twrench = (Ci*lambdai);
        
        % forward recursion
        [p2,h2,dp2dLvi,dh2dLvi,dp2dqei,dh2dqei]  = forwardRecursion(p1,h1,qei,Li,Nf);
        dp2dqei = reshape(dp2dqei,3,Nftot);
        dh2dqei = reshape(dh2dqei,4,Nftot);
        R2 = quat2rotmatrix(h2);
        % backward recursion
        [~,Qc,~,dQcdLvi,~,dQcdqei,~,dQcdw0i] = backwardRecursion(twrench,qei,Nf,Li,wd);
        dQcdqei = reshape(dQcdqei,Nftot,Nftot);
        dQcdw0i = reshape(dQcdw0i,Nftot,6);
        %% BEAM EQUILIBRIUMS
        beameq(1+Nftot*(i-1):Nftot*i,1) = Li*Kad*qei + Qc;
        dbeameqdqa(1+Nftot*(i-1):Nftot*i,2+i) = Kad*qei + dQcdLvi;
        dbeameqdqe(1+Nftot*(i-1):Nftot*i,1+Nftot*(i-1):Nftot*i) = Li*Kad+dQcdqei;
        dbeameqdlambda(1+Nftot*(i-1):Nftot*i,1+dimf+nj*(i-2):dimf+nj*(i-1)) = dQcdw0i*Ci;
    
        %% CONSTRAINTS
        pp = Rp*endpoints(:,i);
        dppdrot = [dRpda*endpoints(:,i),dRpdb*endpoints(:,i),dRpdc*endpoints(:,i)];
        platconstr = p2-(pplat +pp);
        dplatconstrdLvi = dp2dLvi;
        dplatconstrdpbase = [eye(2);zeros(1,2)];
        dplatconstrdqei = dp2dqei;
        dplatconstrdqp = -[eye(3),dppdrot];
        hJi = eul2quat(endangles(:,i)',geometry.endanglesconvention)';
        RJi = Rp*quat2rotmatrix(hJi);
        dRJida = dRpda*quat2rotmatrix(hJi);
        dRJidb = dRpdb*quat2rotmatrix(hJi);
        dRJidc = dRpdc*quat2rotmatrix(hJi);
    
        [platangerr,dplatangerrdLvi,dplatangerrdqei,m2] = roterr(RJi,R2,h2,dh2dLvi,dh2dqei,dRJida,dRJidb,dRJidc);
        dplatangerrdqp =  [zeros(3,3),m2];
        constr(1+dimf+nj*(i-2):dimf+nj*(i-1),1) = Ci'*[platangerr;platconstr;];
        dconstrdqa(1+dimf+nj*(i-2):dimf+nj*(i-1),1:2) = Ci'*[zeros(3,2);dplatconstrdpbase];
        dconstrdqa(1+dimf+nj*(i-2):dimf+nj*(i-1),2+i) = Ci'*[dplatangerrdLvi;dplatconstrdLvi;];
        dconstrdqe(1+dimf+nj*(i-2):dimf+nj*(i-1),1+Nftot*(i-1):Nftot*i) =  Ci'*[dplatangerrdqei;dplatconstrdqei;];
        dconstrdqp(1+dimf+nj*(i-2):dimf+nj*(i-1),:) = Ci'*[dplatangerrdqp;dplatconstrdqp;];
    
        %% PLATFORM EQUILIBRIUM
         
        Adg = [R2,zeros(3); zeros(3), R2]; 
        gwrench = -Adg*(twrench); 
        
        [D1,D2,D3] = derivativeColRotMatQuat(h2);
        dgwrenchdLvi = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dh2dLvi;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dh2dLvi];
        dgwrenchdqei = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dh2dqei;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dh2dqei];
        Adg2 = [eye(3),skew(pp); zeros(3),eye(3)]; 
        m2 = [zeros(3,3),skew(dppdrot(:,1))*gwrench(4:6),skew(dppdrot(:,2))*gwrench(4:6),skew(dppdrot(:,3))*gwrench(4:6)];
    
        wrencheq = wrencheq + Adg2*gwrench; 
        dwrenchdqa(:,2+i) = Adg2*dgwrenchdLvi;
        dwrenchdqe(:,1+Nftot*(i-1):Nftot*i) = Adg2*dgwrenchdqei;
        dwrenchdqp = dwrenchdqp + [m2;zeros(3,6)] ;
        dwrenchdlambda(:,1+dimf+nj*(i-2):dimf+nj*(i-1)) = -Adg2*Adg*Ci;
    end
end

%% Forward problem
forweq = qa-qpv;

%% COLLECT
eq = [beameq;wrencheq;constr;forweq];


gradeq = [dbeameqdqa, dbeameqdqe, zeros(4*Nftot,6), dbeameqdlambda;
          dwrenchdqa, dwrenchdqe, dwrenchdqp, dwrenchdlambda;
          dconstrdqa, dconstrdqe, dconstrdqp, zeros(dimf+3*nj,dimf+3*nj);
          eye(6), zeros(6,4*Nftot), zeros(6,6), zeros(6,dimf+3*nj);];

end