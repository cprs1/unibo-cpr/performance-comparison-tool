function flag = mechconstrUniboCPR(y,actlim,displim,beamsradius,Nf,maxstrain)

xrail = y(1);
yrail = y(2);
Lv = y(3:6);

flag = 1;
% raillimits
if xrail<=displim(1) || xrail>=displim(2) || yrail<=displim(1) || yrail>=displim(2)
    flag = 0;
end

% lenght limits
for i = 1:4
    L = Lv(i);
    if L<=actlim(1) || L>=actlim(2)
        flag=0;
    end
end
% 
% % strain limits
% i = 1;
% while i<=4 && flag == 1
%     % beams
%     qei = y(1+6+3*Nf*(i-1):6+3*Nf*i,1);
%     jmax = 50;
%     L = linspace(0,1,jmax);
%     j = 1;
%     while j<=jmax && flag==1
%         b = PhiMatr(L(j),1,Nf);
%         xi = b*qei;
%         epsilon = sqrt(xi(1)^2+xi(2)^2);
%         strain = beamsradius*epsilon;
%         if strain>maxstrain
%             flag = 0;
%         end
%         % update
%         j = j+1;
%     end
%     %update
%     i = i+1;
% end


end