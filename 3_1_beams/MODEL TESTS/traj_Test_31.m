%% trajectory test
%% PARAMETERS
clear
close all
clc

[geometry,params,Kbt_1,Kbt_2] = createRobotParams();
params.g = [0;0;0]; % gravity
params.tipwrench = [zeros(3,1);0;0;-30]; % tip wrench
[~,nj] = size(geometry.jointmatr);  

%% INVERSE PROBLEM
xP = 0; yP = 0; zP = 0.20; alpha = 0*pi/180; beta = 0*pi/180; gamma = 0*pi/180;
qpv = [xP;yP;zP;alpha;beta;gamma];

%% MODEL PARAMETERS
Nf = 4;
params.Nf = Nf;
M = @(s) PhiMatr(s,1,Nf);
Kad1 = integral(@(s) M(s)'*Kbt_1*M(s),0,1,'ArrayValued',true);
Kad2 = integral(@(s) M(s)'*Kbt_2*M(s),0,1,'ArrayValued',true);
params.rotparams = 'XYZ';

%% Solution
qa0 = [0;0;0.4;0.4;0.4;0.4];
qe0 = repmat([0;zeros(3*Nf-1,1)],4,1);
qp0 = qpv;
lambda0 = zeros(6+3*nj,1);
guess0 = [qa0;qe0;qp0;lambda0];
% load guess0
options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'Algorithm','trust-region','CheckGradients',false,'Maxiterations',50);

j = 1;
jmax = 180;
flag = 1;

flagv1 = NaN*zeros(jmax,1);
flagv2 = NaN*zeros(jmax,1);
flagvs = NaN*zeros(jmax,1);
yv = NaN*zeros(jmax,1);

h = [];
while j<jmax && (flag==1||flag==3 || flag==4)

fun = @(guess) inverse_UniboCPR_31(guess,geometry,params,Kad2,qpv);
[sol,~,flag,~,jac] = fsolve(fun,guess0,options);
    jac = full(jac);
options = optimoptions('fsolve','display','iter-detailed','Maxiterations',50,...
    'SpecifyObjectiveGradient',true,'CheckGradients',false,'FunctionTolerance',1e-6,'OptimalityTolerance',1e-6,'StepTolerance',1e-4,...
       'Algorithm','levenberg-marquardt');
%% TUBE POSITION COMPUTATION
if j==1
    qa0 = sol(1:2);
    qt0 = repmat([5.2;zeros(3*Nf-1,1)],4,1);
    lambdat0 = zeros(4*5,1);
    guesst0 = [qa0;qt0;lambdat0];
    funt = @(guess) tubes_UniboCPR(guess,geometry,params,Kad1,sol(1:2));

[solt,~,~,~,jact] = fsolve(funt,guesst0,options);

end

%% PLOT & STRAIN RECOVERY
delete(h);
h = plotUniboCPR(sol,solt,geometry,Nf,100,geometry.hr);
pause(0.001);
drawnow

%% SINGULARITY AND EQUILIBRIUM STABILITY ANALYSIS

[flag1,flag2] = SingularitiesUniboPrototype(sol,jac,geometry,params);
flags = StabilityUniboPrototype(sol,jac,Nf,nj,params);
if flag1<1e-5
    a=1;
end
%% update
flagv1(j,1) = flag1;
% flagv1(j,1) = norm(inv(jac),Inf);

flagv2(j,1) = flag2;
flagvs(j,1) = flags;
yv(j,1) = qpv(4);
guess0 = sol;
qpv = qpv + [0;0;0;0.5*pi/180;0*pi/180;0];
j = j+1;

end

figure()
% yyaxis left
semilogy(180*yv/pi,flagv1,'r-')
hold on
semilogy(180*yv/pi,flagv2,'k-')
grid minor
% yyaxis right
% plot(180*yv/pi,flagvs,'-')