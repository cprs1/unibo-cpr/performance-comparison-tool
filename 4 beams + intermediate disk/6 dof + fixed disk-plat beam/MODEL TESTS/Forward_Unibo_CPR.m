%% Unibo CPR Structure - forward problem
% Assumed strain mode approach, forward-backward formulation
% Federico Zaccaria 19 Jan 2022

clear
close all
clc
%% PARAMETERS

[geometry,params,Kbt_1,Kbt_2] = createRobotParams();
params.g = [0;0;0]; % gravity
params.tipwrench = [0;0;0;0;0;0]; % tip wrench
[~,nj] = size(geometry.jointmatr);  

%% FORWARD PROBLEM
qpv = [0;0;0.42;0.42;0.4;0.4];

%% MODEL PARAMETERS
Nf = 4;
params.Nf = Nf;
M = @(s) PhiMatr(s,1,Nf);
Kad1 = integral(@(s) M(s)'*Kbt_1*M(s),0,1,'ArrayValued',true);
Kad2 = integral(@(s) M(s)'*Kbt_2*M(s),0,1,'ArrayValued',true);
params.rotparams = 'XYZ';
%% Solution
qa0 = qpv;
qe0 = repmat([0;zeros(3*Nf-1,1)],8,1);
qm0 = zeros(3*Nf,1);
Lf0 = [0.1;0.1;0.1;0.1];
qd0 = [0;0;0.3;0;0;0];
qp0 = [0;0;0.1;0;0;0];
lambda0 = zeros(4*nj+4*4+6,1);
guess0 = [qa0;qe0;qm0;Lf0;qd0;qp0;lambda0];

options = optimoptions('fsolve','display','iter-detailed','Algorithm','trust-region','SpecifyObjectiveGradient',true,'CheckGradients',false,'MaxIterations',5000);
fun = @(guess) forward_UniboCPR_disk(guess,geometry,params,Kad2,qpv);
[sol,~,~,~,jac] = fsolve(fun,guess0,options);
jac = full(jac);

%% TUBE POSITION COMPUTATION
qa0 = sol(1:2);
qt0 = repmat([5.2;zeros(3*Nf-1,1)],4,1);
lambdat0 = zeros(4*5,1);
guesst0 = [qa0;qt0;lambdat0];

funt = @(guess) tubes_UniboCPR(guess,geometry,params,Kad1,sol(1:2));

options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',false,'MaxIterations',5000);

[solt,~,~,~,jact] = fsolve(funt,guesst0,options);

%% PLOT & STRAIN RECOVERY
h = plotUniboCPR_disk(sol,solt,geometry,Nf,100,geometry.hr);

%% SINGULARITY AND EQUILIBRIUM STABILITY ANALYSIS

% [flag1,flag2] = SingularitiesUniboPrototype(sol,jac,geometry,params);
% flag = StabilityUniboPrototype(sol,jac,Nf,nj,params)
% 
% %% MECHANICAL LIMITS EVALUATION
% output = mechanicsUniboPrototype(sol,solt,params,geometry);
% 
% %% PERFORMANCES
% t = PerformancesUniboPrototype(sol,solt,jac,jact,nj,params,geometry);
