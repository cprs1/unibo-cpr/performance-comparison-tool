function [geometry,params,Kbt1,Kbt2] = createRobotParams()

%% TUNABLE PARAMETERS
EI = 0.165;
GJ = EI/1.30;

r = 0.0010; % beam radius
E_beam = 210*10^9; % beam young modulus
% G_beam = 80*10^9; % beam shear modulus
rho_beam = 0; % beam density
L_beam = 1;

L_tube = 0.50; % tube fixed length
rho_tube = 0; % tube density
% E_tube = 0*10^9; % tube young modulus

maxstrain = 0.06; %max strain

db = 0.30; % frame size
dp = 0.10; % mobile base size
dd = 0.08;
de = 0.05; % platform size
hr = 0.5; % ground height

geometry.Ls = 0.20;
%% JOINT 1: fixed joint vertical
Ci = [1,0,0,0,0,0;
      0,1,0,0,0,0;
      0,0,1,0,0,0;
      0,0,0,1,0,0;
      0,0,0,0,1,0;
      0,0,0,0,0,1];  % joint matrix: w = C lambda (mx,my,mz,fx,fy,fz) = C (mlx,mly,flx,fly,flz)


geometry.endanglesconvention = 'XYZ';
geometry.endangles = [-pi,-pi,-pi,-pi;0,0,0,0;0,0,0,0]; % moving platform angles (XYZ sequence)
geometry.platangles = [-pi,-pi,-pi,-pi;zeros(1,4);0,0,0,0]; % XYZ sequence

%% JOINT 1.a: fixed joint vertical + offset
% Ci = [1,0,0,0,0,0;
%       0,1,0,0,0,0;
%       0,0,1,0,0,0;
%       0,0,0,1,0,0;
%       0,0,0,0,1,0;
%       0,0,0,0,0,1];  % joint matrix: w = C lambda (mx,my,mz,fx,fy,fz) = C (mlx,mly,flx,fly,flz)
% 
% 
% geometry.endanglesconvention = 'XYZ';
% ang = +pi/12;
% geometry.endangles = [-pi,-pi,-pi,-pi;0,-ang,0,+ang;0,0,0,0]; % moving platform angles (XYZ sequence)
% geometry.platangles = [-pi,-pi,-pi,-pi;zeros(1,4);0,0,0,0]; % XYZ sequence
%% JOINT 2: revolute joint vertical
% Ci = [1,0,0,0,0;
%       0,1,0,0,0;
%       0,0,0,0,0;
%       0,0,1,0,0;
%       0,0,0,1,0;
%       0,0,0,0,1];  % joint matrix: w = C lambda (mx,my,mz,fx,fy,fz) = C (mlx,mly,flx,fly,flz)
% 
% geometry.endanglesconvention = 'XYZ';
% geometry.endangles = [-pi,-pi,-pi,-pi;0,0,0,0;0,0,0,0]; % moving platform angles (XYZ sequence)
% geometry.platangles = [-pi,-pi,-pi,-pi;zeros(1,4);0,0,0,0]; % XYZ sequence


%% Material Properties
% first part: free beam
A_1 = pi*r^2;
% I_1 = 0.25*pi*r^4;
% J_1 = 2*I_1;
% EI_1 = (E_beam+E_tube)*I_1;
% GJ_1 = G_beam*J_1;

EI_1 = EI;
GJ_1 = GJ;

Kbt1 = diag([EI_1;EI_1;GJ_1]);
rho_1 = rho_beam+rho_tube;

% second part: beam + tube
A_2 = pi*r^2;
% I_2 = 0.25*pi*r^4;
% J_2 = 2*I_2;
% EI_2 = (E_beam)*I_2;
% GJ_2 = G_beam*J_2;
EI_2 = EI;
GJ_2 = GJ;
Kbt2 = diag([EI_2;EI_2;GJ_2]);
rho_2 = rho_beam;

% distributed loads: gravity

fg_1 = rho_1*A_1;
fg_2 = rho_2*A_2;

params.fg1 = fg_1;
params.fg2 = fg_2;
params.Young = E_beam;

%% Mechanical limits
Lmin = 0.1;
Lmax = L_beam;

params.actlims = [Lmin,Lmax-L_tube];
% params.actlims = [0,+Inf];

params.displims = [-db/2,db/2];
% params.displims = [-Inf,+Inf];
% params.displims = [-0,+0.15];

params.maxstrain = maxstrain;

%% ROBOT GEOMETRY

%% columns
pf1 = [-db/2;-db/2;hr];
pf2 = [+db/2;-db/2;hr];
pf3 = [+db/2;+db/2;hr];
pf4 = [-db/2;+db/2;hr];

%% start tube points
pb1 = [-db/2;-db/2;hr];
pb2 = [+db/2;-db/2;hr];
pb3 = [+db/2;+db/2;hr];
pb4 = [-db/2;+db/2;hr];

% pb1 = [-0;-db/2;hr];
% pb2 = [+db/2;0;hr];
% pb3 = [0;+db/2;hr];
% pb4 = [-db/2;0;hr];

%% Mobile base

% % symmetric
pp1 = [-dp/2;-dp/2;0];
pp2 = [+dp/2;-dp/2;0];
pp3 = [+dp/2;+dp/2;0];
pp4 = [-dp/2;+dp/2;0];

%% Disk

% % symmetric
pd1 = [-dd/2;-dd/2;0];
pd2 = [+dd/2;-dd/2;0];
pd3 = [+dd/2;+dd/2;0];
pd4 = [-dd/2;+dd/2;0];

%% END EFFECTOR

% symmetric
pe1 = [-de/2;-de/2;0];
pe2 = [+de/2;-de/2;0];
pe3 = [+de/2;+de/2;0];
pe4 = [-de/2;+de/2;0];


%%
% base joint matrix : do not change
Ct = [1,0,0,0,0;
      0,1,0,0,0;
      0,0,0,0,0;
      0,0,1,0,0;
      0,0,0,1,0;
      0,0,0,0,1];  % joint matrix: w = C lambda (mx,my,mz,fx,fy,fz) = C (mlx,mly,flx,fly,flz)

% disk joint matrix : do not change
Cd = [1,0,0,0;
      0,1,0,0;
      0,0,0,0;
      0,0,1,0;
      0,0,0,1;
      0,0,0,0];  % joint matrix: w = C lambda (mx,my,mz,fx,fy,fz) = C (mlx,mly,flx,fly,flz)

Cs = [1,0,0,0,0;
      0,1,0,0,0;
      0,0,0,0,0;
      0,0,1,0,0;
      0,0,0,1,0;
      0,0,0,0,1;];
% Cs = eye(6);

geometry.basematr = Ct;
geometry.jointmatr = Ci;
geometry.diskmatr = Cd;
geometry.springmatr = Cs;

%% COLLECT
geometry.beamradius = r;
params.beamradius = r;
geometry.basepoints = [pb1,pb2,pb3,pb4];
params.basepoints = [pb1,pb2,pb3,pb4];
geometry.baseangles = [zeros(2,4); 3*pi/4, -3*pi/4, -pi/4, +pi/4];

geometry.framepoints =[pf1,pf2,pf3,pf4];
geometry.diskpoints = [pd1,pd2,pd3,pd4];
geometry.diskanglesconvention = 'XYZ';
% ang = +pi/12;
% geometry.diskangles = [-pi+ang,-pi,-pi-ang,-pi;0,0,0,0;0,0,0,0];
geometry.diskangles = [-pi,-pi,-pi,-pi;0,0,0,0;0,0,0,0];
geometry.platpoints = [pp1,pp2,pp3,pp4];
% geometry.platanglesperm = [0,0,1;1,0,0;0,1,0];
geometry.endpoints = [pe1,pe2,pe3,pe4];
geometry.L_tube = L_tube;
params.Ltube = L_tube;
geometry.hr = hr;

end