function [dtaudqa,dtaudqe,dtaudqet,dtaudlambda,dtaudlambdat] = getTorqueSensitivity(guess,guesst,params,geometry)

Nf = params.Nf;
Nftot = 3*params.Nf;
Ci = geometry.jointmatr;
[~,nj] = size(Ci);
platpoints = geometry.platpoints;
Ct = geometry.basematr;
[~,ntj] = size(Ct);
basepoints = geometry.basepoints;
baseangles = geometry.baseangles;

% initialize:
dFzdqa = zeros(4,6);
dFzdqe = zeros(4,4*Nftot);
dFzdlambda = zeros(4,4*nj);
dFadqa = zeros(2,6);
dFadqe = zeros(2,4*Nftot);
dFadqet = zeros(2,4*Nftot);
dFadlambda = zeros(2,4*nj);
dFadlambdat = zeros(2,4*5);

% extract variables
qet = guesst(1+2:2+4*Nftot,1);
lambdat = guesst(1+2+4*Nftot:2+4*Nftot+4*ntj,1);

L = geometry.L_tube;
g = params.g;
wd = [zeros(3,1);g*params.fg2];

% extract variables
[qa,qe,~,lambda] = unstack(guess,Nftot,nj);
Lv = qa(3:6,1);


for i = 1:4
%% LEGS.
    % extract variables of the th-i beam
    qei = qe(1+Nftot*(i-1):Nftot*i,1);
    lambdai = lambda(1+nj*(i-1):nj*i,1);
    Li = Lv(i);
    wt = (Ci*lambdai);
    % backward recursion
    [~,~,dwbdLvi,~,dwbdqei,~,dwbdw0i,~] = backwardRecursion(wt,qei,Nf,Li,wd); 
    dwbdqei = reshape(dwbdqei,6,Nftot);
    dwbdw0i = reshape(dwbdw0i,6,6);
    dFzdqa(i,2+i) = dwbdLvi(6);
    dFzdqe(i,1+Nftot*(i-1):Nftot*i) = dwbdqei(6,:);
    dwbdlambda = dwbdw0i*Ci;
    dFzdlambda(i,1+nj*(i-1):nj*i) = dwbdlambda(6,:);
      

%% TUBES
% extract variables of the th-i tube
    qeti = qet(1+Nftot*(i-1):Nftot*i,1);
    lambdati = lambdat(1+ntj*(i-1):ntj*i,1);
    p1 = basepoints(:,i);
    h1 = eul2quat(-baseangles(:,i)','XYZ')';
    wl = (Ct*lambdati);
    
    % forward recursion
    [~,h2,~,~,~,dh2dqeti]  = forwardRecursion(p1,h1,qeti,L,Nf);
    Rl = quat2rotmatrix(h2);
    dh2dqeti = reshape(dh2dqeti,4,Nftot);
    Adgl = [Rl,zeros(3);Rl,zeros(3)];
    [D1,D2,D3,~,~,~] = derivativeColRotMatQuat(h2);
    dwl_0dqeti = [(D1*wl(1)+D2*wl(2)+D3*wl(3))*dh2dqeti;(D1*wl(4)+D2*wl(5)+D3*wl(6))*dh2dqeti];
    dwl_0dlambdat = Adgl*Ct;

    pe = platpoints(:,i);
    Adge = [eye(3), skew(pe); zeros(3), eye(3)];
    deqwdqa = Adge*dwbdLvi;
    deqdwdqe = Adge*dwbdqei;
    deqdwdqet = Adge*dwl_0dqeti;
    deqdwdlambda = Adge*dwbdw0i*Ci;
    deqdwdlambdat = Adge*dwl_0dlambdat;
    dFadqa(:,2+i) = -deqwdqa(1:2);
    dFadqe(:,1+Nftot*(i-1):Nftot*i) = -deqdwdqe(1:2,:);
    dFadqet(:,1+Nftot*(i-1):Nftot*i) = -deqdwdqet(1:2,:);
    dFadlambda(:,1+nj*(i-1):nj*i) = -deqdwdlambda(1:2,:);
    dFadlambdat(:,1+5*(i-1):5*i) = -deqdwdlambdat(1:2,:);
end

%% COLLECT
dtaudqa  = [dFadqa;dFzdqa];
dtaudqe  = [dFadqe;dFzdqe]; 
dtaudqet = [dFadqet;zeros(4,4*Nftot)]; 
dtaudlambda = [dFadlambda;dFzdlambda]; 
dtaudlambdat = [dFadlambdat;zeros(4,4*5)];





end