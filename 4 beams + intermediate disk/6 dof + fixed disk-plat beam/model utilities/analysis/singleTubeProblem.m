function [eq,gradeq] = singleTubeProblem(guess,geometry,params,Kad,pb)

% extract parameters
Nf = params.Nf;
Nftot = 3*params.Nf;
Ci = geometry.basematr;
[~,nj] = size(Ci);
g = params.g;
wd = [zeros(3,1);g*params.fg2];

% extract variables
qa = guess(1:2,1);
qe = guess(1+2:2+Nftot,1);
lambda = guess(1+2+Nftot:2+Nftot+nj,1);

pplat = [qa;0];

L = geometry.L_tube;

    % extract variables of the th-i beam
    qei = qe;
    lambdai = lambda;
    p1 = zeros(3,1);
    h1 = eul2quat(zeros(1,3),'XYZ')';
    twrench = (Ci*lambdai);
    [Rj,dRpda,dRpdb,dRpdc] = rotationParametrization([-pi,0,0],'XYZ');

    % forward recursion
    [p2,h2,~,~,dp2dqei,dh2dqei]  = forwardRecursion(p1,h1,qei,L,Nf);
    dp2dqei = reshape(dp2dqei,3,Nftot);
    dh2dqei = reshape(dh2dqei,4,Nftot);
    R2 = quat2rotmatrix(h2);
    % backward recursion
    [~,Qc,~,~,~,dQcdqei,~,dQcdw0i] = backwardRecursion(twrench,qei,Nf,L,wd);
    dQcdqei = reshape(dQcdqei,Nftot,Nftot);
    dQcdw0i = reshape(dQcdw0i,Nftot,6);

    %% BEAM EQUILIBRIUMS
    beameq = L*Kad*qei + Qc;
    dbeameqdqe = L*Kad+dQcdqei;
    dbeameqdlambda = dQcdw0i*Ci;

    %% CONSTRAINTS
    platconstr = p2-pplat;
    dplatconstrdqei = dp2dqei;
    dplatconstrdqa = -[eye(2);zeros(1,2)];
    
    [platangerr,~,dplatangerrdqei,~] = roterr(Rj,R2,h2,zeros(4,1),dh2dqei,dRpda,dRpdb,dRpdc,geometry.platanglesperm);
    constr = Ci'*[platangerr;platconstr;];
    dconstrdqe =  Ci'*[dplatangerrdqei;dplatconstrdqei;];
    dconstrdqa = Ci'*[zeros(3,2);dplatconstrdqa];



%% COLLECT
inv = qa-pb;
eq = [beameq;constr;inv];

gradeq = [zeros(Nftot,2), dbeameqdqe, dbeameqdlambda;
          dconstrdqa, dconstrdqe, zeros(nj,nj);
          eye(2), zeros(2,Nftot), zeros(2,nj)];


end