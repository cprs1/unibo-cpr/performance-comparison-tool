/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ntrp45.h
 *
 * Code generation for function 'ntrp45'
 *
 */

#ifndef NTRP45_H
#define NTRP45_H

/* Include files */
#include "inverse_UniboCPR_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void ntrp45(const double t[3], double t0, const emxArray_real_T *b_y0, double h,
            const emxArray_real_T *f, emxArray_real_T *y);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (ntrp45.h) */
