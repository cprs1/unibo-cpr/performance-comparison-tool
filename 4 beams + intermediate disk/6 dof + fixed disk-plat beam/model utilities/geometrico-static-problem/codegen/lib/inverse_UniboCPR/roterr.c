/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * roterr.c
 *
 * Code generation for function 'roterr'
 *
 */

/* Include files */
#include "roterr.h"
#include "inverse_UniboCPR_emxutil.h"
#include "inverse_UniboCPR_types.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Declarations */
static void i_binary_expand_op(emxArray_real_T *in1, const double in2[9],
                               const emxArray_real_T *in3,
                               const emxArray_real_T *in4,
                               const emxArray_real_T *in5);

/* Function Definitions */
static void i_binary_expand_op(emxArray_real_T *in1, const double in2[9],
                               const emxArray_real_T *in3,
                               const emxArray_real_T *in4,
                               const emxArray_real_T *in5)
{
  emxArray_real_T *t_in2;
  emxArray_real_T *u_in2;
  emxArray_real_T *v_in2;
  const double *in3_data;
  const double *in4_data;
  const double *in5_data;
  double b_in2;
  double c_in2;
  double d_in2;
  double e_in2;
  double f_in2;
  double g_in2;
  double h_in2;
  double i_in2;
  double j_in2;
  double k_in2;
  double l_in2;
  double m_in2;
  double n_in2;
  double o_in2;
  double p_in2;
  double q_in2;
  double r_in2;
  double s_in2;
  double *b_in2_data;
  double *c_in2_data;
  double *in1_data;
  double *in2_data;
  int i;
  int in2_tmp;
  int loop_ub;
  int stride_0_1;
  int stride_1_1;
  int stride_2_1;
  int stride_3_1;
  in5_data = in5->data;
  in4_data = in4->data;
  in3_data = in3->data;
  b_in2 = in2[3];
  c_in2 = in2[4];
  d_in2 = in2[5];
  e_in2 = in2[6];
  f_in2 = in2[7];
  g_in2 = in2[8];
  h_in2 = in2[6];
  i_in2 = in2[7];
  j_in2 = in2[8];
  k_in2 = in2[0];
  l_in2 = in2[1];
  m_in2 = in2[2];
  n_in2 = in2[0];
  o_in2 = in2[1];
  p_in2 = in2[2];
  q_in2 = in2[3];
  r_in2 = in2[4];
  s_in2 = in2[5];
  emxInit_real_T(&t_in2, 2);
  i = t_in2->size[0] * t_in2->size[1];
  t_in2->size[0] = 1;
  if (in4->size[1] == 1) {
    t_in2->size[1] = in3->size[1];
  } else {
    t_in2->size[1] = in4->size[1];
  }
  emxEnsureCapacity_real_T(t_in2, i);
  in2_data = t_in2->data;
  stride_0_1 = (in3->size[1] != 1);
  stride_1_1 = (in4->size[1] != 1);
  stride_2_1 = (in4->size[1] != 1);
  stride_3_1 = (in4->size[1] != 1);
  if (in4->size[1] == 1) {
    loop_ub = in3->size[1];
  } else {
    loop_ub = in4->size[1];
  }
  for (i = 0; i < loop_ub; i++) {
    in2_data[i] = ((((b_in2 * in3_data[3 * (i * stride_0_1)] +
                      c_in2 * in3_data[3 * (i * stride_0_1) + 1]) +
                     d_in2 * in3_data[3 * (i * stride_0_1) + 2]) -
                    e_in2 * in4_data[3 * (i * stride_1_1)]) -
                   f_in2 * in4_data[3 * (i * stride_2_1) + 1]) -
                  g_in2 * in4_data[3 * (i * stride_3_1) + 2];
  }
  emxInit_real_T(&u_in2, 2);
  i = u_in2->size[0] * u_in2->size[1];
  u_in2->size[0] = 1;
  if (in3->size[1] == 1) {
    u_in2->size[1] = in5->size[1];
  } else {
    u_in2->size[1] = in3->size[1];
  }
  emxEnsureCapacity_real_T(u_in2, i);
  b_in2_data = u_in2->data;
  stride_0_1 = (in5->size[1] != 1);
  stride_1_1 = (in3->size[1] != 1);
  stride_2_1 = (in3->size[1] != 1);
  stride_3_1 = (in3->size[1] != 1);
  if (in3->size[1] == 1) {
    loop_ub = in5->size[1];
  } else {
    loop_ub = in3->size[1];
  }
  for (i = 0; i < loop_ub; i++) {
    in2_tmp = i * stride_0_1;
    b_in2_data[i] =
        ((((h_in2 * in5_data[3 * in2_tmp] + i_in2 * in5_data[3 * in2_tmp + 1]) +
           j_in2 * in5_data[3 * in2_tmp + 2]) -
          k_in2 * in3_data[3 * (i * stride_1_1)]) -
         l_in2 * in3_data[3 * (i * stride_2_1) + 1]) -
        m_in2 * in3_data[3 * (i * stride_3_1) + 2];
  }
  emxInit_real_T(&v_in2, 2);
  i = v_in2->size[0] * v_in2->size[1];
  v_in2->size[0] = 1;
  if (in5->size[1] == 1) {
    v_in2->size[1] = in4->size[1];
  } else {
    v_in2->size[1] = in5->size[1];
  }
  emxEnsureCapacity_real_T(v_in2, i);
  c_in2_data = v_in2->data;
  stride_0_1 = (in4->size[1] != 1);
  stride_1_1 = (in5->size[1] != 1);
  stride_2_1 = (in5->size[1] != 1);
  stride_3_1 = (in5->size[1] != 1);
  if (in5->size[1] == 1) {
    loop_ub = in4->size[1];
  } else {
    loop_ub = in5->size[1];
  }
  for (i = 0; i < loop_ub; i++) {
    in2_tmp = i * stride_0_1;
    c_in2_data[i] =
        ((((n_in2 * in4_data[3 * in2_tmp] + o_in2 * in4_data[3 * in2_tmp + 1]) +
           p_in2 * in4_data[3 * in2_tmp + 2]) -
          q_in2 * in5_data[3 * (i * stride_1_1)]) -
         r_in2 * in5_data[3 * (i * stride_2_1) + 1]) -
        s_in2 * in5_data[3 * (i * stride_3_1) + 2];
  }
  i = in1->size[0] * in1->size[1];
  in1->size[0] = 3;
  in1->size[1] = t_in2->size[1];
  emxEnsureCapacity_real_T(in1, i);
  in1_data = in1->data;
  loop_ub = t_in2->size[1];
  for (i = 0; i < loop_ub; i++) {
    in1_data[in1->size[0] * i] = in2_data[i];
  }
  emxFree_real_T(&t_in2);
  loop_ub = u_in2->size[1];
  for (i = 0; i < loop_ub; i++) {
    in1_data[in1->size[0] * i + 1] = b_in2_data[i];
  }
  emxFree_real_T(&u_in2);
  loop_ub = v_in2->size[1];
  for (i = 0; i < loop_ub; i++) {
    in1_data[in1->size[0] * i + 2] = c_in2_data[i];
  }
  emxFree_real_T(&v_in2);
}

void roterr(const double p[9], const double t[9], const double h[4],
            const emxArray_real_T *dhdqa, const emxArray_real_T *dhdqe,
            const double dpda[9], const double dpdb[9], const double dpdc[9],
            double err[3], double derrdqa_data[], int derrdqa_size[2],
            emxArray_real_T *derrdqe, double derrdrot[9])
{
  emxArray_real_T *dd1dqe;
  emxArray_real_T *dd2dqe;
  emxArray_real_T *dd3dqe;
  double D1[12];
  double D2[12];
  double D3[12];
  double dd1dqa[3];
  double dd2dqa[3];
  double dd3dqa[3];
  const double *dhdqa_data;
  const double *dhdqe_data;
  double D1_tmp;
  double D2_tmp;
  double b_D1_tmp;
  double c_D1_tmp;
  double *dd1dqe_data;
  double *dd2dqe_data;
  double *dd3dqe_data;
  double *derrdqe_data;
  int b_i;
  int boffset;
  int coffset;
  int i;
  int j;
  int n;
  dhdqe_data = dhdqe->data;
  dhdqa_data = dhdqa->data;
  /*  function [err,derrdqa,derrdqe,derrdrot] =
   * roterr(p,t,h,dhdqa,dhdqe,dpda,dpdb,dpdc,P) */
  /*  projection between joint and beam directors + permutation matrix */
  /*  d1p = p(:,1); d2p = p(:,2); d3p = p(:,3); */
  /*  d1t = t(:,1); d2t = t(:,2); d3t = t(:,3); */
  /*  errx = P(1,1)*d2p'*d1t+P(1,2)*d2p'*d2t+P(1,3)*d2p'*d3t; */
  /*  erry = P(2,1)*d3p'*d1t+P(2,2)*d3p'*d2t+P(2,3)*d3p'*d3t; */
  /*  errz = P(3,1)*d1p'*d1t+P(3,2)*d1p'*d2t+P(3,3)*d1p'*d3t; */
  err[0] = ((((p[3] * t[6] + p[4] * t[7]) + p[5] * t[8]) - t[3] * p[6]) -
            t[4] * p[7]) -
           t[5] * p[8];
  err[1] = ((((t[0] * p[6] + t[1] * p[7]) + t[2] * p[8]) - p[0] * t[6]) -
            p[1] * t[7]) -
           p[2] * t[8];
  err[2] = ((((p[0] * t[3] + p[1] * t[4]) + p[2] * t[5]) - t[0] * p[3]) -
            t[1] * p[4]) -
           t[2] * p[5];
  /*  DERIVATIVE OF ROTATION MATRIX COLUMNS WRT QUATERNION */
  /*  hL = h1 + h2 i + h3 j + h4 k (h1 = scalar component) */
  /*   D1 = derivative of R(:,1) wrt hL */
  /*   D2 = derivative of R(:,2) wrt hL */
  /*   D3 = derivative of R(:,3) wrt hL */
  D1[0] = 2.0 * h[0];
  D1[3] = 2.0 * h[1];
  D1_tmp = 2.0 * -h[2];
  D1[6] = D1_tmp;
  b_D1_tmp = 2.0 * -h[3];
  D1[9] = b_D1_tmp;
  D1[1] = 2.0 * h[3];
  D1[4] = 2.0 * h[2];
  D1[7] = 2.0 * h[1];
  D1[10] = 2.0 * h[0];
  D1[2] = D1_tmp;
  D1[5] = 2.0 * h[3];
  c_D1_tmp = 2.0 * -h[0];
  D1[8] = c_D1_tmp;
  D1[11] = 2.0 * h[1];
  D2[0] = b_D1_tmp;
  D2[3] = 2.0 * h[2];
  D2[6] = 2.0 * h[1];
  D2[9] = c_D1_tmp;
  D2[1] = 2.0 * h[0];
  D2_tmp = 2.0 * -h[1];
  D2[4] = D2_tmp;
  D2[7] = 2.0 * h[2];
  D2[10] = b_D1_tmp;
  D2[2] = 2.0 * h[1];
  D2[5] = 2.0 * h[0];
  D2[8] = 2.0 * h[3];
  D2[11] = 2.0 * h[2];
  D3[0] = 2.0 * h[2];
  D3[3] = 2.0 * h[3];
  D3[6] = 2.0 * h[0];
  D3[9] = 2.0 * h[1];
  D3[1] = D2_tmp;
  D3[4] = c_D1_tmp;
  D3[7] = 2.0 * h[3];
  D3[10] = 2.0 * h[2];
  D3[2] = 2.0 * h[0];
  D3[5] = D2_tmp;
  D3[8] = D1_tmp;
  D3[11] = 2.0 * h[3];
  for (j = 0; j < 3; j++) {
    dd3dqa[j] = ((D3[j] * dhdqa_data[0] + D3[j + 3] * dhdqa_data[1]) +
                 D3[j + 6] * dhdqa_data[2]) +
                D3[j + 9] * dhdqa_data[3];
    dd2dqa[j] = ((D2[j] * dhdqa_data[0] + D2[j + 3] * dhdqa_data[1]) +
                 D2[j + 6] * dhdqa_data[2]) +
                D2[j + 9] * dhdqa_data[3];
    dd1dqa[j] = ((D1[j] * dhdqa_data[0] + D1[j + 3] * dhdqa_data[1]) +
                 D1[j + 6] * dhdqa_data[2]) +
                D1[j + 9] * dhdqa_data[3];
  }
  n = dhdqe->size[1];
  emxInit_real_T(&dd1dqe, 2);
  j = dd1dqe->size[0] * dd1dqe->size[1];
  dd1dqe->size[0] = 3;
  dd1dqe->size[1] = dhdqe->size[1];
  emxEnsureCapacity_real_T(dd1dqe, j);
  dd1dqe_data = dd1dqe->data;
  for (j = 0; j < n; j++) {
    coffset = j * 3;
    boffset = j << 2;
    for (i = 0; i < 3; i++) {
      dd1dqe_data[coffset + i] =
          ((D1[i] * dhdqe_data[boffset] + D1[i + 3] * dhdqe_data[boffset + 1]) +
           D1[i + 6] * dhdqe_data[boffset + 2]) +
          D1[i + 9] * dhdqe_data[boffset + 3];
    }
  }
  n = dhdqe->size[1];
  emxInit_real_T(&dd2dqe, 2);
  j = dd2dqe->size[0] * dd2dqe->size[1];
  dd2dqe->size[0] = 3;
  dd2dqe->size[1] = dhdqe->size[1];
  emxEnsureCapacity_real_T(dd2dqe, j);
  dd2dqe_data = dd2dqe->data;
  for (j = 0; j < n; j++) {
    coffset = j * 3;
    boffset = j << 2;
    for (i = 0; i < 3; i++) {
      dd2dqe_data[coffset + i] =
          ((D2[i] * dhdqe_data[boffset] + D2[i + 3] * dhdqe_data[boffset + 1]) +
           D2[i + 6] * dhdqe_data[boffset + 2]) +
          D2[i + 9] * dhdqe_data[boffset + 3];
    }
  }
  n = dhdqe->size[1];
  emxInit_real_T(&dd3dqe, 2);
  j = dd3dqe->size[0] * dd3dqe->size[1];
  dd3dqe->size[0] = 3;
  dd3dqe->size[1] = dhdqe->size[1];
  emxEnsureCapacity_real_T(dd3dqe, j);
  dd3dqe_data = dd3dqe->data;
  for (j = 0; j < n; j++) {
    coffset = j * 3;
    boffset = j << 2;
    for (i = 0; i < 3; i++) {
      dd3dqe_data[coffset + i] =
          ((D3[i] * dhdqe_data[boffset] + D3[i + 3] * dhdqe_data[boffset + 1]) +
           D3[i + 6] * dhdqe_data[boffset + 2]) +
          D3[i + 9] * dhdqe_data[boffset + 3];
    }
  }
  /*      derrxdqa = P(1,1)*d2p'*dd1dqa+P(1,2)*d2p'*dd2dqa+P(1,3)*d2p'*dd3dqa;
   */
  /*      derrydqa = P(2,1)*d3p'*dd1dqa+P(2,2)*d3p'*dd2dqa+P(2,3)*d3p'*dd3dqa;
   */
  /*      derrzdqa = P(3,1)*d1p'*dd1dqa+P(3,2)*d1p'*dd2dqa+P(3,3)*d1p'*dd3dqa;
   */
  /*      derrxdqe = P(1,1)*d2p'*dd1dqe+P(1,2)*d2p'*dd2dqe+P(1,3)*d2p'*dd3dqe;
   */
  /*      derrydqe = P(2,1)*d3p'*dd1dqe+P(2,2)*d3p'*dd2dqe+P(2,3)*d3p'*dd3dqe;
   */
  /*      derrzdqe = P(3,1)*d1p'*dd1dqe+P(3,2)*d1p'*dd2dqe+P(3,3)*d1p'*dd3dqe;
   */
  derrdqa_size[0] = 3;
  derrdqa_size[1] = 1;
  derrdqa_data[0] =
      ((((dd3dqa[0] * p[3] + dd3dqa[1] * p[4]) + dd3dqa[2] * p[5]) -
        dd2dqa[0] * p[6]) -
       dd2dqa[1] * p[7]) -
      dd2dqa[2] * p[8];
  derrdqa_data[1] =
      ((((dd1dqa[0] * p[6] + dd1dqa[1] * p[7]) + dd1dqa[2] * p[8]) -
        p[0] * dd3dqa[0]) -
       p[1] * dd3dqa[1]) -
      p[2] * dd3dqa[2];
  derrdqa_data[2] =
      ((((p[0] * dd2dqa[0] + p[1] * dd2dqa[1]) + p[2] * dd2dqa[2]) -
        dd1dqa[0] * p[3]) -
       dd1dqa[1] * p[4]) -
      dd1dqa[2] * p[5];
  if (dd3dqe->size[1] == 1) {
    n = dd2dqe->size[1];
  } else {
    n = dd3dqe->size[1];
  }
  if (dd3dqe->size[1] == 1) {
    coffset = dd2dqe->size[1];
  } else {
    coffset = dd3dqe->size[1];
  }
  if (coffset == 1) {
    coffset = dd2dqe->size[1];
  } else if (dd3dqe->size[1] == 1) {
    coffset = dd2dqe->size[1];
  } else {
    coffset = dd3dqe->size[1];
  }
  if (dd1dqe->size[1] == 1) {
    boffset = dd3dqe->size[1];
  } else {
    boffset = dd1dqe->size[1];
  }
  if (dd1dqe->size[1] == 1) {
    j = dd3dqe->size[1];
  } else {
    j = dd1dqe->size[1];
  }
  if (j == 1) {
    j = dd3dqe->size[1];
  } else if (dd1dqe->size[1] == 1) {
    j = dd3dqe->size[1];
  } else {
    j = dd1dqe->size[1];
  }
  if (dd2dqe->size[1] == 1) {
    i = dd1dqe->size[1];
  } else {
    i = dd2dqe->size[1];
  }
  if (dd2dqe->size[1] == 1) {
    b_i = dd1dqe->size[1];
  } else {
    b_i = dd2dqe->size[1];
  }
  if (b_i == 1) {
    b_i = dd1dqe->size[1];
  } else if (dd2dqe->size[1] == 1) {
    b_i = dd1dqe->size[1];
  } else {
    b_i = dd2dqe->size[1];
  }
  if ((dd3dqe->size[1] == dd2dqe->size[1]) && (n == dd2dqe->size[1]) &&
      (coffset == dd2dqe->size[1]) && (dd1dqe->size[1] == dd3dqe->size[1]) &&
      (boffset == dd3dqe->size[1]) && (j == dd3dqe->size[1]) &&
      (dd2dqe->size[1] == dd1dqe->size[1]) && (i == dd1dqe->size[1]) &&
      (b_i == dd1dqe->size[1])) {
    j = derrdqe->size[0] * derrdqe->size[1];
    derrdqe->size[0] = 3;
    derrdqe->size[1] = dd3dqe->size[1];
    emxEnsureCapacity_real_T(derrdqe, j);
    derrdqe_data = derrdqe->data;
    coffset = dd3dqe->size[1];
    for (j = 0; j < coffset; j++) {
      boffset = 3 * j + 1;
      n = 3 * j + 2;
      derrdqe_data[derrdqe->size[0] * j] =
          ((((p[3] * dd3dqe_data[3 * j] + p[4] * dd3dqe_data[boffset]) +
             p[5] * dd3dqe_data[n]) -
            p[6] * dd2dqe_data[3 * j]) -
           p[7] * dd2dqe_data[boffset]) -
          p[8] * dd2dqe_data[n];
    }
    coffset = dd1dqe->size[1];
    for (j = 0; j < coffset; j++) {
      boffset = 3 * j + 1;
      n = 3 * j + 2;
      derrdqe_data[derrdqe->size[0] * j + 1] =
          ((((p[6] * dd1dqe_data[3 * j] + p[7] * dd1dqe_data[boffset]) +
             p[8] * dd1dqe_data[n]) -
            p[0] * dd3dqe_data[3 * j]) -
           p[1] * dd3dqe_data[boffset]) -
          p[2] * dd3dqe_data[n];
    }
    coffset = dd2dqe->size[1];
    for (j = 0; j < coffset; j++) {
      boffset = 3 * j + 1;
      n = 3 * j + 2;
      derrdqe_data[derrdqe->size[0] * j + 2] =
          ((((p[0] * dd2dqe_data[3 * j] + p[1] * dd2dqe_data[boffset]) +
             p[2] * dd2dqe_data[n]) -
            p[3] * dd1dqe_data[3 * j]) -
           p[4] * dd1dqe_data[boffset]) -
          p[5] * dd1dqe_data[n];
    }
  } else {
    i_binary_expand_op(derrdqe, p, dd3dqe, dd2dqe, dd1dqe);
  }
  emxFree_real_T(&dd3dqe);
  emxFree_real_T(&dd2dqe);
  emxFree_real_T(&dd1dqe);
  /*      dd1pda = dpda(:,1); dd2pda = dpda(:,2); dd3pda = dpda(:,3); */
  /*      dd1pdb = dpdb(:,1); dd2pdb = dpdb(:,2); dd3pdb = dpdb(:,3); */
  /*      dd1pdc = dpdc(:,1); dd2pdc = dpdc(:,2); dd3pdc = dpdc(:,3); */
  /*      derrxda = P(1,1)*dd2pda'*d1t+P(1,2)*dd2pda'*d2t+P(1,3)*dd2pda'*d3t; */
  /*      derryda = P(2,1)*dd3pda'*d1t+P(2,2)*dd3pda'*d2t+P(2,3)*dd3pda'*d3t; */
  /*      derrzda = P(3,1)*dd1pda'*d1t+P(3,2)*dd1pda'*d2t+P(3,3)*dd1pda'*d3t; */
  /*      derrxdb = P(1,1)*dd2pdb'*d1t+P(1,2)*dd2pdb'*d2t+P(1,3)*dd2pdb'*d3t; */
  /*      derrydb = P(2,1)*dd3pdb'*d1t+P(2,2)*dd3pdb'*d2t+P(2,3)*dd3pdb'*d3t; */
  /*      derrzdb = P(3,1)*dd1pdb'*d1t+P(3,2)*dd1pdb'*d2t+P(3,3)*dd1pdb'*d3t; */
  /*      derrxdc = P(1,1)*dd2pdc'*d1t+P(1,2)*dd2pdc'*d2t+P(1,3)*dd2pdc'*d3t; */
  /*      derrydc = P(2,1)*dd3pdc'*d1t+P(2,2)*dd3pdc'*d2t+P(2,3)*dd3pdc'*d3t; */
  /*      derrzdc = P(3,1)*dd1pdc'*d1t+P(3,2)*dd1pdc'*d2t+P(3,3)*dd1pdc'*d3t; */
  derrdrot[0] =
      ((((dpda[3] * t[6] + dpda[4] * t[7]) + dpda[5] * t[8]) - t[3] * dpda[6]) -
       t[4] * dpda[7]) -
      t[5] * dpda[8];
  derrdrot[3] =
      ((((dpdb[3] * t[6] + dpdb[4] * t[7]) + dpdb[5] * t[8]) - t[3] * dpdb[6]) -
       t[4] * dpdb[7]) -
      t[5] * dpdb[8];
  derrdrot[6] =
      ((((dpdc[3] * t[6] + dpdc[4] * t[7]) + dpdc[5] * t[8]) - t[3] * dpdc[6]) -
       t[4] * dpdc[7]) -
      t[5] * dpdc[8];
  derrdrot[1] =
      ((((t[0] * dpda[6] + t[1] * dpda[7]) + t[2] * dpda[8]) - dpda[0] * t[6]) -
       dpda[1] * t[7]) -
      dpda[2] * t[8];
  derrdrot[4] =
      ((((t[0] * dpdb[6] + t[1] * dpdb[7]) + t[2] * dpdb[8]) - dpdb[0] * t[6]) -
       dpdb[1] * t[7]) -
      dpdb[2] * t[8];
  derrdrot[7] =
      ((((t[0] * dpdc[6] + t[1] * dpdc[7]) + t[2] * dpdc[8]) - dpdc[0] * t[6]) -
       dpdc[1] * t[7]) -
      dpdc[2] * t[8];
  derrdrot[2] =
      ((((dpda[0] * t[3] + dpda[1] * t[4]) + dpda[2] * t[5]) - t[0] * dpda[3]) -
       t[1] * dpda[4]) -
      t[2] * dpda[5];
  derrdrot[5] =
      ((((dpdb[0] * t[3] + dpdb[1] * t[4]) + dpdb[2] * t[5]) - t[0] * dpdb[3]) -
       t[1] * dpdb[4]) -
      t[2] * dpdb[5];
  derrdrot[8] =
      ((((dpdc[0] * t[3] + dpdc[1] * t[4]) + dpdc[2] * t[5]) - t[0] * dpdc[3]) -
       t[1] * dpdc[4]) -
      t[2] * dpdc[5];
}

/* End of code generation (roterr.c) */
