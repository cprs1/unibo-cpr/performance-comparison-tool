/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * roterr.h
 *
 * Code generation for function 'roterr'
 *
 */

#ifndef ROTERR_H
#define ROTERR_H

/* Include files */
#include "inverse_UniboCPR_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void roterr(const double p[9], const double t[9], const double h[4],
            const emxArray_real_T *dhdqa, const emxArray_real_T *dhdqe,
            const double dpda[9], const double dpdb[9], const double dpdc[9],
            double err[3], double derrdqa_data[], int derrdqa_size[2],
            emxArray_real_T *derrdqe, double derrdrot[9]);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (roterr.h) */
