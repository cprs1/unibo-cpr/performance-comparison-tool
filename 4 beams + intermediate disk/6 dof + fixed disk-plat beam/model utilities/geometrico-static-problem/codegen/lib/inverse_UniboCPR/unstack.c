/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * unstack.c
 *
 * Code generation for function 'unstack'
 *
 */

/* Include files */
#include "unstack.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */
void unstack(const double guess_data[], double Nftot, double qa[6],
             double qe_data[], int *qe_size, double qp_data[], int *qp_size,
             double lambda_data[], int *lambda_size)
{
  double d;
  double d1;
  int i;
  int i1;
  for (i = 0; i < 6; i++) {
    qa[i] = guess_data[i];
  }
  d = 4.0 * Nftot + 6.0;
  if (d < 7.0) {
    i = 0;
    i1 = 0;
  } else {
    i = 6;
    i1 = (int)d;
  }
  *qe_size = i1 - i;
  for (i1 = 0; i1 < *qe_size; i1++) {
    qe_data[i1] = guess_data[i + i1];
  }
  d1 = 4.0 * Nftot + 7.0;
  if (d1 > d + 6.0) {
    i = 0;
    i1 = 0;
  } else {
    i = (int)d1 - 1;
    i1 = (int)(d + 6.0);
  }
  *qp_size = i1 - i;
  for (i1 = 0; i1 < *qp_size; i1++) {
    qp_data[i1] = guess_data[i + i1];
  }
  if (d1 + 6.0 > (d + 6.0) + 20.0) {
    i = 0;
    i1 = 0;
  } else {
    i = (int)(d1 + 6.0) - 1;
    i1 = (int)((d + 6.0) + 20.0);
  }
  *lambda_size = i1 - i;
  for (i1 = 0; i1 < *lambda_size; i1++) {
    lambda_data[i1] = guess_data[i + i1];
  }
}

/* End of code generation (unstack.c) */
