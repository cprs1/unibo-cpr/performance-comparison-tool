/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * main.c
 *
 * Code generation for function 'main'
 *
 */

/*************************************************************************/
/* This automatically generated example C main file shows how to call    */
/* entry-point functions that MATLAB Coder generated. You must customize */
/* this file for your application. Do not modify this file directly.     */
/* Instead, make a copy of this file, modify it, and integrate it into   */
/* your development environment.                                         */
/*                                                                       */
/* This file initializes entry-point function arguments to a default     */
/* size and value before calling the entry-point functions. It does      */
/* not store or use any values returned from the entry-point functions.  */
/* If necessary, it does pre-allocate memory for returned values.        */
/* You can use this file as a starting point for a main function that    */
/* you can deploy in your application.                                   */
/*                                                                       */
/* After you copy the file, and before you deploy it, you must make the  */
/* following changes:                                                    */
/* * For variable-size function arguments, change the example sizes to   */
/* the sizes that your application requires.                             */
/* * Change the example values of function arguments to the values that  */
/* your application requires.                                            */
/* * If the entry-point functions return values, store these values or   */
/* otherwise use them as required by your application.                   */
/*                                                                       */
/*************************************************************************/

/* Include files */
#include "main.h"
#include "inverse_UniboCPR.h"
#include "inverse_UniboCPR_emxAPI.h"
#include "inverse_UniboCPR_terminate.h"
#include "inverse_UniboCPR_types.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Declarations */
static void argInit_12x12_real_T(double result[144]);

static void argInit_1x2_real_T(double result[2]);

static void argInit_1x3_char_T(char result[3]);

static void argInit_1xd3_char_T(char result_data[], int result_size[2]);

static void argInit_3x1_real_T(double result[3]);

static void argInit_3x3_real_T(double result[9]);

static void argInit_3x4_real_T(double result[12]);

static void argInit_6x1_real_T(double result[6]);

static void argInit_6x5_real_T(double result[30]);

static char argInit_char_T(void);

static void argInit_d86x1_real_T(double result_data[], int *result_size);

static double argInit_real_T(void);

static void argInit_struct0_T(struct0_T *result);

static void argInit_struct1_T(struct1_T *result);

/* Function Definitions */
static void argInit_12x12_real_T(double result[144])
{
  int i;
  /* Loop over the array to initialize each element. */
  for (i = 0; i < 144; i++) {
    /* Set the value of the array element.
Change this value to the value that the application requires. */
    result[i] = argInit_real_T();
  }
}

static void argInit_1x2_real_T(double result[2])
{
  int idx1;
  /* Loop over the array to initialize each element. */
  for (idx1 = 0; idx1 < 2; idx1++) {
    /* Set the value of the array element.
Change this value to the value that the application requires. */
    result[idx1] = argInit_real_T();
  }
}

static void argInit_1x3_char_T(char result[3])
{
  int idx1;
  /* Loop over the array to initialize each element. */
  for (idx1 = 0; idx1 < 3; idx1++) {
    /* Set the value of the array element.
Change this value to the value that the application requires. */
    result[idx1] = argInit_char_T();
  }
}

static void argInit_1xd3_char_T(char result_data[], int result_size[2])
{
  int idx1;
  /* Set the size of the array.
Change this size to the value that the application requires. */
  result_size[0] = 1;
  result_size[1] = 2;
  /* Loop over the array to initialize each element. */
  for (idx1 = 0; idx1 < 2; idx1++) {
    /* Set the value of the array element.
Change this value to the value that the application requires. */
    result_data[idx1] = argInit_char_T();
  }
}

static void argInit_3x1_real_T(double result[3])
{
  int idx0;
  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 3; idx0++) {
    /* Set the value of the array element.
Change this value to the value that the application requires. */
    result[idx0] = argInit_real_T();
  }
}

static void argInit_3x3_real_T(double result[9])
{
  int i;
  /* Loop over the array to initialize each element. */
  for (i = 0; i < 9; i++) {
    /* Set the value of the array element.
Change this value to the value that the application requires. */
    result[i] = argInit_real_T();
  }
}

static void argInit_3x4_real_T(double result[12])
{
  int i;
  /* Loop over the array to initialize each element. */
  for (i = 0; i < 12; i++) {
    /* Set the value of the array element.
Change this value to the value that the application requires. */
    result[i] = argInit_real_T();
  }
}

static void argInit_6x1_real_T(double result[6])
{
  int idx0;
  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 6; idx0++) {
    /* Set the value of the array element.
Change this value to the value that the application requires. */
    result[idx0] = argInit_real_T();
  }
}

static void argInit_6x5_real_T(double result[30])
{
  int i;
  /* Loop over the array to initialize each element. */
  for (i = 0; i < 30; i++) {
    /* Set the value of the array element.
Change this value to the value that the application requires. */
    result[i] = argInit_real_T();
  }
}

static char argInit_char_T(void)
{
  return '?';
}

static void argInit_d86x1_real_T(double result_data[], int *result_size)
{
  int idx0;
  /* Set the size of the array.
Change this size to the value that the application requires. */
  *result_size = 2;
  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 2; idx0++) {
    /* Set the value of the array element.
Change this value to the value that the application requires. */
    result_data[idx0] = argInit_real_T();
  }
}

static double argInit_real_T(void)
{
  return 0.0;
}

static void argInit_struct0_T(struct0_T *result)
{
  double result_tmp;
  int i;
  /* Set the value of each structure field.
Change this value to the value that the application requires. */
  argInit_3x4_real_T(result->endangles);
  argInit_6x5_real_T(result->basematr);
  result_tmp = argInit_real_T();
  result->L_tube = result_tmp;
  result->hr = result_tmp;
  argInit_1x3_char_T(result->endanglesconvention);
  result->beamradius = result_tmp;
  argInit_3x3_real_T(result->platanglesperm);
  memcpy(&result->platangles[0], &result->endangles[0], 12U * sizeof(double));
  memcpy(&result->jointmatr[0], &result->basematr[0], 30U * sizeof(double));
  for (i = 0; i < 12; i++) {
    result->basepoints[i] = result->endangles[i];
    result->baseangles[i] = result->endangles[i];
    result->framepoints[i] = result->endangles[i];
    result->platpoints[i] = result->endangles[i];
    result->endpoints[i] = result->endangles[i];
  }
}

static void argInit_struct1_T(struct1_T *result)
{
  double result_tmp;
  /* Set the value of each structure field.
Change this value to the value that the application requires. */
  result_tmp = argInit_real_T();
  result->fg2 = result_tmp;
  result->Young = result_tmp;
  argInit_1x2_real_T(result->actlims);
  result->maxstrain = result_tmp;
  result->beamradius = result_tmp;
  result->Ltube = result_tmp;
  result->Nf = result_tmp;
  argInit_3x1_real_T(result->g);
  result->fg1 = result_tmp;
  argInit_3x4_real_T(result->basepoints);
  argInit_6x1_real_T(result->tipwrench);
  argInit_1xd3_char_T(result->rotparams.data, result->rotparams.size);
  result->displims[0] = result->actlims[0];
  result->displims[1] = result->actlims[1];
  result->pstart[0] = result->g[0];
  result->pstart[1] = result->g[1];
  result->pstart[2] = result->g[2];
  result->pstartang[0] = result->actlims[0];
  result->pstartang[1] = result->actlims[1];
}

int main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  /* The initialize function is being called automatically from your entry-point
   * function. So, a call to initialize is not included here. */
  /* Invoke the entry-point functions.
You can call entry-point functions multiple times. */
  main_inverse_UniboCPR();
  /* Terminate the application.
You do not need to do this more than one time. */
  inverse_UniboCPR_terminate();
  return 0;
}

void main_inverse_UniboCPR(void)
{
  emxArray_real_T *eq;
  emxArray_real_T *gradeq;
  struct0_T r;
  struct1_T r1;
  double b_dv[144];
  double guess_data[86];
  double b_dv1[6];
  int guess_size;
  /* Initialize function 'inverse_UniboCPR' input arguments. */
  /* Initialize function input argument 'guess'. */
  argInit_d86x1_real_T(guess_data, &guess_size);
  /* Initialize function input argument 'geometry'. */
  /* Initialize function input argument 'params'. */
  /* Initialize function input argument 'Kad'. */
  /* Initialize function input argument 'qpv'. */
  /* Call the entry-point 'inverse_UniboCPR'. */
  emxInitArray_real_T(&eq, 1);
  emxInitArray_real_T(&gradeq, 2);
  argInit_struct0_T(&r);
  argInit_struct1_T(&r1);
  argInit_12x12_real_T(b_dv);
  argInit_6x1_real_T(b_dv1);
  inverse_UniboCPR(guess_data, &guess_size, &r, &r1, b_dv, b_dv1, eq, gradeq);
  emxDestroyArray_real_T(eq);
  emxDestroyArray_real_T(gradeq);
}

/* End of code generation (main.c) */
