function [h,dhdeul] = rot2quatmod(Rp,dRpda,dRpdb,dRpdc)

if trace(Rp)<-1
    error('problem in rot2quatmod')
end

w = sqrt(1+Rp(1,1)+Rp(2,2)+Rp(3,3))/2;
w4 = 4*w;
x = (Rp(3,2)-Rp(2,3))/w4;
y = (Rp(1,3)-Rp(3,1))/w4;
z = (Rp(2,1)-Rp(1,2))/w4;
h = [w;x;y;z];

dhdeul = [];

if nargin>1
    dwda = (1/2)*(1/(2*sqrt(1+Rp(1,1)+Rp(2,2)+Rp(3,3))))*(dRpda(1,1)+dRpda(2,2)+dRpda(3,3));
    dwdb = (1/2)*(1/(2*sqrt(1+Rp(1,1)+Rp(2,2)+Rp(3,3))))*(dRpdb(1,1)+dRpdb(2,2)+dRpdb(3,3));
    dwdc = (1/2)*(1/(2*sqrt(1+Rp(1,1)+Rp(2,2)+Rp(3,3))))*(dRpdc(1,1)+dRpdc(2,2)+dRpdc(3,3));
    dwdeul = [dwda,dwdb,dwdc];

    dw4da = 4*dwda;
    dw4db = 4*dwdb;
    dw4dc = 4*dwdc;

    dxda = ( (dRpda(3,2)-dRpda(2,3))*w4 - (Rp(3,2)-Rp(2,3))*dw4da) /(w4)^2;
    dxdb = ( (dRpdb(3,2)-dRpdb(2,3))*w4 - (Rp(3,2)-Rp(2,3))*dw4db) /(w4)^2;
    dxdc = ( (dRpdc(3,2)-dRpdc(2,3))*w4 - (Rp(3,2)-Rp(2,3))*dw4dc) /(w4)^2;
    dxdeul = [dxda,dxdb,dxdc];

    dyda = ( (dRpda(1,3)-dRpda(3,1))*w4 - (Rp(1,3)-Rp(3,1))*dw4da) /(w4)^2;
    dydb = ( (dRpdb(1,3)-dRpdb(3,1))*w4 - (Rp(1,3)-Rp(3,1))*dw4db) /(w4)^2;
    dydc = ( (dRpdc(1,3)-dRpdc(3,1))*w4 - (Rp(1,3)-Rp(3,1))*dw4dc) /(w4)^2;
    dydeul = [dyda,dydb,dydc];

    dzda = ( (dRpda(2,1)-dRpda(1,2))*w4 - (Rp(2,1)-Rp(1,2))*dw4da) /(w4)^2;
    dzdb = ( (dRpdb(2,1)-dRpdb(1,2))*w4 - (Rp(2,1)-Rp(1,2))*dw4db) /(w4)^2;
    dzdc = ( (dRpdc(2,1)-dRpdc(1,2))*w4 - (Rp(2,1)-Rp(1,2))*dw4dc) /(w4)^2;
    dzdeul = [dzda,dzdb,dzdc];

    dhdeul = [dwdeul;dxdeul;dydeul;dzdeul];
end

end