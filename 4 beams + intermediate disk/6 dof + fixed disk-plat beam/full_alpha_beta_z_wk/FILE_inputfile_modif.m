%% FLOODING ALGORITHM - Spatial CASE
% F.Zaccaria 02 February 2022
% extension to the spatial case of the algorithm provided by "Singularity
% Conditions for Continuum Parallel Robot"

%% INPUT FILE


maxiter = 5;
TOL = 10^-5;
TOL2 = Inf; 
stepsizeZ = 0.02; %X 
stepsizeA = 0.02; %y
stepsizeB = 0.02; %Z NB DO NOT CHANGE WRT PREVIOUS
stepsizerad = 2*pi/180; % NB: do not change wrt previous
stepsizetilt = 2*pi/180; % NB: do not change wrt previous
% boxsize = [-0.3 0.3 -0.3 0.3 0.29 0.31];
boxsize = [-0.3 0.3 -0.3 0.3 0.09 0.11];

params.boxsizeang = [-pi,+pi, 0*pi/180, pi/2];


%% STORE VARIABLES

params.stepsizeZ = stepsizeZ;
params.stepsizeA = stepsizeA;
params.stepsizeB = stepsizeB;
params.stepsizerad = stepsizerad;
params.stepsizetilt = stepsizetilt;
params.maxiter = maxiter;
params.boxsize = boxsize;
params.TOL = TOL;
params.TOL2 = TOL2;
params.solver = 'fsolve';


