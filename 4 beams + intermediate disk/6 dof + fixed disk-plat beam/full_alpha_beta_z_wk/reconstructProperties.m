function [iDx,iDy,iDu,A,flag]= reconstructProperties(pend,WKold,guesses,params,fcn)

% extract points on z slice from WKold
z_coord = pend(3);
z_step = params.stepsizeZ;
ids_slice = WKold(abs(WKold(:,2)-z_coord)<z_step,1);
z_slice = WKold(ids_slice(1),2);

if numel(ids_slice)>0
    WKslice = [WKold(ids_slice,1),WKold(ids_slice,3:end)];
    guesses_slice = guesses(:,ids_slice);
    id = numel(WKslice(:,1));
    WKslice(:,1) = (1:1:id)';
    % planar flooding over the slice
    [WK,flag] = Modified_FloodingPlanarWK(fcn,params,WKslice,guesses_slice,pend);
    % recover values
    nn = numel(WK(:,1));
    WK = [WK(:,1),z_slice*ones(nn,1),WK(:,2:5)];
    [iDx,iDy,iDu] = index_computation(WK,params);
    idwk = WK(WK(:,5)==1,1);
    A = numel(idwk)*(params.stepsizerad*params.stepsizetilt);
else
    iDx = 0;
    iDy = 0;
    iDu = 0;
    flag = 0;
end

end