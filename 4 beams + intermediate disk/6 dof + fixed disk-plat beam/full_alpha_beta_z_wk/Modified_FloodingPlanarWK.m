function [WK,flag] = Modified_FloodingPlanarWK(fcn,params,WKold,guesses,pendrail)
tic;

xP = pendrail(1);
yP = pendrail(2);
%% COLLECT INPUTS
pstart = params.pstartang;

%% DEFINE GRID
table = [WKold(:,2:3)];
np = numel(table(:,1));
idx = 1:1:np;
WK = [idx',table,zeros(np,2)];

%% SOLVE FIRST ITERATION
id2 = getNeighborsPlanar(WK,pstart,params,1.43);
pend = [WK(id2(1),2);WK(id2(1),3)];
flag = checkLimits(  WKold(id2(1),4) , guesses(:,id2(1)) , fcn, xP,yP, params);
% store results
WK(id2(1),4:5) = [flag,1];


% initialize
if flag==1
    idn2 = getNeighborsPlanar(WK,pend,params,1.43);
    idn2 = idn2((idn2~=id2(1))==1);
    wk_todo = idn2;
else
    wk_todo = [];
end

%% MAIN LOOP
while numel(wk_todo)>0
        
    %% EXTRACT POINT
    current_idx = wk_todo(1);
    WK(current_idx,5) = 1;
    wk_todo = wk_todo(2:end);
    pend = [WK(current_idx,2);WK(current_idx,3)];
    
    idw = getNeighborsPlanar(WK,pend,params,1.43);
    idw = idw((idw~=current_idx)==1);
    
        
    flags = checkLimits(  WKold(current_idx,4) , guesses(:,current_idx), fcn , xP,yP, params);
    WK(current_idx,4:5) = [flags,1];

    % if ok: not T1 singu and feasible
    if (flags==1)
       
        idny2 = idw(WK(idw,4)==0 & WK(idw,5)==0);
        wk_todo = [wk_todo;idny2];
        WK(idny2,5) = 1;

     % if T1 singu or not feasible
    else
        WK(current_idx,4) = 3;
    end

end
