function meanindices = FCN_plotIndices(WK,geometry,params)

idwk = WK(:,5) == 1;
vol = (params.stepsizeA*params.stepsizeB*params.stepsizeZ)*numel(WK(idwk,1));
idlimits = WK(:,5)==3;
framepoints = geometry.framepoints;
meanindices.orientX = mean(WK(idwk,7));
meanindices.orientY = mean(WK(idwk,8));
meanindices.orientU = mean(WK(idwk,9));
meanindices.orientA = mean(WK(idwk,10));
meanindices.volume = vol;

% figure()
% plot3(WK(idwk,2),WK(idwk,3),WK(idwk,4),'b.')
% hold on
% plot3(WK(idlimits,2),WK(idlimits,3),WK(idlimits,4),'r.')
% 
% xlabel('x_{[m]}')
% ylabel('y_{[m]}')
% zlabel('z_{[m]}')
% title('Workspace')
% grid on
% axis equal
% axis(boxsize)


%% idx X
figure()
subplot(2,2,1)
scatter3(WK(idwk,2),WK(idwk,3),WK(idwk,4),20,WK(idwk,7),'filled')
hold on
plot3(WK(idlimits,2),WK(idlimits,3),WK(idlimits,4),'r.')

%% FRAME

h1 = line([framepoints(1,1),framepoints(1,2)],[framepoints(2,1),framepoints(2,2)],[framepoints(3,1),framepoints(3,2)],'Color','black','LineWidth',2);
h2 = line([framepoints(1,2),framepoints(1,3)],[framepoints(2,2),framepoints(2,3)],[framepoints(3,2),framepoints(3,3)],'Color','black','LineWidth',2);
h3 = line([framepoints(1,3),framepoints(1,4)],[framepoints(2,3),framepoints(2,4)],[framepoints(3,3),framepoints(3,4)],'Color','black','LineWidth',2);
h4 = line([framepoints(1,4),framepoints(1,1)],[framepoints(2,4),framepoints(2,1)],[framepoints(3,4),framepoints(3,1)],'Color','black','LineWidth',2);
h5 = line([framepoints(1,1),framepoints(1,1)],[framepoints(2,1),framepoints(2,1)],[0,framepoints(3,1)],'Color','black','LineWidth',2);
h6 = line([framepoints(1,2),framepoints(1,2)],[framepoints(2,2),framepoints(2,2)],[0,framepoints(3,2)],'Color','black','LineWidth',2);
h7 = line([framepoints(1,3),framepoints(1,3)],[framepoints(2,3),framepoints(2,3)],[0,framepoints(3,3)],'Color','black','LineWidth',2);
h8 = line([framepoints(1,4),framepoints(1,4)],[framepoints(2,4),framepoints(2,4)],[0,framepoints(3,4)],'Color','black','LineWidth',2);


xlabel('x_{[m]}')
ylabel('y_{[m]}')
zlabel('z_{[m]}')
title(['Orientability +X: mean=' num2str(meanindices.orientX)])
colorbar
caxis([0 pi/2])
grid on
axis equal

%% idx Y
% figure()
subplot(2,2,2)
scatter3(WK(idwk,2),WK(idwk,3),WK(idwk,4),20,WK(idwk,8),'filled')
hold on
plot3(WK(idlimits,2),WK(idlimits,3),WK(idlimits,4),'r.')

h1 = line([framepoints(1,1),framepoints(1,2)],[framepoints(2,1),framepoints(2,2)],[framepoints(3,1),framepoints(3,2)],'Color','black','LineWidth',2);
h2 = line([framepoints(1,2),framepoints(1,3)],[framepoints(2,2),framepoints(2,3)],[framepoints(3,2),framepoints(3,3)],'Color','black','LineWidth',2);
h3 = line([framepoints(1,3),framepoints(1,4)],[framepoints(2,3),framepoints(2,4)],[framepoints(3,3),framepoints(3,4)],'Color','black','LineWidth',2);
h4 = line([framepoints(1,4),framepoints(1,1)],[framepoints(2,4),framepoints(2,1)],[framepoints(3,4),framepoints(3,1)],'Color','black','LineWidth',2);
h5 = line([framepoints(1,1),framepoints(1,1)],[framepoints(2,1),framepoints(2,1)],[0,framepoints(3,1)],'Color','black','LineWidth',2);
h6 = line([framepoints(1,2),framepoints(1,2)],[framepoints(2,2),framepoints(2,2)],[0,framepoints(3,2)],'Color','black','LineWidth',2);
h7 = line([framepoints(1,3),framepoints(1,3)],[framepoints(2,3),framepoints(2,3)],[0,framepoints(3,3)],'Color','black','LineWidth',2);
h8 = line([framepoints(1,4),framepoints(1,4)],[framepoints(2,4),framepoints(2,4)],[0,framepoints(3,4)],'Color','black','LineWidth',2);

xlabel('x_{[m]}')
ylabel('y_{[m]}')
zlabel('z_{[m]}')
title(['Orientability +Y: mean=' num2str(meanindices.orientY)])
colorbar
caxis([0 pi/2])
grid on
axis equal

%% idx U
% figure()
subplot(2,2,3)
scatter3(WK(idwk,2),WK(idwk,3),WK(idwk,4),20,WK(idwk,9),'filled')
hold on
plot3(WK(idlimits,2),WK(idlimits,3),WK(idlimits,4),'r.')

h1 = line([framepoints(1,1),framepoints(1,2)],[framepoints(2,1),framepoints(2,2)],[framepoints(3,1),framepoints(3,2)],'Color','black','LineWidth',2);
h2 = line([framepoints(1,2),framepoints(1,3)],[framepoints(2,2),framepoints(2,3)],[framepoints(3,2),framepoints(3,3)],'Color','black','LineWidth',2);
h3 = line([framepoints(1,3),framepoints(1,4)],[framepoints(2,3),framepoints(2,4)],[framepoints(3,3),framepoints(3,4)],'Color','black','LineWidth',2);
h4 = line([framepoints(1,4),framepoints(1,1)],[framepoints(2,4),framepoints(2,1)],[framepoints(3,4),framepoints(3,1)],'Color','black','LineWidth',2);
h5 = line([framepoints(1,1),framepoints(1,1)],[framepoints(2,1),framepoints(2,1)],[0,framepoints(3,1)],'Color','black','LineWidth',2);
h6 = line([framepoints(1,2),framepoints(1,2)],[framepoints(2,2),framepoints(2,2)],[0,framepoints(3,2)],'Color','black','LineWidth',2);
h7 = line([framepoints(1,3),framepoints(1,3)],[framepoints(2,3),framepoints(2,3)],[0,framepoints(3,3)],'Color','black','LineWidth',2);
h8 = line([framepoints(1,4),framepoints(1,4)],[framepoints(2,4),framepoints(2,4)],[0,framepoints(3,4)],'Color','black','LineWidth',2);

xlabel('x_{[m]}')
ylabel('y_{[m]}')
zlabel('z_{[m]}')
title(['Uniformity: mean=' num2str(meanindices.orientU)])
colorbar
caxis([0 pi/2])
grid on
axis equal

%% idx A
% figure()
subplot(2,2,4)
scatter3(WK(idwk,2),WK(idwk,3),WK(idwk,4),20,WK(idwk,10),'filled')
hold on
plot3(WK(idlimits,2),WK(idlimits,3),WK(idlimits,4),'r.')

h1 = line([framepoints(1,1),framepoints(1,2)],[framepoints(2,1),framepoints(2,2)],[framepoints(3,1),framepoints(3,2)],'Color','black','LineWidth',2);
h2 = line([framepoints(1,2),framepoints(1,3)],[framepoints(2,2),framepoints(2,3)],[framepoints(3,2),framepoints(3,3)],'Color','black','LineWidth',2);
h3 = line([framepoints(1,3),framepoints(1,4)],[framepoints(2,3),framepoints(2,4)],[framepoints(3,3),framepoints(3,4)],'Color','black','LineWidth',2);
h4 = line([framepoints(1,4),framepoints(1,1)],[framepoints(2,4),framepoints(2,1)],[framepoints(3,4),framepoints(3,1)],'Color','black','LineWidth',2);
h5 = line([framepoints(1,1),framepoints(1,1)],[framepoints(2,1),framepoints(2,1)],[0,framepoints(3,1)],'Color','black','LineWidth',2);
h6 = line([framepoints(1,2),framepoints(1,2)],[framepoints(2,2),framepoints(2,2)],[0,framepoints(3,2)],'Color','black','LineWidth',2);
h7 = line([framepoints(1,3),framepoints(1,3)],[framepoints(2,3),framepoints(2,3)],[0,framepoints(3,3)],'Color','black','LineWidth',2);
h8 = line([framepoints(1,4),framepoints(1,4)],[framepoints(2,4),framepoints(2,4)],[0,framepoints(3,4)],'Color','black','LineWidth',2);

xlabel('x_{[m]}')
ylabel('y_{[m]}')
zlabel('z_{[m]}')
title(['Area: mean=' num2str(meanindices.orientA)])
colorbar
caxis([0 1.8])
grid on
axis equal
end