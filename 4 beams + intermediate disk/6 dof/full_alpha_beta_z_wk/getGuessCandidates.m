%% FLOODING ALGORITHM - Spatial CASE
% F.Zaccaria 02 February 2022
% extension to the spatial case of the algorithm provided by "Singularity
% Conditions for Continuum Parallel Robot"

function [idm,idw] = getGuessCandidates(WK,pend,current_idx,params)

idw = getNeighbors(WK,pend,params,1.43);
idw = idw((idw~=current_idx)==1);
idx_wk =(WK(idw,5)==1);
idm = idw(idx_wk==1);

end
