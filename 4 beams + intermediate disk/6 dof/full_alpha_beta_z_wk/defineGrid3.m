%% FLOODING ALGORITHM - Spatial CASE
% F.Zaccaria 02 February 2022
% extension to the spatial case of the algorithm provided by "Singularity
% Conditions for Continuum Parallel Robot"

function grid = defineGrid3(params)
boxsize = params.boxsize;
stepsizeZ = params.stepsizeZ;
stepsizeA = params.stepsizeA;
stepsizeB = params.stepsizeB;

    xU = boxsize(1);
    xB = boxsize(2);
    yU = boxsize(3);
    yB = boxsize(4);
    zU = boxsize(5);
    zB = boxsize(6);

    % x
    n_sampX = floor(abs(xU-xB)/stepsizeZ);
    x = linspace(xU,xB,n_sampX);
    if n_sampX==0
        n_sampX = 1;
        x = (xU+xB)/2;
    end

    % y
    n_sampY = floor(abs(yU-yB)/stepsizeA);
    yy = linspace(yU,yB,n_sampY);
    if n_sampY==0
        n_sampY = 1;
        yy = (yU+yB)/2;
    end

    % z
    n_sampZ = floor(abs(zU-zB)/stepsizeB);
    z = linspace(zU,zB,n_sampZ);
    if n_sampZ==0
        n_sampZ = 1;
        z = (zU+zB)/2;
    end

    [X,Y,Z] = meshgrid(x,yy,z);
    Xp = reshape(X,n_sampX*n_sampY*n_sampZ,1);
    Yp = reshape(Y,n_sampX*n_sampY*n_sampZ,1);
    Zp = reshape(Z,n_sampX*n_sampY*n_sampZ,1);
    grid = [Xp,Yp,Zp];

end