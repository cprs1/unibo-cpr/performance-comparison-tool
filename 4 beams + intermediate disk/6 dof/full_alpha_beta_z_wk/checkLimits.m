function flag = checkLimits(previousIdx,config, fcn, x,y , params)
if previousIdx==1
    sizeconf = numel(config);
    confignew = config + [x;y;zeros(sizeconf-2,1)];
    mech = fcn.mechconstrfcn;
    mechflag = mech(confignew);
 
    % check tubes strains
    tubeflag =1;
    dd = params.tubelims;
    j = 1;
    while j<=4 && tubeflag==1
        basepoint = params.basepoints(1:2,j);
        pc = confignew(1:2);
        dist = norm(basepoint-pc);
        if dist<dd(1) || dist>dd(2)
            tubeflag=0;
        end
        j = j+1;
    end

    if mechflag==1 && tubeflag==1
        flag = 1;
    else
        flag = 3;
    end
else
    flag = 3;
end
end