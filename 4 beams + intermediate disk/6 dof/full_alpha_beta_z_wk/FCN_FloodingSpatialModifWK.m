%% FLOODING ALGORITHM - Spatial CASE
% F.Zaccaria 02 February 2022
% extension to the spatial case of the algorithm provided by "Singularity
% Conditions for Continuum Parallel Robot"

function WKout = FCN_FloodingSpatialModifWK(fcn,params,instantplot,WKold,guesses)
tic;

pstart = params.pstart;

%% DEFINE GRID
table = defineGrid3(params);
np = numel(table(:,1));
idx = 1:1:np;
WKout = [idx',table,zeros(np,7)]; % da ridurre

%% RUN TUBE COMPUTATIOM
[dmin,dmax] = tubeStrainRecover(fcn,params);
params.tubelims = [dmin,dmax];
%% SOLVE FIRST ITERATION
id2 = getNeighbors(WKout,pstart,params,1.43);
pend = [WKout(id2(1),2);WKout(id2(1),3);WKout(id2(1),4)];
[iDx,iDy,iDu,A,flag]= reconstructProperties(pend,WKold,guesses,params,fcn);
% store results
WKout(id2(1),5:10) = [flag,1,iDx,iDy,iDu,A];

% initialize
if flag==1
    idn2 = getNeighbors(WKout,pend,params,1.43);
    idn2 = idn2((idn2~=id2(1))==1);
    wk_todo = idn2;
else
    wk_todo = [];
end

%% MAIN LOOP
while (numel(wk_todo)>0 )    
    %% EXTRACT POINT
    current_idx = wk_todo(1);
    WKout(current_idx,6) = 1;
    wk_todo = wk_todo(2:end);
    pend = [WKout(current_idx,2);WKout(current_idx,3);WKout(current_idx,4)];
    
    [iDx,iDy,iDu,A,flags]= reconstructProperties(pend,WKold,guesses,params,fcn);
    % store results
    WKout(current_idx,5:10) = [flags,1,iDx,iDy,iDu,A];
    % if ok
    if (flags==1)
        idw = getNeighbors(WKout,pend,params,1.43);
        idw = idw((idw~=current_idx)==1);
        idny2 = idw(WKout(idw,5)==0 & WKout(idw,6)==0);
        wk_todo = [wk_todo;idny2];
        WKout(idny2,6) = 1;
        if instantplot == 1
            plot3(pend(1),pend(2),pend(3),'b.')
            drawnow;
        end
    % if not feasible
    else
        WKout(current_idx,5:6) = [3,1];
        if instantplot == 1
            plot3(pend(1),pend(2),pend(3),'r.')
            drawnow;
        end
    end

end


end