/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * rotationParametrization.c
 *
 * Code generation for function 'rotationParametrization'
 *
 */

/* Include files */
#include "rotationParametrization.h"
#include "rt_nonfinite.h"
#include <math.h>
#include <string.h>

/* Function Definitions */
void rotationParametrization(const double ang[3], const char str_data[],
                             const int str_size[2], double Rp[9],
                             double dRpda[9], double dRpdb[9], double dRpdc[9])
{
  static const char cv[3] = {'Z', 'Y', 'Z'};
  static const char cv1[3] = {'X', 'Y', 'Z'};
  double Rp_tmp[9];
  double b_Rp_tmp[9];
  double b_dv[9];
  double b_dv1[9];
  double c_M_tmp[9];
  double c_Rp_tmp[9];
  double d_M_tmp[9];
  double d_Rp_tmp[9];
  double M_tmp;
  double M_tmp_tmp;
  double b_M_tmp;
  double b_M_tmp_tmp;
  double c_M_tmp_tmp;
  double d;
  double d1;
  double d2;
  double d_M_tmp_tmp;
  int dRpda_tmp;
  int exitg1;
  int i;
  int i1;
  int i2;
  int kstr;
  boolean_T b_bool;
  /*  ROTATION MATRIX AND ITS DERIVATIVES wrt its angles */
  b_bool = false;
  if (str_size[1] == 3) {
    kstr = 0;
    do {
      exitg1 = 0;
      if (kstr < 3) {
        if (str_data[kstr] != cv[kstr]) {
          exitg1 = 1;
        } else {
          kstr++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }
  if (b_bool) {
    kstr = 0;
  } else {
    b_bool = false;
    if (str_size[1] == 3) {
      kstr = 0;
      do {
        exitg1 = 0;
        if (kstr < 3) {
          if (str_data[kstr] != cv1[kstr]) {
            exitg1 = 1;
          } else {
            kstr++;
          }
        } else {
          b_bool = true;
          exitg1 = 1;
        }
      } while (exitg1 == 0);
    }
    if (b_bool) {
      kstr = 1;
    } else {
      b_bool = false;
      if (str_size[1] == 2) {
        kstr = 0;
        do {
          exitg1 = 0;
          if (kstr < 2) {
            if (str_data[kstr] != 'T') {
              exitg1 = 1;
            } else {
              kstr++;
            }
          } else {
            b_bool = true;
            exitg1 = 1;
          }
        } while (exitg1 == 0);
      }
      if (b_bool) {
        kstr = 2;
      } else {
        kstr = -1;
      }
    }
  }
  switch (kstr) {
  case 0:
    M_tmp = sin(ang[1]);
    M_tmp_tmp = cos(ang[1]);
    Rp_tmp[0] = M_tmp_tmp;
    Rp_tmp[3] = 0.0;
    Rp_tmp[6] = M_tmp;
    Rp_tmp[2] = -M_tmp;
    Rp_tmp[5] = 0.0;
    Rp_tmp[8] = M_tmp_tmp;
    M_tmp = sin(ang[2]);
    b_M_tmp_tmp = cos(ang[2]);
    b_Rp_tmp[0] = b_M_tmp_tmp;
    b_Rp_tmp[3] = -M_tmp;
    b_Rp_tmp[6] = 0.0;
    b_Rp_tmp[1] = M_tmp;
    b_Rp_tmp[4] = b_M_tmp_tmp;
    b_Rp_tmp[7] = 0.0;
    M_tmp = sin(ang[0]);
    c_M_tmp_tmp = cos(ang[0]);
    c_Rp_tmp[0] = c_M_tmp_tmp;
    c_Rp_tmp[3] = -M_tmp;
    c_Rp_tmp[6] = 0.0;
    c_Rp_tmp[1] = M_tmp;
    c_Rp_tmp[4] = c_M_tmp_tmp;
    c_Rp_tmp[7] = 0.0;
    Rp_tmp[1] = 0.0;
    b_Rp_tmp[2] = 0.0;
    c_Rp_tmp[2] = 0.0;
    Rp_tmp[4] = 1.0;
    b_Rp_tmp[5] = 0.0;
    c_Rp_tmp[5] = 0.0;
    Rp_tmp[7] = 0.0;
    b_Rp_tmp[8] = 1.0;
    c_Rp_tmp[8] = 1.0;
    /*  DERIVATIVE OF ROTATION MATRIX Z WRT ITS ANGLE */
    b_dv[0] = -sin(ang[0]);
    b_dv[3] = -c_M_tmp_tmp;
    b_dv[6] = 0.0;
    b_dv[1] = c_M_tmp_tmp;
    b_dv[4] = -sin(ang[0]);
    b_dv[7] = 0.0;
    for (i = 0; i < 3; i++) {
      d = c_Rp_tmp[i];
      d1 = c_Rp_tmp[i + 3];
      i1 = (int)c_Rp_tmp[i + 6];
      for (i2 = 0; i2 < 3; i2++) {
        d_Rp_tmp[i + 3 * i2] = (d * Rp_tmp[3 * i2] + d1 * Rp_tmp[3 * i2 + 1]) +
                               (double)i1 * Rp_tmp[3 * i2 + 2];
      }
      d = d_Rp_tmp[i];
      d1 = d_Rp_tmp[i + 3];
      d2 = d_Rp_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        Rp[i + 3 * i1] = (d * b_Rp_tmp[3 * i1] + d1 * b_Rp_tmp[3 * i1 + 1]) +
                         d2 * b_Rp_tmp[3 * i1 + 2];
      }
      b_dv[3 * i + 2] = 0.0;
    }
    for (i = 0; i < 3; i++) {
      d = b_dv[i];
      d1 = b_dv[i + 3];
      for (i1 = 0; i1 < 3; i1++) {
        b_dv1[i + 3 * i1] = (d * Rp_tmp[3 * i1] + d1 * Rp_tmp[3 * i1 + 1]) +
                            0.0 * Rp_tmp[3 * i1 + 2];
      }
      d = b_dv1[i];
      d1 = b_dv1[i + 3];
      d2 = b_dv1[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        dRpda[i + 3 * i1] = (d * b_Rp_tmp[3 * i1] + d1 * b_Rp_tmp[3 * i1 + 1]) +
                            d2 * b_Rp_tmp[3 * i1 + 2];
      }
    }
    /*  DERIVATIVE OF ROTATION MATRIX Y WRT ITS ANGLE */
    b_dv[0] = -sin(ang[1]);
    b_dv[3] = 0.0;
    b_dv[6] = M_tmp_tmp;
    b_dv[1] = 0.0;
    b_dv[4] = 0.0;
    b_dv[7] = 0.0;
    b_dv[2] = -M_tmp_tmp;
    b_dv[5] = 0.0;
    b_dv[8] = -sin(ang[1]);
    for (i = 0; i < 3; i++) {
      d = c_Rp_tmp[i];
      d1 = c_Rp_tmp[i + 3];
      i1 = (int)c_Rp_tmp[i + 6];
      for (i2 = 0; i2 < 3; i2++) {
        Rp_tmp[i + 3 * i2] = (d * b_dv[3 * i2] + d1 * b_dv[3 * i2 + 1]) +
                             (double)i1 * b_dv[3 * i2 + 2];
      }
      d = Rp_tmp[i];
      d1 = Rp_tmp[i + 3];
      d2 = Rp_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        dRpdb[i + 3 * i1] = (d * b_Rp_tmp[3 * i1] + d1 * b_Rp_tmp[3 * i1 + 1]) +
                            d2 * b_Rp_tmp[3 * i1 + 2];
      }
    }
    /*  DERIVATIVE OF ROTATION MATRIX Z WRT ITS ANGLE */
    b_dv[0] = -sin(ang[2]);
    b_dv[3] = -b_M_tmp_tmp;
    b_dv[6] = 0.0;
    b_dv[1] = b_M_tmp_tmp;
    b_dv[4] = -sin(ang[2]);
    b_dv[7] = 0.0;
    b_dv[2] = 0.0;
    b_dv[5] = 0.0;
    b_dv[8] = 0.0;
    for (i = 0; i < 3; i++) {
      d = d_Rp_tmp[i];
      d1 = d_Rp_tmp[i + 3];
      d2 = d_Rp_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        dRpdc[i + 3 * i1] =
            (d * b_dv[3 * i1] + d1 * b_dv[3 * i1 + 1]) + d2 * b_dv[3 * i1 + 2];
      }
    }
    break;
  case 1:
    M_tmp = sin(ang[1]);
    M_tmp_tmp = cos(ang[1]);
    Rp_tmp[0] = M_tmp_tmp;
    Rp_tmp[3] = 0.0;
    Rp_tmp[6] = M_tmp;
    Rp_tmp[2] = -M_tmp;
    Rp_tmp[5] = 0.0;
    Rp_tmp[8] = M_tmp_tmp;
    M_tmp = sin(ang[2]);
    b_M_tmp_tmp = cos(ang[2]);
    b_Rp_tmp[0] = b_M_tmp_tmp;
    b_Rp_tmp[3] = -M_tmp;
    b_Rp_tmp[6] = 0.0;
    b_Rp_tmp[1] = M_tmp;
    b_Rp_tmp[4] = b_M_tmp_tmp;
    b_Rp_tmp[7] = 0.0;
    M_tmp = sin(ang[0]);
    c_M_tmp_tmp = cos(ang[0]);
    Rp_tmp[1] = 0.0;
    b_Rp_tmp[2] = 0.0;
    c_Rp_tmp[0] = 1.0;
    Rp_tmp[4] = 1.0;
    b_Rp_tmp[5] = 0.0;
    c_Rp_tmp[3] = 0.0;
    Rp_tmp[7] = 0.0;
    b_Rp_tmp[8] = 1.0;
    c_Rp_tmp[6] = 0.0;
    c_Rp_tmp[1] = 0.0;
    c_Rp_tmp[4] = c_M_tmp_tmp;
    c_Rp_tmp[7] = -M_tmp;
    c_Rp_tmp[2] = 0.0;
    c_Rp_tmp[5] = M_tmp;
    c_Rp_tmp[8] = c_M_tmp_tmp;
    /*  DERIVATIVE OF ROTATION MATRIX X WRT ITS ANGLE */
    for (i = 0; i < 3; i++) {
      i1 = (int)c_Rp_tmp[i];
      d = c_Rp_tmp[i + 3];
      d1 = c_Rp_tmp[i + 6];
      for (i2 = 0; i2 < 3; i2++) {
        d_Rp_tmp[i + 3 * i2] =
            ((double)i1 * Rp_tmp[3 * i2] + d * Rp_tmp[3 * i2 + 1]) +
            d1 * Rp_tmp[3 * i2 + 2];
      }
      d = d_Rp_tmp[i];
      d1 = d_Rp_tmp[i + 3];
      d2 = d_Rp_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        Rp[i + 3 * i1] = (d * b_Rp_tmp[3 * i1] + d1 * b_Rp_tmp[3 * i1 + 1]) +
                         d2 * b_Rp_tmp[3 * i1 + 2];
      }
      b_dv[3 * i] = 0.0;
    }
    b_dv[1] = 0.0;
    b_dv[4] = -sin(ang[0]);
    b_dv[7] = -c_M_tmp_tmp;
    b_dv[2] = 0.0;
    b_dv[5] = c_M_tmp_tmp;
    b_dv[8] = -sin(ang[0]);
    for (i = 0; i < 3; i++) {
      d = b_dv[i + 3];
      d1 = b_dv[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        b_dv1[i + 3 * i1] = (0.0 * Rp_tmp[3 * i1] + d * Rp_tmp[3 * i1 + 1]) +
                            d1 * Rp_tmp[3 * i1 + 2];
      }
      d = b_dv1[i];
      d1 = b_dv1[i + 3];
      d2 = b_dv1[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        dRpda[i + 3 * i1] = (d * b_Rp_tmp[3 * i1] + d1 * b_Rp_tmp[3 * i1 + 1]) +
                            d2 * b_Rp_tmp[3 * i1 + 2];
      }
    }
    /*  DERIVATIVE OF ROTATION MATRIX Y WRT ITS ANGLE */
    b_dv[0] = -sin(ang[1]);
    b_dv[3] = 0.0;
    b_dv[6] = M_tmp_tmp;
    b_dv[1] = 0.0;
    b_dv[4] = 0.0;
    b_dv[7] = 0.0;
    b_dv[2] = -M_tmp_tmp;
    b_dv[5] = 0.0;
    b_dv[8] = -sin(ang[1]);
    for (i = 0; i < 3; i++) {
      i1 = (int)c_Rp_tmp[i];
      d = c_Rp_tmp[i + 3];
      d1 = c_Rp_tmp[i + 6];
      for (i2 = 0; i2 < 3; i2++) {
        Rp_tmp[i + 3 * i2] =
            ((double)i1 * b_dv[3 * i2] + d * b_dv[3 * i2 + 1]) +
            d1 * b_dv[3 * i2 + 2];
      }
      d = Rp_tmp[i];
      d1 = Rp_tmp[i + 3];
      d2 = Rp_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        dRpdb[i + 3 * i1] = (d * b_Rp_tmp[3 * i1] + d1 * b_Rp_tmp[3 * i1 + 1]) +
                            d2 * b_Rp_tmp[3 * i1 + 2];
      }
    }
    /*  DERIVATIVE OF ROTATION MATRIX Z WRT ITS ANGLE */
    b_dv[0] = -sin(ang[2]);
    b_dv[3] = -b_M_tmp_tmp;
    b_dv[6] = 0.0;
    b_dv[1] = b_M_tmp_tmp;
    b_dv[4] = -sin(ang[2]);
    b_dv[7] = 0.0;
    b_dv[2] = 0.0;
    b_dv[5] = 0.0;
    b_dv[8] = 0.0;
    for (i = 0; i < 3; i++) {
      d = d_Rp_tmp[i];
      d1 = d_Rp_tmp[i + 3];
      d2 = d_Rp_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        dRpdc[i + 3 * i1] =
            (d * b_dv[3 * i1] + d1 * b_dv[3 * i1 + 1]) + d2 * b_dv[3 * i1 + 2];
      }
    }
    break;
  case 2:
    M_tmp = sin(-ang[0]);
    M_tmp_tmp = cos(-ang[0]);
    Rp_tmp[0] = M_tmp_tmp;
    Rp_tmp[3] = -M_tmp;
    Rp_tmp[6] = 0.0;
    Rp_tmp[1] = M_tmp;
    Rp_tmp[4] = M_tmp_tmp;
    Rp_tmp[7] = 0.0;
    M_tmp = sin(ang[2]);
    b_M_tmp_tmp = cos(ang[2]);
    b_Rp_tmp[0] = b_M_tmp_tmp;
    b_Rp_tmp[3] = -M_tmp;
    b_Rp_tmp[6] = 0.0;
    b_Rp_tmp[1] = M_tmp;
    b_Rp_tmp[4] = b_M_tmp_tmp;
    b_Rp_tmp[7] = 0.0;
    c_M_tmp_tmp = sin(ang[0]);
    d_M_tmp_tmp = cos(ang[0]);
    M_tmp = sin(ang[1]);
    b_M_tmp = cos(ang[1]);
    c_M_tmp[0] = d_M_tmp_tmp;
    c_M_tmp[3] = -c_M_tmp_tmp;
    c_M_tmp[6] = 0.0;
    c_M_tmp[1] = c_M_tmp_tmp;
    c_M_tmp[4] = d_M_tmp_tmp;
    c_M_tmp[7] = 0.0;
    d_M_tmp[0] = b_M_tmp;
    d_M_tmp[3] = 0.0;
    d_M_tmp[6] = M_tmp;
    Rp_tmp[2] = 0.0;
    b_Rp_tmp[2] = 0.0;
    c_M_tmp[2] = 0.0;
    d_M_tmp[1] = 0.0;
    Rp_tmp[5] = 0.0;
    b_Rp_tmp[5] = 0.0;
    c_M_tmp[5] = 0.0;
    d_M_tmp[4] = 1.0;
    Rp_tmp[8] = 1.0;
    b_Rp_tmp[8] = 1.0;
    c_M_tmp[8] = 1.0;
    d_M_tmp[7] = 0.0;
    d_M_tmp[2] = -M_tmp;
    d_M_tmp[5] = 0.0;
    d_M_tmp[8] = b_M_tmp;
    /*  DERIVATIVE OF ROTATION MATRIX Z WRT ITS ANGLE */
    M_tmp = sin(ang[1]);
    b_M_tmp = cos(ang[1]);
    /*  DERIVATIVE OF ROTATION MATRIX Z WRT ITS ANGLE */
    b_dv[0] = -sin(ang[0]);
    b_dv[3] = -d_M_tmp_tmp;
    b_dv[6] = 0.0;
    b_dv[1] = d_M_tmp_tmp;
    b_dv[4] = -sin(ang[0]);
    b_dv[7] = 0.0;
    for (i = 0; i < 3; i++) {
      d = c_M_tmp[i];
      d1 = c_M_tmp[i + 3];
      i1 = (int)c_M_tmp[i + 6];
      for (i2 = 0; i2 < 3; i2++) {
        c_Rp_tmp[i + 3 * i2] =
            (d * d_M_tmp[3 * i2] + d1 * d_M_tmp[3 * i2 + 1]) +
            (double)i1 * d_M_tmp[3 * i2 + 2];
      }
      d = c_Rp_tmp[i];
      d1 = c_Rp_tmp[i + 3];
      d2 = c_Rp_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        d_Rp_tmp[i + 3 * i1] = (d * Rp_tmp[3 * i1] + d1 * Rp_tmp[3 * i1 + 1]) +
                               d2 * Rp_tmp[3 * i1 + 2];
      }
      d = d_Rp_tmp[i];
      d1 = d_Rp_tmp[i + 3];
      d2 = d_Rp_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        Rp[i + 3 * i1] = (d * b_Rp_tmp[3 * i1] + d1 * b_Rp_tmp[3 * i1 + 1]) +
                         d2 * b_Rp_tmp[3 * i1 + 2];
      }
      b_dv[3 * i + 2] = 0.0;
    }
    c_M_tmp[0] = b_M_tmp;
    c_M_tmp[3] = 0.0;
    c_M_tmp[6] = M_tmp;
    c_M_tmp[1] = 0.0;
    c_M_tmp[4] = 1.0;
    c_M_tmp[7] = 0.0;
    c_M_tmp[2] = -M_tmp;
    c_M_tmp[5] = 0.0;
    c_M_tmp[8] = b_M_tmp;
    for (i = 0; i < 3; i++) {
      d = b_dv[i];
      d1 = b_dv[i + 3];
      d2 = b_dv[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        b_dv1[i + 3 * i1] = (d * c_M_tmp[3 * i1] + d1 * c_M_tmp[3 * i1 + 1]) +
                            d2 * c_M_tmp[3 * i1 + 2];
      }
      d = b_dv1[i];
      d1 = b_dv1[i + 3];
      d2 = b_dv1[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        b_dv[i + 3 * i1] = (d * Rp_tmp[3 * i1] + d1 * Rp_tmp[3 * i1 + 1]) +
                           d2 * Rp_tmp[3 * i1 + 2];
      }
    }
    b_dv1[0] = -sin(-ang[0]);
    b_dv1[3] = -M_tmp_tmp;
    b_dv1[6] = 0.0;
    b_dv1[1] = M_tmp_tmp;
    b_dv1[4] = -sin(-ang[0]);
    b_dv1[7] = 0.0;
    b_dv1[2] = 0.0;
    b_dv1[5] = 0.0;
    b_dv1[8] = 0.0;
    for (i = 0; i < 3; i++) {
      d = b_dv[i];
      d1 = b_dv[i + 3];
      d2 = b_dv[i + 6];
      M_tmp = c_Rp_tmp[i];
      M_tmp_tmp = c_Rp_tmp[i + 3];
      b_M_tmp = c_Rp_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        i2 = 3 * i1 + 1;
        kstr = 3 * i1 + 2;
        dRpda_tmp = i + 3 * i1;
        dRpda[dRpda_tmp] =
            (d * b_Rp_tmp[3 * i1] + d1 * b_Rp_tmp[i2]) + d2 * b_Rp_tmp[kstr];
        d_M_tmp[dRpda_tmp] = (M_tmp * b_dv1[3 * i1] + M_tmp_tmp * b_dv1[i2]) +
                             b_M_tmp * b_dv1[kstr];
      }
      d = d_M_tmp[i];
      d1 = d_M_tmp[i + 3];
      d2 = d_M_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        c_Rp_tmp[i + 3 * i1] =
            (d * b_Rp_tmp[3 * i1] + d1 * b_Rp_tmp[3 * i1 + 1]) +
            d2 * b_Rp_tmp[3 * i1 + 2];
      }
    }
    for (i = 0; i < 9; i++) {
      dRpda[i] -= c_Rp_tmp[i];
    }
    /*  DERIVATIVE OF ROTATION MATRIX Y WRT ITS ANGLE */
    M_tmp = cos(ang[1]);
    c_M_tmp[0] = d_M_tmp_tmp;
    c_M_tmp[3] = -c_M_tmp_tmp;
    c_M_tmp[6] = 0.0;
    c_M_tmp[1] = c_M_tmp_tmp;
    c_M_tmp[4] = d_M_tmp_tmp;
    c_M_tmp[7] = 0.0;
    b_dv[0] = -sin(ang[1]);
    b_dv[3] = 0.0;
    b_dv[6] = M_tmp;
    c_M_tmp[2] = 0.0;
    b_dv[1] = 0.0;
    c_M_tmp[5] = 0.0;
    b_dv[4] = 0.0;
    c_M_tmp[8] = 1.0;
    b_dv[7] = 0.0;
    b_dv[2] = -M_tmp;
    b_dv[5] = 0.0;
    b_dv[8] = -sin(ang[1]);
    for (i = 0; i < 3; i++) {
      d = c_M_tmp[i];
      d1 = c_M_tmp[i + 3];
      d2 = c_M_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        d_M_tmp[i + 3 * i1] =
            (d * b_dv[3 * i1] + d1 * b_dv[3 * i1 + 1]) + d2 * b_dv[3 * i1 + 2];
      }
      d = d_M_tmp[i];
      d1 = d_M_tmp[i + 3];
      d2 = d_M_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        c_M_tmp[i + 3 * i1] = (d * Rp_tmp[3 * i1] + d1 * Rp_tmp[3 * i1 + 1]) +
                              d2 * Rp_tmp[3 * i1 + 2];
      }
      d = c_M_tmp[i];
      d1 = c_M_tmp[i + 3];
      d2 = c_M_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        dRpdb[i + 3 * i1] = (d * b_Rp_tmp[3 * i1] + d1 * b_Rp_tmp[3 * i1 + 1]) +
                            d2 * b_Rp_tmp[3 * i1 + 2];
      }
    }
    /*  DERIVATIVE OF ROTATION MATRIX Z WRT ITS ANGLE */
    b_dv[0] = -sin(ang[2]);
    b_dv[3] = -b_M_tmp_tmp;
    b_dv[6] = 0.0;
    b_dv[1] = b_M_tmp_tmp;
    b_dv[4] = -sin(ang[2]);
    b_dv[7] = 0.0;
    b_dv[2] = 0.0;
    b_dv[5] = 0.0;
    b_dv[8] = 0.0;
    for (i = 0; i < 3; i++) {
      d = d_Rp_tmp[i];
      d1 = d_Rp_tmp[i + 3];
      d2 = d_Rp_tmp[i + 6];
      for (i1 = 0; i1 < 3; i1++) {
        dRpdc[i + 3 * i1] =
            (d * b_dv[3 * i1] + d1 * b_dv[3 * i1 + 1]) + d2 * b_dv[3 * i1 + 2];
      }
    }
    break;
  }
}

/* End of code generation (rotationParametrization.c) */
