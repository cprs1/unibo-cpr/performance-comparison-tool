/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * derivativeColRotMatQuat.c
 *
 * Code generation for function 'derivativeColRotMatQuat'
 *
 */

/* Include files */
#include "derivativeColRotMatQuat.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */
void derivativeColRotMatQuat(const double h[4], double D1[12], double D2[12],
                             double D3[12])
{
  double D1_tmp;
  double D2_tmp;
  double b_D1_tmp;
  double c_D1_tmp;
  /*  DERIVATIVE OF ROTATION MATRIX COLUMNS WRT QUATERNION */
  /*  hL = h1 + h2 i + h3 j + h4 k (h1 = scalar component) */
  /*   D1 = derivative of R(:,1) wrt hL */
  /*   D2 = derivative of R(:,2) wrt hL */
  /*   D3 = derivative of R(:,3) wrt hL */
  D1[0] = 2.0 * h[0];
  D1[3] = 2.0 * h[1];
  D1_tmp = 2.0 * -h[2];
  D1[6] = D1_tmp;
  b_D1_tmp = 2.0 * -h[3];
  D1[9] = b_D1_tmp;
  D1[1] = 2.0 * h[3];
  D1[4] = 2.0 * h[2];
  D1[7] = 2.0 * h[1];
  D1[10] = 2.0 * h[0];
  D1[2] = D1_tmp;
  D1[5] = 2.0 * h[3];
  c_D1_tmp = 2.0 * -h[0];
  D1[8] = c_D1_tmp;
  D1[11] = 2.0 * h[1];
  D2[0] = b_D1_tmp;
  D2[3] = 2.0 * h[2];
  D2[6] = 2.0 * h[1];
  D2[9] = c_D1_tmp;
  D2[1] = 2.0 * h[0];
  D2_tmp = 2.0 * -h[1];
  D2[4] = D2_tmp;
  D2[7] = 2.0 * h[2];
  D2[10] = b_D1_tmp;
  D2[2] = 2.0 * h[1];
  D2[5] = 2.0 * h[0];
  D2[8] = 2.0 * h[3];
  D2[11] = 2.0 * h[2];
  D3[0] = 2.0 * h[2];
  D3[3] = 2.0 * h[3];
  D3[6] = 2.0 * h[0];
  D3[9] = 2.0 * h[1];
  D3[1] = D2_tmp;
  D3[4] = c_D1_tmp;
  D3[7] = 2.0 * h[3];
  D3[10] = 2.0 * h[2];
  D3[2] = 2.0 * h[0];
  D3[5] = D2_tmp;
  D3[8] = D1_tmp;
  D3[11] = 2.0 * h[3];
}

/* End of code generation (derivativeColRotMatQuat.c) */
