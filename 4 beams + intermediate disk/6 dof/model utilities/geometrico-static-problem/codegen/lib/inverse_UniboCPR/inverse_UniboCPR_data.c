/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * inverse_UniboCPR_data.c
 *
 * Code generation for function 'inverse_UniboCPR_data'
 *
 */

/* Include files */
#include "inverse_UniboCPR_data.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Variable Definitions */
const double dv[21] = {0.2,
                       0.075,
                       0.225,
                       0.97777777777777775,
                       -3.7333333333333334,
                       3.5555555555555554,
                       2.9525986892242035,
                       -11.595793324188385,
                       9.8228928516994358,
                       -0.29080932784636487,
                       2.8462752525252526,
                       -10.757575757575758,
                       8.9064227177434727,
                       0.27840909090909088,
                       -0.2735313036020583,
                       0.091145833333333329,
                       0.0,
                       0.44923629829290207,
                       0.65104166666666663,
                       -0.322376179245283,
                       0.13095238095238096};

const double dv1[6] = {0.2, 0.3, 0.8, 0.88888888888888884, 1.0, 1.0};

const double dv2[7] = {0.0012326388888888888,
                       0.0,
                       -0.0042527702905061394,
                       0.036979166666666667,
                       -0.05086379716981132,
                       0.0419047619047619,
                       -0.025};

boolean_T isInitialized_inverse_UniboCPR = false;

/* End of code generation (inverse_UniboCPR_data.c) */
