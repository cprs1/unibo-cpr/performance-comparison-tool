/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * rotationParametrization.h
 *
 * Code generation for function 'rotationParametrization'
 *
 */

#ifndef ROTATIONPARAMETRIZATION_H
#define ROTATIONPARAMETRIZATION_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void rotationParametrization(const double ang[3], const char str_data[],
                             const int str_size[2], double Rp[9],
                             double dRpda[9], double dRpdb[9], double dRpdc[9]);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (rotationParametrization.h) */
