/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * inverse_UniboCPR_initialize.c
 *
 * Code generation for function 'inverse_UniboCPR_initialize'
 *
 */

/* Include files */
#include "inverse_UniboCPR_initialize.h"
#include "inverse_UniboCPR_data.h"
#include "rt_nonfinite.h"
#include <string.h>

/* Function Definitions */
void inverse_UniboCPR_initialize(void)
{
  rt_InitInfAndNaN();
  isInitialized_inverse_UniboCPR = true;
}

/* End of code generation (inverse_UniboCPR_initialize.c) */
