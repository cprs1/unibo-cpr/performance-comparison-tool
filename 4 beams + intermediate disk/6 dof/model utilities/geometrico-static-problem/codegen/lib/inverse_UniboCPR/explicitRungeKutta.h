/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * explicitRungeKutta.h
 *
 * Code generation for function 'explicitRungeKutta'
 *
 */

#ifndef EXPLICITRUNGEKUTTA_H
#define EXPLICITRUNGEKUTTA_H

/* Include files */
#include "inverse_UniboCPR_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
double maxScaledError(const emxArray_real_T *fE, const emxArray_real_T *y,
                      const emxArray_real_T *ynew);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (explicitRungeKutta.h) */
