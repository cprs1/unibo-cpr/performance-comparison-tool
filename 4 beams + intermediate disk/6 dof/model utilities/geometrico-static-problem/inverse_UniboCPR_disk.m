function [eq,gradeq] = inverse_UniboCPR_disk(guess,geometry,params,Kad,qpv)

% extract parameters
Nf = params.Nf;
Nftot = 3*Nf;
rotparams = params.rotparams;
Ci = geometry.jointmatr;
Cd = geometry.diskmatr;
Cs = geometry.springmatr;

[~,nj] = size(Ci);
hr = geometry.hr;
platangles = geometry.platangles;
platpoints = geometry.platpoints;
endpoints = geometry.endpoints;
endangles = geometry.endangles;
diskpoints = geometry.diskpoints;
diskangles = geometry.diskangles;

g = params.g;
wd = [zeros(3,1);g*params.fg2];

% extract variables
[qa,qe1v,qe2v,qm1,Lf,qd,qp,lambdad,lambdat,lambdas]= unstacks(guess,Nftot,nj); %OK

pbase = [qa(1);qa(2);hr];
Lv = qa(3:6,1);
pplat = qp(1:3,1);
pdisk = qd(1:3,1);

[Rp,dRpda,dRpdb,dRpdc] = rotationParametrization(qp(4:6,1),rotparams);
[Rd,dRdda,dRddb,dRddc] = rotationParametrization(qd(4:6,1),rotparams);


% initialize
beameq1 = zeros(4*Nftot,1);
dbeameq1dqa = zeros(4*Nftot,6);
dbeameq1dq1 = zeros(4*Nftot,4*Nftot);
dbeameq1dq2 = zeros(4*Nftot,4*Nftot);
dbeameq1dqm = zeros(4*Nftot,Nftot);
dbeameq1dLf = zeros(4*Nftot,4);
dbeameq1dlamp = zeros(4*Nftot,4*nj);
dbeameq1dlamd = zeros(4*Nftot,4*4);
dbeameq1dlams = zeros(4*Nftot,6);
dbeameq1dqp = zeros(4*Nftot,6);
dbeameq1dqd = zeros(4*Nftot,6);

beameq2 = zeros(4*Nftot,1); 
dbeameq2dqa = zeros(4*Nftot,6);
dbeameq2dq1 = zeros(4*Nftot,4*Nftot);
dbeameq2dq2 = zeros(4*Nftot,4*Nftot);
dbeameq2dqm = zeros(4*Nftot,Nftot);
dbeameq2dLf = zeros(4*Nftot,4);
dbeameq2dlamp = zeros(4*Nftot,4*nj);
dbeameq2dlamd = zeros(4*Nftot,4*4);
dbeameq2dlams = zeros(4*Nftot,6);
dbeameq2dqp = zeros(4*Nftot,6);
dbeameq2dqd = zeros(4*Nftot,6);

platconstr = zeros(4*nj,1);
dplatconstrdqa = zeros(4*nj,6);
dplatconstrdq2 = zeros(4*nj,4*Nftot);
dplatconstrdq1 = zeros(4*nj,4*Nftot);
dplatconstrdqm = zeros(4*nj,Nftot);
dplatconstrdLf = zeros(4*nj,4);
dplatconstrdqp = zeros(4*nj,6);
dplatconstrdqd = zeros(4*nj,6);
dplatconstrdlamp = zeros(4*nj,4*nj);
dplatconstrdlamd = zeros(4*nj,4*4);
dplatconstrdlams = zeros(4*nj,6);

diskconstr = zeros(4*4,1);
dconstrdiskdqa = zeros(4*4,6);
dconstrdiskdq2 = zeros(4*4,4*Nftot);
dconstrdiskdq1 = zeros(4*4,4*Nftot);
dconstrdiskdqm = zeros(4*4,Nftot);
dconstrdiskdLf = zeros(4*4,4);
dconstrdiskdqp = zeros(4*4,6);
dconstrdiskdqd = zeros(4*4,6);
dconstrdiskdlamp = zeros(4*4,4*nj);
dconstrdiskdlamd = zeros(4*4,4*4);
dconstrdiskdlams = zeros(4*4,6);

elconstr = zeros(4,1);
elconstrdqa = zeros(4,6);
elconstrdq1 = zeros(4,4*Nftot);
elconstrdq2 = zeros(4,4*Nftot);
elconstrdqm = zeros(4,Nftot);
elconstrdLf = zeros(4,4);
elconstrdqd = zeros(4,6);
elconstrdqp = zeros(4,6);
elconstrdlamp = zeros(4,4*nj);
elconstrdlamd = zeros(4,4*4);
elconstrdlams = zeros(4,6);

wrencheq = params.tipwrench;
dwrenchdqa = zeros(6,6);
dwrenchdqp = zeros(6,6);
dwrenchdLf = zeros(6,4);
dwrenchdq1 = zeros(6,4*Nftot);
dwrenchdq2 = zeros(6,4*Nftot);
dwrenchdqm = zeros(6,Nftot);
dwrenchdqd = zeros(6,6);
dwrenchdlamp = zeros(6,4*nj);
dwrenchdlamd = zeros(6,4*4);
dwrenchdlams = zeros(6,6);



diskwrencheq = zeros(6,1);
ddiskwrenchdqa = zeros(6,6);
ddiskwrenchdLf = zeros(6,4);
ddiskwrenchdq1 = zeros(6,4*Nftot);
ddiskwrenchdq2 = zeros(6,4*Nftot);
ddiskwrenchdqp = zeros(6,6);
ddiskwrenchdlamd = zeros(6,4*4);
ddiskwrenchdlamp = zeros(6,4*nj);
% ddiskwrenchdlams = zeros(6,6);

%% PASSIVE BEAM (or spring)
% FORWARDs
    % extract variables of the th-i beam
    
    Ls = geometry.Ls;
    p0 = pbase;
    h0 = eul2quat(platangles(:,1)','XYZ')';
    
    % forward recursion 1
    [pL1,hL1,~,~,dpL1dqm,dhL1dqm]  = forwardRecursion(p0,h0,qm1,Ls,Nf);
    Rtip1 = quat2rotmatrix(hL1);
    dhL1dqm = reshape(dhL1dqm,4,Nftot);

    % BACKWARD
    twrench = Cs*lambdas;

    % backward recursion
    [~,Qc2,~,~,~,dQcdqm,~,dQcdw0i] = backwardRecursion(twrench,qm1,Nf,Ls,wd);
    dQc2dqm = reshape(dQcdqm,Nftot,Nftot);
    dQc2dw02 = reshape(dQcdw0i,Nftot,6);
    dQc2dlams = dQc2dw02*Cs;

 % BEAM EQUILIBRIUMS
    beameqm = Ls*Kad*qm1 + Qc2; %OK
    dbeameqmdqa = zeros(Nftot,6);
    dbeameqmdq1 = zeros(Nftot,4*Nftot);
    dbeameqmdq2 = zeros(Nftot,4*Nftot);
    dbeameqmdqm = Ls*Kad + dQc2dqm;
    dbeameqmdLf = zeros(Nftot,4);
    dbeameqmdlamp = zeros(Nftot,4*nj);
    dbeameqmdlamd = zeros(Nftot,4*4);
    dbeameqmdlams = dQc2dlams;
    dbeameqmdqp = zeros(Nftot,6);
    dbeameqmdqd = zeros(Nftot,6);


    % platform
    passconstr = pL1-pdisk; %OK
    dpassconstrdpbase = [eye(2);zeros(1,2)];
    dpassposdqm = reshape(dpL1dqm,3,Nftot);
    dpassposdqd = -[eye(3),zeros(3)];
    hJi = eul2quat([-pi,0,0],geometry.endanglesconvention)';
    RJi = Rd*quat2rotmatrix(hJi);
    dRJida = dRdda*quat2rotmatrix(hJi);
    dRJidb = dRddb*quat2rotmatrix(hJi);
    dRJidc = dRddc*quat2rotmatrix(hJi);

    [passangerr,~,dpassangerrdqm,m2] = roterr(RJi,Rtip1,hL1,zeros(4,1),dhL1dqm,dRJida,dRJidb,dRJidc);

    dpassangerrdqd =  [zeros(3,3),m2];

    passconstr = Cs'*[passangerr;passconstr;]; %OK
    dpassconstrdqa = zeros(6,6);
    dpassconstrdqa(:,1:2) = Cs'*[zeros(3,2);dpassconstrdpbase];
    dpassconstrdqa(:,3:6) = zeros(6,4);
    dpassconstrdqm =  Cs'*[dpassangerrdqm;dpassposdqm;];
    dpassconstrdq1 =  zeros(6,4*Nftot);
    dpassconstrdq2 =  zeros(6,4*Nftot);
    dpassconstrdLf=  zeros(6,4);
    dpassconstrdqd = Cs'*[dpassangerrdqd;dpassposdqd;];
    dpassconstrdqp = zeros(6,6);
    dpassconstrdlamp = zeros(6,4*nj);
    dpassconstrdlamd = zeros(6,4*4);
    dpassconstrdlams = zeros(6,6);

    %% DISK EQUILIBRIUM
    Adg = [Rtip1,zeros(3); zeros(3), Rtip1]; 
    gwrench = -Adg*(twrench); 
    
    [D1,D2,D3] = derivativeColRotMatQuat(hL1);
    dgwrenchdqm = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dhL1dqm;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dhL1dqm];

    diskwrencheq = diskwrencheq + gwrench;  %OK
    ddiskwrenchdqm = dgwrenchdqm;
    ddiskwrenchdqd = zeros(6,6) ;
    ddiskwrenchdlams = -Adg*Cs;

    
%% ACTIVE BEAMS
for i = 1:4

    %% FORWARDs
    % extract variables of the th-i beam
    q1 = qe1v(1+Nftot*(i-1):Nftot*i,1);
    L1 = Lf(i);
    p0 = pbase + platpoints(:,i);
    h0 = eul2quat(platangles(:,i)','XYZ')';
    
    % forward recursion 1
    [pL1,hL1,dpL1dLf,dhL1dLf,dpL1dq1,dhL1dq1]  = forwardRecursion(p0,h0,q1,L1,Nf);
    Rtip1 = quat2rotmatrix(hL1);
    dpL1dqa = zeros(3,1);
    dhL1dqa = zeros(4,1);
    dpL1dq1 = reshape(dpL1dq1,3,Nftot);
    dhL1dq1 = reshape(dhL1dq1,4,Nftot);
    dpL1dq2 = zeros(3,Nftot);
    dhL1dq2 = zeros(4,Nftot);


    % forward recursion 2
    q2 = qe2v(1+Nftot*(i-1):Nftot*i,1);
    L2 = Lv(i)-Lf(i);
    [pL2,hL2,dpL2dLvi,dhL2dLvi,dpL2dq2,dhL2dq2]  = forwardRecursion(pL1,hL1,q2,L2,Nf);
    dhL2dq2 = reshape(dhL2dq2,4,Nftot);
    dpL2dq2 = reshape(dpL2dq2,3,Nftot);
    Rtip2 = quat2rotmatrix(hL2);

    % sensitivity propagation
    y0s = [pL1;hL1;reshape(eye(3),3*3,1);zeros(4*3*2,1);reshape(eye(4),4*4,1)]; 
    funsens = @(s,y) initvalueSensitivity(s,y,L2,q2,Nf);
    [~,ys] = ode45(funsens,[0,1],y0s);
    yss = ys(end,:)';
    dpL2dpL1 = reshape( yss(1+3+4             :3+4+3*3,1),3,3);
    dhL2dpL1 = reshape( yss(1+3+4+3*3         :3+4+3*3+4*3,1),4,3);
    dpL2dhL1 = reshape( yss(1+3+4+3*3+4*3     :3+4+3*3+4*3+3*4,1),3,4);
    dhL2dhL1 = reshape( yss(1+3+4+3*3+4*3+3*4 :3+4+3*3+4*3+3*4+4*4,1),4,4);

    dpL2dqa = dpL2dLvi;
    dhL2dqa = dhL2dLvi;
    dpL2dq1 = dpL2dpL1*dpL1dq1 + dpL2dhL1*dhL1dq1;
    dhL2dq1 = dhL2dhL1*dhL1dq1 + dhL2dpL1*dpL1dq1;
    dpL2dLf = - dpL2dLvi + dpL2dpL1*dpL1dLf + dpL2dhL1*dhL1dLf; 
    dhL2dLf = - dhL2dLvi + dhL2dpL1*dpL1dLf + dhL2dhL1*dhL1dLf;

    %% BACKWARDs
    lambda2 = lambdat(1+nj*(i-1):nj*i,1);
    twrench = Ci*lambda2;

    % backward recursion
    [Fc2,Qc2,dFc2dqa,dQc2dqa,dFc2dq2,dQcdq2,dFc2dw0i,dQcdw0i] = backwardRecursion(twrench,q2,Nf,L2,wd);
    dFc2dLf = - dFc2dqa;
    dFc2dq2 = reshape(dFc2dq2,6,Nftot);
    dQc2dq2 = reshape(dQcdq2,Nftot,Nftot);
    dQc2dq1 = zeros(Nftot,Nftot);
    dQc2dw02 = reshape(dQcdw0i,Nftot,6);
    dFc2dw02 = reshape(dFc2dw0i,6,6);
    dQc2dlamp = dQc2dw02*Ci;
    dQc2dLf = -dQc2dqa;


    % integration 1
    lambda1 = lambdad(1+4*(i-1):4*i,1); % mx my fx fy in local tip frame

    wrench1 = Cd*lambda1 + Fc2;

    [~,Qc1,~,dQc1dLf,~,dQc1dq1,~,dQc1dw0i] = backwardRecursion(wrench1,q1,Nf,L1,wd);
    dQc1dq1 = reshape(dQc1dq1,Nftot,Nftot);
    
    dw01dFc2 = +eye(6);
    dQc1dw01 = reshape(dQc1dw0i,Nftot,6);
    dQc1dqa = dQc1dw01*dw01dFc2*dFc2dqa;
    dQc1dq2 = dQc1dw01*dw01dFc2*dFc2dq2;
    dQc1dLf = dQc1dLf + dQc1dw01*dw01dFc2*dFc2dLf;
    dQc1dlamp = dQc1dw01*dw01dFc2*dFc2dw02*Ci;
    dQc1dlamd = dQc1dw01*Cd;
    
    %% BEAM EQUILIBRIUMS

    beameq1(1+Nftot*(i-1):Nftot*i,1) = L1*Kad*q1 + Qc1; %ok
    dbeameq1dqa(1+Nftot*(i-1):Nftot*i,1:2) = zeros(Nftot,2);
    dbeameq1dqa(1+Nftot*(i-1):Nftot*i,2+i) = dQc1dqa;
    dbeameq1dq1(1+Nftot*(i-1):Nftot*i,1+Nftot*(i-1):Nftot*i) = L1*Kad + dQc1dq1;
    dbeameq1dq2(1+Nftot*(i-1):Nftot*i,1+Nftot*(i-1):Nftot*i) = dQc1dq2;
    dbeameq1dqm(1+Nftot*(i-1):Nftot*i,:) = zeros(Nftot,Nftot);
    dbeameq1dLf(1+Nftot*(i-1):Nftot*i,i) = +Kad*q1 + dQc1dLf;
    dbeameq1dlamp(1+Nftot*(i-1):Nftot*i,1+nj*(i-1):nj*i) = +dQc1dlamp;
    dbeameq1dlamd(1+Nftot*(i-1):Nftot*i,1+4*(i-1):4*i) = +dQc1dlamd;
    dbeameq1dlams(1+Nftot*(i-1):Nftot*i,:) = +zeros(Nftot,6);
    dbeameq1dqp(1+Nftot*(i-1):Nftot*i,:) = zeros(Nftot,6);
    dbeameq1dqd(1+Nftot*(i-1):Nftot*i,:) = zeros(Nftot,6);

    beameq2(1+Nftot*(i-1):Nftot*i,1) = L2*Kad*q2 + Qc2; %ok
    dbeameq2dqa(1+Nftot*(i-1):Nftot*i,1:2) = zeros(Nftot,2);
    dbeameq2dqa(1+Nftot*(i-1):Nftot*i,2+i) = Kad*q2 + dQc2dqa;
    dbeameq2dq1(1+Nftot*(i-1):Nftot*i,1+Nftot*(i-1):Nftot*i) = dQc2dq1;
    dbeameq2dq2(1+Nftot*(i-1):Nftot*i,1+Nftot*(i-1):Nftot*i) = L2*Kad + dQc2dq2;
    dbeameq2dqm(1+Nftot*(i-1):Nftot*i,:) = zeros(Nftot,Nftot);
    dbeameq2dLf(1+Nftot*(i-1):Nftot*i,i) = -Kad*q2 + dQc2dLf;    
    dbeameq2dlamp(1+Nftot*(i-1):Nftot*i,1+nj*(i-1):nj*i) = +dQc2dlamp;
    dbeameq2dlamd(1+Nftot*(i-1):Nftot*i,1+4*(i-1):4*i) = zeros(Nftot,4);
    dbeameq2dlams(1+Nftot*(i-1):Nftot*i,:) = zeros(Nftot,6);
    dbeameq2dqp(1+Nftot*(i-1):Nftot*i,:) = zeros(Nftot,6);
    dbeameq2dqd(1+Nftot*(i-1):Nftot*i,:) = zeros(Nftot,6);

    %% CONSTRAINTS
    % platform
    pp = Rp*endpoints(:,i);
    dppdrot = [dRpda*endpoints(:,i),dRpdb*endpoints(:,i),dRpdc*endpoints(:,i)];
    constrplat = pL2-(pplat +pp); %
    dconstrplatdqa = dpL2dqa;
    dplatconstrdpbase = [eye(2);zeros(1,2)];
    dconstrplatdq2 = dpL2dq2;
    dconstrplatdq1 = dpL2dq1;
    dconstrplatdLf = dpL2dLf;
    dconstrplatdqp = -[eye(3),dppdrot];
    hJi = eul2quat(endangles(:,i)',geometry.endanglesconvention)';
    RJi = Rp*quat2rotmatrix(hJi);
    dRJida = dRpda*quat2rotmatrix(hJi);
    dRJidb = dRpdb*quat2rotmatrix(hJi);
    dRJidc = dRpdc*quat2rotmatrix(hJi);

    [platangerr,dplatangerrdqa,dplatangerrdq2,m2] = roterr(RJi,Rtip2,hL2,dhL2dqa,dhL2dq2,dRJida,dRJidb,dRJidc);
    [~,dplatangerrdLf,dplatangerrdq1,~] = roterr(RJi,Rtip2,hL2,dhL2dLf,dhL2dq1,dRJida,dRJidb,dRJidc);

    dplatangerrdqp =  [zeros(3,3),m2];

    platconstr(1+nj*(i-1):nj*i,1) = Ci'*[platangerr;constrplat;]; %
    dplatconstrdqa(1+nj*(i-1):nj*i,1:2) = Ci'*[zeros(3,2);dplatconstrdpbase];
    dplatconstrdqa(1+nj*(i-1):nj*i,2+i) = Ci'*[dplatangerrdqa;dconstrplatdqa;];
    dplatconstrdq2(1+nj*(i-1):nj*i,1+Nftot*(i-1):Nftot*i) =  Ci'*[dplatangerrdq2;dconstrplatdq2;];
    dplatconstrdq1(1+nj*(i-1):nj*i,1+Nftot*(i-1):Nftot*i) =  Ci'*[dplatangerrdq1;dconstrplatdq1;];
    dplatconstrdqm(1+nj*(i-1):nj*i,:) = zeros(nj,Nftot);
    dplatconstrdLf(1+nj*(i-1):nj*i,i) =  Ci'*[dplatangerrdLf;dconstrplatdLf;];
    dplatconstrdqp(1+nj*(i-1):nj*i,:) = Ci'*[dplatangerrdqp;dconstrplatdqp;];
    dplatconstrdqd(1+nj*(i-1):nj*i,:) = zeros(nj,6);
    dplatconstrdlamp(1+nj*(i-1):nj*i,1+nj*(i-1):nj*i) = zeros(nj,nj);
    dplatconstrdlamd(1+nj*(i-1):nj*i,1+4*(i-1):4*i) = zeros(nj,4);
    dplatconstrdlams(1+nj*(i-1):nj*i,:) = zeros(nj,6);

     %% CONSTRAINTS
    % pdisk
    pd = Rd*diskpoints(:,i);
    dpddrot = [dRdda*diskpoints(:,i),dRddb*diskpoints(:,i),dRddc*diskpoints(:,i)];

    dcdrot = [dRdda'*(pL1-pdisk),dRddb'*(pL1-pdisk),dRddc'*(pL1-pdisk)];
    constrdisk = Rd'*(pL1-pdisk) -diskpoints(:,i); %
    ddiskconstrdqa = Rd'*dpL1dqa;
    ddiskconstrdpbase = Rd'*[eye(2);zeros(1,2)];
    ddiskconstrdq2 = Rd'*dpL1dq2;
    ddiskconstrdq1 = Rd'*dpL1dq1;
    ddiskconstrdLf = Rd'*dpL1dLf;
    ddiskconstrdqd = [-Rd',dcdrot];
    hJi = eul2quat(diskangles(:,i)',geometry.diskanglesconvention)';
    RJi = Rd*quat2rotmatrix(hJi);
    dRJida = dRdda*quat2rotmatrix(hJi);
    dRJidb = dRddb*quat2rotmatrix(hJi);
    dRJidc = dRddc*quat2rotmatrix(hJi);

    [diskangerr,ddiskangerrdLf,ddiskangerrdq1,m2] = roterr(RJi,Rtip1,hL1,dhL1dLf,dhL1dq1,dRJida,dRJidb,dRJidc);
    [~,ddiskangerrdqa,ddiskangerrdq2,~] = roterr(RJi,Rtip1,hL1,dhL1dqa,dhL1dq2,dRJida,dRJidb,dRJidc);

    ddiskangerrdqp =  [zeros(3,3),m2];

    diskconstr(1+4*(i-1):4*i,1) = Cd'*[diskangerr;constrdisk;];
    dconstrdiskdqa(1+4*(i-1):4*i,1:2) = Cd'*[zeros(3,2);ddiskconstrdpbase];
    dconstrdiskdqa(1+4*(i-1):4*i,2+i) = Cd'*[ddiskangerrdqa;ddiskconstrdqa;];
    dconstrdiskdq2(1+4*(i-1):4*i,1+Nftot*(i-1):Nftot*i) =  Cd'*[ddiskangerrdq2;ddiskconstrdq2;];
    dconstrdiskdq1(1+4*(i-1):4*i,1+Nftot*(i-1):Nftot*i) =  Cd'*[ddiskangerrdq1;ddiskconstrdq1;];
    dconstrdiskdqm(1+4*(i-1):4*i,:) =  zeros(4,Nftot);
    dconstrdiskdLf(1+4*(i-1):4*i,i) =  Cd'*[ddiskangerrdLf;ddiskconstrdLf;];
    dconstrdiskdqp(1+4*(i-1):4*i,:) = zeros(4,6);
    dconstrdiskdqd(1+4*(i-1):4*i,:) = Cd'*[ddiskangerrdqp;ddiskconstrdqd;];
    dconstrdiskdlamp(1+4*(i-1):4*i,1+nj*(i-1):nj*i) = zeros(4,nj);
    dconstrdiskdlamd(1+4*(i-1):4*i,1+4*(i-1):4*i) = zeros(4,4);
    dconstrdiskdlams(1+4*(i-1):4*i,:) = zeros(4,6);

    elconstr(i,1) = constrdisk(3);
    elconstrdqa(i,1:2) = ddiskconstrdpbase(3,:);
    elconstrdqa(i,2+i) = ddiskconstrdqa(3);
    elconstrdq1(i, 1+Nftot*(i-1):Nftot*i) = ddiskconstrdq1(3,:);
    elconstrdq2(i, 1+Nftot*(i-1):Nftot*i) = ddiskconstrdq2(3,:);
    elconstrdqm(i, :) = zeros(1,Nftot);
    elconstrdLf(i,i) = ddiskconstrdLf(3);
    elconstrdqd(i,:) = ddiskconstrdqd(3,:);
    elconstrdqp(i,:) = zeros(1,6);
    elconstrdlamp(i,1+nj*(i-1):nj*i) = zeros(1,nj);
    elconstrdlamd(i,1+4*(i-1):4*i) = zeros(1,4);
    elconstrdlams(i,:) = zeros(1,6);

    %% PLATFORM EQUILIBRIUM
     
    Adg = [Rtip2,zeros(3); zeros(3), Rtip2]; 
    gwrench = -Adg*(twrench); 
    
    [D1,D2,D3] = derivativeColRotMatQuat(hL2);
    dgwrenchdqa = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dhL2dqa;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dhL2dqa];
    dgwrenchdLf = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dhL2dLf;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dhL2dLf];
    dgwrenchdq1 = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dhL2dq1;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dhL2dq1];
    dgwrenchdq2 = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dhL2dq2;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dhL2dq2];
    Adg2 = [eye(3),skew(pp); zeros(3),eye(3)]; 
    m2 = [zeros(3,3),skew(dppdrot(:,1))*gwrench(4:6),skew(dppdrot(:,2))*gwrench(4:6),skew(dppdrot(:,3))*gwrench(4:6)];

    wrencheq = wrencheq + Adg2*gwrench; %
    dwrenchdqa(:,2+i) = Adg2*dgwrenchdqa;
    dwrenchdLf(:,i) = Adg2*dgwrenchdLf;
    dwrenchdq1(:,1+Nftot*(i-1):Nftot*i) = Adg2*dgwrenchdq1;
    dwrenchdq2(:,1+Nftot*(i-1):Nftot*i) = Adg2*dgwrenchdq2;
    dwrenchdqm = zeros(6,Nftot);
    dwrenchdqp = dwrenchdqp + [m2;zeros(3,6)] ;
    dwrenchdqd = zeros(6,6);
    dwrenchdlamp(:,1+nj*(i-1):nj*i) = -Adg2*Adg*Ci;
    dwrenchdlamd(:,1+4*(i-1):4*i) = zeros(6,4);
    dwrenchdlams = zeros(6,6);

    %% DISK EQUILIBRIUM
    Adg = [Rtip1,zeros(3); zeros(3), Rtip1]; 
    twrench = Cd*lambda1;
    gwrench = -Adg*(twrench); 
    
    [D1,D2,D3] = derivativeColRotMatQuat(hL1);
    dgwrenchdqa = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dhL1dqa;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dhL1dqa];
    dgwrenchdLf = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dhL1dLf;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dhL1dLf];
    dgwrenchdq1 = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dhL1dq1;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dhL1dq1];
    dgwrenchdq2 = -[(D1*twrench(1)+D2*twrench(2)+D3*twrench(3))*dhL1dq2;(D1*twrench(4)+D2*twrench(5)+D3*twrench(6))*dhL1dq2];
    Adg2 = [eye(3),skew(pd); zeros(3),eye(3)]; 
    m2 = [zeros(3,3),skew(dpddrot(:,1))*gwrench(4:6),skew(dpddrot(:,2))*gwrench(4:6),skew(dpddrot(:,3))*gwrench(4:6)];

    diskwrencheq = diskwrencheq + Adg2*gwrench; %
    ddiskwrenchdqa(:,1:2) = zeros(6,2);
    ddiskwrenchdqa(:,2+i) = Adg2*dgwrenchdqa;
    ddiskwrenchdLf(:,i) = Adg2*dgwrenchdLf;
    ddiskwrenchdq1(:,1+Nftot*(i-1):Nftot*i) = Adg2*dgwrenchdq1;
    ddiskwrenchdq2(:,1+Nftot*(i-1):Nftot*i) = Adg2*dgwrenchdq2;
    ddiskwrenchdqd = ddiskwrenchdqd + [m2;zeros(3,6)] ;
    ddiskwrenchdqp = zeros(6,6);
    ddiskwrenchdlamd(:,1+4*(i-1):4*i) = -Adg2*Adg*Cd;
    ddiskwrenchdlamp(:,1+nj*(i-1):nj*i) = zeros(6,nj);

end

%% Forward problem
inveq = qp-qpv;

%% COLLECT
diskwrencheq = [diskwrencheq(4:6);diskwrencheq(1:3)];
wrencheq = [wrencheq(4:6);wrencheq(1:3)];

eq = [beameq1;beameq2;beameqm;elconstr;diskwrencheq;wrencheq;diskconstr;platconstr;passconstr;inveq];


gbeameq1 = [dbeameq1dqa,dbeameq1dq1,dbeameq1dq2,dbeameq1dqm,dbeameq1dLf,dbeameq1dqd,dbeameq1dqp,dbeameq1dlamd,dbeameq1dlamp,dbeameq1dlams];
gbeameq2 = [dbeameq2dqa,dbeameq2dq1,dbeameq2dq2,dbeameq2dqm,dbeameq2dLf,dbeameq2dqd,dbeameq2dqp,dbeameq2dlamd,dbeameq2dlamp,dbeameq2dlams];
gbeameqm = [dbeameqmdqa,dbeameqmdq1,dbeameqmdq2,dbeameqmdqm,dbeameqmdLf,dbeameqmdqd,dbeameqmdqp,dbeameqmdlamd,dbeameqmdlamp,dbeameqmdlams];
gelconstr = [elconstrdqa,elconstrdq1,elconstrdq2,elconstrdqm,elconstrdLf,elconstrdqd,elconstrdqp,elconstrdlamd,elconstrdlamp,elconstrdlams];
gdiskwrencheq = [ddiskwrenchdqa,ddiskwrenchdq1,ddiskwrenchdq2,ddiskwrenchdqm,ddiskwrenchdLf,ddiskwrenchdqd,ddiskwrenchdqp,ddiskwrenchdlamd,ddiskwrenchdlamp,ddiskwrenchdlams];
gwrencheq = [dwrenchdqa,dwrenchdq1,dwrenchdq2,dwrenchdqm,dwrenchdLf,dwrenchdqd,dwrenchdqp,dwrenchdlamd,dwrenchdlamp,dwrenchdlams];
gdiskconstr = [dconstrdiskdqa,dconstrdiskdq1,dconstrdiskdq2,dconstrdiskdqm,dconstrdiskdLf,dconstrdiskdqd,dconstrdiskdqp,dconstrdiskdlamd,dconstrdiskdlamp,dconstrdiskdlams];
gplatconstr = [dplatconstrdqa,dplatconstrdq1,dplatconstrdq2,dplatconstrdqm,dplatconstrdLf,dplatconstrdqd,dplatconstrdqp,dplatconstrdlamd,dplatconstrdlamp,dplatconstrdlams];
gpassconstr = [dpassconstrdqa,dpassconstrdq1,dpassconstrdq2,dpassconstrdqm,dpassconstrdLf,dpassconstrdqd,dpassconstrdqp,dpassconstrdlamd,dpassconstrdlamp,dpassconstrdlams];
ginveq = [zeros(6,6),zeros(6,4*Nftot),zeros(6,4*Nftot),zeros(6,Nftot),zeros(6,4),zeros(6,6),eye(6),zeros(6,4*4),zeros(6,4*nj),zeros(6,6)];


gdiskwrencheq = [gdiskwrencheq(4:6,:);gdiskwrencheq(1:3,:)];
gwrencheq = [gwrencheq(4:6,:);gwrencheq(1:3,:)];

gradeq = [gbeameq1;gbeameq2;gbeameqm;gelconstr;gdiskwrencheq;gwrencheq;gdiskconstr;gplatconstr;gpassconstr;ginveq];

end