function flag = StabilityUniboPrototype(sol,jac,Nf,nj,params)
Nftot = 3*params.Nf;
%% First set of eqns: beams
rotparams = params.rotparams;

[~,~,~,~,~,qd,qp,~,~,~]= unstacks(sol,Nftot,nj);

euld = qd(4:6);
Dd = rot2twist(euld,rotparams);
jac(1+9*Nftot+4+3:9*Nftot+4+6,:) = Dd'*jac(1+9*Nftot+4+3:9*Nftot+4+6,:);
eulp = qp(4:6);
Dp = rot2twist(eulp,rotparams);
jac(1+9*Nftot+4+6+3:9*Nftot+4+6+6,:) = Dp'*jac(1+9*Nftot+4+6+3:9*Nftot+4+6+6,:);

jacr = [jac(1:9*Nftot,:);jac(1+9*Nftot+4:end-4,:)]; % we need to remove the influence of the Lf!
Ub1 = jacr(1:9*Nftot+6+6,1+6:6+9*Nftot); % wrt elastic coordinate of beams
Ub2 = jacr(1:9*Nftot+6+6,1+6+9*Nftot:6+9*Nftot+4);
Ub3 = jacr(1:9*Nftot+6+6,1+6+9*Nftot+4:6+9*Nftot+4+6);
Pb1 = jacr(1:9*Nftot+6+6,1+6+9*Nftot+4+6:6+9*Nftot+4+6+6); % wrt plat vars
Gb1 = jacr(1:9*Nftot+6+6,1+6+9*Nftot+4+6+6:6+9*Nftot+4+6+6+4*4+4*nj+6); % wrt beams multipliers

Z = null([Ub2,Gb1]');

%% STABILITY
H = [Ub1,Ub3,Pb1];
Hr = Z'*H*Z;

[~,flag] = chol(Hr);

end