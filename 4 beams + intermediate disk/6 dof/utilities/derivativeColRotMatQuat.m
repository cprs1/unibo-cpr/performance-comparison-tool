% DERIVATIVE OF ROTATION MATRIX COLUMNS WRT QUATERNION
% hL = h1 + h2 i + h3 j + h4 k (h1 = scalar component)
%  D1 = derivative of R(:,1) wrt hL
%  D2 = derivative of R(:,2) wrt hL
%  D3 = derivative of R(:,3) wrt hL
function [D1,D2,D3,D1t,D2t,D3t] = derivativeColRotMatQuat(h)
h1 = h(1);
h2 = h(2);
h3 = h(3);
h4 = h(4);


D1 = 2*[+h1,+h2,-h3,-h4;
        +h4,+h3,+h2,+h1;
        -h3,+h4,-h1,+h2;];
D2 = 2*[-h4,+h3,+h2,-h1;
        +h1,-h2,+h3,-h4;
        +h2,+h1,+h4,+h3];
D3 = 2*[+h3,+h4,+h1,+h2;
        -h2,-h1,+h4,+h3;
        +h1,-h2,-h3,+h4];
    
D1t = 2*[+h1,+h2,-h3,-h4;
        -h4,+h3,+h2,-h1;
        +h3,+h4,+h1,+h2;];
D2t = 2*[+h4,+h3,+h2,+h1;
        +h1,-h2,+h3,-h4;
        -h2,-h1,+h4,+h3];
D3t = 2*[-h3,+h4,-h1,+h2;
        +h2,+h1,+h4,+h3;
        +h1,-h2,-h3,+h4];
end