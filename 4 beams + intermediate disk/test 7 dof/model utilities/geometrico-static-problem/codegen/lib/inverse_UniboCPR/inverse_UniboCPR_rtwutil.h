/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * inverse_UniboCPR_rtwutil.h
 *
 * Code generation for function 'inverse_UniboCPR_rtwutil'
 *
 */

#ifndef INVERSE_UNIBOCPR_RTWUTIL_H
#define INVERSE_UNIBOCPR_RTWUTIL_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
extern double rt_powd_snf(double u0, double u1);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (inverse_UniboCPR_rtwutil.h) */
