/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * PhiMatr.h
 *
 * Code generation for function 'PhiMatr'
 *
 */

#ifndef PHIMATR_H
#define PHIMATR_H

/* Include files */
#include "inverse_UniboCPR_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void PhiMatr(double x, double Nf, emxArray_real_T *M);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (PhiMatr.h) */
