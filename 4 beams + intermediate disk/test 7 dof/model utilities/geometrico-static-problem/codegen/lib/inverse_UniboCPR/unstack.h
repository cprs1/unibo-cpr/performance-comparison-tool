/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * unstack.h
 *
 * Code generation for function 'unstack'
 *
 */

#ifndef UNSTACK_H
#define UNSTACK_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
void unstack(const double guess_data[], double Nftot, double qa[6],
             double qe_data[], int *qe_size, double qp_data[], int *qp_size,
             double lambda_data[], int *lambda_size);

#ifdef __cplusplus
}
#endif

#endif
/* End of code generation (unstack.h) */
