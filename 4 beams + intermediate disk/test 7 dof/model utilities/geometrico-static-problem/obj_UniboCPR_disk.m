function [res,gradres] = obj_UniboCPR_disk(guess)
% 
Lv = guess(1+2:2+4,1);
[res,id] = max(Lv);

nn = max(size(guess));
gradres = zeros(nn,1);
gradres(2+id,1) = 1;

% 
% qa = guess(1:2,1);
% [res] = qa(1)^2+qa(2)^2;
% 
% 
% gradres = zeros(nn,1);
% gradres(1:2,1) = [1;1];
end