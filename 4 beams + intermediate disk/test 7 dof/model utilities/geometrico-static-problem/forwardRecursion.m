function [p,h,dpdqa,dhdqa,dpdqe,dhdqe] = forwardRecursion(p1L,h1L,qei,L,Nf)
Nftot = 3*Nf;

y2F = [p1L;h1L;zeros(3+3*Nftot,1);zeros(4,1);zeros(4*Nftot,1)];
fun = @(s,y) OdefunAssumedForward(s,y,qei,Nf,L,'variable');
[~,y] = ode45(fun,[0,1],y2F);
ygeom = y(end,:)';

p = ygeom(1:3,1);
h = ygeom(4:7,1);
dpdqa = ygeom(1+7:7+3,1);
dpdqe = ygeom(1+7+3:7+3+3*Nftot,1);
dhdqa = ygeom(1+7+3+3*Nftot:7+3+3*Nftot+4,1);
dhdqe = ygeom(1+7+3+3*Nftot+4:7+3+3*Nftot+4+4*Nftot,1);

end