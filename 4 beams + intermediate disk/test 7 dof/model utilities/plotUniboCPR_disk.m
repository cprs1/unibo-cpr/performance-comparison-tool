function h = plotUniboCPR_disk(guess,guesst,geometry,Nf,Nsh,hr)
Nftot = 3*Nf;
h = [];
[~,nj] = size(geometry.jointmatr);  

[qa,qe1v,qe2v,qm1,Lf,qd,qp,lambdad,lambdat,lambdas]= unstacks(guess,Nftot,nj);


     Ls = geometry.Ls;


xp = qa(1);
yp = qa(2);
pbase  = [xp;yp;hr];
Lv = qa(3:6,1);
Lt = geometry.L_tube;

framepoints = geometry.framepoints;
basepoints = geometry.basepoints;
baseangles = geometry.baseangles;
platangles = geometry.platangles;
platpoints = geometry.platpoints;


% figure()
plot3(xp,yp,hr,'ro','LineWidth',2)
hold on
pend = zeros(3,4);
pdisk = zeros(3,4);
for i = 1:4
     %% FIRST STEP
    % extract variables
    Lb = Lf(i);
    p01 = pbase + platpoints(:,i);
    h01 = eul2quat(platangles(:,i)','XYZ')';
    qe1 = qe1v(1+Nftot*(i-1):Nftot*i,1);
    % integrate
    y01 = [p01;h01;zeros(3+3*3*Nf,1);zeros(4,1);zeros(4*3*Nf,1)];
    fun1 = @(s,y) OdefunAssumedForward(s,y,qe1,Nf,Lb,'variable');
    [s1,y1] = ode45(fun1,[0,1],y01);
    % spline results
    sspan1 = linspace(0,Lb,Nsh);
    posbeamsb = spline(Lb*s1,y1(:,1:3)',sspan1);
    %% SECOND STEP
    % extract variables
    Ld = Lv(i)-Lf(i);
    p02 = y1(end,1:3)';
    h02 = y1(end,4:7)';
    qe2 = qe2v(1+3*Nf*(i-1):i*3*Nf,1);
    % integrate
    y02 = [p02;h02;zeros(3+3*3*Nf,1);zeros(4,1);zeros(4*3*Nf,1)];
    fun2 = @(s,y) OdefunAssumedForward(s,y,qe2,Nf,Ld,'variable');
    [s2,y2] = ode45(fun2,[0,1],y02);
    % spline results
    sspan2 = linspace(0,Ld,Nsh);
    posbeamsd = spline(Ld*s2,y2(:,1:3)',sspan2);



    h1 = plot3(posbeamsb(1,:),posbeamsb(2,:),posbeamsb(3,:),'b-','LineWidth',2);
    h2 = plot3(posbeamsb(1,end),posbeamsb(2,end),posbeamsb(3,end),'rs','LineWidth',2);
    h3 = plot3(posbeamsb(1,1),posbeamsb(2,1),posbeamsb(3,1),'bs','LineWidth',2);
    h4 = plot3(posbeamsd(1,:),posbeamsd(2,:),posbeamsd(3,:),'b-','LineWidth',2);
    h5 = plot3(posbeamsd(1,end),posbeamsd(2,end),posbeamsd(3,end),'rs','LineWidth',2);
    h = [h;h1;h2;h3;h4;h5];
    pdisk(:,i) = posbeamsb(:,end);
    pend(:,i) = posbeamsd(:,end);
end

%% SPRING
     %% FIRST STEP

    % extract variables
    Lb = Ls;
    p01 = pbase;
    h01 = eul2quat(platangles(:,1)','XYZ')';
    qe1 = qm1;
    % integrate
    y01 = [p01;h01;zeros(3+3*3*Nf,1);zeros(4,1);zeros(4*3*Nf,1)];
    fun1 = @(s,y) OdefunAssumedForward(s,y,qe1,Nf,Lb,'variable');
    [s1,y1] = ode45(fun1,[0,1],y01);
    % spline results
    sspan1 = linspace(0,Lb,Nsh);
    pospass = spline(Lb*s1,y1(:,1:3)',sspan1);

    h1 = plot3(pospass(1,:),pospass(2,:),pospass(3,:),'b-','LineWidth',2);
    h2 = plot3(pospass(1,end),pospass(2,end),pospass(3,end),'rs','LineWidth',2);
    h = [h;h1;h2];

%% BASE
h1 = line([pend(1,1),pend(1,2)],[pend(2,1),pend(2,2)],[pend(3,1),pend(3,2)],'Color','red','LineWidth',2);
h2 = line([pend(1,2),pend(1,3)],[pend(2,2),pend(2,3)],[pend(3,2),pend(3,3)],'Color','red','LineWidth',2);
h3 = line([pend(1,3),pend(1,4)],[pend(2,3),pend(2,4)],[pend(3,3),pend(3,4)],'Color','red','LineWidth',2);
h4 = line([pend(1,4),pend(1,1)],[pend(2,4),pend(2,1)],[pend(3,4),pend(3,1)],'Color','red','LineWidth',2);

h = [h;h1;h2;h3;h4];

%% DISK
h1 = line([pdisk(1,1),pdisk(1,2)],[pdisk(2,1),pdisk(2,2)],[pdisk(3,1),pdisk(3,2)],'Color','red','LineWidth',2);
h2 = line([pdisk(1,2),pdisk(1,3)],[pdisk(2,2),pdisk(2,3)],[pdisk(3,2),pdisk(3,3)],'Color','red','LineWidth',2);
h3 = line([pdisk(1,3),pdisk(1,4)],[pdisk(2,3),pdisk(2,4)],[pdisk(3,3),pdisk(3,4)],'Color','red','LineWidth',2);
h4 = line([pdisk(1,4),pdisk(1,1)],[pdisk(2,4),pdisk(2,1)],[pdisk(3,4),pdisk(3,1)],'Color','red','LineWidth',2);

h = [h;h1;h2;h3;h4];
%% PLATFORM
ppoints = pbase + platpoints;
h1 = line([ppoints(1,1),ppoints(1,2)],[ppoints(2,1),ppoints(2,2)],[ppoints(3,1),ppoints(3,2)],'Color','black','LineWidth',2);
h2 = line([ppoints(1,2),ppoints(1,3)],[ppoints(2,2),ppoints(2,3)],[ppoints(3,2),ppoints(3,3)],'Color','black','LineWidth',2);
h3 = line([ppoints(1,3),ppoints(1,4)],[ppoints(2,3),ppoints(2,4)],[ppoints(3,3),ppoints(3,4)],'Color','black','LineWidth',2);
h4 = line([ppoints(1,4),ppoints(1,1)],[ppoints(2,4),ppoints(2,1)],[ppoints(3,4),ppoints(3,1)],'Color','black','LineWidth',2);

h = [h;h1;h2;h3;h4];

%% FRAME

h1 = line([framepoints(1,1),framepoints(1,2)],[framepoints(2,1),framepoints(2,2)],[framepoints(3,1),framepoints(3,2)],'Color','black','LineWidth',2);
h2 = line([framepoints(1,2),framepoints(1,3)],[framepoints(2,2),framepoints(2,3)],[framepoints(3,2),framepoints(3,3)],'Color','black','LineWidth',2);
h3 = line([framepoints(1,3),framepoints(1,4)],[framepoints(2,3),framepoints(2,4)],[framepoints(3,3),framepoints(3,4)],'Color','black','LineWidth',2);
h4 = line([framepoints(1,4),framepoints(1,1)],[framepoints(2,4),framepoints(2,1)],[framepoints(3,4),framepoints(3,1)],'Color','black','LineWidth',2);
h5 = line([framepoints(1,1),framepoints(1,1)],[framepoints(2,1),framepoints(2,1)],[0,framepoints(3,1)],'Color','black','LineWidth',2);
h6 = line([framepoints(1,2),framepoints(1,2)],[framepoints(2,2),framepoints(2,2)],[0,framepoints(3,2)],'Color','black','LineWidth',2);
h7 = line([framepoints(1,3),framepoints(1,3)],[framepoints(2,3),framepoints(2,3)],[0,framepoints(3,3)],'Color','black','LineWidth',2);
h8 = line([framepoints(1,4),framepoints(1,4)],[framepoints(2,4),framepoints(2,4)],[0,framepoints(3,4)],'Color','black','LineWidth',2);

h = [h;h1;h2;h3;h4;h5;h6;h7;h8];


%% TUBES
qet = guesst(1+2:2+4*Nftot,1);
for j = 1:4
    qei = qet(1+Nftot*(j-1):Nftot*j,1);
    
    %% FORWARD RECURSION
    p1L = basepoints(:,j);
    h1L = eul2quat(baseangles(:,j)','XYZ')';
    % SECOND PART: free beam
    y2F = [p1L;h1L;zeros(3+3*3*Nf,1);zeros(4,1);zeros(4*3*Nf,1)];
    fun = @(s,y) OdefunAssumedForward(s,y,qei,Nf,Lt,'variable');
    [s1,y] = ode45(fun,[0,1],y2F);

    pos1 = y(:,1:3);
    sspan1 = linspace(0,Lt,Nsh);
    posbeam1 = spline(Lt*s1,pos1',sspan1);
    h1 = plot3(posbeam1(1,:),posbeam1(2,:),posbeam1(3,:),'m-','LineWidth',2);
%     h2 = plot3(posbeam1(1,end),posbeam1(2,end),posbeam1(3,end),'rs','LineWidth',2);
%     h3 = plot3(posbeam1(1,1),posbeam1(2,1),posbeam1(3,1),'bs','LineWidth',2);
    h = [h;h1];

end

%%
axis equal
grid minor
axis([-.3 .3 -.3 .3 0 .8])

end