clear
close all
clc
%% PARAMETERS

[geometry,params,Kbt_1,Kbt_2] = createRobotParams();
params.g = [0;0;0]; % gravity
params.tipwrench = [0;0;0;0;0;-30]; % tip wrench
[~,nj] = size(geometry.jointmatr);  

%% INVERSE PROBLEM
xP = 0; yP = 0; zP = 0.1; alpha = 30*pi/180; beta = 25*pi/180; gamma = 0*pi/180;
qpv = [xP;yP;zP;alpha;beta;gamma];

%% MODEL PARAMETERS
Nf = 4;
params.Nf = Nf;
M = @(s) PhiMatr(s,1,Nf);
Kad1 = integral(@(s) M(s)'*Kbt_1*M(s),0,1,'ArrayValued',true);
Kad2 = integral(@(s) M(s)'*Kbt_2*M(s),0,1,'ArrayValued',true);
params.rotparams = 'TT';
%% Solution
qa0 = [0;0;0.4;0.4;0.4;0.4];
qe0 = repmat([0;zeros(3*Nf-1,1)],8,1);
qm0 = zeros(3*Nf,1);
Lf0 = [0.1;0.1;0.1;0.1];
qd0 = [0;0;0.3;0;0;0];
qp0 = qpv;
lambda0 = zeros(4*nj+4*4+6,1);
Ls0 = 0.6;
guess0 = [qa0;qe0;qm0;Lf0;qd0;qp0;lambda0;Ls0];

load guess0

options = optimoptions('fmincon','Algorithm','active-set','display','iter-detailed','MaxIterations',50, ...
    'SpecifyObjectiveGradient',true,'SpecifyConstraintGradient',true,'CheckGradients',false);

j = 1;
jmax = 180;
flag = 1;

flagv1 = NaN*zeros(jmax,1);
flagv2 = NaN*zeros(jmax,1);
flagvs = NaN*zeros(jmax,1);
yv = NaN*zeros(jmax,1);

h = [];
while j<jmax && flag>=1

fun = @(guess)  obj_UniboCPR_disk(guess);
constr = @(guess) inverse_UniboCPR_disk_7a(guess,geometry,params,Kad2,qpv);

[sol,~,flag] = fmincon(fun,guess0,[],[],[],[],[],[],constr,options);

[c,ceq,gc,gceq] = constr(sol);
jac = gceq(1:end-1,:)';

%% TUBE POSITION COMPUTATION
if j==1
    qa0 = sol(1:2);
    qt0 = repmat([5.2;zeros(3*Nf-1,1)],4,1);
    lambdat0 = zeros(4*5,1);
    guesst0 = [qa0;qt0;lambdat0];
    funt = @(guess) tubes_UniboCPR(guess,geometry,params,Kad1,sol(1:2));
    optionst = optimoptions('fsolve','display','iter-detailed','Maxiterations',50,'SpecifyObjectiveGradient',true,'CheckGradients',false,'Maxiterations',50);

[solt,~,~,~,jact] = fsolve(funt,guesst0,optionst);

end

%% PLOT & STRAIN RECOVERY
delete(h);
geometry.Ls = sol(end);
h = plotUniboCPR_disk(sol,solt,geometry,Nf,100,geometry.hr);
pause(0.001);
hold on
drawnow

%% SINGULARITY AND EQUILIBRIUM STABILITY ANALYSIS

% [flag1,flag2] = SingularitiesUniboPrototypeDisk(sol,jac,geometry,params);
flags = StabilityUniboPrototype(sol,jac,Nf,nj,params);

%% update
flagv1(j,1) = sol(end);
% flagv1(j,1) = norm(inv(jac),Inf);

flagv2(j,1) = 0;
flagvs(j,1) = flags;
yv(j,1) = qpv(5);
guess0 = sol;
qpv = qpv + [0;0;0;0*pi/180;1*pi/180;0];
j = j+1;

end

%%
figure()
% yyaxis left
semilogy(180*yv/pi,flagv1,'r-')
hold on
semilogy(180*yv/pi,flagv2,'k-')
grid minor
% yyaxis right
% plot(180*yv/pi,flagvs,'-')