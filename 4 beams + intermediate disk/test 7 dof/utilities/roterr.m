% function [err,derrdqa,derrdqe,derrdrot] = roterr(p,t,h,dhdqa,dhdqe,dpda,dpdb,dpdc,P)

function [err,derrdqa,derrdqe,derrdrot] = roterr(p,t,h,dhdqa,dhdqe,dpda,dpdb,dpdc)

% projection between joint and beam directors + permutation matrix

% d1p = p(:,1); d2p = p(:,2); d3p = p(:,3);
% d1t = t(:,1); d2t = t(:,2); d3t = t(:,3);

% errx = P(1,1)*d2p'*d1t+P(1,2)*d2p'*d2t+P(1,3)*d2p'*d3t;
% erry = P(2,1)*d3p'*d1t+P(2,2)*d3p'*d2t+P(2,3)*d3p'*d3t;
% errz = P(3,1)*d1p'*d1t+P(3,2)*d1p'*d2t+P(3,3)*d1p'*d3t;

errx = p(1,2)*t(1,3)+p(2,2)*t(2,3)+p(3,2)*t(3,3)-p(1,3)*t(1,2)-p(2,3)*t(2,2)-p(3,3)*t(3,2);
erry = p(1,3)*t(1,1)+p(2,3)*t(2,1)+p(3,3)*t(3,1)-p(1,1)*t(1,3)-p(2,1)*t(2,3)-p(3,1)*t(3,3);
errz = p(1,1)*t(1,2)+p(2,1)*t(2,2)+p(3,1)*t(3,2)-p(1,2)*t(1,1)-p(2,2)*t(2,1)-p(3,2)*t(3,1);

err = [errx;erry;errz];

derrdqa = [];
derrdqe = [];

if nargin>2
    [D1,D2,D3] = derivativeColRotMatQuat(h);
    dd1dqa = D1*dhdqa;
    dd2dqa = D2*dhdqa;
    dd3dqa = D3*dhdqa;
    dd1dqe = D1*dhdqe;
    dd2dqe = D2*dhdqe;
    dd3dqe = D3*dhdqe;
    
%     derrxdqa = P(1,1)*d2p'*dd1dqa+P(1,2)*d2p'*dd2dqa+P(1,3)*d2p'*dd3dqa;
%     derrydqa = P(2,1)*d3p'*dd1dqa+P(2,2)*d3p'*dd2dqa+P(2,3)*d3p'*dd3dqa;
%     derrzdqa = P(3,1)*d1p'*dd1dqa+P(3,2)*d1p'*dd2dqa+P(3,3)*d1p'*dd3dqa;
%     derrxdqe = P(1,1)*d2p'*dd1dqe+P(1,2)*d2p'*dd2dqe+P(1,3)*d2p'*dd3dqe;
%     derrydqe = P(2,1)*d3p'*dd1dqe+P(2,2)*d3p'*dd2dqe+P(2,3)*d3p'*dd3dqe;
%     derrzdqe = P(3,1)*d1p'*dd1dqe+P(3,2)*d1p'*dd2dqe+P(3,3)*d1p'*dd3dqe;

    derrxdqa = p(1,2)*dd3dqa(1,:)+p(2,2)*dd3dqa(2,:)+p(3,2)*dd3dqa(3,:)-p(1,3)*dd2dqa(1,:)-p(2,3)*dd2dqa(2,:)-p(3,3)*dd2dqa(3,:);
    derrxdqe = p(1,2)*dd3dqe(1,:)+p(2,2)*dd3dqe(2,:)+p(3,2)*dd3dqe(3,:)-p(1,3)*dd2dqe(1,:)-p(2,3)*dd2dqe(2,:)-p(3,3)*dd2dqe(3,:);
    derrydqa = p(1,3)*dd1dqa(1,:)+p(2,3)*dd1dqa(2,:)+p(3,3)*dd1dqa(3,:)-p(1,1)*dd3dqa(1,:)-p(2,1)*dd3dqa(2,:)-p(3,1)*dd3dqa(3,:);
    derrydqe = p(1,3)*dd1dqe(1,:)+p(2,3)*dd1dqe(2,:)+p(3,3)*dd1dqe(3,:)-p(1,1)*dd3dqe(1,:)-p(2,1)*dd3dqe(2,:)-p(3,1)*dd3dqe(3,:);   
    derrzdqa = p(1,1)*dd2dqa(1,:)+p(2,1)*dd2dqa(2,:)+p(3,1)*dd2dqa(3,:)-p(1,2)*dd1dqa(1,:)-p(2,2)*dd1dqa(2,:)-p(3,2)*dd1dqa(3,:);
    derrzdqe = p(1,1)*dd2dqe(1,:)+p(2,1)*dd2dqe(2,:)+p(3,1)*dd2dqe(3,:)-p(1,2)*dd1dqe(1,:)-p(2,2)*dd1dqe(2,:)-p(3,2)*dd1dqe(3,:);
    
    
    derrdqa = [derrxdqa;derrydqa;derrzdqa];
    derrdqe = [derrxdqe;derrydqe;derrzdqe];
    
%     dd1pda = dpda(:,1); dd2pda = dpda(:,2); dd3pda = dpda(:,3);
%     dd1pdb = dpdb(:,1); dd2pdb = dpdb(:,2); dd3pdb = dpdb(:,3);
%     dd1pdc = dpdc(:,1); dd2pdc = dpdc(:,2); dd3pdc = dpdc(:,3);

%     derrxda = P(1,1)*dd2pda'*d1t+P(1,2)*dd2pda'*d2t+P(1,3)*dd2pda'*d3t;
%     derryda = P(2,1)*dd3pda'*d1t+P(2,2)*dd3pda'*d2t+P(2,3)*dd3pda'*d3t;
%     derrzda = P(3,1)*dd1pda'*d1t+P(3,2)*dd1pda'*d2t+P(3,3)*dd1pda'*d3t;
%     derrxdb = P(1,1)*dd2pdb'*d1t+P(1,2)*dd2pdb'*d2t+P(1,3)*dd2pdb'*d3t;
%     derrydb = P(2,1)*dd3pdb'*d1t+P(2,2)*dd3pdb'*d2t+P(2,3)*dd3pdb'*d3t;
%     derrzdb = P(3,1)*dd1pdb'*d1t+P(3,2)*dd1pdb'*d2t+P(3,3)*dd1pdb'*d3t;
%     derrxdc = P(1,1)*dd2pdc'*d1t+P(1,2)*dd2pdc'*d2t+P(1,3)*dd2pdc'*d3t;
%     derrydc = P(2,1)*dd3pdc'*d1t+P(2,2)*dd3pdc'*d2t+P(2,3)*dd3pdc'*d3t;
%     derrzdc = P(3,1)*dd1pdc'*d1t+P(3,2)*dd1pdc'*d2t+P(3,3)*dd1pdc'*d3t;
    
    derrxda = dpda(1,2)*t(1,3)+dpda(2,2)*t(2,3)+dpda(3,2)*t(3,3)-dpda(1,3)*t(1,2)-dpda(2,3)*t(2,2)-dpda(3,3)*t(3,2);
    derryda = dpda(1,3)*t(1,1)+dpda(2,3)*t(2,1)+dpda(3,3)*t(3,1)-dpda(1,1)*t(1,3)-dpda(2,1)*t(2,3)-dpda(3,1)*t(3,3);
    derrzda = dpda(1,1)*t(1,2)+dpda(2,1)*t(2,2)+dpda(3,1)*t(3,2)-dpda(1,2)*t(1,1)-dpda(2,2)*t(2,1)-dpda(3,2)*t(3,1);
    derrxdb = dpdb(1,2)*t(1,3)+dpdb(2,2)*t(2,3)+dpdb(3,2)*t(3,3)-dpdb(1,3)*t(1,2)-dpdb(2,3)*t(2,2)-dpdb(3,3)*t(3,2);
    derrydb = dpdb(1,3)*t(1,1)+dpdb(2,3)*t(2,1)+dpdb(3,3)*t(3,1)-dpdb(1,1)*t(1,3)-dpdb(2,1)*t(2,3)-dpdb(3,1)*t(3,3);
    derrzdb = dpdb(1,1)*t(1,2)+dpdb(2,1)*t(2,2)+dpdb(3,1)*t(3,2)-dpdb(1,2)*t(1,1)-dpdb(2,2)*t(2,1)-dpdb(3,2)*t(3,1);
    derrxdc = dpdc(1,2)*t(1,3)+dpdc(2,2)*t(2,3)+dpdc(3,2)*t(3,3)-dpdc(1,3)*t(1,2)-dpdc(2,3)*t(2,2)-dpdc(3,3)*t(3,2);
    derrydc = dpdc(1,3)*t(1,1)+dpdc(2,3)*t(2,1)+dpdc(3,3)*t(3,1)-dpdc(1,1)*t(1,3)-dpdc(2,1)*t(2,3)-dpdc(3,1)*t(3,3);
    derrzdc = dpdc(1,1)*t(1,2)+dpdc(2,1)*t(2,2)+dpdc(3,1)*t(3,2)-dpdc(1,2)*t(1,1)-dpdc(2,2)*t(2,1)-dpdc(3,2)*t(3,1);
    derrdrot = [derrxda,derrxdb,derrxdc;derryda,derrydb,derrydc;derrzda,derrzdb,derrzdc];
end