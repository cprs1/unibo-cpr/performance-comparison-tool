%% Robot Geometry Parameters

%% PARAMETERS

[geometry,params,Kbt_1,Kbt_2] = createRobotParams();
params.g = [0;0;0]; % gravity
params.tipwrench = [zeros(3,1);0;0;-30]; % tip wrench
[~,nj] = size(geometry.jointmatr);  

%% INVERSE PROBLEM
xP = 0; yP = 0; zP = 0.10; alpha = 0*pi/180; beta = 15*pi/180; gamma = 0*pi/180;
qpv = [xP;yP;zP;alpha;beta;gamma];

%% MODEL PARAMETERS
Nf = 4;
params.Nf = Nf;
M = @(s) PhiMatr(s,1,Nf);
Kad1 = integral(@(s) M(s)'*Kbt_1*M(s),0,1,'ArrayValued',true);
Kad2 = integral(@(s) M(s)'*Kbt_2*M(s),0,1,'ArrayValued',true);
params.rotparams = 'TT';


pstart = [zP;alpha;beta];
params.pstart = pstart;
params.pstartang = [alpha,beta];

%% Simulation functions
fcn.objetivefcn = @(y,var) inverse_UniboCPR_disk_7a(y,geometry,params,Kad2,[xP;yP;var;0]);
fcn.objmin = @(y) obj_UniboCPR_disk(y);
% fcn.mechconstrfcn = @(y) mechconstrUniboCPR(y,params.actlims,params.displims,geometry.beamradius,params.Nf,params.maxstrain);
fcn.singufcn = @(y,jac) SingularitiesUniboPrototypeDisk(y,jac,geometry,params);
fcn.stabilityfcn = @(y,jac) StabilityUniboPrototype(y,jac,Nf,nj,params);
fcn.tubesfcn = @(y,pb) singleTubeProblem(y,geometry,params,Kad1,pb);

%% First Initial Guess

qa0 = [0;0;0.4;0.4;0.4;0.4];
qe0 = repmat([0;zeros(3*Nf-1,1)],8,1);
qm0 = zeros(3*Nf,1);
Lf0 = [0.1;0.1;0.1;0.1];
qd0 = [0;0;0.3;0;0;0];
qp0 = qpv;
lambda0 = zeros(4*nj+4*4+6,1);
guess0 = [qa0;qe0;qm0;Lf0;qd0;qp0;lambda0;0.2];


options = optimoptions('fmincon','Algorithm','sqp','display','iter-detailed','MaxIterations',10, ...
    'SpecifyObjectiveGradient',true,'SpecifyConstraintGradient',true,'CheckGradients',false);
fun = @(guess) obj_UniboCPR_disk(guess);
constr = @(guess) inverse_UniboCPR_disk_7a(guess,geometry,params,Kad2,qpv);

sol = fmincon(fun,guess0,[],[],[],[],[],[],constr,options);

[c,ceq,gc,gceq] = constr(sol);
jac = gceq(1:end-1,:)';

Ls = sol(end);
geometry.Ls = Ls;
params.y0 = sol;

%% TUBE POSITION COMPUTATION
qa0 = sol(1:2);
qt0 = repmat([5.5;zeros(3*Nf-1,1)],4,1);
lambdat0 = zeros(4*5,1);
guesst0 = [qa0;qt0;lambdat0];

funt = @(guess) tubes_UniboCPR(guess,geometry,params,Kad1,sol(1:2));

options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',false);

[solt,~,~,~,jact] = fsolve(funt,guesst0,options);

%% PLOT & STRAIN RECOVERY
h = plotUniboCPR_disk(sol,solt,geometry,Nf,100,geometry.hr);
