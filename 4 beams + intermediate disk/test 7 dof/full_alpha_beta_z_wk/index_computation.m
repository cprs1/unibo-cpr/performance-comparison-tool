function [idx,idy,idu,A] = index_computation(WK,params)

stepsizeA = params.stepsizeA;

%% X direction
idxvals1 = WK(abs(WK(:,3))<2*stepsizeA & WK(:,5) == 1);
idxvals2 = WK(abs(abs(WK(:,3))-pi)<2*stepsizeA & WK(:,5) == 1);
if numel(idxvals1)>0
    idx1 = max(WK(idxvals1,4));
else
    idx1 = 0;
end

if numel(idxvals2)>0
    idx2 = max(WK(idxvals2,4));
else
    idx2 = 0;
end


idx = max(idx1,idx2);


%% Y DIRECTION

idyvals1 = WK(abs(abs(WK(:,3))-1*pi/2)<2*stepsizeA & WK(:,5) == 1);
idyvals2 = WK(abs(abs(WK(:,3))-3*pi/2)<2*stepsizeA & WK(:,5) == 1);

if numel(idyvals1)>0
    idy1 = max(WK(idyvals1,4));
else
    idy1 = 0;
end

if numel(idyvals2)>0
    idy2 = max(WK(idyvals2,4));
else
    idy2 = 0;
end

idy = max(idy1,idy2);

%% uniformity
n_sampA = floor(  abs(  min(WK(:,3))  -max(WK(:,3))  )/stepsizeA);
x = linspace(min(WK(:,3)),max(WK(:,3)),n_sampA);

i = 1;
idu = Inf;
while i<=n_sampA && idu>0
    valsu = WK(abs(WK(:,3)-x(i))<2*stepsizeA & WK(:,5) == 1);
    if numel(valsu)>0
        idlocal = min(WK(valsu,4));
        idu = min(idu,idlocal);
    else
        idu = 0;
    end
    i = i+1;
end

%% AREA
    idwk = WK(WK(:,5)==1,1);
A = numel(idwk)*(params.stepsizeA*params.stepsizeB);
end