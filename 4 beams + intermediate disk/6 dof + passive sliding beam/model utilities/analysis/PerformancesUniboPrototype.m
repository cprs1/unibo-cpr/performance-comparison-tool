function output = PerformancesUniboPrototype(sol,solt,jac,jact,nj,params,geometry)

Nftot = 3*params.Nf;
%% First set of eqns: beams
rotparams = params.rotparams;
eul = sol(1+6+4*Nftot+3:6+4*Nftot+6,1);
D = rot2twist(eul,rotparams);
jac(1+4*Nftot:4*Nftot+3,:) = D'*jac(1+4*Nftot:4*Nftot+3,:);

[Wb1,Wb2] = getLoadMatrix(geometry,params);

% beam eq and plat eq
Ab1 = jac(1:4*Nftot+6,1:6); % wrt act var
Ub1 = jac(1:4*Nftot+6,1+6:6+4*Nftot); % wrt elastic coordinate of beams
Eb1 = zeros(4*Nftot+6,4*Nftot); % wrt elastic coordinate of the tubes
Pb1 = jac(1:4*Nftot+6,1+6+4*Nftot:6+4*Nftot+6); % wrt plat vars
Gb1 = jac(1:4*Nftot+6,1+6+4*Nftot+6:6+4*Nftot+6+4*nj); % wrt beams multipliers
Gb2 = zeros(4*Nftot+6,4*5); % wrt tube multipliers
Tb1 = zeros(4*Nftot+6,6); % wrt torques
% constraint eq.
Ab2 = jac(1+4*Nftot+6:4*Nftot+6+4*nj,1:6); % wrt act var
Ub2 = jac(1+4*Nftot+6:4*Nftot+6+4*nj,1+6:6+4*Nftot); % wrt elastic coordinate
Eb2 = zeros(4*nj,4*Nftot);
Pb2 = jac(1+4*Nftot+6:4*Nftot+6+4*nj,1+6+4*Nftot:6+4*Nftot+6); % wrt plat vars
Tb2 = zeros(4*nj,6);


%% TUBES
Wt1 = zeros(4*Nftot,6);
Wt2 = zeros(4*5,6);

% beam eq and plat eq
At1 = [jact(1:4*Nftot,1:2),zeros(4*Nftot,4)]; % wrt act var
Ut1 = zeros(4*Nftot,4*Nftot); % wrt elastic coordinate of beams
Et1 = jact(1:4*Nftot,1+2:2+4*Nftot); % wrt elastic coordinate of the tubes
Pt1 = zeros(4*Nftot,6); % wrt plat vars
Gt1 = zeros(4*Nftot,4*nj); % wrt beams multipliers
Gt2 = jact(1:4*Nftot,1+2+4*Nftot:2+4*Nftot+4*5); % wrt tube multipliers
Tt1 = zeros(4*Nftot,6); % wrt torques
% constraint eq.
At2 = [jact(1+4*Nftot:4*Nftot+4*5,1:2),zeros(4*5,4)]; % wrt act var
Ut2 = zeros(4*5,4*Nftot); % wrt elastic coordinate
Et2 = jact(1+4*Nftot:4*Nftot+4*5,1+2:2+4*Nftot);
Pt2 = zeros(4*5,6); % wrt plat vars
Tt2 = zeros(4*5,6);

%% TORQUES
[AT1,UT1,ET1,GT1,GT2] = getTorqueSensitivity(sol,solt,params,geometry);
PT1 = zeros(6,6);
TT1 = -eye(6);
WT1 = zeros(6,6);

%% COLLECT:
A1 = [Ab1;At1;AT1];
U1 = [Ub1;Ut1;UT1];
E1 = [Eb1;Et1;ET1];
P1 = [Pb1;Pt1;PT1];
T1 = [Tb1;Tt1;TT1];
W1 = [Wb1;Wt1;WT1];
G1 = [Gb1;Gt1;GT1];
G2 = [Gb2;Gt2;GT2];

A2 = [Ab2;At2];
U2 = [Ub2;Ut2];
E2 = [Eb2;Et2];
P2 = [Pb2;Pt2];
T2 = [Tb2;Tt2];
W2 = [Wb2;Wt2];

G = [G1,G2];
Z = null(G');

%% ASSEMBLY
A = [Z'*A1;A2];
U = [Z'*U1;U2];
E = [Z'*E1;E2];
T = [Z'*T1;T2];
W = [Z'*W1;W2];
P = [Z'*P1;P2];

m1 = -[A,U,E,T]\P;
m2 = -[A,U,E,T]\W;

J = m1(1:6,:); % qp-->qa
K = m2(1:6,:); % wp --> qa
C = m1(end-5:end,:); % qp-->tau
H = m2(end-5:end,:); % wp-->tau

%% Performance metrics
% transmissions

R = [J, K; C, H]; % [qa;tau] = R [qp;w] (input-output map)
e1 = [+1;+1;+1]; e2 = [+1;-1;+1]; e3 = [+1;+1;-1]; e4 = [+1;-1;-1];
z0 = zeros(3,1);
input11 = [e1;z0;z0;z0];input12 = [e2;z0;z0;z0];input13 = [e3;z0;z0;z0];input14 = [e4;z0;z0;z0];
input21 = [z0;e1;z0;z0];input22 = [z0;e2;z0;z0];input23 = [z0;e3;z0;z0];input24 = [z0;e4;z0;z0];
input31 = [z0;z0;e1;z0];input32 = [z0;z0;e2;z0];input33 = [z0;z0;e3;z0];input34 = [z0;z0;e4;z0];
input41 = [z0;z0;z0;e1];input42 = [z0;z0;z0;e2];input43 = [z0;z0;z0;e3];input44 = [z0;z0;z0;e4];

resput11 = R*input11;resput12 = R*input12;resput13 = R*input13;resput14 = R*input14;
resput21 = R*input21;resput22 = R*input22;resput23 = R*input23;resput24 = R*input24;
resput31 = R*input31;resput32 = R*input32;resput33 = R*input33;resput34 = R*input34;
resput41 = R*input41;resput42 = R*input42;resput43 = R*input43;resput44 = R*input44;

% displacements
output.pos2raild = max([norm(resput11(1:2),Inf);norm(resput12(1:2),Inf);norm(resput13(1:2),Inf);norm(resput14(1:2),Inf);]);
output.pos2actud = max([norm(resput11(3:6),Inf);norm(resput12(3:6),Inf);norm(resput13(3:6),Inf);norm(resput14(3:6),Inf);]);
output.ori2raild = max([norm(resput21(1:2),Inf);norm(resput22(1:2),Inf);norm(resput23(1:2),Inf);norm(resput24(1:2),Inf);]);
output.ori2actud = max([norm(resput21(3:6),Inf);norm(resput22(3:6),Inf);norm(resput23(3:6),Inf);norm(resput24(3:6),Inf);]);
output.mom2raild = max([norm(resput31(1:2),Inf);norm(resput32(1:2),Inf);norm(resput33(1:2),Inf);norm(resput34(1:2),Inf);]);
output.mom2actud = max([norm(resput31(3:6),Inf);norm(resput32(3:6),Inf);norm(resput33(3:6),Inf);norm(resput34(3:6),Inf);]);
output.for2raild = max([norm(resput41(1:2),Inf);norm(resput42(1:2),Inf);norm(resput43(1:2),Inf);norm(resput44(1:2),Inf);]);
output.for2actud = max([norm(resput41(3:6),Inf);norm(resput42(3:6),Inf);norm(resput43(3:6),Inf);norm(resput44(3:6),Inf);]);
% efforts
output.pos2raile = max([norm(resput11(7:8),Inf);norm(resput11(7:8),Inf);norm(resput11(7:8),Inf);norm(resput11(7:8),Inf);]);
output.pos2actue = max([norm(resput11(9:12),Inf);norm(resput11(9:12),Inf);norm(resput11(9:12),Inf);norm(resput11(9:12),Inf);]);
output.ori2raile = max([norm(resput21(7:8),Inf);norm(resput21(7:8),Inf);norm(resput21(7:8),Inf);norm(resput21(7:8),Inf);]);
output.ori2actue = max([norm(resput21(9:12),Inf);norm(resput21(9:12),Inf);norm(resput21(9:12),Inf);norm(resput21(9:12),Inf);]);
output.mom2raile = max([norm(resput31(7:8),Inf);norm(resput31(7:8),Inf);norm(resput31(7:8),Inf);norm(resput31(7:8),Inf);]);
output.mom2actue = max([norm(resput31(9:12),Inf);norm(resput31(9:12),Inf);norm(resput31(9:12),Inf);norm(resput31(9:12),Inf);]);
output.for2raile = max([norm(resput41(7:8),Inf);norm(resput41(7:8),Inf);norm(resput41(7:8),Inf);norm(resput41(7:8),Inf);]);
output.for2actue = max([norm(resput41(9:12),Inf);norm(resput41(9:12),Inf);norm(resput41(9:12),Inf);norm(resput41(9:12),Inf);]);

% powers
Q = [C';H']*[J,K];
powput11 = input11'*Q*input11;powput12 = input12'*Q*input12;powput13 = input13'*Q*input13;powput14 = input11'*Q*input14;
powput21 = input21'*Q*input21;powput22 = input22'*Q*input22;powput23 = input23'*Q*input23;powput24 = input24'*Q*input24;
powput31 = input31'*Q*input31;powput32 = input32'*Q*input32;powput33 = input33'*Q*input33;powput34 = input34'*Q*input34;
powput41 = input41'*Q*input41;powput42 = input42'*Q*input42;powput43 = input43'*Q*input43;powput44 = input44'*Q*input44;

output.pos2pow = max([abs(powput11);abs(powput12);abs(powput13);abs(powput14);]);
output.ori2pow = max([abs(powput21);abs(powput22);abs(powput23);abs(powput24);]);
output.mom2pow = max([abs(powput31);abs(powput32);abs(powput33);abs(powput34);]);
output.for2pow = max([abs(powput41);abs(powput42);abs(powput43);abs(powput44);]);
end