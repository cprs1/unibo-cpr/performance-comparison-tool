function [t1,t2] = SingularitiesUniboPrototypeDiskPass(sol,jac,geometry,params)
Ci = geometry.jointmatr;
[~,nj] = size(Ci);
Nftot = 3*params.Nf;
%% First set of eqns: beams
rotparams = params.rotparams;

[~,~,~,~,~,~,qd,qp,~,~,~]= unstacks(sol,Nftot,nj);

euld = qd(4:6);
Dd = rot2twist(euld,rotparams);
jac(1+10*Nftot+5+3:10*Nftot+5+6,:) = Dd'*jac(1+10*Nftot+5+3:10*Nftot+5+6,:);
eulp = qp(4:6);
Dp = rot2twist(eulp,rotparams);
jac(1+10*Nftot+5+6+3:10*Nftot+5+6+6,:) = Dp'*jac(1+10*Nftot+5+6+3:10*Nftot+5+6+6,:);


% beam eq and plat eq
Ab1 = jac(1:10*Nftot+5+6+6,1:6); % wrt act var
Ub1 = jac(1:10*Nftot+5+6+6,1+6:6+10*Nftot+5+6); % wrt elastic coordinate of beams
Pb1 = jac(1:10*Nftot+5+6+6,1+6+10*Nftot+5+6:6+10*Nftot+5+6+6); % wrt plat vars
Gb1 = jac(1:10*Nftot+5+6+6,1+6+10*Nftot+5+6+6:6+10*Nftot+5+6+6+4*nj+4*4+12); % wrt beams multipliers
% constraint eq.
Ab2 = jac(1+10*Nftot+5+6+6:10*Nftot+5+6+6+4*4+4*nj+12,1:6); % wrt act var
Ub2 = jac(1+10*Nftot+5+6+6:10*Nftot+5+6+6+4*4+4*nj+12,1+6:6+10*Nftot+5+6); % wrt elastic coordinate
Pb2 = jac(1+10*Nftot+5+6+6:10*Nftot+5+6+6+4*4+4*nj+12,1+6+10*Nftot+5+6:6+10*Nftot+5+6+6); % wrt plat vars

Z = null(Gb1');

%% COLLECT
A = [Z'*Ab1;Ab2];
U = [Z'*Ub1;Ub2];
P = [Z'*Pb1;Pb2];

%% SINGULARITIES
T1 = [A,U];
T2 = [P,U];

t1 = rcond(T1);
t2 = rcond(T2);

end