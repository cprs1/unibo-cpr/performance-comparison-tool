function strainmax = getMaxStrain(qe,Nf,r)

nsamples = 100;
x = linspace(0,1,nsamples);

eq_strain = zeros(nsamples,1);
for j = 1:nsamples
        M = PhiMatr(x(j),1,Nf);
        strain = M*qe;
        eq_strain(j,1) = sqrt(strain(1)^2+strain(2)^2); % neglect torsion
end

strainmax = r*max(eq_strain);