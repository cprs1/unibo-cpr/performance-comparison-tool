function yd = OdefunAssumedForwardExt(s,y,qei,Nf,L,str)
Nftot = 6*Nf;

qe1 = qei(1:3*Nf,1);
qe2 = qei(1+3*Nf:6*Nf,1);

h = y(4:7,1);
dhdqe1 = reshape(y(1+3+4+3*Nftot:3+4+3*Nftot+4*3*Nf),4,3*Nf);

R = quat2rotmatrix(h);
Phi = PhiMatr(s,1,Nf);
k = Phi*qe1;
v = [0;0;1]+Phi*qe2;

k1 = k(1); k2 = k(2); k3 = k(3);
A = [0,-k1,-k2,-k3;
    +k1,0,+k3,-k2;
    +k2,-k3,0,+k1;
    +k3,+k2,-k1,0];

[D1,D2,D3,~,~,~] = derivativeColRotMatQuat(h);

Dk = [-h(2),-h(3),-h(4);+h(1),-h(4),+h(3);+h(4),+h(1),-h(2);-h(3),+h(2),+h(1)];
Dv = v(1)*D1+v(2)*D2+v(3)*D3;

switch str
    case 'fix'        
        pd = R*v;
        hd = 0.5*A*h;
        dpdqe1d = Dv*dhdqe1;
        dpdqe2d = R*Phi;
        dhdqe1d = 0.5*(Dk*Phi+A*dhdqe1);
        dhdqe2d = zeros(4,3*Nf);
        yd = L*[pd;hd;
                reshape(dpdqe1d,3*3*Nf,1);reshape(dpdqe2d,3*3*Nf,1);
                reshape(dhdqe1d,4*3*Nf,1);reshape(dhdqe2d,4*3*Nf,1)];        
    otherwise
        error('Define settings for beams length');
end

end