function yd = initvalueSensitivityExt(s,y,L,qei,Nf)
% Nftot = 6*Nf;

qe1 = qei(1:3*Nf,1);
qe2 = qei(1+3*Nf:6*Nf,1);

h = y(1+3:3+4,1);
dhdp0 = reshape(y(1+3+4+3*3:3+4+3*3+3*4,1),4,3);
dhdh0 = reshape(y(1+3+4+3*3+4*3+3*4:3+4+3*3+4*3+3*4+4*4,1),4,4);

R = quat2rotmatrix(h);
Phi = PhiMatr(s,1,Nf);
k = Phi*qe1;
v = [0;0;1]+Phi*qe2;

k1 = k(1); k2 = k(2); k3 = k(3);
A = [0,-k1,-k2,-k3;
    +k1,0,+k3,-k2;
    +k2,-k3,0,+k1;
    +k3,+k2,-k1,0];

[D1,D2,D3,~,~,~] = derivativeColRotMatQuat(h);
Dv = v(1)*D1+v(2)*D2+v(3)*D3;

% derivatives
pd = R*v;
hd = 0.5*A*h;
dpdp0d = Dv*dhdp0;
dhdp0d = 0.5*A*dhdp0;
dpdh0d = Dv*dhdh0;
dhdh0d = 0.5*A*dhdh0;

yd = L*[pd;hd;reshape(dpdp0d,3*3,1);reshape(dhdp0d,4*3,1);reshape(dpdh0d,3*4,1);reshape(dhdh0d,4*4,1);];
end