%% BACKWARD ODEs of a single beam
% for assumed strain mode approach
function yd = OdefunAssumeBackwardExt(s,y,qei,~,Nf,L,str)
Nftot = 6*Nf;
gamma = y(1:6,1);
dgammadqe = reshape(y(1+6+Nftot:6+Nftot+6*Nftot),6,Nftot);
dgammadwL = reshape(y(1+6+Nftot+6*Nftot+Nftot*Nftot:6+Nftot+6*Nftot+Nftot*Nftot+6*6),6,6);


Phi = PhiMatr(s,1,Nf);
Phidiag = [Phi,zeros(3,3*Nf);zeros(3,3*Nf),Phi]; 

B = eye(6);
xi = [0;0;0;0;0;1] + Phidiag*qei;

sk = skew(xi(1:3));
sv = skew(xi(4:6));

adxi = [sk, zeros(3); sv , sk];

matderiv1 = [+Phidiag(3,:)*gamma(2)-Phidiag(2,:)*gamma(3);
             -Phidiag(3,:)*gamma(1)+Phidiag(1,:)*gamma(3);
             +Phidiag(2,:)*gamma(1)-Phidiag(1,:)*gamma(2);
             +Phidiag(3,:)*gamma(5)-Phidiag(2,:)*gamma(6);
             -Phidiag(3,:)*gamma(4)+Phidiag(1,:)*gamma(6);
             +Phidiag(2,:)*gamma(4)-Phidiag(1,:)*gamma(5)];

matderiv2 = [+Phidiag(6,:)*gamma(5)-Phidiag(5,:)*gamma(6);
            -Phidiag(6,:)*gamma(4)+Phidiag(4,:)*gamma(6);
            +Phidiag(5,:)*gamma(4)-Phidiag(4,:)*gamma(5);
            zeros(3,Nftot);];
        
matderiv = matderiv1+matderiv2;

%% these infos should be provided by forward integration
dwddqe = zeros(6,Nftot);
wd = zeros(6,1);

%% diff.eq
switch str
    case 'fix'
        gammad = adxi'*gamma -wd;
        Qcd = (B*Phidiag)'*gamma;

        dgammadqed = matderiv + adxi'*dgammadqe -dwddqe;
        dgammadwLd = adxi'*dgammadwL;
        dQcdqed = (B*Phidiag)'*dgammadqe;
        dQcdwLd = (B*Phidiag)'*dgammadwL;
        
        yd = L*[gammad;Qcd;reshape(dgammadqed,6*Nftot,1);reshape(dQcdqed,Nftot*Nftot,1);reshape(dgammadwLd,6*6,1);reshape(dQcdwLd,6*Nftot,1)];

    otherwise
        error('Define Correctly Settings')
end



    
end